package com.sc.student2student.adapter;

import com.sc.student2student.R;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.SparseArray;
import android.view.ViewGroup;

import com.sc.student2student.fragement.Product_fragement;
import com.sc.student2student.fragement.Service_fragement;


/**
 * Created by Saturncube-5 on 4/24/2017.
 */

public class HomepagerAdapter extends FragmentStatePagerAdapter {
    String name[];
    private SparseArray<Fragment> registeredFragments = new SparseArray<>();

    public HomepagerAdapter(FragmentManager fm, String[] strings) {
        super(fm);
        this.name = strings;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return Product_fragement.newInstance();
            case 1:
                return Service_fragement.newInstance();
            default:
                return Product_fragement.newInstance();

        }

    }

    @Override
    public int getCount() {
        return name.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return name[position];
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Fragment fragment = (Fragment) super.instantiateItem(container, position);
        registeredFragments.put(position, fragment);
        return fragment;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        registeredFragments.remove(position);
        super.destroyItem(container, position, object);
    }

    public Fragment getRegisteredFragment(int position) {
        return registeredFragments.get(position);
    }
}
