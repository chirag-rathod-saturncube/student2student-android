package com.sc.student2student.fragement;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.VolleyError;
import com.sc.student2student.databinding.ServiceFragementBinding;
import com.sc.student2student.Interface.Service_Refresh;
import com.sc.student2student.R;
import com.sc.student2student.adapter.Service_Adapter;
import com.sc.student2student.api.GetService;
import com.sc.student2student.common.AppConstants;
import com.sc.student2student.common.GridSpacingItemDecoration;
import com.sc.student2student.common.Preferences;
import com.sc.student2student.moduls.Product;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


public class Service_fragement extends Fragment implements Service_Refresh {


    String TAG = getClass().getSimpleName();


    View view;
    ServiceFragementBinding binding;
    LinearLayoutManager layoutManager;
    GridLayoutManager gridLayoutManager;
    Service_Adapter adapter;


    public Service_fragement() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static Service_fragement newInstance() {
        return new Service_fragement();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.service_fragement, container, false);
        AppConstants.service_refresh = this;

        return view;
    }

    private void initView() {
        layoutManager = new LinearLayoutManager(getActivity());
        gridLayoutManager = new GridLayoutManager(getActivity(), 2);
        int spacing = 10; // 50px
        boolean includeEdge = true;
        binding.recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, spacing, includeEdge));
    }


    @Override
    public void onDetach() {
        super.onDetach();

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        binding = ServiceFragementBinding.bind(view);
        initView();

        LoadData(true);
        binding.edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String query = s.toString().toLowerCase();

                final List<Product> filteredList = new ArrayList<>();

                for (int i = 0; i < AppConstants.serivceList.size(); i++) {

                    final String text = AppConstants.serivceList.get(i).getProduct_name().toLowerCase();
                    final String about = AppConstants.serivceList.get(i).getProduct_desc().toLowerCase();
                    if (text.contains(query) || about.contains(query)) {

                        filteredList.add(AppConstants.serivceList.get(i));
                    }
                }


                if (Preferences.getValue_Boolean(getActivity(), Preferences.LIST_TYPE, true)) {
                    binding.recyclerView.setLayoutManager(gridLayoutManager);
                    adapter = new Service_Adapter(getActivity(), filteredList, R.layout.item_prodcut);
                    binding.recyclerView.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                } else {
                    binding.recyclerView.setLayoutManager(layoutManager);
                    adapter = new Service_Adapter(getActivity(), filteredList, R.layout.item_product_list);
                    binding.recyclerView.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        binding.swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                LoadData(false);
            }
        });


    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(!AppConstants._hasLoadedOnce_product);
        if (this.isVisible()) {
            // we check that the fragment is becoming visible
            if (isVisibleToUser && !AppConstants._hasLoadedOnce_product) {
                LoadData(true);
                AppConstants._hasLoadedOnce_product = true;
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();


    }

    private void LoadData(boolean Isload) {
        String param = "&user_id=" + Preferences.getValue_String(getActivity(), Preferences.USER_ID);
        param = param + "&location=";
        param = param + "&mile=" + Preferences.getValue_String(getActivity(), Preferences.MILES);
        param = param + "&min_price=" + Preferences.getValue_String(getActivity(), Preferences.MIN_PRICE);
        param = param + "&max_price=" + Preferences.getValue_String(getActivity(), Preferences.MAX_PRICE);
        param = param + "&sort=" + Preferences.getValue_String(getActivity(), Preferences.SORT_BY);
        try {
            param = param + "&address=" + URLEncoder.encode(Preferences.getValue_String(getActivity(), Preferences.ADDRESS), AppConstants.encodeType);
            param = param + "&city=" + URLEncoder.encode(Preferences.getValue_String(getActivity(), Preferences.CITY), AppConstants.encodeType);
            param = param + "&state=" + URLEncoder.encode(Preferences.getValue_String(getActivity(), Preferences.STATE), AppConstants.encodeType);
            param = param + "&country=" + URLEncoder.encode(Preferences.getValue_String(getActivity(), Preferences.COUNTRY), AppConstants.encodeType);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        param = param + "&date=" + new SimpleDateFormat("ddMMyyHHmmss").format(Calendar.getInstance().getTime());
        new GetService(param, getActivity(), new GetService.OnResultReceived() {
            @Override
            public void onResult(String result) throws UnsupportedEncodingException {

                binding.swipe.setRefreshing(false);
                if (AppConstants.serivceList.size() > 0) {
                    binding.relSearch.setVisibility(View.VISIBLE);
                    if (Preferences.getValue_Boolean(getActivity(), Preferences.LIST_TYPE, true)) {
                        binding.recyclerView.setLayoutManager(gridLayoutManager);
                        adapter = new Service_Adapter(getActivity(), AppConstants.serivceList, R.layout.item_prodcut);
                        binding.recyclerView.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                    } else {
                        binding.recyclerView.setLayoutManager(layoutManager);
                        adapter = new Service_Adapter(getActivity(), AppConstants.serivceList, R.layout.item_product_list);
                        binding.recyclerView.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                    }
                } else {
                    binding.relSearch.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailed(VolleyError result) throws UnsupportedEncodingException {
                binding.swipe.setRefreshing(false);
            }
        }, Isload);




     /*   int spacing = 10; // 50px
        boolean includeEdge = true;
        binding.recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, spacing, includeEdge));*/


    }


    public void ChangeListType() {
        if (AppConstants.serivceList.size() > 0) {
            Log.e("TAG", "Refresh Service");
            //adapter.notifyDataSetChanged();
            if (Preferences.getValue_Boolean(getActivity(), Preferences.LIST_TYPE, true)) {
                binding.recyclerView.setLayoutManager(gridLayoutManager);
                adapter = new Service_Adapter(getActivity(), AppConstants.serivceList, R.layout.item_prodcut);
                binding.recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                Log.e("IF", "Refresh Service");
            } else {
                binding.recyclerView.setLayoutManager(layoutManager);
                adapter = new Service_Adapter(getActivity(), AppConstants.serivceList, R.layout.item_product_list);
                binding.recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                Log.e("ELSE", "Refresh Service");
            }
        }
    }

    @Override
    public void load() {
        LoadData(true);
    }

    @Override
    public void refresh_view() {
        ChangeListType();
    }
}
