package com.sc.student2student.Activity;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.edmodo.cropper.CropImageView;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.sc.student2student.R;
import com.sc.student2student.api.ChangePasswordApi;
import com.sc.student2student.api.EditProfileApi;
import com.sc.student2student.api.GetCityApi;
import com.sc.student2student.common.AppConstants;
import com.sc.student2student.common.CustomMultipartRequest;
import com.sc.student2student.common.DialogManager;
import com.sc.student2student.common.LoadingDialog;
import com.sc.student2student.common.Preferences;
import com.sc.student2student.common.WebServices;
import com.sc.student2student.databinding.ActivityEditProfileBinding;
import com.sc.student2student.databinding.ChangePasswordDialogBinding;
import com.sc.student2student.moduls.City;
import com.sc.student2student.volley.VolleySingleton;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.sc.student2student.common.AppConstants.cityArrayList;

public class EditProfileActivity extends AppCompatActivity {
    public static final int REQUEST_CODE_TAKE_PICTURE = 2;
    public static final int REQUEST_CODE_GALLERY = 1;
    private static final int PLACE_PICKER_REQUEST = 3;
    final private int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124;
    ActivityEditProfileBinding binding;
    String selectedImagePath = "";
    ArrayList<File> mFilePartData = new ArrayList<>();
    Map<String, String> mHeaderPart = new HashMap<>();
    Map<String, String> mStringPart = new HashMap<>();
    LoadingDialog loadingDialog;
    private Bitmap bitmap = null;
    private String TAG = getClass().getSimpleName();
    private ArrayAdapter<City> cityArrayAdapter;

    public static void changePassord(final Context activity) {
        final ChangePasswordDialogBinding dialogBinding;
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        LayoutInflater inflater = ((Activity) activity).getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.change_password_dialog, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog b = dialogBuilder.create();
        b.show();
        dialogBinding = ChangePasswordDialogBinding.bind(dialogView);

        dialogBinding.btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean flagValidate = true;
                String validateMsg = "";
                if (dialogBinding.edtOldpassword.getText().toString().trim().equals("")) {
                    validateMsg = "Please enter old password";
                    flagValidate = false;
                } else if (dialogBinding.edtNewpassword.getText().toString().trim().equals("")) {
                    validateMsg = "Please enter new password";
                    flagValidate = false;
                } else if (dialogBinding.edtConfirmpassword.getText().toString().trim().equals("")) {
                    validateMsg = "Please enter confirm password";
                    flagValidate = false;
                } else if (!dialogBinding.edtConfirmpassword.getText().toString().trim().equals(dialogBinding.edtNewpassword.getText().toString())) {
                    validateMsg = "Not match confirm password";
                    flagValidate = false;
                }

                if (flagValidate) {
                    b.dismiss();
                    String param = "&oldPass=" + dialogBinding.edtOldpassword.getText().toString();
                    param = param + "&newPass=" + dialogBinding.edtNewpassword.getText().toString();
                    param = param + "&user_id=" + Preferences.getValue_String(activity, Preferences.USER_ID);
                    param = param + "&date=" + new SimpleDateFormat("ddMMyyHHmmss").format(Calendar.getInstance().getTime());
                    new ChangePasswordApi(param, activity, new ChangePasswordApi.OnResultReceived() {
                        @Override
                        public void onResult(String result) throws UnsupportedEncodingException {

                        }
                    }, true);

                } else {
                    DialogManager.errorDialog(activity, activity.getString(R.string.validation_title), validateMsg);

                }

            }
        });

        dialogBinding.btnConfirm.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    dialogBinding.btnConfirm.setTextColor(Color.WHITE);
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    dialogBinding.btnConfirm.setTextColor(Color.rgb(6, 190, 188));
                }
                return false;
            }
        });

        dialogBinding.btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
            }
        });

        dialogBinding.btnCancel.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    dialogBinding.btnCancel.setTextColor(Color.WHITE);
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    dialogBinding.btnCancel.setTextColor(Color.rgb(6, 190, 188));
                }
                return false;
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_edit_profile);
        initView();
        getCity();
        VolleySingleton.getInstance().init(getApplicationContext());
       /* mHeaderPart.put("Content-type", "multipart/form-data;");
        mHeaderPart.put("Content-Type", "application/json");
        mHeaderPart.put("Accept", "application/json");
        mHeaderPart.put("Cache-Control", "no-cache");
        mHeaderPart.put("deviceID", Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID));*/
        binding.relToolbar.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

/*
        binding.imgQuestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogManager.errorDialog(EditProfileActivity.this, "Location", "Your location will be visible for other users," +
                        " Therefore we recommend you to select only the city in which you're located and not your entire address.");
            }
        });
*/

        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            AppConstants.mFileTemp = new File(Environment.getExternalStorageDirectory(), AppConstants.TEMP_PHOTO_FILE_NAME);
        } else {
            AppConstants.mFileTemp = new File(getFilesDir(), AppConstants.TEMP_PHOTO_FILE_NAME);
        }

        binding.relUploadPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= 23) {
                    if (checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                            && checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                            && checkSelfPermission(android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        getAllPermission();
                    } else {
                        startDialog();
                    }
                } else {
                    startDialog();
                }
            }
        });

        binding.relToolbar.imgOption2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isValidate()) {
                    String param = "&user_id=" + Preferences.getValue_String(EditProfileActivity.this, Preferences.USER_ID);
                    try {
                        param = param + "&name=" + URLEncoder.encode(binding.edtName.getText().toString(), AppConstants.encodeType);
                        param = param + "&about=" + URLEncoder.encode(binding.edtAboutproduct.getText().toString(), AppConstants.encodeType);
//                        param = param + "&location=" + URLEncoder.encode(AppConstants.user.getLocation(), AppConstants.encodeType);
//                        param = param + "&latitude=" + URLEncoder.encode(AppConstants.user.getLatitude(), AppConstants.encodeType);
//                        param = param + "&longitude=" + URLEncoder.encode(AppConstants.user.getLongitude(), AppConstants.encodeType);
//                        param = param + "&address=" + URLEncoder.encode(AppConstants.user.getAddress(), AppConstants.encodeType);
//                        param = param + "&state=" + URLEncoder.encode(AppConstants.user.getState(), AppConstants.encodeType);
//                        param = param + "&city=" + URLEncoder.encode(AppConstants.user.getCity(), AppConstants.encodeType);
//                        param = param + "&country=" + URLEncoder.encode(AppConstants.user.getCountry(), AppConstants.encodeType);
                        param = param + "&address=" + "";
                        param = param + "&location=" + "";
                        param = param + "&state=" + "";
                        param = param + "&latitude=" + URLEncoder.encode(Preferences.getValue_String(EditProfileActivity.this, Preferences.LATITUDE_PROFILE), AppConstants.encodeType);
                        param = param + "&longitude=" + URLEncoder.encode(Preferences.getValue_String(EditProfileActivity.this, Preferences.LONGITUDE_PROFILE), AppConstants.encodeType);
                        param = param + "&city=" + URLEncoder.encode(Preferences.getValue_String(EditProfileActivity.this, Preferences.CITY_PROFILE), AppConstants.encodeType);
                        param = param + "&country=" + "";

                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    param = param + "&date=" + new SimpleDateFormat("ddMMyyHHmmss").format(Calendar.getInstance().getTime());
                    if (mFilePartData.size() > 0) {
                        String url = WebServices.EDIT_PROFILE + param;

                        Log.e("Url", url);

                        loadingDialog = new LoadingDialog(EditProfileActivity.this, "", false);

                        CustomMultipartRequest mCustomRequest = new CustomMultipartRequest(Request.Method.POST, EditProfileActivity.this, url, new Response.Listener<NetworkResponse>() {
                            @Override
                            public void onResponse(NetworkResponse networkResponse) {

                                Log.e("mudit", new String(networkResponse.data));

                                try {
                                    JSONObject response = new JSONObject(new String(networkResponse.data));

                                    if (response.has("error")) {
                                        if (loadingDialog != null) {
                                            loadingDialog.hide();
                                        }
                                        DialogManager.errorDialog(EditProfileActivity.this, "Validation Error", response.optString("error"));
                                    } else if (response.has("success")) {
                                        DialogManager.errorDialog(EditProfileActivity.this, "success", response.optString("success"), new DialogManager.OnactionResult() {
                                            @Override
                                            public void Action() {
                                                finish();
                                            }
                                        });
                                        if (loadingDialog != null) {
                                            loadingDialog.hide();
                                        }


                                    } else {

                                        Toast.makeText(EditProfileActivity.this, getResources().getString(R.string.something_was_wrong_with_api), Toast.LENGTH_LONG).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }


                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError volleyError) {
                                Log.e("onErrorResponse", volleyError.getMessage());
                            }
                        }, mFilePartData, mStringPart, mHeaderPart, "image");

                        mCustomRequest.setRetryPolicy(new DefaultRetryPolicy(
                                30000,
                                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                        VolleySingleton.getInstance().addToRequestQueue(mCustomRequest);

                    } else {
                        new EditProfileApi(param, EditProfileActivity.this, new EditProfileApi.OnResultReceived() {
                            @Override
                            public void onResult(String result) throws UnsupportedEncodingException {

                            }
                        }, true);
                    }
                }
            }
        });

        binding.relChangepassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changePassord(EditProfileActivity.this);
            }
        });

/*
        binding.imgChangelocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //startActivity(new Intent(EditProfileActivity.this, SelectLocationActivity.class));
                try {
                    PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                    startActivityForResult(builder.build(EditProfileActivity.this), PLACE_PICKER_REQUEST);
                } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }
            }
        });
*/
        binding.spnCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int i, long id) {
                Log.e(TAG, "spnCity.Click(){...}");
                //getCity();
                AppConstants.city = (City) binding.spnCity.getSelectedItem();
                if (i != 0) {
                    Preferences.setValue(EditProfileActivity.this, Preferences.CITY_PROFILE, AppConstants.city.getCity_name());
                    Preferences.setValue(EditProfileActivity.this, Preferences.LATITUDE_PROFILE, AppConstants.city.getLatitude());
                    Preferences.setValue(EditProfileActivity.this, Preferences.LONGITUDE_PROFILE, AppConstants.city.getLongitude());
                }
                Log.e(TAG, "Click... City : " + Preferences.getValue_String(EditProfileActivity.this, Preferences.CITY_PROFILE));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void getCity() {
        if (AppConstants.isNetworkAvailable(getString(R.string.network_title), getString(R.string.network_msg), EditProfileActivity.this)) {
            new GetCityApi(EditProfileActivity.this, new GetCityApi.OnResultReceived() {
                @Override
                public void OnResult(String result) {

                    cityArrayAdapter = new ArrayAdapter<City>(EditProfileActivity.this, R.layout.item_spinner_text_layout, cityArrayList) {
                        @Override
                        public boolean isEnabled(int position) {
                            if (position == 0) {
                                return false;
                            } else {
                                return true;
                            }
                        }
                    };
                    Log.e(TAG, "GetCity CITY : " + Preferences.getValue_String(EditProfileActivity.this, Preferences.CITY_PROFILE));
                    cityArrayAdapter.setDropDownViewResource(R.layout.spinner_item2);
                    binding.spnCity.setAdapter(cityArrayAdapter);

                    for (int i = 0; i < AppConstants.cityArrayList.size(); i++) {
                        if (AppConstants.cityArrayList.get(i).getCity_name().equals(Preferences.getValue_String(EditProfileActivity.this, Preferences.CITY_PROFILE))) {
                            binding.spnCity.setSelection(i);
                        }
                    }
                    // dialogBinding.spnCity.setSelection(Preferences.getValue_String(ProductListActivity.this, Preferences.CITY).equals(AppConstants.city.getCity_name()));
                    cityArrayAdapter.notifyDataSetChanged();
                }
            }, true);
        }
    }


    private void initView() {
        binding.relToolbar.tvTitle.setText("Edit Profile");
        binding.relToolbar.imgOption2.setVisibility(View.VISIBLE);
        binding.relToolbar.imgOption2.setImageResource(R.drawable.ic_done);
    }

    private void setScreenData() {

        if (!AppConstants.user.getProfile_pic().equals("")) {
            Picasso.with(EditProfileActivity.this).load(AppConstants.user.getProfile_pic()).placeholder(R.drawable.profile).into(binding.imgPic);
        }
        if (!AppConstants.user.getProfile_pic().equals("")) {
            Picasso.with(EditProfileActivity.this).load(AppConstants.user.getProfile_pic()).into(binding.imgEmpty);
        }
        binding.edtAboutproduct.setText(AppConstants.user.getAbout());
        binding.edtName.setText(AppConstants.user.getName());
//        binding.edtLocation.setText(AppConstants.user.getLocation());

    }

    public void startDialog() {
        android.support.v7.app.AlertDialog.Builder myAlertDialog = new android.support.v7.app.AlertDialog.Builder(EditProfileActivity.this);
        myAlertDialog.setTitle("Select Photo");
        myAlertDialog.setMessage("How would you like to upload Photo?");

        myAlertDialog.setPositiveButton("Gallery",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        openGallery();
                    }
                });

        myAlertDialog.setNegativeButton("Camera",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        takePicture();
                    }
                });

        myAlertDialog.show();
    }

    private void startCropImage() {

        final Dialog dialog = new Dialog(EditProfileActivity.this,
                R.style.DialogFullScreen);

        dialog.setContentView(R.layout.dialog_crop);
        dialog.setCancelable(false);
        final CropImageView cropImageView = (CropImageView) dialog.findViewById(R.id.CropImageView);
        final ImageButton rotateButtonLeft = (ImageButton) dialog.findViewById(R.id.Button_rotate_left);
        final ImageButton rotateButtonRight = (ImageButton) dialog.findViewById(R.id.Button_rotate_right);
        final Button CROP = (Button) dialog.findViewById(R.id.Crop);
        final Button CANCEL = (Button) dialog.findViewById(R.id.Cancel);

        Log.e("IMAGE_PATH", AppConstants.mFileTemp.getPath());
        Log.e("IMAGE", BitmapFactory.decodeFile(AppConstants.mFileTemp.getPath()) + "");

        bitmap = AppConstants.getRescaledBitmap(AppConstants.mFileTemp.getPath());

        try {
            Bitmap bitmap = BitmapFactory.decodeFile(AppConstants.mFileTemp.getPath());
            bitmap.getWidth();
            cropImageView.setImageBitmap(bitmap);
            cropImageView.setAspectRatio(15, 15);
            cropImageView.setFixedAspectRatio(true);
        } catch (Exception e) {
            Toast.makeText(EditProfileActivity.this, "Image Not Available",
                    Toast.LENGTH_SHORT).show();
            CROP.setVisibility(View.GONE);
        }
        rotateButtonLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cropImageView.rotateImage(-90);
            }
        });


        rotateButtonRight.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                cropImageView.rotateImage(90);
            }
        });

        CROP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String imagePath;
                bitmap = cropImageView.getCroppedImage();


                Log.e("TAG", "ImagePath: " + AppConstants.profile_path);
                binding.imgEmpty.setImageBitmap(bitmap);
                binding.imgPic.setImageBitmap(bitmap);
                if (storeImageToSDCard(bitmap) != null) {

                    if (mFilePartData.size() > 0) {
                        mFilePartData.set(0, storeImageToSDCard(bitmap));
                    } else {
                        mFilePartData.add(0, storeImageToSDCard(bitmap));
                    }
                    Log.e("mFilePartData size", "" + mFilePartData.size());
                }
                /*May be upload here.. */

                dialog.dismiss();
            }
        });
        CANCEL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS: {
                Map<String, Integer> perms = new HashMap<String, Integer>();
                // Initial
                perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                // Fill with results
                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);
                // Check for ACCESS_FINE_LOCATION
                if (perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    // All Permissions Granted
                    Toast.makeText(EditProfileActivity.this, "All Permissions are Granted", Toast.LENGTH_SHORT)
                            .show();
                    startDialog();
                } else {
                    // Permission Denied
                    Toast.makeText(EditProfileActivity.this, "Some Permission is Denied", Toast.LENGTH_SHORT)
                            .show();
                }
            }
            break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private File storeImageToSDCard(Bitmap processedBitmap) {
        File file = null;
        try {
            // TODO Auto-generated method stub
            OutputStream output;
            File filepath = Environment.getExternalStorageDirectory();
            File dir = new File(filepath.getAbsolutePath() + "/" + AppConstants.APP_NAME + "/");
            dir.mkdirs();
            String imge_name = AppConstants.APP_NAME + System.currentTimeMillis() + ".jpg";
            file = new File(dir, imge_name);
            if (file.exists()) {
                file.delete();
                file.createNewFile();
            } else {
                file.createNewFile();
            }

            try {
                output = new FileOutputStream(file);
                processedBitmap.compress(Bitmap.CompressFormat.PNG, 50, output);
                output.flush();
                output.close();
                int file_size = Integer.parseInt(String.valueOf(file.length() / 1024));

                selectedImagePath = file.getAbsolutePath();

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return file;
    }

    private void openGallery() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, REQUEST_CODE_GALLERY);
    }

    private void takePicture() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        try {
            Uri mImageCaptureUri = null;
            // mImageCaptureUri = Uri.fromFile(AppConstants.mFileTemp);
            mImageCaptureUri = FileProvider.getUriForFile(EditProfileActivity.this, this.getApplicationContext().getPackageName() + ".provider", AppConstants.mFileTemp);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
            intent.putExtra("return-data", true);
            startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        }
    }

    //Permission methods

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != Activity.RESULT_OK) {
            return;
        }
        switch (requestCode) {
            case REQUEST_CODE_GALLERY:
                try {
                    InputStream inputStream = getContentResolver().openInputStream(data.getData());
                    FileOutputStream fileOutputStream = new FileOutputStream(AppConstants.mFileTemp);
                    AppConstants.copyStream(inputStream, fileOutputStream);
                    fileOutputStream.close();
                    inputStream.close();
                    Log.e("TAG", "Starts For Crop: ");
                    startCropImage();
                } catch (Exception e) {
                    Log.e("TAG", "Exception: " + e.getMessage());
                }
                break;
            case REQUEST_CODE_TAKE_PICTURE:
                Log.e("TAG", "Starts For Crop: ");
                startCropImage();
                break;
            case PLACE_PICKER_REQUEST:
                Place place = PlacePicker.getPlace(this, data);
                if (place != null) {
                    LatLng latLng = place.getLatLng();
                    Log.e("TAG", "Latitude : " + latLng.latitude);
                    Log.e("TAG", "Longitude : " + latLng.longitude);
                    Log.e("TAG", "Address : " + place.getAddress());
                    AppConstants.user.setLatitude("" + latLng.latitude);
                    AppConstants.user.setLongitude("" + latLng.longitude);
                    String address = "" + place.getAddress();
                    AppConstants.user.setAddress("" + place.getAddress());
                    AppConstants.user.setLocation("" + place.getAddress());
                  /*  new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                             binding.edtLocation.setText(AppConstants.user.getLocation());
                        }
                    }, 100);*/
                    if (!address.equals("")) {
                        if (address.contains(",")) {
                            String part[] = address.split(",");
                            if (part.length == 2) {
                                Log.e("TAG", "Have tow values : ");
                                Log.e("TAG", "Country : " + part[part.length - 1]);
                                Log.e("TAG", "Stat : " + part[part.length - 2]);
                                AppConstants.user.setCountry(part[part.length - 1]);
                                AppConstants.user.setState(part[part.length - 2]);
                            } else if (part.length >= 3) {
                                Log.e("TAG", "Have three or more then there values : ");
                                Log.e("TAG", "Country : " + part[part.length - 1]);
                                Log.e("TAG", "Stat : " + part[part.length - 2]);
                                Log.e("TAG", "City : " + part[part.length - 3]);
                                AppConstants.user.setCountry(part[part.length - 1]);
                                AppConstants.user.setState(part[part.length - 2]);
                                AppConstants.user.setCity(part[part.length - 3]);
                            } else {
                                Log.e("TAG: ", "Please select proper location.");
                            }
                        }
                    }
                    DialogManager.errorDialog(EditProfileActivity.this, "Location", "Your location will be visible for other users," +
                            " Therefore we recommend you to select only the city in which you're located and not your entire address.");
                    break;
                }
        }
    }

    public void getAllPermission() {
        List<String> permissionsNeeded = new ArrayList<String>();
        if (Build.VERSION.SDK_INT >= 23) {
            final List<String> permissionsList = new ArrayList<String>();

            if (!addPermission(permissionsList, android.Manifest.permission.CAMERA))
                permissionsNeeded.add("Camera");
            if (!addPermission(permissionsList, android.Manifest.permission.READ_EXTERNAL_STORAGE))
                permissionsNeeded.add("Read External Storage");
            if (!addPermission(permissionsList, Manifest.permission.WRITE_EXTERNAL_STORAGE))
                permissionsNeeded.add("Write External Storage");

            if (permissionsList.size() > 0) {
                if (permissionsNeeded.size() > 0) {
                    // Need Rationale
                    String message = "You need to grant access to " + permissionsNeeded.get(0);
                    for (int i = 1; i < permissionsNeeded.size(); i++)
                        message = message + ", " + permissionsNeeded.get(i);
                    showMessageOKCancel(message,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (Build.VERSION.SDK_INT >= 23) {
                                        requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                                                REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                                    }
                                }
                            });
                    return;
                }
                requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                        REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                return;
            }
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new android.app.AlertDialog.Builder(EditProfileActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    private boolean addPermission(List<String> permissionsList, String permission) {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                permissionsList.add(permission);
                // Check for Rationale Option
                if (!shouldShowRequestPermissionRationale(permission))
                    return false;
            }
        }
        return true;
    }

    private boolean isValidate() {
        boolean flagValidate = true;
        String validateMsg = "";


        if (binding.edtName.getText().toString().trim().equals("")) {
            validateMsg = getString(R.string.text_validate_last_name);
            flagValidate = false;
        }

/*
        if (binding.edtLocation.getText().toString().trim().equals("")) {
            validateMsg = getString(R.string.text_validate_select_location);
            flagValidate = false;
        }
*/


        if (flagValidate) {
            return true;
        } else {
            DialogManager.errorDialog(EditProfileActivity.this, getString(R.string.validation_title), validateMsg);
            return false;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (AppConstants.view_profile) {
            setScreenData();
        } else {
            Log.e("TAG", "Already fill data: ");
            //binding.edtLocation.setText(AppConstants.user.getLocation());
        }
    }
}
