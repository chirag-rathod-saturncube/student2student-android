package com.sc.student2student.Activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.view.View;

import com.sc.student2student.R;
import com.sc.student2student.adapter.CategoryAdapter;
import com.sc.student2student.api.GetCategory;
import com.sc.student2student.common.AppConstants;
import com.sc.student2student.common.GridSpacingItemDecoration;
import com.sc.student2student.databinding.ActivityCategoriesBinding;
import com.sc.student2student.moduls.Category;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class CategoriesActivity extends AppCompatActivity {
    public static CategoriesActivity activity;
    ActivityCategoriesBinding binding;
    CategoryAdapter adapter;
    GridLayoutManager gridLayoutManager;

    public static CategoriesActivity getInstance() {
        return activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_categories);
        initView();
        activity = this;


        String param = "&type=" + AppConstants.TYPE;
        param = param + "&date=" + new SimpleDateFormat("ddMMyyHHmmss").format(Calendar.getInstance().getTime());
        new GetCategory(param, CategoriesActivity.this, new GetCategory.OnResultReceived() {
            @Override
            public void onResult(String result) throws UnsupportedEncodingException {
                if (AppConstants.categoryList.size() > 0) {
                    int spacing = 10; // 50px
                    boolean includeEdge = true;
                    binding.recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, spacing, includeEdge));
                    binding.recyclerView.setLayoutManager(gridLayoutManager);
                    adapter = new CategoryAdapter(CategoriesActivity.this, AppConstants.categoryList, R.layout.item_category);
                    binding.recyclerView.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                }
            }
        }, true);


    }

    private void initView() {
        gridLayoutManager = new GridLayoutManager(CategoriesActivity.this, 2);

        binding.relToolbar.tvTitle.setText("Categories");

        binding.relToolbar.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    private void LoadData() {
        AppConstants.categoryList.clear();
        for (int i = 0; i < 20; i++) {
            Category product = new Category();
            product.setId("" + i);
            product.setCategory_name("Category " + i);
            product.setCategory_image("http://cpn.canon-europe.com/files/product/cameras/powershot_g7_x_mark_ii/overview_big.jpg");
            AppConstants.categoryList.add(product);
        }

    }
}
