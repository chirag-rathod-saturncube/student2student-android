package com.sc.student2student.adapter;

import com.sc.student2student.R;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;


import com.sc.student2student.common.AppConstants;


/**
 * Created by pc4 on 10/16/2015.
 */
public class IntroShowAdapter extends PagerAdapter {
    Context context;
    int resource;
    private LayoutInflater inflater;


    public IntroShowAdapter(Context context, int resource) {
        this.context = context;
        this.resource = resource;
    }

    @Override
    public int getCount() {

        //Log.e("SIZE", "" + CurrentUser.images_to_view.size());
        return AppConstants.pic.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewLayout = inflater.inflate(resource, container, false);
        ImageView imageView = (ImageView) viewLayout.findViewById(R.id.image_in_view_images);


        imageView.setPadding(4, 4, 4, 4);
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);

        imageView.setImageResource(AppConstants.pic.get(position));
        // Picasso.with(context).load(WebServices.BASE_URL_IMAGE + AppConstants.gridItems.get(position).getOriginalImage()).placeholder(R.drawable.animated_dialog).error(R.drawable.no_image).into(imageView);

        // mAttacher = new PhotoViewAttacher(imageView);
        ((ViewPager) container).addView(viewLayout, 0);


        return viewLayout;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((RelativeLayout) object);
    }
}