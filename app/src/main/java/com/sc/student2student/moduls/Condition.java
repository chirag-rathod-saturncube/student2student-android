package com.sc.student2student.moduls;

/**
 * Created by Saturncube10 on 29-May-17.
 */

public class Condition {

    String con_id, con_title;

    public String getCon_id() {
        return con_id;
    }

    public void setCon_id(String con_id) {
        this.con_id = con_id;
    }

    public String getCon_title() {
        return con_title;
    }

    public void setCon_title(String con_title) {
        this.con_title = con_title;
    }

    public String toString() {
        return this.con_title;
    }
}
