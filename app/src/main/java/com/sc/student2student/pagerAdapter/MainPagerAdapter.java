package com.sc.student2student.pagerAdapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.sc.student2student.fragement.ProductFragment;
import com.sc.student2student.fragement.ServiceFragment;

/**
 * Created by pc4 on 12/21/2016.
 */

public class MainPagerAdapter extends FragmentPagerAdapter {
    private Context context;
    private String[] data;

   /* public MyPagerAdapter(android.support.v4.app.FragmentManager fm) {
        super(fm);
    }*/

    public MainPagerAdapter(FragmentManager fm, Context context, String[] data) {
        super(fm);
        this.context = context;
        this.data = data;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return data[position];
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public int getCount() {
        return data.length;
    }

    public void setData(String[] data) {
        this.data = data;
    }


    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return ProductFragment.newInstance("Products");
            case 1:
                return ServiceFragment.newInstance("Services");
            default:
                return ProductFragment.newInstance("Products");
        }
    }
}