package com.sc.student2student.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.VolleyError;
import com.sc.student2student.Activity.NotificationActivity;
import com.sc.student2student.Activity.ProductDetailsActivity;
import com.sc.student2student.Activity.RatingActivity;
import com.sc.student2student.Activity.ReviewsActivity;
import com.sc.student2student.R;
import com.sc.student2student.api.AcceptApi;
import com.sc.student2student.api.AcceptServiceApi;
import com.sc.student2student.api.DeleteNotificationApi;
import com.sc.student2student.api.GetProductDetails;
import com.sc.student2student.api.GetRatingApi;
import com.sc.student2student.api.RejectApi;
import com.sc.student2student.api.Reject_ServiceApi;
import com.sc.student2student.api.ReviewsApi;
import com.sc.student2student.common.AppConstants;
import com.sc.student2student.common.Preferences;
import com.sc.student2student.databinding.DialogAcceptRejectBinding;
import com.sc.student2student.databinding.ItemNotificationBinding;
import com.sc.student2student.moduls.Notification;
import com.squareup.picasso.Picasso;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


/**
 * Created by pc4 on 5/30/2016.
 */
public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.CustomViewHolder> {
    List<Notification> infos = null;
    int layout;
    ItemNotificationBinding categoryBinding;
    private ArrayList<Notification> arrayList;
    private Context mContext;
    private String TAG = getClass().getSimpleName();

    public NotificationAdapter(Context context, List<Notification> list, int layout) {
        this.infos = list;
        this.arrayList = new ArrayList<>();
        this.arrayList.addAll(infos);
        this.mContext = context;
        this.layout = layout;
    }


    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(layout, null);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final CustomViewHolder customViewHolder, final int pos) {
        final Notification list = infos.get(pos);
        categoryBinding.tvTime.setText(list.getCrt_date());
        categoryBinding.tvUsername.setText(list.getNotification_msg());
        Log.e(TAG, "########################### : " + list.getRat_provide());
        if (!list.getProfile_pic().equals("")) {
            Picasso.with(mContext).load(list.getProfile_pic()).placeholder(R.drawable.profile).error(R.drawable.profile).transform(AppConstants.getTransformation(categoryBinding.imgProfile)).into(categoryBinding.imgProfile);
        }


        categoryBinding.swipeLayout.setSwipeEnabled(true);

        categoryBinding.relDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String envelope;

                envelope = "&user_id=" + Preferences.getValue_String(mContext, Preferences.USER_ID);
                envelope = envelope + "&notification_id=" + list.getNoti_id();
                envelope = envelope + "&date=" + new SimpleDateFormat("ddMMyyHHmmss").format(Calendar.getInstance().getTime());
                infos.remove(pos);
                notifyDataSetChanged();

                new DeleteNotificationApi(envelope, mContext, new DeleteNotificationApi.OnResultReceived() {
                    @Override
                    public void onResult(String result) throws UnsupportedEncodingException {

                    }
                }, false);

            }
        });


        if (list.getNf_type().equals("1")) {
            categoryBinding.tvRating.setVisibility(View.INVISIBLE);

            //categoryBinding.relDelSwipe.setVisibility(View.GONE);
            //categoryBinding.relAccept.setVisibility(View.GONE);
            //categoryBinding.relAccept.setVisibility(View.GONE);

            //categoryBinding.swipeLayout.setSwipeEnabled(false);
            categoryBinding.relAcpRej.setVisibility(View.VISIBLE);

        } else if (list.getNf_type().equals("4")) {
            categoryBinding.tvRating.setVisibility(View.INVISIBLE);

            //categoryBinding.relDelSwipe.setVisibility(View.GONE);
            // categoryBinding.relAccept.setVisibility(View.GONE);
            //categoryBinding.relAccept.setVisibility(View.GONE);

            //categoryBinding.swipeLayout.setSwipeEnabled(false);
            categoryBinding.relAcpRej.setVisibility(View.VISIBLE);

        } else if (list.getNf_type().equals("9")) {
            categoryBinding.tvRating.setVisibility(View.INVISIBLE);
            categoryBinding.relAcpRej.setVisibility(View.GONE);




        } else if (list.getNf_type().equals("6")) {
            if (list.getRat_provide().equals("0")) {
                categoryBinding.tvRating.setVisibility(View.VISIBLE);

            } else if (list.getRat_provide().equals("2")) {
                categoryBinding.tvRating.setVisibility(View.GONE);
            } else {
                categoryBinding.tvRating.setVisibility(View.GONE);
            }
            // categoryBinding.relDelSwipe.setVisibility(View.GONE);
            // categoryBinding.relAccept.setVisibility(View.GONE);
            //  categoryBinding.relAccept.setVisibility(View.GONE);

            //categoryBinding.swipeLayout.setSwipeEnabled(false);
            categoryBinding.relAcpRej.setVisibility(View.GONE);

        } else if (list.getNf_type().equals("2")) {
            categoryBinding.tvRating.setVisibility(View.INVISIBLE);
            // categoryBinding.relDelSwipe.setVisibility(View.GONE);
            // categoryBinding.relAccept.setVisibility(View.GONE);
            //  categoryBinding.relAccept.setVisibility(View.GONE);
            //  categoryBinding.swipeLayout.setSwipeEnabled(false);

            categoryBinding.relAcpRej.setVisibility(View.GONE);
        } else {
            categoryBinding.tvRating.setVisibility(View.INVISIBLE);
            // categoryBinding.relDelSwipe.setVisibility(View.GONE);
            // categoryBinding.relAccept.setVisibility(View.GONE);
            //categoryBinding.relAccept.setVisibility(View.GONE);
            // categoryBinding.swipeLayout.setSwipeEnabled(false);
            categoryBinding.relAcpRej.setVisibility(View.GONE);
        }


        /*categoryBinding.relAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Accept_Dialog(mContext, "Are you sure want to accept?", 1, list.getNoti_id(), list);

            }
        });
        categoryBinding.relReject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Accept_Dialog(mContext, "Are you sure want to reject?", 2, list.getNoti_id(), list);
            }
        });*/

        categoryBinding.tvAcp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Accept_Dialog(mContext, "Are you sure want to accept?", 1, list.getNoti_id(), list);

            }
        });
        categoryBinding.tvRej.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Accept_Dialog(mContext, "Are you sure want to reject?", 2, list.getNoti_id(), list);
            }
        });


        categoryBinding.tvRating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String param = "&user_id=" + list.getNot_by_user_id();
                param = param + "&log_user_id=" + Preferences.getValue_String(mContext, Preferences.USER_ID);
                param = param + "&date=" + new SimpleDateFormat("ddMMyyHHmmss").format(Calendar.getInstance().getTime());
                new GetRatingApi(param, mContext, new GetRatingApi.OnResultReceived() {
                    @Override
                    public void onResult(String result) throws UnsupportedEncodingException {
                        AppConstants.notification = list;
                        AppConstants.pos = pos;
                        mContext.startActivity(new Intent(mContext, RatingActivity.class));
                    }
                }, true);
            }
        });

        categoryBinding.relMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (list.getNf_type().equals("2")) {
                    String envelope = "&user_id=" + Preferences.getValue_String(mContext, Preferences.USER_ID);
                    new ReviewsApi(envelope, mContext, new ReviewsApi.OnResultReceived() {
                        @Override
                        public void onResult(String result) throws UnsupportedEncodingException {
                            mContext.startActivity(new Intent(mContext, ReviewsActivity.class));
                        }
                    }, true);
                } else {
                    String param = "&user_id=" + Preferences.getValue_String(mContext, Preferences.USER_ID);
                    param = param + "&pro_id=" + list.getPro_id();
                    param = param + "&date=" + new SimpleDateFormat("ddMMyyHHmmss").format(Calendar.getInstance().getTime());
                    new GetProductDetails(param, mContext, new GetProductDetails.OnResultReceived() {
                        @Override
                        public void onResult(String result) throws UnsupportedEncodingException {
                            AppConstants.pos = pos;
                            AppConstants.TYPE = list.getType();
                            mContext.startActivity(new Intent(mContext, ProductDetailsActivity.class));
                        }

                        @Override
                        public void onFailed(VolleyError result) throws UnsupportedEncodingException {

                        }
                    }, true);
                }
            }
        });


    }

    public void Accept_Dialog(Context activity, String msg, final int action, final String noti_id, final Notification notification) {
        final DialogAcceptRejectBinding dialogBinding;
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        LayoutInflater inflater = ((Activity) activity).getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_accept_reject, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog b = dialogBuilder.create();
        b.show();
        dialogBinding = DialogAcceptRejectBinding.bind(dialogView);

        dialogBinding.tvDialogMsg.setText(msg);
        if (action == 1) {
            dialogBinding.btnConfirm.setText("Accept");
        } else {
            dialogBinding.btnConfirm.setText("Reject");
        }


        dialogBinding.btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
                if (action == 1) {
                    if (notification.getNf_type().equals("1")) {
                        String param = "&noti_id=" + noti_id;
                        param = param + "&date=" + new SimpleDateFormat("ddMMyyHHmmss").format(Calendar.getInstance().getTime());
                        new AcceptApi(param, mContext, new AcceptApi.OnResultReceived() {
                            @Override
                            public void onResult(String result) throws UnsupportedEncodingException {
                                ((NotificationActivity) mContext).getNotification();
                            }
                        }, true);
                    } else if (notification.getNf_type().equals("4")) {
                        String param = "&noti_id=" + noti_id;
                        param = param + "&date=" + new SimpleDateFormat("ddMMyyHHmmss").format(Calendar.getInstance().getTime());
                        new AcceptServiceApi(param, mContext, new AcceptServiceApi.OnResultReceived() {
                            @Override
                            public void onResult(String result) throws UnsupportedEncodingException {
                                ((NotificationActivity) mContext).getNotification();
                            }
                        }, true);
                    }
                } else {
                    if (notification.getNf_type().equals("1")) {
                        String param = "&noti_id=" + noti_id;
                        param = param + "&date=" + new SimpleDateFormat("ddMMyyHHmmss").format(Calendar.getInstance().getTime());
                        new RejectApi(param, mContext, new RejectApi.OnResultReceived() {
                            @Override
                            public void onResult(String result) throws UnsupportedEncodingException {
                                ((NotificationActivity) mContext).getNotification();
                            }
                        }, true);
                    } else if (notification.getNf_type().equals("4")) {
                        String param = "&noti_id=" + noti_id;
                        param = param + "&date=" + new SimpleDateFormat("ddMMyyHHmmss").format(Calendar.getInstance().getTime());
                        new Reject_ServiceApi(param, mContext, new Reject_ServiceApi.OnResultReceived() {
                            @Override
                            public void onResult(String result) throws UnsupportedEncodingException {
                                ((NotificationActivity) mContext).getNotification();
                            }
                        }, true);
                    }
                }

            }
        });

        dialogBinding.btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();

            }
        });


    }

    @Override
    public int getItemCount() {
        return (null != infos ? infos.size() : 0);
    }


    public class CustomViewHolder extends RecyclerView.ViewHolder {


        public CustomViewHolder(View view) {
            super(view);
            categoryBinding = ItemNotificationBinding.bind(view);
        }
    }
}

