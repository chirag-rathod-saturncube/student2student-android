package com.sc.student2student.Activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.sc.student2student.R;
import com.sc.student2student.api.DeleteAccountApi;
import com.sc.student2student.api.LogoutApi;
import com.sc.student2student.api.ProductGroupListApi;
import com.sc.student2student.api.UpdateNotificationApi;
import com.sc.student2student.api.getProfileApi;
import com.sc.student2student.common.AppConstants;
import com.sc.student2student.common.DialogManager;
import com.sc.student2student.common.Preferences;
import com.sc.student2student.common.Utils;
import com.sc.student2student.databinding.HomeBinding;
import com.sc.student2student.fcm.MyFcmListenerService;
import com.sc.student2student.pagerAdapter.MainPagerAdapter;
import com.squareup.picasso.Picasso;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import static com.sc.student2student.common.AppConstants.ACTION;
import static com.sc.student2student.common.AppConstants.ACTION_PRODUCT;
import static com.sc.student2student.common.AppConstants.HELP;
import static com.sc.student2student.common.AppConstants.HELP_ACTION;
import static com.sc.student2student.common.AppConstants.MYLIST;
import static com.sc.student2student.common.AppConstants.NEW;
import static com.sc.student2student.common.AppConstants.PRODUCT;
import static com.sc.student2student.common.AppConstants.SERVICE;
import static com.sc.student2student.common.AppConstants.TYPE;
import static com.sc.student2student.common.AppConstants.USER_ID;
import static com.sc.student2student.common.AppConstants.WATCH_LIST;
import static com.sc.student2student.common.AppConstants.chatMsg;
import static com.sc.student2student.common.AppConstants.isNeedRefreshCounter;
import static com.sc.student2student.common.AppConstants.isNetworkAvailable;
import static com.sc.student2student.common.AppConstants.isSell;
import static com.sc.student2student.common.AppConstants.notificationMsg;
import static com.sc.student2student.common.AppConstants.page_display;
import static com.sc.student2student.common.AppConstants.user;

public class HomeActivity extends AppCompatActivity {
    HomeBinding binding;
    MainPagerAdapter pagerAdapter;
    boolean productFlag = true, serviceFlag = true;
    boolean flagRegister = false;
    private Boolean exit = false;
    private String TAG = getClass().getSimpleName();
    private String[] TITLES = new String[0];
    private BroadcastReceiver myReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.e(TAG, "Messages : " + AppConstants.totalMessages + " Notifications: " + AppConstants.totalNotification);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    binding.textDialogUnreadCount.setText(AppConstants.totalMessages);
                    binding.tvTotalNotification.setText(AppConstants.totalNotification);
                }
            });

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.home);
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                Log.e(TAG, "Simple Intent : ");
            } else if (extras.getBoolean("NotiClick")) {
                Log.e(TAG, "Notification Intent : ");
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        chatMsg = "";
                        notificationMsg = "";
                        Preferences.setValue(HomeActivity.this, Preferences.NOT_REQUEST_ID, "");
                        AppConstants.groupMsgArrayList.clear();
                        /*Preferences.saveGroupMsgArrayList(HomeActivity.this, Preferences.GROUP_MSG_ARRAY_LIST, AppConstants.groupMsgArrayList);
                        Preferences.setValue(HomeActivity.this, Preferences.isSaveGroupMsg, false);*/
                    }
                }, 100);

            }

        }


        binding.tvTotalNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String envelope = "&user_id=" + Preferences.getValue_String(HomeActivity.this, Preferences.USER_ID);
                envelope = envelope + "&date=" + new SimpleDateFormat("ddMMyyHHmmss").format(Calendar.getInstance().getTime());
                new UpdateNotificationApi(envelope, HomeActivity.this, new UpdateNotificationApi.OnResultReceived() {
                    @Override
                    public void onResult(String result) throws UnsupportedEncodingException {
                        startActivity(new Intent(HomeActivity.this, NotificationActivity.class));
                    }
                }, true);

            }
        });


        binding.imgChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, InboxProductActivity.class));
            }
        });
        binding.imgNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String envelope = "&user_id=" + Preferences.getValue_String(HomeActivity.this, Preferences.USER_ID);
                envelope = envelope + "&date=" + new SimpleDateFormat("ddMMyyHHmmss").format(Calendar.getInstance().getTime());
                new UpdateNotificationApi(envelope, HomeActivity.this, new UpdateNotificationApi.OnResultReceived() {
                    @Override
                    public void onResult(String result) throws UnsupportedEncodingException {
                        startActivity(new Intent(HomeActivity.this, NotificationActivity.class));
                    }
                }, true);
            }
        });


        binding.textDialogUnreadCount.setVisibility(View.GONE);
        if (isNetworkAvailable(getString(R.string.network_title),
                getString(R.string.network_msg), HomeActivity.this)) {
            String envelope = "&user_id=" + Preferences.getValue_String(HomeActivity.this, Preferences.USER_ID);
            envelope = envelope + "&date=" + new SimpleDateFormat("ddMMyyHHmmss").format(Calendar.getInstance().getTime());
            new ProductGroupListApi(envelope, HomeActivity.this, new ProductGroupListApi.OnResultReceived() {
                @Override
                public void onResult(String result) throws UnsupportedEncodingException {
                    //Log.e(TAG, "totalNotification : " + AppConstants.totalNotification);
                    if (Integer.parseInt(AppConstants.totalNotification) > 0) {
                        Log.e(TAG, "if - >");
                        binding.tvTotalNotification.setText(AppConstants.totalNotification);
                    } else {
                        Log.e(TAG, "else");
                        binding.tvTotalNotification.setVisibility(View.GONE);
                    }
                    //changeCounter();
                }
            }, false);
        }

        binding.fab1.setEnabled(true);
        binding.fab2.setEnabled(true);
        binding.menu3.setClosedOnTouchOutside(true);
        binding.fab1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binding.menu3.isOpened())
                    binding.menu3.close(true);
                Log.e(TAG, "Clieck");
                if (!Preferences.getValue_Boolean(HomeActivity.this, Preferences.isLocationUpdated, true)) {
                    DialogManager.errorDialog(HomeActivity.this, "Error!", "Please set location from my profile then you will be able to sell product or provide service", new DialogManager.OnactionResult() {
                        @Override
                        public void Action() {
                            USER_ID = Preferences.getValue_String(HomeActivity.this, Preferences.USER_ID);
                            startActivity(new Intent(HomeActivity.this, MyProfileActivity.class));
                        }
                    });
                } else {
                    TYPE = PRODUCT;
                    ACTION_PRODUCT = NEW;
                    isSell = true;
                    startActivity(new Intent(HomeActivity.this, SellPorSerActivity.class));
                }
            }
        });

        binding.fab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binding.menu3.isOpened())
                    binding.menu3.close(true);
                if (!Preferences.getValue_Boolean(HomeActivity.this, Preferences.isLocationUpdated, true)) {
                    DialogManager.errorDialog(HomeActivity.this, "Error!", "Please set location from my profile then you will be able to sell product or provide service", new DialogManager.OnactionResult() {
                        @Override
                        public void Action() {
                            USER_ID = Preferences.getValue_String(HomeActivity.this, Preferences.USER_ID);
                            startActivity(new Intent(HomeActivity.this, MyProfileActivity.class));
                        }
                    });
                } else {
                    TYPE = SERVICE;
                    ACTION_PRODUCT = NEW;
                    isSell = true;
                    startActivity(new Intent(HomeActivity.this, SellPorSerActivity.class));
                }
            }
        });


        //Get ViewProfile
        if (isNetworkAvailable(getString(R.string.network_title),
                getString(R.string.network_msg), HomeActivity.this)) {
            if (!Preferences.getValue_Boolean(HomeActivity.this, Preferences.isLocationUpdated, true)) {
                Log.e(TAG, "Check location : ");
                String envelope = "&user_id=" + Preferences.getValue_String(HomeActivity.this, Preferences.USER_ID);
                envelope = envelope + "&date=" + new SimpleDateFormat("ddMMyyHHmmss").format(Calendar.getInstance().getTime());
                new getProfileApi(envelope, HomeActivity.this, new getProfileApi.OnResultReceived() {
                    @Override
                    public void onResult(String result) throws UnsupportedEncodingException {
                        if (!user.getLatitude().equals("")
                                && !user.getLongitude().equals("")) {
                            Log.e(TAG, "Location already updated : ");
                            Preferences.setValue(HomeActivity.this, Preferences.isLocationUpdated, true);
                        } else {
                            Preferences.setValue(HomeActivity.this, Preferences.isLocationUpdated, false);
                            Log.e(TAG, "Location not updated : ");
                        }
                    }
                }, false);
            }
        }

        binding.textDialogUnreadCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomeActivity.this, InboxProductActivity.class));
            }
        });
/*
        binding.textDialogUnreadCount1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, NotificationActivity.class));
                binding.textDialogUnreadCount1.setVisibility(View.GONE);
            }
        });
*/


        TITLES = new String[]{"PRODUCTS", "SERVICES"};
        pagerAdapter = new MainPagerAdapter(getSupportFragmentManager(), HomeActivity.this, TITLES);
        binding.viewPager.setOffscreenPageLimit(2);
        binding.viewPager.setAdapter(pagerAdapter);
        binding.tabs.setViewPager(binding.viewPager);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                loadData(binding.viewPager.getCurrentItem());
            }
        }, 500);


        //Navigation drawer events
        binding.imgDrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                binding.navDrawer.openDrawer(GravityCompat.START);
            }
        });


        binding.draweritem.relHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.navDrawer.closeDrawer(GravityCompat.START);
            }
        });

        binding.draweritem.relSellproduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.navDrawer.closeDrawer(GravityCompat.START);
                if (!Preferences.getValue_Boolean(HomeActivity.this, Preferences.isLocationUpdated, true)) {
                    DialogManager.errorDialog(HomeActivity.this, "Error!", "Please set location from my profile then you will be able to sell product or provide service", new DialogManager.OnactionResult() {
                        @Override
                        public void Action() {
                            USER_ID = Preferences.getValue_String(HomeActivity.this, Preferences.USER_ID);
                            startActivity(new Intent(HomeActivity.this, MyProfileActivity.class));
                        }
                    });
                } else {
                    TYPE = PRODUCT;
                    ACTION_PRODUCT = NEW;
                    //startActivity(new Intent(HomeActivity.this, CategoriesActivity.class));
                    isSell = true;
                    startActivity(new Intent(HomeActivity.this, SellPorSerActivity.class));
                }
            }
        });

        binding.draweritem.relRequestservice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.navDrawer.closeDrawer(GravityCompat.START);
                if (!Preferences.getValue_Boolean(HomeActivity.this, Preferences.isLocationUpdated, true)) {
                    DialogManager.errorDialog(HomeActivity.this, "Error!", "Please set location from my profile then you will be able to sell product or provide service", new DialogManager.OnactionResult() {
                        @Override
                        public void Action() {
                            USER_ID = Preferences.getValue_String(HomeActivity.this, Preferences.USER_ID);
                            startActivity(new Intent(HomeActivity.this, MyProfileActivity.class));
                        }
                    });
                } else {
                    TYPE = SERVICE;
                    ACTION_PRODUCT = NEW;
                    //startActivity(new Intent(HomeActivity.this, CategoriesActivity.class));
                    isSell = true;
                    startActivity(new Intent(HomeActivity.this, SellPorSerActivity.class));
                }
            }
        });

        binding.draweritem.relCreatealert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.navDrawer.closeDrawer(GravityCompat.START);
                startActivity(new Intent(HomeActivity.this, AlertListActivity.class));
            }
        });
        binding.draweritem.relProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.navDrawer.closeDrawer(GravityCompat.START);
                USER_ID = Preferences.getValue_String(HomeActivity.this, Preferences.USER_ID);
                startActivity(new Intent(HomeActivity.this, MyProfileActivity.class));
            }
        });
        binding.draweritem.relHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.navDrawer.closeDrawer(GravityCompat.START);
                USER_ID = Preferences.getValue_String(HomeActivity.this, Preferences.USER_ID);
                startActivity(new Intent(HomeActivity.this, MyProfileActivity.class));

            }
        });
        binding.draweritem.relWatchlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.navDrawer.closeDrawer(GravityCompat.START);
                ACTION = WATCH_LIST;
                USER_ID = Preferences.getValue_String(HomeActivity.this, Preferences.USER_ID);
                startActivity(new Intent(HomeActivity.this, WatchListActivity.class));
            }
        });

/*
        binding.draweritem.relNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.navDrawer.closeDrawer(GravityCompat.START);
                startActivity(new Intent(HomeActivity.this, NotificationActivity.class));
            }
        });
*/
/*
        binding.draweritem.relChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.navDrawer.closeDrawer(GravityCompat.START);
                //DialogsActivity.start(HomeActivity.this);
                startActivity(new Intent(HomeActivity.this, InboxProductActivity.class));
            }
        });
*/
        binding.draweritem.relHelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.navDrawer.closeDrawer(GravityCompat.START);
                HELP_ACTION = HELP;
                startActivity(new Intent(HomeActivity.this, HelpActivity.class));
            }
        });
        binding.draweritem.relLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.navDrawer.closeDrawer(GravityCompat.START);
                String param = "&device_id=" + Settings.Secure.getString(HomeActivity.this.getContentResolver(), Settings.Secure.ANDROID_ID);
                param = param + "&date=" + new SimpleDateFormat("ddMMyyHHmmss").format(Calendar.getInstance().getTime());
                new LogoutApi(param, HomeActivity.this, new LogoutApi.OnResultReceived() {
                    @Override
                    public void onResult(String result) throws UnsupportedEncodingException {
                        //ChatLogOut();
                    }
                }, true);
            }
        });


        binding.draweritem.relMyProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.navDrawer.closeDrawer(GravityCompat.START);
                ACTION = MYLIST;
                page_display = 0;
                startActivity(new Intent(HomeActivity.this, WatchListActivity.class));
            }
        });

        binding.draweritem.relMyService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.navDrawer.closeDrawer(GravityCompat.START);
                ACTION = MYLIST;
                page_display = 1;
                startActivity(new Intent(HomeActivity.this, WatchListActivity.class));
            }
        });


        binding.draweritem.relDemo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.navDrawer.closeDrawer(GravityCompat.START);
                startActivity(new Intent(HomeActivity.this, DemoActivity.class));
            }
        });


        binding.draweritem.relDeleteAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.navDrawer.closeDrawer(GravityCompat.START);
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case DialogInterface.BUTTON_POSITIVE:
                                //Yes button clicked
                                String param = "&user_id=" + Preferences.getValue_String(HomeActivity.this, Preferences.USER_ID);
                                param = param + "&date=" + new SimpleDateFormat("ddMMyyHHmmss").format(Calendar.getInstance().getTime());

                                new DeleteAccountApi(param, HomeActivity.this, new DeleteAccountApi.OnResultReceived() {
                                    @Override
                                    public void onResult(String result) throws UnsupportedEncodingException {

                                        startActivity(new Intent(HomeActivity.this, LoginActivity.class));
                                        finish();
                                    }
                                }, true);
                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                //No button clicked
                                binding.navDrawer.closeDrawer(GravityCompat.START);
                                break;
                        }
                    }
                };

                AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);
                builder.setMessage("Are you sure want to delete your Account?").setPositiveButton("Yes", dialogClickListener)
                        .setNegativeButton("No", dialogClickListener).show();

            }
        });


        binding.viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                loadData(binding.viewPager.getCurrentItem());
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        if (!flagRegister) {
            Log.e(TAG, "Register broadcast listener... ... ");
            registerReceiver(myReceiver, new IntentFilter(MyFcmListenerService.INTENT_FILTER));
            flagRegister = true;
        }
    }


    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        finishApp();
    }

    public void finishApp() {
        if (binding.navDrawer.isDrawerOpen(GravityCompat.START)) {
            binding.navDrawer.closeDrawer(GravityCompat.START);
        } else {
            if (exit) {
                finish();
            } else {
                Toast.makeText(this, "Press Back again to exit.", Toast.LENGTH_SHORT).show();
                exit = true;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        exit = false;
                    }
                }, 3 * 1000);
            }
        }
    }

/*
    private void changeCounter() {
        count = Integer.parseInt(AppConstants.totalMessages);

        if (count > 0) {
            Log.e(TAG, "Counter................" + count);
            binding.textDialogUnreadCount.setVisibility(View.VISIBLE);
            binding.textDialogUnreadCount.setText("" + count);

        } else {
            binding.textDialogUnreadCount.setVisibility(View.GONE);
        }

        */
/*for (int i = 0; i < productGroupArrayList.size(); i++) {
            if (Preferences.getValue_String(HomeActivity.this, productGroupArrayList.get(i).getGroup_id())
                    != null && !Preferences.getValue_String(HomeActivity.this, productGroupArrayList.get(i).getGroup_id()).equals("")) {
                int prefValue = Integer.parseInt(Preferences.getValue_String(HomeActivity.this, productGroupArrayList.get(i).getGroup_id()));
                int newValue = Integer.parseInt(productGroupArrayList.get(i).getTotalMessages());
                int finalValue = newValue - prefValue;
                if (finalValue > 0) {
                    count = count + finalValue;
                } else {
                    // count = count + Integer.parseInt(AppConstants.productGroupArrayList.get(i).getTotalMessages());
                }
            } else {
                count = count + Integer.parseInt(productGroupArrayList.get(i).getTotalMessages());
            }
        }*//*

    }
*/

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (flagRegister) {
            unregisterReceiver(myReceiver);
            flagRegister = false;
        }
    }

    @Override
    protected void onResume() {

        Log.e(TAG, "onResume(){..}");

        if (isNetworkAvailable(getString(R.string.network_title),
                getString(R.string.network_msg), HomeActivity.this)) {
            String envelope = "&user_id=" + Preferences.getValue_String(HomeActivity.this, Preferences.USER_ID);
            envelope = envelope + "&date=" + new SimpleDateFormat("ddMMyyHHmmss").format(Calendar.getInstance().getTime());
            new ProductGroupListApi(envelope, HomeActivity.this, new ProductGroupListApi.OnResultReceived() {
                @Override
                public void onResult(String result) throws UnsupportedEncodingException {
                    //Log.e(TAG, "totalNotification : " + AppConstants.totalNotification);
                    if (Integer.parseInt(AppConstants.totalNotification) > 0) {
                        Log.e(TAG, "if - >");

                        binding.tvTotalNotification.setText(AppConstants.totalNotification);
                        binding.tvTotalNotification.setVisibility(View.VISIBLE);
                    } else {
                        Log.e(TAG, "else");
                        binding.tvTotalNotification.setVisibility(View.GONE);
                    }

                    if (Integer.parseInt(AppConstants.totalMessages) > 0) {
                        Log.e(TAG, "if - >");
                        binding.textDialogUnreadCount.setText(AppConstants.totalMessages);
                        binding.textDialogUnreadCount.setVisibility(View.VISIBLE);
                    } else {
                        Log.e(TAG, "else");
                        binding.textDialogUnreadCount.setVisibility(View.GONE);
                    }

                    //changeCounter();
                }
            }, false);
        }
        isSell = false;
        super.onResume();

        if (!Preferences.getValue_String(HomeActivity.this, Preferences.USER_PIC).equals("")) {
            Picasso.with(HomeActivity.this).load(Preferences.getValue_String(HomeActivity.this, Preferences.USER_PIC)).placeholder(R.drawable.profile).into(binding.draweritem.profilePic);
        }

        if (isNeedRefreshCounter) {
            //changeCounter();
            isNeedRefreshCounter = false;
        }

        binding.draweritem.tvUsername.setText(Preferences.getValue_String(HomeActivity.this, Preferences.USER_NAME));
        binding.draweritem.tvEmail.setText(Preferences.getValue_String(HomeActivity.this, Preferences.USER_EMAIL));

    }

    private void loadData(int currentItem) {
        switch (currentItem) {
            case 0:
                if (Utils.notifierProductCategory != null) {
                    if (productFlag) {
                        Utils.notifierProductCategory.loadFragData(true);
                        productFlag = false;
                    } else {
                        Log.e(TAG, "Already loaded pending");
                    }
                }
                break;
            case 1:
                if (Utils.notifierServiceCategory != null) {
                    if (serviceFlag) {
                        Utils.notifierServiceCategory.loadFragData(true);
                        serviceFlag = false;
                    } else {
                        Log.e(TAG, "Already loaded pending");
                    }
                }
                break;
        }
    }


}
