package com.sc.student2student.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.SeekBar;

import com.android.volley.VolleyError;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.sc.student2student.R;
import com.sc.student2student.adapter.PlaceAutocompleteAdapter;
import com.sc.student2student.adapter.Product_Adapter;
import com.sc.student2student.api.GetCityApi;
import com.sc.student2student.api.GetProduct;
import com.sc.student2student.common.AppConstants;
import com.sc.student2student.common.GridSpacingItemDecoration;
import com.sc.student2student.common.Preferences;
import com.sc.student2student.databinding.ActivityProductListBinding;
import com.sc.student2student.databinding.FilterLayoutBinding;
import com.sc.student2student.moduls.City;
import com.sc.student2student.moduls.Product;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static com.sc.student2student.common.AppConstants.cityArrayList;

public class ProductListActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {
    private static final int PLACE_PICKER_REQUEST = 3;
    private static final LatLngBounds BOUNDS_GREATER_SYDNEY = new LatLngBounds(
            new LatLng(-34.041458, 150.790100), new LatLng(-33.682247, 151.383362));
    ActivityProductListBinding binding;
    String TAG = getClass().getSimpleName();
    boolean flagIsNeedOpen = false;
    View rootView;
    boolean flagLoading = true;
    int pastVisibleItems, visibleItemCount, totalItemCount;
    int pageCount = 0;
    int limit = 20;
    Product_Adapter adapter;
    LinearLayoutManager layoutManager;
    GridLayoutManager gridLayoutManager;
    String sort_by = "";
    private GoogleApiClient mGoogleApiClient;
    private PlaceAutocompleteAdapter mAdapter;
    private ArrayAdapter<City> cityArrayAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_product_list);
        //binding.swipeRefresh.setRefreshing(false);
        Log.e(TAG, "ProductListActivity");
        AppConstants.resetfiler(ProductListActivity.this);
        initView();
        loadFragData(true);
        buildGoogleApiClient();
        binding.imgFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filterDialog(ProductListActivity.this);
            }
        });
        binding.imgListtype.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e(TAG, "Change Ui To List");
                Preferences.setValue_Boolean(ProductListActivity.this, Preferences.LIST_TYPE, false);
                setView();
                changeListUi();
            }
        });
        binding.imgGridtype.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e(TAG, "Change Ui To Grid");
                Preferences.setValue_Boolean(ProductListActivity.this, Preferences.LIST_TYPE, true);
                setView();
                changeListUi();
            }
        });

        binding.edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String query = s.toString().toLowerCase();

                final List<Product> filteredList = new ArrayList<>();
                for (int i = 0; i < AppConstants.productList.size(); i++) {
                    final String text = AppConstants.productList.get(i).getProduct_name().toLowerCase();
                    final String about = AppConstants.productList.get(i).getProduct_desc().toLowerCase();
                    if (text.contains(query) || about.contains(query)) {
                        filteredList.add(AppConstants.productList.get(i));
                    }
                }

                if (Preferences.getValue_Boolean(ProductListActivity.this, Preferences.LIST_TYPE, true)) {
                    binding.recyclerView.setLayoutManager(gridLayoutManager);
                    adapter = new Product_Adapter(ProductListActivity.this, filteredList, R.layout.item_prodcut);
                    binding.recyclerView.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                } else {
                    binding.recyclerView.setLayoutManager(layoutManager);
                    adapter = new Product_Adapter(ProductListActivity.this, filteredList, R.layout.item_product_list);
                    binding.recyclerView.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

/*
        binding.view.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }
            @Override
            public void onPageSelected(int position) {
                fragment = adapter.getRegisteredFragment(position);
            }
            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
*/
      /*  binding.imgListtype.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Preferences.setValue_Boolean(MainActivity.this, Preferences.LIST_TYPE, false);
                setView();
                if (AppConstants.product_refresh != null) {
                    AppConstants.product_refresh.refresh_view();
                }
                if (AppConstants.service_refresh != null) {
                    AppConstants.service_refresh.refresh_view();
                }
                if (fragment != null) {
                    if (fragment instanceof Product_fragement) {
                        ((Product_fragement) fragment).ChangeListType();
                    } else if (fragment instanceof Service_fragement) {
                        ((Service_fragement) fragment).ChangeListType();
                    }
                } else {
                    Log.e("TAG", "Fragement null");
                }
            }
        });*/
       /* binding.imgGridtype.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Preferences.setValue_Boolean(MainActivity.this, Preferences.LIST_TYPE, true);
                setView();
                if (AppConstants.product_refresh != null) {
                    AppConstants.product_refresh.refresh_view();
                }
                if (AppConstants.service_refresh != null) {
                    AppConstants.service_refresh.refresh_view();
                }
                if (fragment != null) {
                    if (fragment instanceof Product_fragement) {
                        ((Product_fragement) fragment).ChangeListType();
                    } else if (fragment instanceof Service_fragement) {
                        ((Service_fragement) fragment).ChangeListType();
                    }

                } else {
                    Log.e("TAG", "Fragement null");
                }
            }
        });*/

        binding.swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadFragData(false);
                binding.edtSearch.setText("");
            }
        });

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != Activity.RESULT_OK) {
            return;
        }
        switch (requestCode) {
            case PLACE_PICKER_REQUEST:
                Place place = PlacePicker.getPlace(this, data);
                if (place != null) {
                    LatLng latLng = place.getLatLng();
                    Log.e("TAG", "Latitude : " + latLng.latitude);
                    Log.e("TAG", "Longitude : " + latLng.longitude);
                    Log.e("TAG", "Address : " + place.getAddress());
                    Preferences.setValue(ProductListActivity.this, Preferences.LOCATION, "" + place.getAddress());
                    filterDialog(ProductListActivity.this);
                }
                break;
        }

        /*   if (flagIsNeedOpen) {
            filterDialog(MainActivity.this);
            flagIsNeedOpen = false;
        }*/


    }


    private void changeListUi() {
        if (AppConstants.productList.size() > 0) {
            if (Preferences.getValue_Boolean(ProductListActivity.this, Preferences.LIST_TYPE, true)) {
                binding.recyclerView.setLayoutManager(gridLayoutManager);
                adapter = new Product_Adapter(ProductListActivity.this, AppConstants.productList, R.layout.item_prodcut);
                binding.recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            } else {
                binding.recyclerView.setLayoutManager(layoutManager);
                adapter = new Product_Adapter(ProductListActivity.this, AppConstants.productList, R.layout.item_product_list);
                binding.recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }
        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    public void filterDialog(final Context activity) {
        final FilterLayoutBinding dialogBinding;
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        LayoutInflater inflater = ((Activity) activity).getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.filter_layout, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog b = dialogBuilder.create();
        b.show();
        dialogBinding = FilterLayoutBinding.bind(dialogView);
        getCity(dialogBinding);
//        dialogBinding.edtLocation.setText(Preferences.getValue_String(activity, Preferences.LOCATION));
        dialogBinding.edtMin.setText(Preferences.getValue_String(activity, Preferences.MIN_PRICE));
        dialogBinding.edtMax.setText(Preferences.getValue_String(activity, Preferences.MAX_PRICE));
        if (Preferences.getValue_String(activity, Preferences.SORT_BY).equals("closest")) {
            dialogBinding.rbClose.setChecked(true);
        } else if (Preferences.getValue_String(activity, Preferences.SORT_BY).equals("newest")) {
            dialogBinding.rbNew.setChecked(true);
        } else {
            dialogBinding.rbClose.setChecked(false);
            dialogBinding.rbNew.setChecked(false);
        }
        //dialogBinding.sb.incrementProgressBy(5);
        if (!Preferences.getValue_String(activity, Preferences.MILES).equals("")) {
            dialogBinding.sb.setProgress(Integer.parseInt(Preferences.getValue_String(activity, Preferences.MILES)));
            dialogBinding.lblMiles.setText("" + Integer.parseInt(Preferences.getValue_String(activity, Preferences.MILES)) + " km");
        } else {
            dialogBinding.sb.setProgress(0);
            dialogBinding.lblMiles.setText("0 km");
        }
        mAdapter = new PlaceAutocompleteAdapter(this, mGoogleApiClient, BOUNDS_GREATER_SYDNEY, null);
/*
        dialogBinding.edtLocation.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                hideSoftKeyboard();
                dialogBinding.edtLocation.clearFocus();
                String address = dialogBinding.edtLocation.getText().toString();
                Log.e("TAG", "Address: " + address);
                String[] parts = address.split(",");
                Log.e("TAG", "Country: " + parts[parts.length - 1]);
                //getLocationFromAddress(address);
            }
        });
*/

        dialogBinding.spnCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int i, long id) {
                Log.e(TAG, "spnCity.Click(){...}");
                //getCity();
                AppConstants.city = (City) dialogBinding.spnCity.getSelectedItem();
                Preferences.setValue(ProductListActivity.this, Preferences.selectCityPosition, "" + i);
                if (i != 0) {
                    Preferences.setValue(ProductListActivity.this, Preferences.CITY, AppConstants.city.getCity_name());
                }
                Log.e(TAG, "Click... City : " + Preferences.getValue_String(ProductListActivity.this, Preferences.CITY));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        dialogBinding.lblLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flagIsNeedOpen = true;
                try {
                    PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                    startActivityForResult(builder.build(ProductListActivity.this), PLACE_PICKER_REQUEST);
                } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }
                b.dismiss();
            }
        });
//        dialogBinding.edtLocation.setAdapter(mAdapter);
        dialogBinding.sb.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                // Display the current progress of SeekBar
                dialogBinding.lblMiles.setText("" + i + " km");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
        dialogBinding.rbClose.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    sort_by = dialogBinding.rbClose.getText().toString();
                }
            }
        });
        dialogBinding.rbNew.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    sort_by = dialogBinding.rbNew.getText().toString();
                }
            }
        });
        dialogBinding.btnApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e(TAG, "btnApply.Click(){...}");
//                Preferences.setValue(activity, Preferences.LOCATION, dialogBinding.edtLocation.getText().toString());
                Preferences.setValue(activity, Preferences.MILES, "" + dialogBinding.sb.getProgress());
                Preferences.setValue(activity, Preferences.MIN_PRICE, dialogBinding.edtMin.getText().toString());
                Preferences.setValue(activity, Preferences.MAX_PRICE, dialogBinding.edtMax.getText().toString());
                Log.e(TAG, "city : " + AppConstants.city.getCity_name());
                Preferences.setValue(activity, Preferences.CITY, AppConstants.city.getCity_name());
                Preferences.setValue(activity, Preferences.LATITUDE, AppConstants.city.getLatitude());
                Preferences.setValue(activity, Preferences.LONGITUDE, AppConstants.city.getLongitude());

//                String strAdd = dialogBinding.edtLocation.getText().toString();
//                if (!strAdd.equals("")) {
//                    String[] address = strAdd.split(",");
//                    Log.e("Address length-->", "" + address.length);
//                    int add_length = address.length;
//                    if (add_length == 5) {
//                        Preferences.setValue(activity, Preferences.COUNTRY, address[4].trim());
//                        Preferences.setValue(activity, Preferences.STATE, address[3].trim());
//                        Preferences.setValue(activity, Preferences.CITY, address[2].trim());
//                        Preferences.setValue(activity, Preferences.ADDRESS, address[0].trim() + "," + address[1].trim());
//
//
//                    } else if (add_length == 4) {
//                        Preferences.setValue(activity, Preferences.COUNTRY, address[3].trim());
//                        Preferences.setValue(activity, Preferences.STATE, address[2].trim());
//                        Preferences.setValue(activity, Preferences.CITY, address[1].trim());
//                        Preferences.setValue(activity, Preferences.ADDRESS, address[0].trim());
//
//                    } else if (add_length == 3) {
//                        Preferences.setValue(activity, Preferences.COUNTRY, address[2].trim());
//                        Preferences.setValue(activity, Preferences.STATE, address[1].trim());
//                        Preferences.setValue(activity, Preferences.CITY, address[0].trim());
//                        Preferences.setValue(activity, Preferences.ADDRESS, "");
//                    } else if (add_length == 2) {
//                        Preferences.setValue(activity, Preferences.COUNTRY, address[1].trim());
//                        Preferences.setValue(activity, Preferences.STATE, address[0].trim());
//                        Preferences.setValue(activity, Preferences.CITY, "");
//                        Preferences.setValue(activity, Preferences.ADDRESS, "");
//                    } else {
//                        Preferences.setValue(activity, Preferences.COUNTRY, address[0].trim());
//                        Preferences.setValue(activity, Preferences.STATE, "");
//                        Preferences.setValue(activity, Preferences.CITY, "");
//                        Preferences.setValue(activity, Preferences.ADDRESS, "");
//                    }
//                } else {
//                    Preferences.setValue(activity, Preferences.COUNTRY, "");
//                    Preferences.setValue(activity, Preferences.STATE, "");
//                    Preferences.setValue(activity, Preferences.CITY, "");
//                    Preferences.setValue(activity, Preferences.ADDRESS, "");
//                }
                if (!sort_by.equals("")) {
                    Preferences.setValue(activity, Preferences.SORT_BY, sort_by.toLowerCase());
                } else {
                    Preferences.setValue(activity, Preferences.SORT_BY, sort_by);
                }
               /* if (AppConstants.product_refresh != null) {
                    AppConstants.product_refresh.load();
                }
                if (AppConstants.service_refresh != null) {
                    AppConstants.service_refresh.load();
                }*/
                loadFragData(true);
                b.dismiss();
            }
        });
        dialogBinding.btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Preferences.setValue(activity, Preferences.selectCityPosition, "");
                Preferences.setValue(activity, Preferences.CITY, "");
                AppConstants.resetfiler(activity);
                /*if (AppConstants.product_refresh != null) {
                    AppConstants.product_refresh.load();
                }
                if (AppConstants.service_refresh != null) {
                    AppConstants.service_refresh.load();
                }*/
                loadFragData(true);
                b.dismiss();
            }
        });
    }

    public void hideSoftKeyboard() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e(TAG, "Resume(){..}Product" + AppConstants.isChangeListing);
        if (AppConstants.isChangeListing) {
            //binding.swipeRefresh.setRefreshing(false);
            Log.e(TAG, "Resume(){..}IF");
            AppConstants.isChangeListing = false;
            loadFragData(true);
        } else {
            //binding.swipeRefresh.setRefreshing(false);
            Log.e(TAG, "Resume(){..}Else");
        }
        setView();
    }

    private void setView() {
        if (Preferences.getValue_Boolean(ProductListActivity.this, Preferences.LIST_TYPE, true)) {
            binding.imgListtype.setVisibility(View.VISIBLE);
            binding.imgGridtype.setVisibility(View.GONE);
        } else {
            binding.imgListtype.setVisibility(View.GONE);
            binding.imgGridtype.setVisibility(View.VISIBLE);
        }
    }

    private void initView() {
        layoutManager = new LinearLayoutManager(ProductListActivity.this);
        gridLayoutManager = new GridLayoutManager(ProductListActivity.this, 2);
        int spacing = 10; // 50px
        boolean includeEdge = true;
        binding.recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, spacing, includeEdge));
        //binding.relLoadMore.setVisibility(View.GONE);
        binding.imgDrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        binding.tvCatTitle.setText(AppConstants.cat.getCategory_name());
    }

    public void getCity(final FilterLayoutBinding dialogBinding) {
        if (AppConstants.isNetworkAvailable(getString(R.string.network_title), getString(R.string.network_msg), ProductListActivity.this)) {
            new GetCityApi(ProductListActivity.this, new GetCityApi.OnResultReceived() {
                @Override
                public void OnResult(String result) {

                    cityArrayAdapter = new ArrayAdapter<City>(ProductListActivity.this, R.layout.item_spinner_text_layout, cityArrayList) {
                        @Override
                        public boolean isEnabled(int position) {
                            if (position == 0) {
                                return false;
                            } else {
                                return true;
                            }
                        }
                    };
                    Log.e(TAG, "GetCity CITY : " + Preferences.getValue_String(ProductListActivity.this, Preferences.CITY));
                    cityArrayAdapter.setDropDownViewResource(R.layout.spinner_item2);
                    dialogBinding.spnCity.setAdapter(cityArrayAdapter);
/*
                    dialogBinding.spnCity.setSelection(
                            Integer.parseInt(Preferences.getValue_String(ProductListActivity.this, Preferences.selectCityPosition).equals("") ?
                                    "0" : Preferences.getValue_String(ProductListActivity.this, Preferences.selectCityPosition)
                            ));
*/
                    for (int i = 0; i < AppConstants.cityArrayList.size(); i++) {
                        if (AppConstants.cityArrayList.get(i).getCity_name().equals(Preferences.getValue_String(ProductListActivity.this, Preferences.CITY))) {
                            dialogBinding.spnCity.setSelection(i);
                        }
                    }
                    // dialogBinding.spnCity.setSelection(Preferences.getValue_String(ProductListActivity.this, Preferences.CITY).equals(AppConstants.city.getCity_name()));
                    cityArrayAdapter.notifyDataSetChanged();
                }
            }, true);
        }
    }

    public void loadFragData(boolean flag) {
        binding.swipeRefresh.setRefreshing(false);
        Log.e(TAG, "loadFragData(){..}Product");
        String param = "&user_id=" + Preferences.getValue_String(ProductListActivity.this, Preferences.USER_ID);
        param = param + "&location=";
        param = param + "&mile=" + Preferences.getValue_String(ProductListActivity.this, Preferences.MILES);
        param = param + "&min_price=" + Preferences.getValue_String(ProductListActivity.this, Preferences.MIN_PRICE);
        param = param + "&max_price=" + Preferences.getValue_String(ProductListActivity.this, Preferences.MAX_PRICE);
        param = param + "&sort=" + Preferences.getValue_String(ProductListActivity.this, Preferences.SORT_BY);
        try {
            param = param + "&latitude=" + URLEncoder.encode(Preferences.getValue_String(ProductListActivity.this, Preferences.LATITUDE), AppConstants.encodeType);
            param = param + "&longitude=" + URLEncoder.encode(Preferences.getValue_String(ProductListActivity.this, Preferences.LONGITUDE), AppConstants.encodeType);
            param = param + "&city=" + URLEncoder.encode(Preferences.getValue_String(ProductListActivity.this, Preferences.CITY), AppConstants.encodeType);
            param = param + "&address=" + URLEncoder.encode(Preferences.getValue_String(ProductListActivity.this, Preferences.ADDRESS), AppConstants.encodeType);
            param = param + "&state=" + URLEncoder.encode(Preferences.getValue_String(ProductListActivity.this, Preferences.STATE), AppConstants.encodeType);
            param = param + "&country=" + URLEncoder.encode(Preferences.getValue_String(ProductListActivity.this, Preferences.COUNTRY), AppConstants.encodeType);
            param = param + "&category=" + AppConstants.cat.getCat_id();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        param = param + "&date=" + new SimpleDateFormat("ddMMyyHHmmss").format(Calendar.getInstance().getTime());
        new GetProduct(param, ProductListActivity.this, new GetProduct.OnResultReceived() {
            @Override
            public void onResult(String result) throws UnsupportedEncodingException {
                binding.swipeRefresh.setRefreshing(false);
                if (AppConstants.productList.size() > 0) {
                    //binding.swipeRefresh.setRefreshing(false);
                    binding.relSearch.setVisibility(View.VISIBLE);
                    binding.tvNoRecord.setVisibility(View.GONE);
                    if (Preferences.getValue_Boolean(ProductListActivity.this, Preferences.LIST_TYPE, true)) {
                        binding.recyclerView.setLayoutManager(gridLayoutManager);
                        adapter = new Product_Adapter(ProductListActivity.this, AppConstants.productList, R.layout.item_prodcut);
                        binding.recyclerView.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                    } else {
                        binding.recyclerView.setLayoutManager(layoutManager);
                        adapter = new Product_Adapter(ProductListActivity.this, AppConstants.productList, R.layout.item_product_list);
                        binding.recyclerView.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                    }
                } else {
                    if (AppConstants.TYPE.equals(AppConstants.PRODUCT)) {
                        binding.tvNoRecord.setText("No products found");
                    } else {
                        binding.tvNoRecord.setText("No services found");
                    }
                    binding.tvNoRecord.setVisibility(View.VISIBLE);
                    binding.relSearch.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailed(VolleyError result) throws UnsupportedEncodingException {
                binding.swipeRefresh.setRefreshing(false);
            }
        }, flag);

        /*if (AppConstants.isNetworkAvailable(ProductListActivity.this)) {
            pageCount = 0;
            AppConstants.quotesPendingArrayList.clear();
            if (Preferences.getValue_String(ProductListActivity.this, Preferences.ROLE).equals(AppConstants.CLIENT)) {
                String envelope = "&user_id=" + Preferences.getValue_String(ProductListActivity.this, Preferences.USER_ID);
                envelope = envelope + "&start=" + pageCount;
                new ClientPendingQuotesApi(envelope, ProductListActivity.this, new ClientPendingQuotesApi.OnResultReceived() {
                    @Override
                    public void onResult(String result) throws UnsupportedEncodingException {
                        flagLoading = AppConstants.resetDataFlag(result, limit);
                        binding.swipeRefresh.setRefreshing(false);
                        binding.relNoRecord.setVisibility(View.GONE);
                        adapter = new PendingQuotesAdapter(ProductListActivity.this, AppConstants.quotesPendingArrayList);
                        binding.recyclerView.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                    }
                }, flag);
            } else {
                String envelope = "&user_id=" + Preferences.getValue_String(ProductListActivity.this, Preferences.USER_ID);
                envelope = envelope + "&start=" + pageCount;
                new ManagerNewQuotesApi(envelope, ProductListActivity.this, new ManagerNewQuotesApi.OnResultReceived() {
                    @Override
                    public void onResult(String result) throws UnsupportedEncodingException {
                        flagLoading = AppConstants.resetDataFlag(result, limit);
                        binding.swipeRefresh.setRefreshing(false);
                        binding.relNoRecord.setVisibility(View.GONE);
                        adapter = new PendingQuotesAdapter(ProductListActivity.this, AppConstants.quotesPendingArrayList);
                        binding.recyclerView.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                    }
                }, false);
            }
        }*/
    }
}
