package com.sc.student2student.adapter;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sc.student2student.Activity.AlertListActivity;
import com.sc.student2student.Activity.CreateAlertActivity;
import com.sc.student2student.R;
import com.sc.student2student.api.ActiveAlertApi;
import com.sc.student2student.api.DeleteAlertApi;
import com.sc.student2student.common.AppConstants;
import com.sc.student2student.databinding.ItemAlertlistBinding;
import com.sc.student2student.moduls.Alert;
import com.squareup.picasso.Picasso;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Saturncube-5 on 08-Nov-17.
 */

public class AlertListAdapter extends RecyclerView.Adapter<AlertListAdapter.ViewHolder> {

    private Context mContext;
    private String TAG = getClass().getSimpleName();
    private ArrayList arrayList;
    List<Alert> infos = null;

    public AlertListAdapter(Context mContext, ArrayList<Alert> arrayList) {
        this.mContext = mContext;
        this.arrayList = arrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ItemAlertlistBinding binding = DataBindingUtil.inflate(inflater, R.layout.item_alertlist, parent, false);
        return new ViewHolder(binding.getRoot(), binding);

    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        AppConstants.alertArrayListposition = 0;

        final Alert alert = (Alert) arrayList.get(position);
       /* if (alert.getSub_category_name().equals("")) {
            holder.binding.tvCategory.setText(alert.getMain_category_name());
        } else {
            holder.binding.tvCategory.setText(alert.getSub_category_name() + "(" + alert.getMain_category_name() + ")");
        }*/
        //holder.binding.tvDistance.setText(alert.getDistance());

        if (alert.getActive_flag().equals("1")) {
            holder.binding.switchStatus.setChecked(true);
        } else {
            holder.binding.switchStatus.setChecked(false);
        }
        if (!alert.getProfile_pic().equals("")) {
            Picasso.with(mContext).load(alert.getProfile_pic()).placeholder(R.drawable.profile).error(R.drawable.profile).transform(AppConstants.getTransformation(holder.binding.imgProfile)).into(holder.binding.imgProfile);
        } else {
            holder.binding.imgProfile.setImageResource(R.drawable.profile);
        }

        holder.binding.tvAlertName.setText(alert.getAlert_name());
        holder.binding.switchStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.binding.switchStatus.isChecked()) {
                    String envelope = "&alert_id=" + alert.getAlert_id();
                    envelope = envelope + "&alert_status=" + 1;

                    new ActiveAlertApi(envelope, mContext, new ActiveAlertApi.OnResultReceived() {
                        @Override
                        public void onResult(String result) throws UnsupportedEncodingException {

                        }
                    }, true);

                } else {
                    String envelope = "&alert_id=" + alert.getAlert_id();
                    envelope = envelope + "&alert_status=" + 0;

                    new ActiveAlertApi(envelope, mContext, new ActiveAlertApi.OnResultReceived() {
                        @Override
                        public void onResult(String result) throws UnsupportedEncodingException {

                        }
                    }, true);

                }
            }
        });
//        holder.binding.switchStatus.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
//            }
//        });
        holder.binding.relMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppConstants.alertArrayListposition = position;
                Log.e(TAG, "POSITION : " + position);
                Log.e(TAG, " AppConstants.alertArrayListposition" + AppConstants.alertArrayListposition);
                AppConstants.editAlert = true;
                mContext.startActivity(new Intent(mContext, CreateAlertActivity.class));
            }
        });
        holder.binding.relDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String alertid = "&alert_id=" + alert.getAlert_id();
                new DeleteAlertApi(alertid, mContext, new DeleteAlertApi.OnResultReceived() {
                    @Override
                    public void onResult(String result) throws UnsupportedEncodingException {
                        Log.e(TAG, "OnDelete(){...}");
                        ((AlertListActivity) mContext).getAlert();
                    }
                }, true);
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ItemAlertlistBinding binding;

        public ViewHolder(View itemView, ItemAlertlistBinding binding) {
            super(itemView);
            this.binding = binding;
        }
    }
}
