package com.sc.student2student.Activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;

import com.sc.student2student.R;
import com.sc.student2student.adapter.AlertListAdapter;
import com.sc.student2student.api.GetAlertApi;
import com.sc.student2student.common.AppConstants;
import com.sc.student2student.common.Preferences;
import com.sc.student2student.databinding.ActivityAlertListBinding;

import java.io.UnsupportedEncodingException;

public class AlertListActivity extends AppCompatActivity {

    private String TAG = getClass().getSimpleName();
    private ActivityAlertListBinding binding;
    private AlertListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_alert_list);
        initView();
        Log.e(TAG,"OnCreate(){...}");
        getAlert();

        binding.toolbar.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        binding.addAlert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppConstants.editAlert = false;
                startActivity(new Intent(AlertListActivity.this, CreateAlertActivity.class));

            }
        });

        binding.SwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Log.e(TAG,"setOnRefreshListener(){...}");
                getAlert();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e(TAG,"onResume(){...}");
        getAlert();
    }

    private void initView() {
        binding.toolbar.tvTitle.setText("AlertList");
        binding.recyclerview.setLayoutManager(new LinearLayoutManager(this));
    }

    public void getAlert() {
        Log.e(TAG, "Array Size : " + AppConstants.alertArrayList.size());


        String envelope = "&user_id=" + Preferences.getValue_String(AlertListActivity.this, Preferences.USER_ID);

        new GetAlertApi(envelope, AlertListActivity.this, new GetAlertApi.OnResultReceived() {
            @Override
            public void onResult(String result) throws UnsupportedEncodingException {

                if (AppConstants.alertArrayList.size() > 0) {
                    binding.tvNoDataFound.setVisibility(View.GONE);
                    adapter = new AlertListAdapter(AlertListActivity.this, AppConstants.alertArrayList);
                    binding.recyclerview.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                    binding.SwipeRefreshLayout.setRefreshing(false);
                } else {
                    binding.tvNoDataFound.setVisibility(View.VISIBLE);
                }


            }
        }, true);
    }
}
