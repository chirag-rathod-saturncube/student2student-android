package com.sc.student2student.moduls;


import com.sc.student2student.common.ExpandableRecyclerAdapter;

/**
 * Created by Saturncube10 on 08-Nov-16.
 */

public class Faq extends ExpandableRecyclerAdapter.ListItem {
    public String Text;
    public int Icon;
    public FaqItem times;


    public Faq(String group, int icon) {
        super(1000);

        Text = group;
        Icon = icon;
    }

    public Faq(FaqItem times) {
        super(1001);

        this.times = times;
    }
}