package com.sc.student2student.api;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.sc.student2student.R;
import com.sc.student2student.common.AppConstants;
import com.sc.student2student.common.DialogManager;
import com.sc.student2student.common.LoadingDialog;
import com.sc.student2student.common.WebServices;
import com.sc.student2student.moduls.Cat;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by pc4 on 7/6/2016.
 */
public class GetAlertSubCategoryApi {
    String TAG = getClass().getSimpleName();
    Context mContext;
    String envelope = "";
    LoadingDialog loadingDialog;
    RequestQueue queue;
    boolean flagProgress;
    private String url = WebServices.GET_CATEGORIES;
    private OnResultReceived onResultReceived;

    public GetAlertSubCategoryApi(String envelope, Context mContext, OnResultReceived onResultReceived, boolean flagProgress) {
        this.envelope = envelope;
        this.mContext = mContext;
        this.onResultReceived = onResultReceived;
        this.flagProgress = flagProgress;
        queue = Volley.newRequestQueue(mContext);
        getResponse();
    }

    public void show() {
        if (flagProgress) {
            loadingDialog = new LoadingDialog(mContext, "", false);
        }
    }

    public void hide() {
        if (flagProgress) {
            loadingDialog.hide();
        }
    }

    public void getResponse() {
        show();
        Log.e(TAG, "Url: " + url + envelope);
        JsonObjectRequest postRequest = new JsonObjectRequest(com.android.volley.Request.Method.GET, url + envelope, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.e(TAG, "Response: " + response);
                            if (response.has("error")) {
                                hide();
                                DialogManager.errorDialog(mContext, "Error!", response.optString("error"));
                            } else if (response.has("success")) {
                                JSONArray object = response.getJSONArray("result");
                                AppConstants.alertSubCatArrayList.clear();
                                Cat status = new Cat();
                                status.setCategory_name("Please select subcategory");
                                AppConstants.alertSubCatArrayList.add(status);
                                if (object.length() > 0) {
                                    for (int i = 0; i < object.length(); i++) {
                                        Cat cat = new Cat();
                                        JSONObject obj = object.getJSONObject(i);
                                        cat.setCat_id(obj.optString("cat_id"));
                                        cat.setCategory_name(obj.optString("category_name"));
                                        cat.setCat_img(obj.optString("cat_img"));
                                        cat.setCat_type(obj.optString("cat_type"));
                                        cat.setParent_id(obj.optString("parent_id"));
                                        cat.setActive_flag(obj.optString("active_flag"));
                                        cat.setTotal_product(obj.optString("total_product"));
                                        cat.setHave_subcategory(obj.optString("have_subcategory"));
                                        AppConstants.alertSubCatArrayList.add(cat);
                                    }
                                }
                                hide();
                                if (onResultReceived != null) try {
                                    onResultReceived.onResult(response.toString());
                                } catch (UnsupportedEncodingException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                hide();
                                Toast.makeText(mContext, mContext.getString(R.string.something_was_wrong_with_api), Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception e) {
                            hide();
                            Log.e(TAG, "Line " + Thread.currentThread().getStackTrace()[2].getLineNumber() + " Error:... " + e.getMessage());
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hide();
                        VolleyLog.d("Error.Response", "");
                        AppConstants.VolleyException(mContext, error);
                        if (onResultReceived != null) try {
                            onResultReceived.onFailed(error);
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                }
        ) {
            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Cache-Control", "no-cache");
                return params;
            }
        };
        AppConstants.SetTimeOut(postRequest);
        queue.add(postRequest);
    }


    public interface OnResultReceived {
        public void onResult(String result) throws UnsupportedEncodingException;

        public void onFailed(VolleyError result) throws UnsupportedEncodingException;
    }
}
