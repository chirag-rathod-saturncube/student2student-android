package com.sc.student2student.Activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;

import com.android.volley.VolleyError;
import com.sc.student2student.R;
import com.sc.student2student.adapter.CatAdapter;
import com.sc.student2student.api.GetSubCatApi;
import com.sc.student2student.common.AppConstants;
import com.sc.student2student.common.GridSpacingItemDecoration;
import com.sc.student2student.databinding.ActivitySubCategoriesBinding;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class SubCategoriesActivity extends AppCompatActivity {
    public static SubCategoriesActivity activity;
    ActivitySubCategoriesBinding binding;
    String TAG = getClass().getSimpleName();
    View rootView;
    boolean flagLoading = true;
    int pastVisibleItems, visibleItemCount, totalItemCount;
    int pageCount = 0;
    int limit = 20;
    CatAdapter adapter;
    LinearLayoutManager layoutManager;
    GridLayoutManager gridLayoutManager;

    public static SubCategoriesActivity getInstance() {
        return activity;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_sub_categories);
        initView();
        activity = this;
        loadFragData(true);

        binding.swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadFragData(false);
            }
        });

    }

    private void initView() {
        layoutManager = new LinearLayoutManager(SubCategoriesActivity.this);
        gridLayoutManager = new GridLayoutManager(SubCategoriesActivity.this, 2);
        int spacing = 10; // 50px
        boolean includeEdge = true;
        binding.recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, spacing, includeEdge));
        //binding.relLoadMore.setVisibility(View.GONE);
        binding.toolbar.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        binding.toolbar.tvTitle.setText(AppConstants.cat.getCategory_name());
    }

    public void loadFragData(boolean flag) {
        Log.e(TAG, "Product fragment....");
        String envelope = "&type=" + AppConstants.TYPE;
        envelope = envelope + "&parent_id=" + AppConstants.cat.getCat_id();
        envelope = envelope + "&date=" + new SimpleDateFormat("ddMMyyHHmmss").format(Calendar.getInstance().getTime());
        new GetSubCatApi(envelope, SubCategoriesActivity.this, new GetSubCatApi.OnResultReceived() {
            @Override
            public void onResult(String result) throws UnsupportedEncodingException {
                binding.swipeRefresh.setRefreshing(false);
                if (AppConstants.subCatArrayList.size() > 0) {
                    binding.relSearch.setVisibility(View.VISIBLE);

                    binding.recyclerView.setLayoutManager(gridLayoutManager);
                    adapter = new CatAdapter(SubCategoriesActivity.this, AppConstants.subCatArrayList, AppConstants.TYPE, "2");
                    binding.recyclerView.setAdapter(adapter);
                    adapter.notifyDataSetChanged();

                   /* if (Preferences.getValue_Boolean(SubCategoriesActivity.this, Preferences.LIST_TYPE, true)) {
                        binding.recyclerView.setLayoutManager(gridLayoutManager);
                        adapter = new CatAdapter(SubCategoriesActivity.this, AppConstants.subCatArrayList, R.layout.item_cat, AppConstants.TYPE);
                        binding.recyclerView.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                    } else {
                        binding.recyclerView.setLayoutManager(layoutManager);
                        adapter = new CatAdapter(SubCategoriesActivity.this, AppConstants.subCatArrayList, R.layout.item_cat, AppConstants.TYPE);
                        binding.recyclerView.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                    }*/
                } else {
                    binding.relSearch.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailed(VolleyError result) throws UnsupportedEncodingException {
                binding.swipeRefresh.setRefreshing(false);
            }
        }, flag);


        /*if (AppConstants.isNetworkAvailable(getActivity())) {
            pageCount = 0;
            AppConstants.quotesPendingArrayList.clear();
            if (Preferences.getValue_String(getActivity(), Preferences.ROLE).equals(AppConstants.CLIENT)) {
                String envelope = "&user_id=" + Preferences.getValue_String(getActivity(), Preferences.USER_ID);
                envelope = envelope + "&start=" + pageCount;
                new ClientPendingQuotesApi(envelope, getActivity(), new ClientPendingQuotesApi.OnResultReceived() {
                    @Override
                    public void onResult(String result) throws UnsupportedEncodingException {
                        flagLoading = AppConstants.resetDataFlag(result, limit);
                        binding.swipeRefresh.setRefreshing(false);
                        binding.relNoRecord.setVisibility(View.GONE);
                        adapter = new PendingQuotesAdapter(getActivity(), AppConstants.quotesPendingArrayList);
                        binding.recyclerView.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                    }
                }, flag);
            } else {
                String envelope = "&user_id=" + Preferences.getValue_String(getActivity(), Preferences.USER_ID);
                envelope = envelope + "&start=" + pageCount;
                new ManagerNewQuotesApi(envelope, getActivity(), new ManagerNewQuotesApi.OnResultReceived() {
                    @Override
                    public void onResult(String result) throws UnsupportedEncodingException {
                        flagLoading = AppConstants.resetDataFlag(result, limit);
                        binding.swipeRefresh.setRefreshing(false);
                        binding.relNoRecord.setVisibility(View.GONE);
                        adapter = new PendingQuotesAdapter(getActivity(), AppConstants.quotesPendingArrayList);
                        binding.recyclerView.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                    }
                }, false);
            }
        }*/
    }

}
