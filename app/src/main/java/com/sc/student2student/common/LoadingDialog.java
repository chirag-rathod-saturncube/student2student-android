package com.sc.student2student.common;

import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources.NotFoundException;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.sc.student2student.R;


public class LoadingDialog extends Dialog {
    Object mAct;
    Context mContext;
    Typeface Roboto_reg;
    private Dialog mpd = null;
    private LayoutInflater lyt_Inflater = null;

    public LoadingDialog(Context context) {
        super(context);

        this.mContext = context;
        LodingDialog2(context, "", false);

    }

    public LoadingDialog(Context context, Object act) {
        super(context);
        this.mAct = act;
        this.mContext = context;
        LodingDialog2(context, "", true);
    }

    public LoadingDialog(Context context, String LoadingText) {
        super(context);
        LodingDialog2(context, LoadingText, true);

    }

    public LoadingDialog(Context context, String LoadingText, boolean cancelable) {
        super(context);
        LodingDialog2(context, LoadingText, cancelable);
    }

    public void LodingDialog2(Context context, String LoadingText,
                              boolean cancelable) {
        lyt_Inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View view_lyt = lyt_Inflater.inflate(R.layout.dialog_loading,
                null);


        ImageView img = (ImageView) view_lyt.findViewById(R.id.img);
        img.setColorFilter(Color.rgb(255, 255, 255));

        Animation rotation = AnimationUtils.loadAnimation(context, R.anim.progress_animation);
        rotation.setRepeatCount(Animation.INFINITE);
        img.startAnimation(rotation);

        try {
            mpd = new Dialog(context, R.style.Theme_Dialog);
            mpd.requestWindowFeature(Window.FEATURE_NO_TITLE);
            mpd.setContentView(view_lyt);
            mpd.setCancelable(cancelable);
            mpd.show();
        } catch (NotFoundException e) {
            e.printStackTrace();
        }

    }

    public void hide() {
        if (mpd != null) {
            if (mpd.isShowing()) {
                mpd.dismiss();
            }
        }
    }

    public void hide1() {
        mpd.dismiss();
    }
}