package com.sc.student2student.Activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.sc.student2student.R;
import com.sc.student2student.adapter.ReviewsAdapter;
import com.sc.student2student.api.ReviewsApi;
import com.sc.student2student.api.getProfileApi;
import com.sc.student2student.common.AppConstants;
import com.sc.student2student.common.Preferences;
import com.sc.student2student.databinding.ActivityMyProfileBinding;
import com.squareup.picasso.Picasso;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class MyProfileActivity extends AppCompatActivity {
    private String TAG = getClass().getSimpleName();
    ActivityMyProfileBinding binding;
    ReviewsAdapter reviewsAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_my_profile);
        initView();

        binding.relToolbar.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        binding.relToolbar.imgOption2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.view_profile = true;
                startActivity(new Intent(MyProfileActivity.this, EditProfileActivity.class));
            }
        });
        binding.relProducts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.ACTION = AppConstants.MYLIST;
                AppConstants.page_display = 0;
                startActivity(new Intent(MyProfileActivity.this, WatchListActivity.class));
            }
        });
        binding.relServices.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.ACTION = AppConstants.MYLIST;
                AppConstants.page_display = 1;
                startActivity(new Intent(MyProfileActivity.this, WatchListActivity.class));
            }
        });
    }

    private void initView() {
        binding.relToolbar.tvTitle.setText("Profile");
        binding.relToolbar.imgOption2.setImageResource(R.drawable.edit);

       /* if (AppConstants.USER_ID.equals(Preferences.getValue_String(MyProfileActivity.this, Preferences.USER_ID))) {
            binding.relToolbar.imgOption2.setVisibility(View.VISIBLE);
            binding.lblAbout.setText("About me");
            binding.relRating.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String envelope = "&user_id=" + Preferences.getValue_String(MyProfileActivity.this, Preferences.USER_ID);
                    new ReviewsApi(envelope, MyProfileActivity.this, new ReviewsApi.OnResultReceived() {
                        @Override
                        public void onResult(String result) throws UnsupportedEncodingException {
                            startActivity(new Intent(MyProfileActivity.this, ReviewsActivity.class));

                           if (AppConstants.reviewsArraylist.size() > 0) {
                            ReviewsDialog(MyProfileActivity.this);
                            }

                        }
                    }, true);
                }
            });

        } else {
            Log.e("TAG", "Other user: " + AppConstants.user.getName());
            binding.relToolbar.imgOption2.setVisibility(View.GONE);
            binding.lblAbout.setText("About" + " " + AppConstants.user.getName());
            binding.relRating.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String envelope = "&user_id=" + AppConstants.user.getUser_id();
                    new ReviewsApi(envelope, MyProfileActivity.this, new ReviewsApi.OnResultReceived() {
                        @Override
                        public void onResult(String result) throws UnsupportedEncodingException {
                            startActivity(new Intent(MyProfileActivity.this, ReviewsActivity.class));

                           if (AppConstants.reviewsArraylist.size() > 0) {
                            ReviewsDialog(MyProfileActivity.this);
                            }

                        }
                    }, true);
                }
            });
        }*/
    }

    /*private void ReviewsDialog(final Context mContext) {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mContext);
        final LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_reviews, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog b = dialogBuilder.create();
        b.show();
        final RecyclerView recyclerview = (RecyclerView) dialogView.findViewById(R.id.recyclerview);
        final TextView tv_No_rating = (TextView) dialogView.findViewById(R.id.tv_No_rating);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(MyProfileActivity.this);
        recyclerview.setLayoutManager(linearLayoutManager);
        if (AppConstants.reviewsArraylist.size() > 0) {
            tv_No_rating.setVisibility(View.GONE);
        } else {
            tv_No_rating.setVisibility(View.VISIBLE);
        }
        reviewsAdapter = new ReviewsAdapter(MyProfileActivity.this, AppConstants.reviewsArraylist);
        recyclerview.setAdapter(reviewsAdapter);
        reviewsAdapter.notifyDataSetChanged();
    }*/

    private void setScreenData() {


        binding.tvMemberdate.setText("Member since " + AppConstants.user.getCrt_date());


        if (!AppConstants.user.getProfile_pic().equals("")) {
            Picasso.with(MyProfileActivity.this).load(AppConstants.user.getProfile_pic()).placeholder(R.drawable.profile).transform(AppConstants.getTransformation(binding.imgPic)).into(binding.imgPic);
        }
        if (!AppConstants.user.getProfile_pic().equals("")) {
            Picasso.with(MyProfileActivity.this).load(AppConstants.user.getProfile_pic()).transform(AppConstants.getTransformation(binding.imgEmpty)).into(binding.imgEmpty);
        }


        if (AppConstants.USER_ID.equals(Preferences.getValue_String(MyProfileActivity.this, Preferences.USER_ID))) {
            binding.lblAbout.setText("About me");
            binding.relToolbar.imgOption2.setVisibility(View.VISIBLE);

            binding.relRating.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String envelope = "&user_id=" + Preferences.getValue_String(MyProfileActivity.this, Preferences.USER_ID);
                    envelope = envelope + "&date=" + new SimpleDateFormat("ddMMyyHHmmss").format(Calendar.getInstance().getTime());
                    new ReviewsApi(envelope, MyProfileActivity.this, new ReviewsApi.OnResultReceived() {
                        @Override
                        public void onResult(String result) throws UnsupportedEncodingException {
                            startActivity(new Intent(MyProfileActivity.this, ReviewsActivity.class));
                            /*if (AppConstants.reviewsArraylist.size() > 0) {
                                ReviewsDialog(MyProfileActivity.this);
                            }*/
                        }
                    }, true);
                }
            });


        } else {
            binding.lblAbout.setText("About " + AppConstants.user.getName());
            binding.relToolbar.imgOption2.setVisibility(View.GONE);

            binding.relRating.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String envelope = "&user_id=" + AppConstants.user.getUser_id();
                    envelope = envelope + "&date=" + new SimpleDateFormat("ddMMyyHHmmss").format(Calendar.getInstance().getTime());
                    new ReviewsApi(envelope, MyProfileActivity.this, new ReviewsApi.OnResultReceived() {
                        @Override
                        public void onResult(String result) throws UnsupportedEncodingException {
                            startActivity(new Intent(MyProfileActivity.this, ReviewsActivity.class));
                            /*if (AppConstants.reviewsArraylist.size() > 0) {
                                ReviewsDialog(MyProfileActivity.this);
                            }*/
                        }
                    }, true);
                }
            });


        }


        binding.tvAboutproduct.setText(AppConstants.user.getAbout());

        if (Preferences.getValue_String(MyProfileActivity.this, Preferences.USER_ID).equals(AppConstants.user.getUser_id())) {
            //binding.tvLocation.setText(AppConstants.user.getLocation());
            binding.tvLocation.setText(Preferences.getValue_String(MyProfileActivity.this, Preferences.CITY_PROFILE));
        } else {
            //binding.tvLocation.setText(AppConstants.user.getCity());
            binding.tvLocation.setText(Preferences.getValue_String(MyProfileActivity.this, Preferences.CITY_PROFILE));
        }
        //binding.tvLocation.setText(AppConstants.Current_City);
        binding.lblReview.setText("Review " + "(" + AppConstants.user.getRating_counter() + ")");
        binding.tvReview.setText("" + Float.parseFloat(AppConstants.user.getRating()));
        binding.ratingBar.setRating(Float.parseFloat(AppConstants.user.getRating()));
    }

    @Override
    protected void onResume() {
        super.onResume();
        String param = "&user_id=" + AppConstants.USER_ID;
        param = param + "&date=" + new SimpleDateFormat("ddMMyyHHmmss").format(Calendar.getInstance().getTime());
        new getProfileApi(param, MyProfileActivity.this, new getProfileApi.OnResultReceived() {
            @Override
            public void onResult(String result) throws UnsupportedEncodingException {
                setScreenData();
                if (!AppConstants.user.getLatitude().equals("")
                        && !AppConstants.user.getLongitude().equals("")) {
                    Preferences.setValue(MyProfileActivity.this, Preferences.isLocationUpdated, true);
                } else {
                    Preferences.setValue(MyProfileActivity.this, Preferences.isLocationUpdated, false);
                }
            }
        }, true);
    }
}
