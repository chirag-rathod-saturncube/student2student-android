package com.sc.student2student.Activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.android.volley.VolleyError;
import com.sc.student2student.R;
import com.sc.student2student.adapter.CatAdapter;
import com.sc.student2student.api.GetCatApi;
import com.sc.student2student.common.AppConstants;
import com.sc.student2student.common.GridSpacingItemDecoration;
import com.sc.student2student.databinding.ActivitySellProductNewBinding;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class SellPorSerActivity extends AppCompatActivity {
    public static SellPorSerActivity activity;
    ActivitySellProductNewBinding binding;
    String TAG = getClass().getSimpleName();
    View rootView;
    boolean flagLoading = true;
    int pastVisibleItems, visibleItemCount, totalItemCount;
    int pageCount = 0;
    int limit = 20;
    CatAdapter adapter;
    LinearLayoutManager layoutManager;
    GridLayoutManager gridLayoutManager;

    public static SellPorSerActivity getInstance() {
        return activity;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_sell_product_new);
        activity = this;
        initView();
        loadFragData(true);
    }

    private void initView() {
        layoutManager = new LinearLayoutManager(SellPorSerActivity.this);
        gridLayoutManager = new GridLayoutManager(SellPorSerActivity.this, 2);
        int spacing = 10; // 50px
        boolean includeEdge = true;
        binding.recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, spacing, includeEdge));
        //binding.relLoadMore.setVisibility(View.GONE);
        binding.toolbar.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        if (AppConstants.TYPE.equals(AppConstants.PRODUCT)) {
            RelativeLayout.LayoutParams layoutParams =
                    new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT
                            , ViewGroup.LayoutParams.WRAP_CONTENT);
            layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
            binding.toolbar.tvTitle.setLayoutParams(layoutParams);
            binding.toolbar.tvTitle.setText("Select product category");
        } else {
            RelativeLayout.LayoutParams layoutParams =
                    new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT);
            layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
            binding.toolbar.tvTitle.setLayoutParams(layoutParams);
            binding.toolbar.tvTitle.setText("Select service category");
        }
    }

    public void loadFragData(boolean flag) {
        Log.e(TAG, "Product fragment....");
        String envelope = "&type=" + AppConstants.TYPE;
        envelope = envelope + "&parent_id=0";
        envelope = envelope + "&date=" + new SimpleDateFormat("ddMMyyHHmmss").format(Calendar.getInstance().getTime());
        new GetCatApi(envelope, SellPorSerActivity.this, new GetCatApi.OnResultReceived() {
            @Override
            public void onResult(String result) throws UnsupportedEncodingException {
                binding.swipeRefresh.setRefreshing(false);
                if (AppConstants.catArrayList.size() > 0) {
                    //binding.relSearch.setVisibility(View.VISIBLE);

                    binding.recyclerView.setLayoutManager(gridLayoutManager);
                    adapter = new CatAdapter(SellPorSerActivity.this, AppConstants.catArrayList, AppConstants.TYPE, "2");
                    binding.recyclerView.setAdapter(adapter);
                    adapter.notifyDataSetChanged();

                   /* if (Preferences.getValue_Boolean(SellPorSerActivity.this, Preferences.LIST_TYPE, true)) {
                        binding.recyclerView.setLayoutManager(gridLayoutManager);
                        adapter = new CatAdapter(SellPorSerActivity.this, AppConstants.catArrayList, R.layout.item_cat, AppConstants.TYPE);
                        binding.recyclerView.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                    } else {
                        binding.recyclerView.setLayoutManager(layoutManager);
                        adapter = new CatAdapter(SellPorSerActivity.this, AppConstants.catArrayList, R.layout.item_cat, AppConstants.TYPE);
                        binding.recyclerView.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                    }*/
                } else {
                    // binding.relSearch.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailed(VolleyError result) throws UnsupportedEncodingException {
                binding.swipeRefresh.setRefreshing(false);
            }
        }, flag);

        /*if (AppConstants.isNetworkAvailable(getActivity())) {
            pageCount = 0;
            AppConstants.quotesPendingArrayList.clear();
            if (Preferences.getValue_String(getActivity(), Preferences.ROLE).equals(AppConstants.CLIENT)) {
                String envelope = "&user_id=" + Preferences.getValue_String(getActivity(), Preferences.USER_ID);
                envelope = envelope + "&start=" + pageCount;
                new ClientPendingQuotesApi(envelope, getActivity(), new ClientPendingQuotesApi.OnResultReceived() {
                    @Override
                    public void onResult(String result) throws UnsupportedEncodingException {
                        flagLoading = AppConstants.resetDataFlag(result, limit);
                        binding.swipeRefresh.setRefreshing(false);
                        binding.relNoRecord.setVisibility(View.GONE);
                        adapter = new PendingQuotesAdapter(getActivity(), AppConstants.quotesPendingArrayList);
                        binding.recyclerView.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                    }
                }, flag);
            } else {
                String envelope = "&user_id=" + Preferences.getValue_String(getActivity(), Preferences.USER_ID);
                envelope = envelope + "&start=" + pageCount;
                new ManagerNewQuotesApi(envelope, getActivity(), new ManagerNewQuotesApi.OnResultReceived() {
                    @Override
                    public void onResult(String result) throws UnsupportedEncodingException {
                        flagLoading = AppConstants.resetDataFlag(result, limit);
                        binding.swipeRefresh.setRefreshing(false);
                        binding.relNoRecord.setVisibility(View.GONE);
                        adapter = new PendingQuotesAdapter(getActivity(), AppConstants.quotesPendingArrayList);
                        binding.recyclerView.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                    }
                }, false);
            }
        }*/
    }


}
