package com.sc.student2student.fragement;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.VolleyError;
import com.sc.student2student.Interface.Service_Watch_Refresh;
import com.sc.student2student.R;
import com.sc.student2student.adapter.ServiceAdapter;
import com.sc.student2student.api.GetMylistService;
import com.sc.student2student.api.GetWatchlistService;
import com.sc.student2student.common.AppConstants;
import com.sc.student2student.common.GridSpacingItemDecoration;
import com.sc.student2student.common.Preferences;
import com.sc.student2student.databinding.ProductFragementBinding;
import com.sc.student2student.moduls.Product;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


public class Service_watchlist_fragement extends Fragment implements Service_Watch_Refresh {


    String TAG = getClass().getSimpleName();


    View view;
    ProductFragementBinding binding;
    LinearLayoutManager layoutManager;
    GridLayoutManager gridLayoutManager;
    ServiceAdapter adapter;


    public Service_watchlist_fragement() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static Service_watchlist_fragement newInstance() {
        return new Service_watchlist_fragement();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.product_fragement, container, false);

        AppConstants.service_watch_refresh = this;
        return view;
    }

    private void initView() {
        layoutManager = new LinearLayoutManager(getActivity());
        gridLayoutManager = new GridLayoutManager(getActivity(), 2);
        int spacing = 10; // 50px
        boolean includeEdge = true;
        binding.recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, spacing, includeEdge));
    }


    @Override
    public void onDetach() {
        super.onDetach();

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        binding = ProductFragementBinding.bind(view);
        initView();

        binding.edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String query = s.toString().toLowerCase();

                final List<Product> filteredList = new ArrayList<>();

                for (int i = 0; i < AppConstants.watch_serviceList.size(); i++) {

                    final String text = AppConstants.watch_serviceList.get(i).getProduct_name().toLowerCase();
                    final String about = AppConstants.watch_serviceList.get(i).getProduct_desc().toLowerCase();
                    if (text.contains(query) || about.contains(query)) {

                        filteredList.add(AppConstants.watch_serviceList.get(i));
                    }
                }
               /* if (AppConstants.watch_serviceList.size() > 0) {
                    binding.tvNoproductfound.setVisibility(View.GONE);
                } else {
                    binding.tvNoproductfound.setVisibility(View.VISIBLE);
                }*/

                if (Preferences.getValue_Boolean(getActivity(), Preferences.LIST_TYPE, true)) {
                    binding.recyclerView.setLayoutManager(gridLayoutManager);
                    adapter = new ServiceAdapter(getActivity(), filteredList, R.layout.item_prodcut, Service_watchlist_fragement.this);
                    binding.recyclerView.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                } else {
                    binding.recyclerView.setLayoutManager(layoutManager);
                    adapter = new ServiceAdapter(getActivity(), filteredList, R.layout.item_product_list, Service_watchlist_fragement.this);
                    binding.recyclerView.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        binding.swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                LoadData(false);
            }
        });


    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(!AppConstants._hasLoadedOnce_product);
        if (this.isVisible()) {
            // we check that the fragment is becoming visible
            if (isVisibleToUser && !AppConstants._hasLoadedOnce_product) {
                // LoadData(true);
                AppConstants._hasLoadedOnce_product = true;
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        LoadData(true);
    }

    public void LoadData(boolean Isload) {
        String param = "&user_id=" + Preferences.getValue_String(getActivity(), Preferences.USER_ID);
        param = param + "&type=2";
        param = param + "&loggedin_user_id=" + Preferences.getValue_String(getActivity(), Preferences.USER_ID);
        param = param + "&date=" + new SimpleDateFormat("ddMMyyHHmmss").format(Calendar.getInstance().getTime());
        if (AppConstants.ACTION.equals(AppConstants.WATCH_LIST)) {
            new GetWatchlistService(param, getActivity(), new GetWatchlistService.OnResultReceived() {
                @Override
                public void onResult(String result) throws UnsupportedEncodingException {
                    binding.swipe.setRefreshing(false);
                    if (AppConstants.watch_serviceList.size() > 0) {
                        binding.relSearch.setVisibility(View.VISIBLE);
                        binding.tvNoproductfound.setVisibility(View.GONE);
                        if (Preferences.getValue_Boolean(getActivity(), Preferences.LIST_TYPE, true)) {
                            binding.recyclerView.setLayoutManager(gridLayoutManager);
                            adapter = new ServiceAdapter(getActivity(), AppConstants.watch_serviceList, R.layout.item_prodcut, Service_watchlist_fragement.this);
                            binding.recyclerView.setAdapter(adapter);
                            adapter.notifyDataSetChanged();
                        } else {
                            binding.recyclerView.setLayoutManager(layoutManager);
                            adapter = new ServiceAdapter(getActivity(), AppConstants.watch_serviceList, R.layout.item_product_list, Service_watchlist_fragement.this);
                            binding.recyclerView.setAdapter(adapter);
                            adapter.notifyDataSetChanged();
                        }
                    } else {
                        binding.relSearch.setVisibility(View.GONE);
                        binding.tvNoproductfound.setVisibility(View.VISIBLE);
                        binding.tvNoproductfound.setText("You did not add any services to your watch list yet");
                    }
                }

                @Override
                public void onFailed(VolleyError result) throws UnsupportedEncodingException {
                    binding.swipe.setRefreshing(false);
                }
            }, Isload);
        } else {
            new GetMylistService(param, getActivity(), new GetMylistService.OnResultReceived() {
                @Override
                public void onResult(String result) throws UnsupportedEncodingException {

                    binding.swipe.setRefreshing(false);
                    if (AppConstants.watch_serviceList.size() > 0) {
                        binding.relSearch.setVisibility(View.VISIBLE);
                        binding.tvNoproductfound.setVisibility(View.GONE);
                        if (Preferences.getValue_Boolean(getActivity(), Preferences.LIST_TYPE, true)) {
                            binding.recyclerView.setLayoutManager(gridLayoutManager);
                            adapter = new ServiceAdapter(getActivity(), AppConstants.watch_serviceList, R.layout.item_prodcut, Service_watchlist_fragement.this);
                            binding.recyclerView.setAdapter(adapter);
                            adapter.notifyDataSetChanged();
                        } else {
                            binding.recyclerView.setLayoutManager(layoutManager);
                            adapter = new ServiceAdapter(getActivity(), AppConstants.watch_serviceList, R.layout.item_product_list, Service_watchlist_fragement.this);
                            binding.recyclerView.setAdapter(adapter);
                            adapter.notifyDataSetChanged();
                        }
                    } else {
                        binding.relSearch.setVisibility(View.GONE);
                        binding.tvNoproductfound.setVisibility(View.VISIBLE);
                        binding.tvNoproductfound.setText("You do not offer any services yet");
                    }
                }

                @Override
                public void onFailed(VolleyError result) throws UnsupportedEncodingException {
                    binding.swipe.setRefreshing(false);
                }
            }, Isload);
        }




     /*   int spacing = 10; // 50px
        boolean includeEdge = true;
        binding.recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, spacing, includeEdge));*/


    }


    public void ChangeListType() {

        if (AppConstants.watch_serviceList.size() > 0) {

            //adapter.notifyDataSetChanged();
            if (Preferences.getValue_Boolean(getActivity(), Preferences.LIST_TYPE, true)) {

                binding.recyclerView.setLayoutManager(gridLayoutManager);
                adapter = new ServiceAdapter(getActivity(), AppConstants.watch_serviceList, R.layout.item_prodcut, Service_watchlist_fragement.this);
                binding.recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            } else {
                binding.recyclerView.setLayoutManager(layoutManager);
                adapter = new ServiceAdapter(getActivity(), AppConstants.watch_serviceList, R.layout.item_product_list, Service_watchlist_fragement.this);
                binding.recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }
        }
    }


    @Override
    public void load() {
        LoadData(true);
    }

    @Override
    public void refresh_view() {
        ChangeListType();
    }
}
