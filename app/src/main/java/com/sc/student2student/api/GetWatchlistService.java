package com.sc.student2student.api;

import com.sc.student2student.R;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import com.sc.student2student.common.AppConstants;
import com.sc.student2student.common.DialogManager;
import com.sc.student2student.common.LoadingDialog;
import com.sc.student2student.common.WebServices;
import com.sc.student2student.moduls.Images;
import com.sc.student2student.moduls.Product;
import com.sc.student2student.moduls.User;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by pc4 on 7/6/2016.
 */
public class GetWatchlistService {

    String TAG = getClass().getSimpleName();
    Context mContext;
    String envelope = "";
    LoadingDialog loadingDialog;
    RequestQueue queue;
    boolean flagProgress;
    private String url = WebServices.GET_WATCHLIST;
    private OnResultReceived onResultReceived;

    public GetWatchlistService(String envelope, Context mContext, OnResultReceived onResultReceived, boolean flagProgress) {
        this.envelope = envelope;
        this.mContext = mContext;
        this.onResultReceived = onResultReceived;
        this.flagProgress = flagProgress;
        queue = Volley.newRequestQueue(mContext);
        getResponse();
    }

    public void show() {
        if (flagProgress) {
            loadingDialog = new LoadingDialog(mContext, "", false);
        }
    }

    public void hide() {
        if (flagProgress) {
            loadingDialog.hide();
        }
    }

    public void getResponse() {
        show();
        Log.e(TAG, "Url: " + url + envelope);
        JsonObjectRequest postRequest = new JsonObjectRequest(com.android.volley.Request.Method.GET, url + envelope, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.e(TAG, "Response: " + response);
                            if (response.has("error")) {
                                hide();
                                DialogManager.errorDialog(mContext, "Error!", response.optString("error"));
                            } else if (response.has("success")) {
                                JSONArray object = response.getJSONArray("result");
                                AppConstants.watch_serviceList.clear();
                                if (object.length() > 0) {
                                    for (int i = 0; i < object.length(); i++) {
                                        /*Product product = new Product();
                                        JSONObject obj = object.getJSONObject(i);
                                        product.setProduct_id(obj.optString("pro_id"));
                                        product.setUser_id(obj.optString("user_id"));
                                        product.setProduct_name(obj.optString("pro_title"));
                                        product.setProduct_desc(obj.optString("pro_desc"));
                                        product.setPrice(obj.optString("price"));
                                        product.setType(obj.optString("type"));
                                        product.setIs_watch(obj.optString("is_watch"));

                                        JSONObject user = obj.getJSONObject("userInfo");

                                        User product_user = new User();

                                        product_user.setName(user.optString("name"));
                                        product_user.setEmail(user.optString("email"));
                                        product_user.setProfile_pic(user.optString("profile_pic"));
                                        product_user.setLocation(user.optString("location"));
                                        product_user.setLatitude(user.optString("latitude"));
                                        product_user.setLongitude(user.optString("longitude"));
                                        product_user.setAbout(user.optString("about"));
                                        product_user.setChat_pass(user.optString("chat_pass"));
                                        product_user.setRating(user.optString("rating"));
                                        product.setProduct_user(product_user);
                                        JSONArray image = obj.getJSONArray("images");
                                        ArrayList<Images> list = new ArrayList<>();
                                        list.clear();
                                        if (image.length() > 0) {
                                            for (int j = 0; j < image.length(); j++) {
                                                JSONObject object1 = image.getJSONObject(j);
                                                product.setProduct_image(image.getJSONObject(0).optString("image"));
                                                Images images = new Images();
                                                images.setImg_id(object1.optString("img_id"));
                                                images.setImage(object1.optString("image"));

                                                list.add(images);
                                            }
                                            product.setImages(list);
                                        } else {
                                            product.setProduct_image("");
                                            product.setImages(list);
                                        }
*/

                                        Product product = new Product();
                                        JSONObject obj = object.getJSONObject(i);
                                        product.setProduct_id(obj.optString("pro_id"));
                                        product.setUser_id(obj.optString("user_id"));
                                        product.setProduct_name(obj.optString("pro_title"));
                                        product.setProduct_desc(obj.optString("pro_desc"));
                                        product.setPrice(obj.optString("price"));
                                        product.setType(obj.optString("type"));

                                        product.setIs_watch(obj.optString("is_watch"));

                                        //product.setStatus(obj.optString("offerFlag"));


                                        JSONObject user = obj.getJSONObject("userInfo");

                                        User product_user = new User();

                                        product_user.setName(user.optString("name"));
                                        product_user.setEmail(user.optString("email"));
                                        product_user.setProfile_pic(user.optString("profile_pic"));
                                        product_user.setLocation(user.optString("location"));
                                        product_user.setLatitude(user.optString("latitude"));
                                        product_user.setLongitude(user.optString("longitude"));
                                        product_user.setAbout(user.optString("about"));
                                        product_user.setChat_pass(user.optString("chat_pass"));
                                        product_user.setRating(user.optString("rating"));
                                        product.setDistance(obj.optString("distance"));
                                        product.setCrt_date(obj.optString("crt_date"));


                                        JSONArray image = obj.getJSONArray("images");
                                        ArrayList<Images> list = new ArrayList<>();
                                        list.clear();
                                        if (image.length() > 0) {
                                            for (int j = 0; j < image.length(); j++) {
                                                JSONObject object1 = image.getJSONObject(j);
                                                product.setProduct_image(image.getJSONObject(0).optString("image"));
                                                product.setProduct_small_image(image.getJSONObject(0).optString("image-small"));
                                                product.setProduct_medium_image(image.getJSONObject(0).optString("image-medium"));
                                                Images images = new Images();
                                                images.setImg_id(object1.optString("img_id"));
                                                images.setImage(object1.optString("image"));
                                                images.setImage_small(object1.optString("image-small"));
                                                images.setImage_medium(object1.optString("image-medium"));

                                                list.add(images);
                                            }
                                            product.setImages(list);
                                        } else {
                                            product.setProduct_image("");
                                            product.setImages(list);
                                        }

                                        AppConstants.watch_serviceList.add(product);
                                    }
                                }

                                hide();
                                if (onResultReceived != null) try {
                                    onResultReceived.onResult(response.toString());
                                } catch (UnsupportedEncodingException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                hide();
                                Toast.makeText(mContext, mContext.getString(R.string.something_was_wrong_with_api), Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception e) {
                            hide();
                            Log.e(TAG, "Line " + Thread.currentThread().getStackTrace()[2].getLineNumber() + " Error:... " + e.getMessage());
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hide();
                        VolleyLog.d("Error.Response", "");
                        AppConstants.VolleyException(mContext, error);
                        if (onResultReceived != null) try {
                            onResultReceived.onFailed(error);
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                }
        ) {
            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Cache-Control", "no-cache");
                return params;
            }
        };
        AppConstants.SetTimeOut(postRequest);
        queue.add(postRequest);
    }


    public interface OnResultReceived {
        public void onResult(String result) throws UnsupportedEncodingException;

        public void onFailed(VolleyError result) throws UnsupportedEncodingException;
    }
}
