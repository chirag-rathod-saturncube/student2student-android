package com.sc.student2student.fragement;

import com.sc.student2student.R;

import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources.NotFoundException;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;

import com.sc.student2student.adapter.SlideShowAdapter;
import com.sc.student2student.moduls.Images;

import java.util.ArrayList;


public class Slider_fragement extends Dialog {
    ViewPager view_pager;
    SlideShowAdapter slideShowAdapter;
    private Dialog mpd = null;
    private LayoutInflater lyt_Inflater = null;


    public Slider_fragement(Context context, boolean b, int pos, ArrayList<Images> list) {
        super(context);

        LodingDialog2(context, b, pos, list);

    }


    public void LodingDialog2(final Context context, boolean cancelable, int pos, ArrayList<Images> list) {


        lyt_Inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View view_lyt = lyt_Inflater.inflate(R.layout.slider_fragement, null);
        view_pager = (ViewPager) view_lyt.findViewById(R.id.view_pager);

        slideShowAdapter = new SlideShowAdapter(context, R.layout.slide_item, list, false);
        view_pager.setAdapter(slideShowAdapter);
        slideShowAdapter.notifyDataSetChanged();
        view_pager.setCurrentItem(pos);
        try {

            mpd = new Dialog(context, R.style.Theme_Dialog1);
            mpd.requestWindowFeature(Window.FEATURE_NO_TITLE);
            mpd.setContentView(view_lyt);
            mpd.setCancelable(cancelable);
            mpd.show();

        } catch (NotFoundException e) {
            e.printStackTrace();
        }


    }


    public void hide() {
        if (mpd != null) {
            if (mpd.isShowing()) {
                mpd.dismiss();
            }
        }
    }

    public void hide1() {


        mpd.dismiss();
    }


}