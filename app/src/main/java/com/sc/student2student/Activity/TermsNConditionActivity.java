package com.sc.student2student.Activity;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.View;

import com.sc.student2student.R;
import com.sc.student2student.common.AppConstants;
import com.sc.student2student.common.DialogManager;
import com.sc.student2student.common.MyTagHandler;
import com.sc.student2student.common.Preferences;
import com.sc.student2student.databinding.ActivityTermsNconditionBinding;

public class TermsNConditionActivity extends AppCompatActivity {

    ActivityTermsNconditionBinding binding;
    String TAG = getClass().getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_terms_ncondition);

        binding.tvPdf.setText(fromHtml(getString(R.string.pdf_text)));

        binding.btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String currentVersion = "";
                try {
                    PackageManager manager = getPackageManager();
                    PackageInfo info = null;
                    info = manager.getPackageInfo(getPackageName(), 0);
                    currentVersion = info.versionName;

                    Log.e(TAG, "Version Name : " + currentVersion);

                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }

                if (binding.checkbox.isChecked()) {
                    Preferences.setValue(TermsNConditionActivity.this, Preferences.USER_EMAIL, AppConstants.emailTemp);
                    Preferences.setValue(TermsNConditionActivity.this, Preferences.VERSION_NAME, currentVersion);
                    startActivity(new Intent(TermsNConditionActivity.this, HomeActivity.class));
                    finish();
                } else {
                    DialogManager.errorDialog(TermsNConditionActivity.this, getString(R.string.validation_title), getString(R.string.text_validate_checkbox));
                }
            }
        });

    }

    @SuppressWarnings("deprecation")
    public static Spanned fromHtml(String html) {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(html, null, new MyTagHandler());
        } else {
            result = Html.fromHtml(html, null, new MyTagHandler());
        }
        return result;
    }

}
