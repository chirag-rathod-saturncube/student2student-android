package com.sc.student2student.Activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.sc.student2student.R;
import com.sc.student2student.adapter.ReviewsAdapter;
import com.sc.student2student.common.AppConstants;

public class ReviewsActivity extends AppCompatActivity {

    TextView tv_No_rating, tv_title, tv_average;
    RecyclerView recyclerview;
    LinearLayoutManager linearLayoutManager;
    ReviewsAdapter reviewsAdapter;
    RatingBar ratingBar;
    ImageView img_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reviews);
        recyclerview = (RecyclerView) findViewById(R.id.recyclerview);
        tv_No_rating = (TextView) findViewById(R.id.tv_No_rating);
        tv_average = (TextView) findViewById(R.id.tv_average);
        img_back = (ImageView) findViewById(R.id.img_back);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        ratingBar = (RatingBar) findViewById(R.id.ratingBar);
        tv_title = (TextView) findViewById(R.id.tv_title);
        tv_title.setText("Reviews");
        linearLayoutManager = new LinearLayoutManager(ReviewsActivity.this);
        recyclerview.setLayoutManager(linearLayoutManager);
        tv_average.setText("Average Rating (" + AppConstants.reviewsArraylist.size() + ")");
        ratingBar.setRating(Float.parseFloat(AppConstants.user.getRating()));
        if (AppConstants.reviewsArraylist.size() > 0) {
            tv_No_rating.setVisibility(View.GONE);
        } else {
            tv_No_rating.setVisibility(View.VISIBLE);
        }
        reviewsAdapter = new ReviewsAdapter(ReviewsActivity.this, AppConstants.reviewsArraylist);
        recyclerview.setAdapter(reviewsAdapter);
        reviewsAdapter.notifyDataSetChanged();
    }
}
