package com.sc.student2student.Activity;

import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.Spanned;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.sc.student2student.R;
import com.sc.student2student.api.RegisterApi;
import com.sc.student2student.common.AppConstants;
import com.sc.student2student.common.DialogManager;
import com.sc.student2student.common.MyTagHandler;
import com.sc.student2student.databinding.ActivityRegisterBinding;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class RegisterActivity extends AppCompatActivity {
    ActivityRegisterBinding binding;
    private String TAG = getClass().getSimpleName();

    @SuppressWarnings("deprecation")
    public static Spanned fromHtml(String html) {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(html, null, new MyTagHandler());
        } else {
            result = Html.fromHtml(html, null, new MyTagHandler());
        }
        return result;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_register);

        binding.tvTermsncondition.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {
                Integer pageNumber = 0;
                final AlertDialog.Builder dialogbuilder = new AlertDialog.Builder(RegisterActivity.this);
                final LayoutInflater layoutInflater = getLayoutInflater();
                final View dialogview = layoutInflater.inflate(R.layout.itempdfviewer, null);
                dialogbuilder.setView(dialogview);
                final AlertDialog d = dialogbuilder.create();
                d.show();
                final TextView textView = (TextView) dialogview.findViewById(R.id.tv_pdf);
                textView.setText(fromHtml(getString(R.string.pdf_text)));

                /*final PDFView pdfView= (PDFView) dialogview.findViewById(R.id.pdfviewer);
                pdfView.fromAsset("Student.pdf")
                        .defaultPage(pageNumber)
                        .enableSwipe(true)
                        .swipeHorizontal(false)
                        .enableAnnotationRendering(true)
                        .scrollHandle(new DefaultScrollHandle(RegisterActivity.this))
                        .load();*/

                //startActivity(new Intent(RegisterActivity.this, TermsNConditionActivity.class));
            }
        });

        binding.btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        register();
                    }
                }, 300);
            }
        });

        binding.edtPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    register();
                }
                return false;
            }
        });

           /* binding.btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });*/

    }

    public void register() {
        if (isValidate()) {
            if (AppConstants.isNetworkAvailable(getString(R.string.network_title), getString(R.string.network_msg), RegisterActivity.this)) {
                try {
                    String param = "&name=" + URLEncoder.encode(binding.edtName.getText().toString(), AppConstants.encodeType);
                    param = param + "&email=" + binding.edtEmail.getText().toString();
                    param = param + "&password=" + binding.edtPassword.getText().toString();
                    param = param + "&date=" + new SimpleDateFormat("ddMMyyHHmmss").format(Calendar.getInstance().getTime());
                    new RegisterApi(param, RegisterActivity.this, new RegisterApi.OnResultReceived() {
                        @Override
                        public void onResult(String result) throws UnsupportedEncodingException {

                        }
                    }, true);
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private boolean isValidate() {
        boolean flagValidate = true;
        String validateMsg = "";


        if (binding.edtName.getText().toString().trim().equals("")) {
            validateMsg = getString(R.string.text_validate_last_name);
            flagValidate = false;
        } else if (binding.edtEmail.getText().toString().trim().equals("")) {
            validateMsg = getString(R.string.text_validate_email);
            flagValidate = false;
        } else if (!AppConstants.validateEmail(binding.edtEmail.getText().toString().trim())) {
            validateMsg = getString(R.string.text_validate_email);
            flagValidate = false;
        } else if (binding.edtPassword.getText().toString().trim().equals("")) {
            validateMsg = getString(R.string.text_validate_password);
            flagValidate = false;
        } else if (binding.edtCPassword.getText().toString().trim().equals("")) {
            validateMsg = getString(R.string.text_validate_confirm_password);
            flagValidate = false;
        } else if (!binding.edtCPassword.getText().toString().trim()
                .equals(binding.edtPassword.getText().toString().trim())) {
            validateMsg = getString(R.string.text_validate_password_same);
            flagValidate = false;
        } else if (!binding.checkbox.isChecked()) {
            validateMsg = getString(R.string.text_validate_checkbox);
            flagValidate = false;
        }
        if (flagValidate) {
            return true;
        } else {
            DialogManager.errorDialog(RegisterActivity.this, getString(R.string.validation_title), validateMsg);
            return false;
        }
    }

}

