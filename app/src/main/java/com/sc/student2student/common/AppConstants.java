package com.sc.student2student.common;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.sc.student2student.Interface.Product_Refresh;
import com.sc.student2student.Interface.Product_Watch_Refresh;
import com.sc.student2student.Interface.Service_Refresh;
import com.sc.student2student.Interface.Service_Watch_Refresh;
import com.sc.student2student.R;
import com.sc.student2student.moduls.Alert;
import com.sc.student2student.moduls.Cat;
import com.sc.student2student.moduls.Category;
import com.sc.student2student.moduls.ChatMessage;
import com.sc.student2student.moduls.City;
import com.sc.student2student.moduls.Condition;
import com.sc.student2student.moduls.Demo;
import com.sc.student2student.moduls.GroupMsg;
import com.sc.student2student.moduls.Messages;
import com.sc.student2student.moduls.Notification;
import com.sc.student2student.moduls.PricePer;
import com.sc.student2student.moduls.Product;
import com.sc.student2student.moduls.ProductGroup;
import com.sc.student2student.moduls.Reviews;
import com.sc.student2student.moduls.User;
import com.squareup.picasso.Transformation;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.regex.Pattern;

/**
 * Created by pc4 on 2/24/2017.
 */

public class AppConstants {
    public static final String APP_CONSTANT = "APP_CONSTANT";

    public static final String FACEBOOK = "2";
    public static final String WEB = "1";
    public static final String GOOGLE = "3";
    public static final String SFINDIT = "SFINDIT";

    public static final String APP_NAME = "Students";
    public static final String INBOX = "INBOX";
    public static final String PRODUCT_DETAILS = "PRODUCT_DETAILS";
    public static final String WATCH_LIST = "WATCH_LIST";
    public static final String MYLIST = "MYLIST";
    public static final String HELP = "HELP";
    public static final String FAQ = "FAQ";
    public static final String PRODUCT = "1";
    public static final String SERVICE = "2";
    public static final String NEW = "new";
    public static final String EDIT = "edit";
    public static final String SELL_PRODUCT = "SELL_PRODUCT";
    public static final String VIEW_PRODUCT = "VIEW_PRODUCT";
    public static final String TEMP_PHOTO_FILE_NAME = "my_dp.jpg";
    public static String PRODUCT_ID = "PRODUCT_ID";
    public static String action = "action";
    public static boolean chatproduct;
    public static boolean editAlert;
    public static String registerInfo = "";
    public static int regStatus = 0;
    public static String connectTo = "";
    public static String map_lng = "";
    public static String map_lat = "";
    public static String map_address = "";
    public static String map_country = "";
    public static User user = new User();
    public static String USER_ID = "";
    public static Category category;
    public static Product product;
    public static Notification notification;
    public static int pos;
    public static int page_display;
    public static boolean view_profile = true;
    public static String ACTION = "";
    public static String PROFILE_ACTION = "";
    public static String HELP_ACTION = "";
    public static String TYPE = AppConstants.PRODUCT;
    public static String ACTION_PRODUCT = "";
    public static String user_rating = "";
    public static String user_review = "";
    public static String chat_username = "";
    public static String chat_user_id = "";
    public static String chat_userprofile = "";
    public static String Current_City = "";
    public static String totalNotification = "";
    public static String totalMessages = "";
    // public static boolean notificationflag = false;
    //Alpesh development
    public static Cat cat = new Cat();
    public static City city = new City();
    public static boolean isSell = false;
    public static Product_Refresh product_refresh;
    public static Product_Watch_Refresh product_watch_refresh;
    public static Service_Refresh service_refresh;


    /*Applications list provider*/
  /*  public static ArrayList<Event> eventArrayList = new ArrayList<>();
    public static ArrayList<Event> myEventArrayList = new ArrayList<>();
    public static Event singleEventDetails = new Event();
    public static User singleUser = new User();
*/
    public static Service_Watch_Refresh service_watch_refresh;
    public static ArrayList<Product> productList = new ArrayList<>();
    public static ArrayList<Product> serivceList = new ArrayList<>();
    public static ArrayList<Product> watch_productList = new ArrayList<>();
    public static ArrayList<Product> watch_serviceList = new ArrayList<>();
    public static ArrayList<Category> categoryList = new ArrayList<>();
    public static ArrayList<Condition> condtionList = new ArrayList<>();
    public static ArrayList<PricePer> priceperList = new ArrayList<>();
    public static ArrayList<Notification> notificationList = new ArrayList<>();
    public static ArrayList<File> photo_list = new ArrayList<>();
    public static ArrayList<Integer> pic = new ArrayList<Integer>();
    public static ArrayList<Reviews> reviewsArraylist = new ArrayList<>();
    public static boolean _hasLoadedOnce_product = false;
    public static boolean isChangeListing = false;
    public static ArrayList<Cat> catArrayList = new ArrayList<>();
    public static ArrayList<Cat> catProArrayList = new ArrayList<>();
    public static ArrayList<Cat> catSerArrayList = new ArrayList<>();
    public static ArrayList<Cat> subCatArrayList = new ArrayList<>();
    public static ArrayList<ProductGroup> productGroupArrayList = new ArrayList<>();
    public static ArrayList<Messages> messagesArrayList = new ArrayList<>();
    public static ArrayList<Messages> messagesReloadArrayList = new ArrayList<>();
    public static ProductGroup productGroup = new ProductGroup();
    public static ChatMessage chatMessage = new ChatMessage();
    public static ChatMessage chatMessageReload = new ChatMessage();
    //Notification implement
    public static ArrayList<GroupMsg> groupMsgArrayList = new ArrayList<>(5);
    public static String chatMsg = "";
    public static String notificationMsg = "";
    public static String encodeType = "UTF-8";
    public static String profile_path = "";
    public static boolean isNeedReopenApp = false;
    public static boolean isChatScreenOpen = false;
    public static boolean isNotificationScreenOpen = false;
    public static boolean isNeedRefreshCounter = false;
    public static String group_id = "";
    public static String unread_count = "";
    public static File mFileTemp;
    public static String send_image_path = "";
    public static int send_position = 0;
    public static String productID;
    public static List<Demo> demoArrayList = new ArrayList<>();
    public static String emailTemp = "";

    //Alert Module
    public static ArrayList<Cat> alertCatArrayList = new ArrayList<>();
    public static ArrayList<Cat> alertSubCatArrayList = new ArrayList<>();
    public static ArrayList<Alert> alertArrayList = new ArrayList<>();
    public static ArrayList<City> cityArrayList = new ArrayList<>();
    public static int alertArrayListposition;


    @SuppressLint("ClickableViewAccessibility")
    public static boolean isNetworkAvailable(final String title, final String message, final Context context) {
        Config.getmycontext(context);
        if (Config.oConnectivityManager.getActiveNetworkInfo() != null && Config.oConnectivityManager.getActiveNetworkInfo().isAvailable() && Config.oConnectivityManager.getActiveNetworkInfo().isConnected()) {
            return true;
        } else {

            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            final View dialogView = inflater.inflate(R.layout.dialog_internet_connection, null);
            dialogBuilder.setView(dialogView);
            final AlertDialog b = dialogBuilder.create();

            b.show();


            final ImageView imgClose = (ImageView) dialogView.findViewById(R.id.img_close);
            final TextView tvTitle = (TextView) dialogView.findViewById(R.id.tv_dialog_title);
            final TextView tvMsg = (TextView) dialogView.findViewById(R.id.tv_dialog_msg);
            final TextView tvOk = (TextView) dialogView.findViewById(R.id.tv_ok);

            tvTitle.setText("Retry");
            tvOk.setText("OK");
            tvMsg.setText("Please check internet connectivity");

            imgClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    b.dismiss();
                }
            });

            tvOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    b.dismiss();

                }
            });


            tvOk.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        tvOk.setTextColor(Color.WHITE);
                    } else if (event.getAction() == MotionEvent.ACTION_UP) {
                        tvOk.setTextColor(Color.WHITE);
                    }
                    return false;
                }
            });

            return false;
        }
    }

    public static boolean validateEmail(String email) {
        Pattern pattern = Pattern
                .compile("[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}");
        // Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }

    public static void copyStream(InputStream input, OutputStream output)
            throws IOException {
        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }
    }

    public static Bitmap getRescaledBitmap(String filePath) {
        Bitmap bitmap = null;
        try {
            int targetWidth = 600; // your arbitrary fixed limit
            int targetHeight = 600;
            File image = new File(filePath);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            bitmap = BitmapFactory.decodeFile(image.getAbsolutePath(), options);
            int ImageWidth = bitmap.getWidth();
            int ImageHeight = bitmap.getHeight();
            Log.e("before WxH", ImageWidth + "x" + ImageHeight);
            if (ImageHeight <= ImageWidth) {
                // targetWidth = 480;
                targetHeight = (int) (ImageHeight * targetWidth / (double) ImageWidth); // casts
                if (ImageWidth > targetWidth) {
                    bitmap = ScalingUtilities.decodeFile(image.getAbsolutePath(), targetWidth, targetHeight);
                    bitmap = ScalingUtilities.createScaledBitmap(bitmap,
                            targetWidth, targetHeight);
                    OutputStream imagefile = new FileOutputStream(
                            image.getAbsoluteFile());
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 80, imagefile);
                } else {
                    OutputStream imagefile = new FileOutputStream(
                            image.getAbsoluteFile());
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, imagefile);
                }
                Log.e("after WxH", bitmap.getWidth() + "x" + bitmap.getHeight());
            } else {
                targetWidth = (int) (ImageWidth * targetHeight / (double) ImageHeight); // casts
                if (ImageHeight > targetHeight) {
                    bitmap = ScalingUtilities.decodeFile(
                            image.getAbsolutePath(), targetWidth, targetHeight);
                    bitmap = ScalingUtilities.createScaledBitmap(bitmap,
                            targetWidth, targetHeight);
                    OutputStream imagefile = new FileOutputStream(
                            image.getAbsoluteFile());
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 80, imagefile);
                } else {
                    OutputStream imagefile = new FileOutputStream(
                            image.getAbsoluteFile());
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, imagefile);
                }
                Log.e("WxH", bitmap.getWidth() + "x" + bitmap.getHeight());
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return bitmap;

    }

    public static void SetTimeOut(JsonObjectRequest postRequest) {
        postRequest.setRetryPolicy(new DefaultRetryPolicy(
                30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

    public static Transformation getTransformation(final ImageView imageView) {
        final Transformation transformation = new Transformation() {
            @Override
            public Bitmap transform(Bitmap source) {
                int targetWidth = imageView.getWidth();
                if (targetWidth == 0) {
                    targetWidth = 140;
                }
                double aspectRatio = (double) source.getHeight() / (double) source.getWidth();
                int targetHeight = (int) (targetWidth * aspectRatio);
                Bitmap result = Bitmap.createScaledBitmap(source, targetWidth, targetHeight, false);
                if (result != source) {
                    // Same bitmap is returned if sizes are the same
                    source.recycle();
                }
                return result;
            }

            @Override
            public String key() {
                return "transformation" + " desiredWidth";
            }
        };

        return transformation;
    }

    public static void VolleyException(Context mContext, VolleyError error) {
        if (error instanceof TimeoutError) {
            Toast.makeText(mContext,
                    mContext.getString(R.string.network_msg),
                    Toast.LENGTH_LONG).show();
        } else if (error instanceof AuthFailureError) {
            Toast.makeText(mContext,
                    mContext.getString(R.string.network_msg),
                    Toast.LENGTH_LONG).show();
        } else if (error instanceof ServerError) {
            Toast.makeText(mContext,
                    mContext.getString(R.string.something_was_wrong_with_api),
                    Toast.LENGTH_LONG).show();
        } else if (error instanceof NoConnectionError) {

            Toast.makeText(mContext,
                    mContext.getString(R.string.network_msg),
                    Toast.LENGTH_LONG).show();
        } else if (error instanceof ParseError) {
            Toast.makeText(mContext,
                    mContext.getString(R.string.something_was_wrong_with_api),
                    Toast.LENGTH_LONG).show();
        }
    }

    public static void resetfiler(Context activity) {
        Preferences.setValue(activity, Preferences.LOCATION, "");
        Preferences.setValue(activity, Preferences.MILES, "");
        Preferences.setValue(activity, Preferences.MIN_PRICE, "");
        Preferences.setValue(activity, Preferences.MAX_PRICE, "");
        Preferences.setValue(activity, Preferences.SORT_BY, "");
        Preferences.setValue(activity, Preferences.COUNTRY, "");
        Preferences.setValue(activity, Preferences.STATE, "");
        Preferences.setValue(activity, Preferences.ADDRESS, "");
        //Preferences.setValue(activity, Preferences.LATITUDE, "");
        //Preferences.setValue(activity, Preferences.LONGITUDE, "");
    }


    public static void printHashKey(Context context) {
        try {
            PackageInfo info = context.getPackageManager().getPackageInfo("com.sc.student2student", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e("KeyHash for Facebook:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
    }


    public static String getSampleName(Context mContext) {
        String[] array = mContext.getResources().getStringArray(R.array.array_person_name);
        String randomStr = array[new Random().nextInt(array.length)];
        return randomStr;
    }

    public static String getSampleImagesUrl(Context mContext) {
        String[] array = mContext.getResources().getStringArray(R.array.array_person_images);
        String randomStr = array[new Random().nextInt(array.length)];
        return randomStr;
    }


}
