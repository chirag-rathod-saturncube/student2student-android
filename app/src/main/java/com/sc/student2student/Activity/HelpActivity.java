package com.sc.student2student.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.sc.student2student.R;
import com.sc.student2student.adapter.FaqAdapter;
import com.sc.student2student.common.AppConstants;
import com.sc.student2student.common.ExpandableRecyclerAdapter;
import com.sc.student2student.moduls.Faq;
import com.sc.student2student.moduls.FaqItem;

import java.util.ArrayList;
import java.util.List;

public class HelpActivity extends AppCompatActivity {
    TextView tvTitle;

    RecyclerView mRecyclerView;
    LinearLayoutManager mLayoutManager;
    FaqAdapter adapter;
    List<Faq> faqList;
    ImageView img_back;
    TextView rel_bottom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);
        initView();
        faqList = new ArrayList<>();
        LoadFaqforNeedypeople();

        adapter = new FaqAdapter(HelpActivity.this, faqList);
        adapter.setMode(ExpandableRecyclerAdapter.MODE_ACCORDION);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(HelpActivity.this));
        mRecyclerView.setAdapter(adapter);

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        rel_bottom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.HELP_ACTION = AppConstants.FAQ;
                startActivity(new Intent(HelpActivity.this, FaqActivity.class));
            }
        });

    }

    private void initView() {
        rel_bottom = (TextView) findViewById(R.id.btn_confirm);
        tvTitle = (TextView) findViewById(R.id.tv_title);

        img_back = (ImageView) findViewById(R.id.img_back);

        mRecyclerView = (RecyclerView) findViewById(R.id.rv_list);
        mLayoutManager = new LinearLayoutManager(HelpActivity.this);
        mLayoutManager = new LinearLayoutManager(HelpActivity.this, LinearLayoutManager.VERTICAL, false);
        mLayoutManager.setSmoothScrollbarEnabled(true);
        mLayoutManager.setRecycleChildrenOnDetach(true);
        mRecyclerView.setLayoutManager(mLayoutManager);
        tvTitle.setText("Help");


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


    private void LoadFaqforNeedypeople() {
        faqList.add(new Faq("Getting Started", R.drawable.getting_starred));
        FaqItem faqItem = new FaqItem();
        faqItem.setAnswer("Getting started is easy and straightforward.\n Are you looking for products and services in your neighborhood, than simply start looking directly at the homepage.\n" +
                "If you intend to offer products & services then create them using the options displayed in the menu. Good luck!");
        faqList.add(new Faq(faqItem));


        faqList.add(new Faq("Selling", R.drawable.selling));
        FaqItem faqItem1 = new FaqItem();
        faqItem1.setAnswer("Once you are registered and logged in you can start selling your products right away! Simply create a new product in the menu by selecting sell product");
        faqList.add(new Faq(faqItem1));

        faqList.add(new Faq("Buying", R.drawable.buying));
        FaqItem faqItem2 = new FaqItem();
        faqItem2.setAnswer("If you are looking for products and services you can go through the product categories or directly enter your search term. Found the product that you want? Simply make an offer to the seller, or contact them directly to arrange your deal!");
        faqList.add(new Faq(faqItem2));


        faqList.add(new Faq("Trust & Safety", R.drawable.trust_safety));
        FaqItem faqItem3 = new FaqItem();
        faqItem3.setAnswer("Your trust and safety is of utmost importance to us. Although we aim to make sure that no fake products and crooks are present at the platform, we can't guarantee it. By behaving in a clever way the risks on scams can be minimized, for example by using the recommendations listed below.\n\n" +
                "Safe trading on Student2Student\n\n" +
                "The majority of the sellers and buyers on Student2Student has good intensions and is trustworthy.  However, there is also a small group of people that misuses the trust of others. Check our list of advises to make sure that your trading will be safe, secure and successful. \n" +
                "Advises to keep in mind while doing business on Student2Student: \n\n" +
                "1) Check the other user that you're doing business with, for example check how long he/she is member, his/her other products and try to make personal contact. \n\n" +
                "2) Use common sense, if something sounds too good to be true, that is often the case!\n\n" +
                "3) Preferably do not meet in remote places and do not carry a large amount of cash with you. \n\n" +
                "4) Make clear agreements on the price, place and guarantee of the product/service. \n\n" +
                "In the case of an incident, suspicion of abuse you can file a complaint against the specific user on his/her profile page. We will then look into the matter and take action when needed.");
        faqList.add(new Faq(faqItem3));


        faqList.add(new Faq("Technical Support", R.drawable.technical_support));
        FaqItem faqItem5 = new FaqItem();
        faqItem5.setAnswer("In the case you encounter bugs or other type of software failures please contact us directly.\n");
        faqList.add(new Faq(faqItem5));
    }
}
