package com.sc.student2student.fragement;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.VolleyError;
import com.sc.student2student.R;
import com.sc.student2student.adapter.CatAdapter;
import com.sc.student2student.api.GetSerCatApi;
import com.sc.student2student.common.AppConstants;
import com.sc.student2student.common.GridSpacingItemDecoration;
import com.sc.student2student.common.UINotifierServiceCategory;
import com.sc.student2student.common.Utils;
import com.sc.student2student.databinding.FragmentServiceCategoryBinding;

import java.io.UnsupportedEncodingException;

import static android.text.style.TtsSpan.ARG_TEXT;

/**
 * Created by pc4 on 20/07/2017.
 */

public class ServiceFragment extends Fragment implements UINotifierServiceCategory {
    FragmentServiceCategoryBinding binding;

    String TAG = getClass().getSimpleName();
    View rootView;

    boolean flagLoading = true;
    int pastVisibleItems, visibleItemCount, totalItemCount;
    int pageCount = 0;
    int limit = 20;
    CatAdapter adapter;
    GridLayoutManager gridLayoutManager;

    public static Fragment newInstance(String text) {
        Fragment frag = new ServiceFragment();
        Bundle args = new Bundle();
        args.putString(ARG_TEXT, text);
        frag.setArguments(args);
        return frag;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_service_category, container, false);
        Utils.notifierServiceCategory = this;
        binding = FragmentServiceCategoryBinding.bind(rootView);
        initView();
        binding.swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                AppConstants.catArrayList.clear();
                /*if (adapter != null)
                    adapter.notifyDataSetChanged();*/
                loadFragData(false);
            }
        });
        return rootView;
    }

    private void initView() {
        gridLayoutManager = new GridLayoutManager(getActivity(), 2, LinearLayoutManager.VERTICAL, false);
        binding.recyclerView.setLayoutManager(gridLayoutManager);
        gridLayoutManager.setSmoothScrollbarEnabled(true);
        int spacing = 10; // 50px
        boolean includeEdge = true;
        binding.recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, spacing, includeEdge));
    }


    @Override
    public void loadFragData(boolean flag) {
        Log.e(TAG, "Service fragment....");
        String envelope = "&type=" + AppConstants.SERVICE;
        envelope = envelope + "&parent_id=0";
        new GetSerCatApi(envelope, getContext(), new GetSerCatApi.OnResultReceived() {
            @Override
            public void onResult(String result) throws UnsupportedEncodingException {
                binding.swipeRefresh.setRefreshing(false);
                if (AppConstants.catSerArrayList.size() > 0) {
                    //binding.relSearch.setVisibility(View.VISIBLE);
                    //binding.tvNoRecord.setVisibility(View.GONE);
                    binding.recyclerView.setLayoutManager(gridLayoutManager);
                    adapter = new CatAdapter(getActivity(), AppConstants.catSerArrayList, AppConstants.SERVICE, "1");
                    binding.recyclerView.setAdapter(adapter);
                    adapter.notifyDataSetChanged();

                } else {
                    // binding.relSearch.setVisibility(View.GONE);
                    //binding.tvNoRecord.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailed(VolleyError result) throws UnsupportedEncodingException {
                binding.swipeRefresh.setRefreshing(false);
            }
        }, flag);



        /*if (AppConstants.isNetworkAvailable(getActivity())) {
            pageCount = 0;
            AppConstants.quotesPendingArrayList.clear();
            if (Preferences.getValue_String(getActivity(), Preferences.ROLE).equals(AppConstants.CLIENT)) {
                String envelope = "&user_id=" + Preferences.getValue_String(getActivity(), Preferences.USER_ID);
                envelope = envelope + "&start=" + pageCount;
                new ClientPendingQuotesApi(envelope, getActivity(), new ClientPendingQuotesApi.OnResultReceived() {
                    @Override
                    public void onResult(String result) throws UnsupportedEncodingException {
                        flagLoading = AppConstants.resetDataFlag(result, limit);
                        binding.swipeRefresh.setRefreshing(false);
                        binding.relNoRecord.setVisibility(View.GONE);
                        adapter = new PendingQuotesAdapter(getActivity(), AppConstants.quotesPendingArrayList);
                        binding.recyclerView.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                    }
                }, flag);
            } else {
                String envelope = "&user_id=" + Preferences.getValue_String(getActivity(), Preferences.USER_ID);
                envelope = envelope + "&start=" + pageCount;
                new ManagerNewQuotesApi(envelope, getActivity(), new ManagerNewQuotesApi.OnResultReceived() {
                    @Override
                    public void onResult(String result) throws UnsupportedEncodingException {
                        flagLoading = AppConstants.resetDataFlag(result, limit);
                        binding.swipeRefresh.setRefreshing(false);
                        binding.relNoRecord.setVisibility(View.GONE);
                        adapter = new PendingQuotesAdapter(getActivity(), AppConstants.quotesPendingArrayList);
                        binding.recyclerView.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                    }
                }, false);
            }
        }*/
    }
}
