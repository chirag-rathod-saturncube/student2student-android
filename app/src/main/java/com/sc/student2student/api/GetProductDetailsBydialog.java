package com.sc.student2student.api;

import com.sc.student2student.R;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.sc.student2student.common.AppConstants;
import com.sc.student2student.common.LoadingDialog;
import com.sc.student2student.common.WebServices;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by pc4 on 7/6/2016.
 */
public class GetProductDetailsBydialog {

    String TAG = getClass().getSimpleName();
    Context mContext;
    String envelope = "";
    LoadingDialog loadingDialog;
    RequestQueue queue;
    boolean flagProgress;
    private String url = WebServices.GET_PRODUCT_BY_DIALOG;
    private OnResultReceived onResultReceived;

    public GetProductDetailsBydialog(String envelope, Context mContext, OnResultReceived onResultReceived, boolean flagProgress) {
        this.envelope = envelope;
        this.mContext = mContext;
        this.onResultReceived = onResultReceived;
        this.flagProgress = flagProgress;
        queue = Volley.newRequestQueue(mContext);
        getResponse();
    }

    public void show() {
        if (flagProgress) {
            loadingDialog = new LoadingDialog(mContext, "", false);
        }
    }

    public void hide() {
        if (flagProgress) {
            loadingDialog.hide();
        }
    }

    public void getResponse() {
        show();
        Log.e(TAG, "Url: " + url + envelope);
        JsonObjectRequest postRequest = new JsonObjectRequest(com.android.volley.Request.Method.GET, url + envelope, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        hide();
                        if (onResultReceived != null) try {
                            onResultReceived.onResult(response);
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hide();
                        VolleyLog.d("Error.Response", "");
                        AppConstants.VolleyException(mContext, error);
                        if (onResultReceived != null) try {
                            onResultReceived.onFailed(error);
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                }
        ) {
            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Cache-Control", "no-cache");
                return params;
            }
        };
        AppConstants.SetTimeOut(postRequest);
        queue.add(postRequest);
    }


    public interface OnResultReceived {
        public void onResult(JSONObject result) throws UnsupportedEncodingException;

        public void onFailed(VolleyError result) throws UnsupportedEncodingException;
    }
}
