package com.sc.student2student.moduls;

/**
 * Created by win 7 on 7/27/2017.
 */

public class Cat {
    String cat_id, category_name, cat_img, cat_type, parent_id, active_flag, total_product, have_subcategory;


    public String getCat_id() {
        return cat_id;
    }

    public void setCat_id(String cat_id) {
        this.cat_id = cat_id;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getCat_img() {
        return cat_img;
    }

    public void setCat_img(String cat_img) {
        this.cat_img = cat_img;
    }

    public String getCat_type() {
        return cat_type;
    }

    public void setCat_type(String cat_type) {
        this.cat_type = cat_type;
    }

    public String getParent_id() {
        return parent_id;
    }

    public void setParent_id(String parent_id) {
        this.parent_id = parent_id;
    }

    public String getActive_flag() {
        return active_flag;
    }

    public void setActive_flag(String active_flag) {
        this.active_flag = active_flag;
    }

    public String getTotal_product() {
        return total_product;
    }

    public void setTotal_product(String total_product) {
        this.total_product = total_product;
    }

    public String getHave_subcategory() {
        return have_subcategory;
    }

    public void setHave_subcategory(String have_subcategory) {
        this.have_subcategory = have_subcategory;
    }

    @Override
    public String toString() {
        return category_name;
    }
}
