package com.sc.student2student.common;

import android.content.Context;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class WebServices {
    public static final String RESPONSE_UNWANTED = "UNWANTED";

    //private static final String BASE_URL = "http://demo.saturncube.com/Student2Student/API/api.php?";
    //public static final String DOMAIN = "http://demo.saturncube.com/Student2Student/API/";

    //private static final String BASE_URL = "http://students2students.nl/API/api_version_2.php?";
    private static final String BASE_URL = "https://students2students.nl/API/api_version_2.php?";
    public static final String DOMAIN = "students2students.nl/API/";

    public static final String REGISTER_URL = BASE_URL + "action=register";
    public static final String LOGIN_API = BASE_URL + "action=login";
    public static final String FORGOT_PASSWORD = BASE_URL + "action=confirmForgetPass";
    public static final String GET_PROFILE = BASE_URL + "action=viewProfile";
    public static final String EDIT_PROFILE = BASE_URL + "action=editProfile";
    public static final String CHANGE_PASSWORD = BASE_URL + "action=changePassword";
    public static final String GET_CATEGORY = BASE_URL + "action=getCategory";
    public static final String GET_CONDITION = BASE_URL + "action=getConditions";
    public static final String GET_PRICE_PER = BASE_URL + "action=getPrice";
    public static final String ADD_SELL_PRODUCT = BASE_URL + "action=addProductOrService";
    public static final String GET_PRODUCT = BASE_URL + "action=getProducts";
    public static final String GET_PRODUCT_DETAILS = BASE_URL + "action=getProductDetail";
    public static final String GET_SERVICE = BASE_URL + "action=getServices";
    public static final String ADD_WATCHLIST = BASE_URL + "action=addToWatchList";
    public static final String REMOVE_WATCHLIST = BASE_URL + "action=removeFromWatchList";
    public static final String MAKE_AN_OFFER = BASE_URL + "action=makeOffer";
    public static final String PROVIDE_SERVICE = BASE_URL + "action=provideService";
    public static final String GET_NOTIFICATION = BASE_URL + "action=getNotification";
    public static final String LOG_OUT = BASE_URL + "action=logout";
    public static final String ACCEPT = BASE_URL + "action=acceptOffer";
    public static final String ACCEPT_SERVICE = BASE_URL + "action=acceptService";
    public static final String REJECT = BASE_URL + "action=rejectOffer";
    public static final String REJECT_SERVICE = BASE_URL + "action=rejectService";
    public static final String GET_WATCHLIST = BASE_URL + "action=myWatchList";
    public static final String GET_MYLIST = BASE_URL + "action=myProductOrServices";
    public static final String ADD_RATING = BASE_URL + "action=addRating";
    public static final String GET_RATING = BASE_URL + "action=getRating";
    public static final String REPORT_PRODUCT = BASE_URL + "action=reportProduct";
    public static final String REPORT_USER = BASE_URL + "action=reportUser";
    public static final String UPDATE_NOTIFICATION = BASE_URL + "action=updatenotification";
    public static final String UPDATE_MESSAGE = BASE_URL + "action=updatemessage";

    public static final String GET_PRODUCT_BY_DIALOG = BASE_URL + "action=getproduct_dialog";
    public static final String DELETE_PRODUCT = BASE_URL + "action=deleteProduct";
    public static final String DELETE_PRODUCT_IMAGE = BASE_URL + "action=deleteProductimage";
    public static final String UPDATE_PRODUCT = BASE_URL + "action=updateProduct";
    public static final String DELETE_ACCOUNT = BASE_URL + "action=deleteUser";
    public static final String SEND_NOTIFICATION = BASE_URL + "action=send_message";

    /*Api chatting*/
    public static final String PRODUCT_GROUP_LIST_API = BASE_URL + "action=getInbox";
    public static final String PRODUCT_CHAT_HISTORY_API = BASE_URL + "action=getGroupDetails";
    public static final String CREATE_PRODUCT_CHAT_API = BASE_URL + "action=createGroup";
    public static final String SEND_MESSAGE_API = BASE_URL + "action=addMsgInGroup";

    public static final String REVIEWS = BASE_URL + "action=rating";
    public static final String GET_CATEGORIES = BASE_URL + "action=getCategory";

    //note in used
    public static final String ADD_DIALOG = BASE_URL + "action=addProductdialog";

    //Delete
    public static final String DELETE_CHAT = BASE_URL + "action=deleteChat";
    public static final String DELETE_NOTIFICATION = BASE_URL + "action=removeFromnotification";

    //Alert Module
    public static final String CREATE_ALERT = BASE_URL + "action=addAlert";
    public static final String GET_ALERT = BASE_URL + "action=getAlert";
    public static final String DELETE_ALERT = BASE_URL + "action=deleteAlert";
    public static final String ACTIVE_ALERT = BASE_URL + "action=changeAlertStatus";
    public static final String EDIT_ALERT = BASE_URL + "action=saveAlert";

    //Updates
    public static final String GET_CITY_API = BASE_URL + "action=getCity";


    public static String callGetRequest(Context mContext, String url) throws IOException {
        StringBuilder response = null;
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        con.setRequestMethod("GET");
        con.addRequestProperty("Cache-Control", "no-cache");
        con.addRequestProperty("Content-Type", "application/json");

        //Retrieving Data
        BufferedReader bufferResponse;
        if (con.getResponseCode() / 100 == 2) {
            bufferResponse = new BufferedReader(new InputStreamReader(con.getInputStream()));
        } else {
            bufferResponse = new BufferedReader(new InputStreamReader(con.getErrorStream()));
        }

        String line;
        String newResponse = "";
        while ((line = bufferResponse.readLine()) != null) {
            newResponse = newResponse + line;
        }

        bufferResponse.close();
        return newResponse;
    }

    public static String uploadMultipartData(Context context, String server_url) {
        MultipartEntityBuilder multipartEntity = MultipartEntityBuilder.create();
        File file = new File("image_path");
        multipartEntity.addBinaryBody("image", file, ContentType.create("image/jpeg"), AppConstants.APP_NAME + "_" + file.getName());
        return multiPost(context, server_url, multipartEntity);
    }

    public static String uploadUserPic(Context context, String server_url) {
        MultipartEntityBuilder multipartEntity = MultipartEntityBuilder.create();
        if (!AppConstants.profile_path.equals("")) {
            Log.e(context.getClass().getSimpleName(), "Multipart image path: " + AppConstants.profile_path);
            File file = new File(AppConstants.profile_path);
            multipartEntity.addBinaryBody("profile_img", file, ContentType.create("image/jpeg"), "Sfindit_" + file.getName());
        }
        return multiPost(context, server_url, multipartEntity);
    }

    public static String multiPost(Context context, String urlString, MultipartEntityBuilder entityBuilder) {

        try {
            HttpEntity entity = entityBuilder.build();
            HttpURLConnection conn = (HttpURLConnection) new URL(urlString).openConnection();
            conn.setUseCaches(false);
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Accept-Charset", "UTF-8");
            conn.setRequestProperty("Connection", "Keep-Alive");
            conn.setRequestProperty("Cache-Control", "no-cache");

            conn.setRequestProperty("Content-length", entity.getContentLength() + "");
            conn.setRequestProperty(entity.getContentType().getName(), entity.getContentType().getValue());
            OutputStream os = conn.getOutputStream();
            entity.writeTo(os);
            os.close();

            if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                return readStream(conn.getInputStream());
            }

        } catch (Exception e) {
            Log.e("MainActivity", "multipart post error " + e + "(" + urlString + ")");
        }
        return null;
    }

    private static String readStream(InputStream in) {
        BufferedReader reader = null;
        StringBuilder builder = new StringBuilder();
        try {
            reader = new BufferedReader(new InputStreamReader(in));
            String line = "";
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return builder.toString();
    }

}
