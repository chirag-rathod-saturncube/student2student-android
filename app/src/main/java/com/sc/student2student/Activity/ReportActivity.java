package com.sc.student2student.Activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.sc.student2student.R;
import com.sc.student2student.api.ReportUserApi;
import com.sc.student2student.common.AppConstants;
import com.sc.student2student.common.DialogManager;
import com.sc.student2student.common.Preferences;
import com.sc.student2student.databinding.ActivityReportBinding;
import com.squareup.picasso.Picasso;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class ReportActivity extends AppCompatActivity {
    ActivityReportBinding binding;
    boolean flag = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_report);
        setdata();
        binding.relToolbar.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        binding.tvOthers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        binding.scollview.scrollTo(0, binding.scollview.getBottom());
                    }
                }, 1000);

                if (!flag) {
                    binding.relMessage.setVisibility(View.VISIBLE);
                    flag = true;
                } else {
                    binding.relMessage.setVisibility(View.GONE);
                    flag = false;
                }

            }
        });


        binding.btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binding.edtDesc.getText().toString().equals("")) {
                    DialogManager.errorDialog(ReportActivity.this, "Error!", "Please enter Message");
                } else {
                    sendingReport(binding.edtDesc.getText().toString());
                }
            }
        });
    }

    private void setdata() {
        if (!AppConstants.chat_userprofile.equals("")) {
            Picasso.with(ReportActivity.this).load(AppConstants.chat_userprofile).transform(AppConstants.getTransformation(binding.imgProfile)).into(binding.imgProfile);
        }

        binding.tvUsername.setText(AppConstants.chat_username);
        binding.relToolbar.tvTitle.setText("Report");
    }


    public void SendReport(View view) {

        //  Toast.makeText(ReportActivity.this, ((TextView) view).getText().toString(), Toast.LENGTH_SHORT).show();

        if (view instanceof TextView) {
            sendingReport(((TextView) view).getText().toString());
        }
    }


    public void sendingReport(String message) {
        String param = "&user_id=" + AppConstants.chatMessage.getProd_user_id();
        param = param + "&reportUser=" + Preferences.getValue_String(ReportActivity.this, Preferences.USER_ID);
        try {
            param = param + "&msg=" + URLEncoder.encode(message, AppConstants.encodeType);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        param = param + "&date=" + new SimpleDateFormat("ddMMyyHHmmss").format(Calendar.getInstance().getTime());
        new ReportUserApi(param, ReportActivity.this, new ReportUserApi.OnResultReceived() {
            @Override
            public void onResult(String result) throws UnsupportedEncodingException {
                finish();
            }
        }, true);
    }
}
