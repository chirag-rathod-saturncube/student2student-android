package com.sc.student2student.adapter;

import com.sc.student2student.R;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sc.student2student.R;
import com.sc.student2student.common.AppConstants;
import com.sc.student2student.common.ExpandableRecyclerAdapter;
import com.sc.student2student.moduls.Faq;

import java.util.List;

public class FaqAdapter extends ExpandableRecyclerAdapter<Faq> {
    public static final int TYPE_PERSON = 1001;

    public FaqAdapter(Context context, List<Faq> listItem) {
        super(context);
        setItems(listItem);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case TYPE_HEADER:
                if (AppConstants.HELP_ACTION.equals(AppConstants.FAQ)) {
                    return new HeaderViewHolder(inflate(R.layout.item_header, parent));
                } else {
                    return new HeaderViewHolder(inflate(R.layout.help_item_header, parent));
                }
            case TYPE_PERSON:
                return new PersonViewHolder(inflate(R.layout.faq_item, parent));
            default:
                return new PersonViewHolder(inflate(R.layout.faq_item, parent));
        }
    }

    @Override
    public void onBindViewHolder(ExpandableRecyclerAdapter.ViewHolder holder, int position) {
        switch (getItemViewType(position)) {
            case TYPE_HEADER:
                ((HeaderViewHolder) holder).bind(position);
                break;
            case TYPE_PERSON:
            default:
                ((PersonViewHolder) holder).bind(position);
                break;
        }
    }

    public class HeaderViewHolder extends ExpandableRecyclerAdapter.HeaderViewHolder {
        TextView name;
        ImageView item_header_image;

        public HeaderViewHolder(View view) {
            super(view, (ImageView) view.findViewById(R.id.item_arrow));
            name = (TextView) view.findViewById(R.id.item_header_name);
            item_header_image = (ImageView) view.findViewById(R.id.item_header_image);
        }

        public void bind(int position) {
            super.bind(position);
            name.setText(visibleItems.get(position).Text);
            if (AppConstants.HELP_ACTION.equals(AppConstants.FAQ)) {
                item_header_image.setVisibility(View.GONE);
            } else {
                item_header_image.setVisibility(View.VISIBLE);
                item_header_image.setImageResource(visibleItems.get(position).Icon);
            }
        }
    }

    public class PersonViewHolder extends ExpandableRecyclerAdapter.ViewHolder {
        TextView tv_name;

        public PersonViewHolder(View view) {
            super(view);
            this.tv_name = (TextView) view.findViewById(R.id.tv_name);
        }

        public void bind(final int position) {
            if (!visibleItems.get(position).times.getAnswer().equals("")) {
                tv_name.setText(visibleItems.get(position).times.getAnswer());
            } else {
                tv_name.setText("");
            }

            tv_name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });

        }
    }

    /*private List<PeopleListItem> getSampleItems() {
        List<PeopleListItem> items = new ArrayList<>();

        items.add(new PeopleListItem("Friends"));
        items.add(new PeopleListItem("Bill", "Smith"));
        items.add(new PeopleListItem("John", "Doe"));
        items.add(new PeopleListItem("Frank", "Hall"));
        items.add(new PeopleListItem("Sue", "West"));
        items.add(new PeopleListItem("Family"));
        items.add(new PeopleListItem("Drew", "Smith"));
        items.add(new PeopleListItem("Chris", "Doe"));
        items.add(new PeopleListItem("Alex", "Hall"));
        items.add(new PeopleListItem("Associates"));
        items.add(new PeopleListItem("John", "Jones"));
        items.add(new PeopleListItem("Ed", "Smith"));
        items.add(new PeopleListItem("Jane", "Hall"));
        items.add(new PeopleListItem("Tim", "Lake"));
        items.add(new PeopleListItem("Colleagues"));
        items.add(new PeopleListItem("Carol", "Jones"));
        items.add(new PeopleListItem("Alex", "Smith"));
        items.add(new PeopleListItem("Kristin", "Hall"));
        items.add(new PeopleListItem("Pete", "Lake"));

        return items;
    }*/
}
