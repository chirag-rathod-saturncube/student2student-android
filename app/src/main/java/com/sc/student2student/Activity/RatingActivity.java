package com.sc.student2student.Activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.sc.student2student.R;
import com.sc.student2student.api.AddRatingApi;
import com.sc.student2student.common.AppConstants;
import com.sc.student2student.common.Preferences;
import com.sc.student2student.databinding.ActivityRatingBinding;
import com.squareup.picasso.Picasso;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class RatingActivity extends AppCompatActivity {


    ActivityRatingBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_rating);
        initView();
        setdata();

        binding.relToolbar.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        binding.btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String param = "&user_id=" + AppConstants.notification.getNot_by_user_id();
                param = param + "&rating=" + binding.ratingBar.getRating();
                param = param + "&log_user_id=" + Preferences.getValue_String(RatingActivity.this, Preferences.USER_ID);
                try {
                    param = param + "&title=" + URLEncoder.encode(binding.edtDesc.getText().toString(), AppConstants.encodeType);
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                param = param + "&date=" + new SimpleDateFormat("ddMMyyHHmmss").format(Calendar.getInstance().getTime());
                new AddRatingApi(param, RatingActivity.this, new AddRatingApi.OnResultReceived() {
                    @Override
                    public void onResult(String result) throws UnsupportedEncodingException {
                        AppConstants.notificationList.get(AppConstants.pos).setNf_type("3");
                        AppConstants.notificationList.get(AppConstants.pos).setRat_provide("1");
                        finish();
                    }
                }, true);

                /*if (binding.edtDesc.getText().toString().equals("")) {
                    DialogManager.errorDialog(RatingActivity.this, "Error!", "Please enter description");
                } else {
                   //remove desc validation
                }*/
            }
        });
    }

    private void initView() {
        binding.relToolbar.tvTitle.setText("Review");
    }

    private void setdata() {
        if (!AppConstants.notification.getProduct_image().equals("")) {
            Picasso.with(RatingActivity.this).load(AppConstants.notification.getProduct_image()).transform(AppConstants.getTransformation(binding.imgProduct)).into(binding.imgProduct);
        }
        if (!AppConstants.notification.getProfile_pic().equals("")) {
            Picasso.with(RatingActivity.this).load(AppConstants.notification.getProfile_pic()).transform(AppConstants.getTransformation(binding.imgProfile)).into(binding.imgProfile);
        }

        binding.tvProductname.setText(AppConstants.notification.getProduct_name());
        binding.tvUsername.setText(AppConstants.notification.getName());

        binding.edtDesc.setText(AppConstants.user_review);
        binding.ratingBar.setRating(Float.parseFloat(AppConstants.user_rating));
    }
}
