package com.sc.student2student.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.sc.student2student.R;
import com.sc.student2student.api.DeleteProductImageApi;
import com.sc.student2student.common.AppConstants;
import com.sc.student2student.fragement.Slider_fragement;
import com.sc.student2student.moduls.Images;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;


/**
 * Created by pc4 on 5/30/2016.
 */
public class PhotoAdapter extends RecyclerView.Adapter<PhotoAdapter.CustomViewHolder> {
    ArrayList<Images> infos = null;
    OnResultReceived onResultReceived;
    private ArrayList<Images> arrayList;
    private Context mContext;
    private String TAG = getClass().getSimpleName();

    public PhotoAdapter(Context context, ArrayList<Images> list, OnResultReceived OnResultReceived) {
        this.infos = list;
        this.arrayList = new ArrayList<>();
        this.arrayList.addAll(infos);
        this.mContext = context;
        this.onResultReceived = OnResultReceived;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_photo, null);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final CustomViewHolder customViewHolder, final int pos) {
        final Images list = infos.get(pos);
        customViewHolder.progress_product.setVisibility(View.VISIBLE);

        Picasso.with(mContext).load(list.getImage()).transform(AppConstants.getTransformation(customViewHolder.img_photo1)).into(customViewHolder.img_photo1, new Callback() {
            @Override
            public void onSuccess() {
                customViewHolder.progress_product.setVisibility(View.GONE);
            }

            @Override
            public void onError() {
                customViewHolder.progress_product.setVisibility(View.GONE);
            }
        });
        customViewHolder.img_remove1.setVisibility(View.VISIBLE);

        customViewHolder.img_photo1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Slider_fragement slider_fragement = new Slider_fragement(mContext, true, pos, infos);
            }
        });


        customViewHolder.img_remove1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String param = "&img_id=" + list.getImg_id();
                param = param + "&date=" + new SimpleDateFormat("ddMMyyHHmmss").format(Calendar.getInstance().getTime());
                new DeleteProductImageApi(param, mContext, new DeleteProductImageApi.OnResultReceived() {
                    @Override
                    public void onResult(String result) throws UnsupportedEncodingException {
                        infos.remove(pos);
                        notifyDataSetChanged();
                    }
                }, true);
            }
        });


    }

    public ArrayList<Images> getFiles() {
        return infos;
    }

    @Override
    public int getItemCount() {
        return (null != infos ? infos.size() : 0);
    }

    public interface OnResultReceived {
        public void onResult(int i);
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {

        ImageView img_photo1, img_remove1;
        ProgressBar progress_product;

        public CustomViewHolder(View view) {
            super(view);

            this.img_photo1 = (ImageView) view.findViewById(R.id.img_pic);
            this.img_remove1 = (ImageView) view.findViewById(R.id.img_remove);
            this.progress_product = (ProgressBar) view.findViewById(R.id.progress_product);


        }
    }
}

