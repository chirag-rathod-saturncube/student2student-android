package com.sc.student2student.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sc.student2student.Activity.SellProductActivity;
import com.sc.student2student.common.AppConstants;
import com.sc.student2student.databinding.ItemCategoryBinding;
import com.sc.student2student.moduls.Category;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by pc4 on 5/30/2016.
 */
public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.CustomViewHolder> {
    List<Category> infos = null;
    int layout;
    ItemCategoryBinding categoryBinding;
    private ArrayList<Category> arrayList;
    private Context mContext;
    private String TAG = getClass().getSimpleName();

    public CategoryAdapter(Context context, List<Category> list, int layout) {
        this.infos = list;
        this.arrayList = new ArrayList<>();
        this.arrayList.addAll(infos);
        this.mContext = context;
        this.layout = layout;
    }


    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(layout, null);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final CustomViewHolder customViewHolder, final int pos) {
        final Category list = infos.get(pos);
        categoryBinding.tvCategory.setText(list.getCategory_name());

        if (!list.getCategory_image().equals("")) {
            Picasso.with(mContext).load(list.getCategory_image()).transform(AppConstants.getTransformation(categoryBinding.categoryImage)).into(categoryBinding.categoryImage);
        }

        categoryBinding.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.category = list;
                mContext.startActivity(new Intent(mContext, SellProductActivity.class));
            }
        });
    }


    @Override
    public int getItemCount() {
        return (null != infos ? infos.size() : 0);
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {


        public CustomViewHolder(View view) {
            super(view);
            categoryBinding = ItemCategoryBinding.bind(view);
        }
    }
}

