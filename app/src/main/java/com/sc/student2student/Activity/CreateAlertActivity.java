package com.sc.student2student.Activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.SeekBar;

import com.android.volley.VolleyError;
import com.sc.student2student.R;
import com.sc.student2student.api.CreateAlertApi;
import com.sc.student2student.api.EditAlertApi;
import com.sc.student2student.api.GetAlertCategoryApi;
import com.sc.student2student.api.GetAlertSubCategoryApi;
import com.sc.student2student.common.AppConstants;
import com.sc.student2student.common.DialogManager;
import com.sc.student2student.common.Preferences;
import com.sc.student2student.databinding.ActivityCreateAlertBinding;
import com.sc.student2student.moduls.Cat;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

import static com.sc.student2student.common.AppConstants.alertArrayList;
import static com.sc.student2student.common.AppConstants.alertArrayListposition;
import static com.sc.student2student.common.AppConstants.alertCatArrayList;
import static com.sc.student2student.common.AppConstants.alertSubCatArrayList;

public class CreateAlertActivity extends AppCompatActivity {

    ActivityCreateAlertBinding binding;
    ArrayAdapter<Cat> catArrayAdapter;
    ArrayAdapter<Cat> subcatArrayAdapter;
    ArrayAdapter<String> selectTypeAdapter;
    ArrayList<String> arrayList = new ArrayList<String>();
    ArrayList<String> catarrayList = new ArrayList<String>();
    ArrayList<String> subcatarrayList = new ArrayList<String>();
    String TAG = getClass().getSimpleName();
    String[] stringsArray1;
    int type = 1;
    String cat_id;
    String sub_cat_id;
    private int productPosition;

    public int getProductPosition() {
        return productPosition;
    }

    public void setProductPosition(int productPosition) {
        this.productPosition = productPosition;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_create_alert);
        initView();
        setPlaceHolderSpinner();
        if (AppConstants.editAlert) {
            edit();
        }

        binding.toolbar.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        binding.spnChoose.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    case 0:
                        setProductPosition(0);
                        break;
                    case 1:
                        type = i;
                        getCategories("" + i, "" + 0);
                        break;
                    case 2:
                        type = i;
                        getCategories(i + "", "" + 0);
                        break;
                }
//                Log.e(TAG, "Select" + stringsArray1[i]);
//                if (stringsArray1[0].equals("Select One")){
//
//
//                }else if (stringsArray1[i].equals("Product")) {
//                    type = 1;
//                    getCategories(type + "", 0 + "");
//
//                } else {
//                    type = 2;
//                    getCategories(type + "", 0 + "");
//                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        binding.spnCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 0) {

                } else {
                    Cat cat = (Cat) binding.spnCategory.getSelectedItem();
                    Log.e("TAG", "CATEGORY_ID : " + cat.getCat_id());
                    cat_id = cat.getCat_id();
                    //on respones call subcategoryApi
                    getSubCategories(type + "", cat_id);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        binding.spnSubCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 0) {

                } else {
                    Cat cat = (Cat) binding.spnSubCategory.getSelectedItem();
                    Log.e("TAG", "SUB_CATEGORY_ID : " + cat.getCat_id());
                    sub_cat_id = cat.getCat_id();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        binding.btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (AppConstants.editAlert) {
                    if (isValidate()) {
                        try {
                            String envelope = "&alert_id=" + AppConstants.alertArrayList.get(AppConstants.alertArrayListposition).getAlert_id();

                            envelope = envelope + "&alert_name=" + URLEncoder.encode(AppConstants.alertArrayList.get(AppConstants.alertArrayListposition).getAlert_name(), AppConstants.encodeType);

                            envelope = envelope + "&category=" + cat_id;
                            if (binding.spnSubCategory.getSelectedItemPosition() == 0) {
                                envelope = envelope + "&sub_category=" + 0;
                            } else {
                                envelope = envelope + "&sub_category=" + sub_cat_id;
                            }
                            envelope = envelope + "&user_id=" + Preferences.getValue_String(CreateAlertActivity.this, Preferences.USER_ID);
                            envelope = envelope + "&distance=" + binding.sb.getProgress();

                            new EditAlertApi(envelope, CreateAlertActivity.this, new EditAlertApi.OnResultReceived() {
                                @Override
                                public void onResult(String result) throws UnsupportedEncodingException {

                                }
                            }, true);
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }

                } else {
                    if (isValidate()) {
                        String envelope = "&alert_name=" + binding.edtAlertName.getText().toString();
                        envelope = envelope + "&category=" + cat_id;
                        if (binding.spnSubCategory.getSelectedItemPosition() == 0) {
                            envelope = envelope + "&sub_category=" + 0;
                        } else {
                            envelope = envelope + "&sub_category=" + sub_cat_id;
                        }
                        envelope = envelope + "&user_id=" + Preferences.getValue_String(CreateAlertActivity.this, Preferences.USER_ID);
                        envelope = envelope + "&distance=" + binding.sb.getProgress();
                        new CreateAlertApi(envelope, CreateAlertActivity.this, new CreateAlertApi.OnResultReceived() {
                            @Override
                            public void onResult(String result) throws UnsupportedEncodingException {

                            }
                        }, true);
                    }
                }
            }
        });


        binding.sb.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener()

        {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                // Display the current progress of SeekBar
                binding.lblMiles.setText("" + i + " km");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });


    }

    private void edit() {
        binding.toolbar.tvTitle.setText("Edit Alert");
        binding.btnSave.setText("Save");

        //Set Edit Data
        Log.e(TAG, "TYPE : " + AppConstants.alertArrayList.get(AppConstants.alertArrayListposition).getType());
        Log.e(TAG, "AppConstants.alertArrayListposition : " + AppConstants.alertArrayListposition);
        if (AppConstants.alertArrayList.get(AppConstants.alertArrayListposition).getType().equals("1")) {
            binding.spnChoose.setSelection(1);
            Log.e(TAG, "binding.spnChoose.setSelection(1) : " + "if");
        } else {
            Log.e(TAG, "binding.spnChoose.setSelection(2) : " + "else");
            binding.spnChoose.setSelection(2);
        }
        binding.edtAlertName.setText(alertArrayList.get(alertArrayListposition).getAlert_name());
        binding.sb.setProgress(Integer.parseInt(String.valueOf(alertArrayList.get(alertArrayListposition).getDistance())));
        Log.e(TAG, "get distance : " + alertArrayList.get(alertArrayListposition).getDistance());
        binding.lblMiles.setText(alertArrayList.get(alertArrayListposition).getDistance());
    }

    private void setPlaceHolderSpinner() {

        //Set PlaceHolder For Category Spinner
        arrayList.clear();
        arrayList.add("Please Select");
        arrayList.add("Product");
        arrayList.add("Service");
        selectTypeAdapter = new ArrayAdapter<String>(CreateAlertActivity.this, R.layout.item_spinner_text_layout, arrayList) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    return false;
                } else {
                    return true;
                }
            }
        };


        selectTypeAdapter.setDropDownViewResource(R.layout.spinner_item2);
        binding.spnChoose.setAdapter(selectTypeAdapter);
        selectTypeAdapter.notifyDataSetChanged();

        //Set PlaceHolder For Category Spinner
        catarrayList.clear();
        catarrayList.add("Please select category");
        selectTypeAdapter = new ArrayAdapter<String>(CreateAlertActivity.this, R.layout.item_spinner_text_layout, catarrayList);
        selectTypeAdapter.setDropDownViewResource(R.layout.spinner_item2);
        binding.spnCategory.setAdapter(selectTypeAdapter);
        selectTypeAdapter.notifyDataSetChanged();

        //Set PlaceHolder For SubCategory Spinner
        subcatarrayList.clear();
        subcatarrayList.add("Please select subcategory");
        selectTypeAdapter = new ArrayAdapter<String>(CreateAlertActivity.this, R.layout.item_spinner_text_layout, subcatarrayList);
        selectTypeAdapter.setDropDownViewResource(R.layout.spinner_item2);
        binding.spnSubCategory.setAdapter(selectTypeAdapter);
        selectTypeAdapter.notifyDataSetChanged();

    }

    private void initView() {

        binding.toolbar.tvTitle.setText("Create Alert");

    }

    private void getCategories(String type, String parent_id) {
        String envelope = "&type=" + type;
        envelope = envelope + "&parent_id=" + 0;
        new GetAlertCategoryApi(envelope, CreateAlertActivity.this, new GetAlertCategoryApi.OnResultReceived() {
            @Override
            public void onResult(String result) throws UnsupportedEncodingException {
                //spinner set

                catArrayAdapter = new ArrayAdapter<Cat>(CreateAlertActivity.this, R.layout.item_spinner_text_layout, alertCatArrayList) {
                    @Override
                    public boolean isEnabled(int position) {
                        if (position == 0) {
                            return false;
                        } else {
                            return true;
                        }
                    }
                };

                catArrayAdapter.setDropDownViewResource(R.layout.spinner_item2);
                binding.spnCategory.setAdapter(catArrayAdapter);
                catArrayAdapter.notifyDataSetChanged();

                Log.e(TAG, "Category : Calling Method : ");
                if (AppConstants.editAlert) {
                    Log.e(TAG, "Category : Calling Method : " + " if ");
                    Log.e(TAG, "Category : Calling Method : " + " if " + AppConstants.alertArrayList.get(AppConstants.alertArrayListposition).getMain_category_name());
                    binding.spnCategory.setSelection(getCategoryPosition(AppConstants.alertArrayList.get(AppConstants.alertArrayListposition).getMain_category_name()));
                }
//                if (AppConstants.editAlert) {
//                    for (int i = 0; i < AppConstants.catArrayList.size(); i++) {
//                        if (AppConstants.catArrayList.get(i).getCategory_name().equals(alertArrayList.get(alertArrayListposition).getMain_category_name())) {
//                            binding.spnCategory.setSelection(i);
//                            Log.e(TAG, "Spn_Category : " + i);
//                        }
//
//                    }
//                }

            }

            @Override
            public void onFailed(VolleyError result) throws UnsupportedEncodingException {

            }
        }, false);
    }


    private void getSubCategories(String type, String parent_id) {
        String param = "&type=" + type;
        param = param + "&parent_id=" + parent_id;

        new GetAlertSubCategoryApi(param, CreateAlertActivity.this, new GetAlertSubCategoryApi.OnResultReceived() {
            @Override
            public void onResult(String result) throws UnsupportedEncodingException {

                subcatArrayAdapter = new ArrayAdapter<Cat>(CreateAlertActivity.this, R.layout.item_spinner_text_layout, alertSubCatArrayList);

                subcatArrayAdapter.setDropDownViewResource(R.layout.spinner_item2);
                binding.spnSubCategory.setAdapter(subcatArrayAdapter);
                subcatArrayAdapter.notifyDataSetChanged();
                Log.e(TAG, "SubCategory : Calling Method  ");
                if (AppConstants.editAlert) {
                    Log.e(TAG, "Category : Calling Method : " + " if ");
                    Log.e(TAG, "Category : Calling Method : " + " if " + AppConstants.alertArrayList.get(AppConstants.alertArrayListposition).getSub_category_name());
                    binding.spnSubCategory.setSelection(getSubCategoryPosition(AppConstants.alertArrayList.get(AppConstants.alertArrayListposition).getSub_category_name()));
                }


            }

            @Override
            public void onFailed(VolleyError result) throws UnsupportedEncodingException {

            }
        }, false);

    }

    private boolean isValidate() {
        boolean flagValidate = true;
        String validateMsg = "";

        if (binding.spnChoose.getSelectedItemPosition() == 0) {
            validateMsg = "Please select Product or Service";
            flagValidate = false;
        } else if (binding.spnCategory.getSelectedItemPosition() == 0) {
            validateMsg = "Please select Category";
            flagValidate = false;
        }
        if (flagValidate) {
            return true;
        } else {
            DialogManager.errorDialog(CreateAlertActivity.this, getString(R.string.validation_title), validateMsg);
            return false;
        }
    }

    private int getCategoryPosition(String categoryName) {
        Log.e(TAG, "getCategoryPosition(){...} : ");
        Log.e(TAG, "categoryName(){...} : " + categoryName);
        Log.e(TAG, "alertCatArrayList.size : " + AppConstants.alertCatArrayList.size());
        int pos = 0;
        for (int i = 0; i < AppConstants.alertCatArrayList.size(); i++) {
            if (categoryName.equals(AppConstants.alertCatArrayList.get(i).getCategory_name())) {
                pos = i;
                Log.e(TAG, "position(){...} : " + pos);
                Log.e(TAG, "AppConstants.alertCatArrayList.get(i).getCategory_name()(){...} : " + AppConstants.alertCatArrayList.get(i).getCategory_name());
                break;
            }
        }
        return pos;
    }

    private int getSubCategoryPosition(String subcategoryName) {
        Log.e(TAG, "getSubCategoryPosition(){...} : ");
        Log.e(TAG, "subcategoryName(){...} : " + subcategoryName);
        Log.e(TAG, "alertSubCatArrayList.size : " + AppConstants.alertSubCatArrayList.size());
        int pos = 0;
        for (int i = 0; i < AppConstants.alertSubCatArrayList.size(); i++) {
            if (subcategoryName.equals(AppConstants.alertSubCatArrayList.get(i).getCategory_name())) {
                pos = i;
                Log.e(TAG, "position(){...} : " + pos);
                Log.e(TAG, "AppConstants.alertSubCatArrayList.get(i).getCategory_name()(){...} : " + AppConstants.alertSubCatArrayList.get(i).getCategory_name());
                break;
            }
        }
        return pos;
    }

    private int getPosition(String categoryName, ArrayList<Cat> arrayList) {
        int pos = 0;
        for (int i = 0; i < arrayList.size(); i++) {
            if (categoryName.equals(arrayList.get(i).getCategory_name())) {
                pos = i;
                break;
            }
        }
        return pos;
    }

}
