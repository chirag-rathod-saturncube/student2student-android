package com.sc.student2student.moduls;

/**
 * Created by Saturncube-5 on 17-Jul-17.
 */

public class Reviews {


    String r_id, ID, rating, log_ID, title, name;

    public String getR_id() {
        return r_id;
    }

    public void setR_id(String r_id) {
        this.r_id = r_id;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getLog_ID() {
        return log_ID;
    }

    public void setLog_ID(String log_ID) {
        this.log_ID = log_ID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
