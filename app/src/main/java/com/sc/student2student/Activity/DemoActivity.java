package com.sc.student2student.Activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;

import com.sc.student2student.R;
import com.sc.student2student.adapter.DemoAdapter;
import com.sc.student2student.common.AppConstants;
import com.sc.student2student.databinding.ActivityDemoBinding;
import com.sc.student2student.moduls.Demo;

public class DemoActivity extends AppCompatActivity {
    ActivityDemoBinding binding;

    GridLayoutManager gridLayoutManager;
    DemoAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_demo);
        iniView();

        AppConstants.demoArrayList.clear();
        for (int i = 0; i < 500; i++) {
            Demo demo = new Demo();
            demo.setId("" + i);
            demo.setName(AppConstants.getSampleName(DemoActivity.this));
            demo.setImageUrl(AppConstants.getSampleImagesUrl(DemoActivity.this));
            AppConstants.demoArrayList.add(demo);
        }

        if (AppConstants.demoArrayList.size() > 0) {
            adapter = new DemoAdapter(DemoActivity.this, AppConstants.demoArrayList);
            binding.recyclerView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }


    }

    private void iniView() {


        gridLayoutManager = new GridLayoutManager(DemoActivity.this, 2, LinearLayoutManager.VERTICAL, false);
        binding.recyclerView.setLayoutManager(gridLayoutManager);
    }

}
