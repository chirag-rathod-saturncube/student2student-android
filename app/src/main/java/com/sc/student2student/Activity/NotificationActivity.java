package com.sc.student2student.Activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.sc.student2student.R;
import com.sc.student2student.adapter.NotificationAdapter;
import com.sc.student2student.api.GetNotification;
import com.sc.student2student.common.AppConstants;
import com.sc.student2student.common.Preferences;
import com.sc.student2student.databinding.ActivityNotificationBinding;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class NotificationActivity extends AppCompatActivity {
    ActivityNotificationBinding binding;
    LinearLayoutManager layoutManager;
    NotificationAdapter adapter;
    boolean flagReopen = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                //Cry about not being clicked on
                flagReopen = false;
            } else if (extras.getBoolean("NotiClick")) {
                //Do your stuff here mate :)
                flagReopen = true;
            }

        }


        binding = DataBindingUtil.setContentView(this, R.layout.activity_notification);
        initView();


        getNotification();


        binding.relToolbar.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (adapter != null) {
            adapter.notifyDataSetChanged();
            getNotification();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        AppConstants.isNotificationScreenOpen = false;
    }


    @Override
    protected void onStart() {
        super.onStart();
        AppConstants.isNotificationScreenOpen = true;
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (flagReopen) {
            Intent intent1 = new Intent(NotificationActivity.this, HomeActivity.class);
            intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent1);
            finish();
        } else {
            finish();
        }
    }

    private void initView() {
        // AppConstants.notificationflag = true;
        layoutManager = new LinearLayoutManager(this);
        binding.recyclerView.setLayoutManager(layoutManager);
        binding.relToolbar.tvTitle.setText("Notifications");
    }

    public void getNotification() {
        String param = "&user_id=" + Preferences.getValue_String(NotificationActivity.this, Preferences.USER_ID);
        param = param + "&date=" + new SimpleDateFormat("ddMMyyHHmmss").format(Calendar.getInstance().getTime());
        new GetNotification(param, NotificationActivity.this, new GetNotification.OnResultReceived() {
            @Override
            public void onResult(String result) throws UnsupportedEncodingException {
                if (AppConstants.notificationList.size() > 0) {
                    binding.tvNoProduct.setVisibility(View.GONE);
                    adapter = new NotificationAdapter(NotificationActivity.this, AppConstants.notificationList, R.layout.item_notification);
                    binding.recyclerView.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                } else {
                    binding.tvNoProduct.setVisibility(View.VISIBLE);
                }
            }
        }, true);
    }


}
