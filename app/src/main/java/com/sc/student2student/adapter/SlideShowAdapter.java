package com.sc.student2student.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.bogdwellers.pinchtozoom.ImageMatrixTouchHandler;
import com.sc.student2student.R;
import com.sc.student2student.common.AppConstants;
import com.sc.student2student.fragement.Slider_fragement;
import com.sc.student2student.moduls.Images;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


/**
 * Created by pc4 on 10/16/2015.
 */
public class SlideShowAdapter extends PagerAdapter {
    Context context;
    int resource;
    ArrayList<Images> list;
    boolean flag;
    private LayoutInflater inflater;


    public SlideShowAdapter(Context context, int resource, ArrayList<Images> list, boolean flag) {
        this.context = context;
        this.resource = resource;
        this.list = list;
        this.flag = flag;
    }

    @Override
    public int getCount() {

        //Log.e("SIZE", "" + CurrentUser.images_to_view.size());
        return list.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewLayout = inflater.inflate(resource, container, false);
        final ImageView imageView = (ImageView) viewLayout.findViewById(R.id.image_in_view_images);
        final ProgressBar progressBar = (ProgressBar) viewLayout.findViewById(R.id.progress_product);
        if (flag) {
            imageView.setOnTouchListener(null);
        } else {
            /*new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    ImageMatrixTouchHandler touchHandler = new ImageMatrixTouchHandler(context);
                    touchHandler.animateZoomOutToFit(200);
                    imageView.setOnTouchListener(new ImageMatrixTouchHandler(context));
                }
            }, 1000);*/
            imageView.setOnTouchListener(new ImageMatrixTouchHandler(context));
        }
        Log.e("TAG", "out");
        Log.e("TAG", list.get(position).getImage());
        Log.e("TAG", list.get(position).getImage_small());
        if (!list.get(position).getImage_small().equals("")) {
            Log.e("TAG", "getImage_small");
            if (flag) {
                imageView.setPadding(0, 0, 0, 0);
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            }

            Picasso.with(context)
                    .load(list.get(position).getImage_small())
                    .transform(AppConstants.getTransformation(imageView))
                    .into(imageView, new Callback() {
                        @Override
                        public void onSuccess() {


                            Log.e("TAG", "Success1");

                            progressBar.setVisibility(View.GONE);
                            if (!list.get(position).getImage_medium().equals("")) {
                                Log.e("TAG", "getImage_medium");
                                if (flag) {
                                    imageView.setPadding(0, 0, 0, 0);
                                    imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                                }

                                Picasso.with(context)
                                        .load(list.get(position).getImage_medium())
                                        .transform(AppConstants.getTransformation(imageView))
                                        .into(imageView, new Callback() {
                                            @Override
                                            public void onSuccess() {

                                                Log.e("TAG", "Success2");

                                                progressBar.setVisibility(View.GONE);
                                                if (!list.get(position).getImage().equals("")) {
                                                    Log.e("TAG", "getImage");
                                                    if (flag) {
                                                        imageView.setPadding(0, 0, 0, 0);
                                                        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                                                    }

                                                    Picasso.with(context)
                                                            .load(list.get(position).getImage())
                                                            .transform(AppConstants.getTransformation(imageView))
                                                            .into(imageView, new Callback() {
                                                                @Override
                                                                public void onSuccess() {

                                                                    Log.e("TAG", "Success3");

                                                                    progressBar.setVisibility(View.GONE);
                                                                }

                                                                @Override
                                                                public void onError() {
                                                                    progressBar.setVisibility(View.GONE);
                                                                }
                                                            });

                                                } else {
                                                    progressBar.setVisibility(View.GONE);
                                                    imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                                                    imageView.setImageResource(R.drawable.noimage);
                                                }
                                            }

                                            @Override
                                            public void onError() {
                                                progressBar.setVisibility(View.GONE);
                                            }
                                        });

                            } else {
                                progressBar.setVisibility(View.GONE);
                                imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                                imageView.setImageResource(R.drawable.noimage);
                            }
                        }

                        @Override
                        public void onError() {
                            progressBar.setVisibility(View.GONE);
                        }
                    });

        } else {
            progressBar.setVisibility(View.GONE);
            imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            imageView.setImageResource(R.drawable.noimage);
        }

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (flag) {
                    Slider_fragement slider_fragement = new Slider_fragement(context, true, position, list);
                }
            }
        });


        ((ViewPager) container).addView(viewLayout, 0);

        return viewLayout;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((RelativeLayout) object);
    }
}