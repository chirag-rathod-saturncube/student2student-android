package com.sc.student2student.api;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.sc.student2student.R;
import com.sc.student2student.common.AppConstants;
import com.sc.student2student.common.DialogManager;
import com.sc.student2student.common.LoadingDialog;
import com.sc.student2student.common.WebServices;
import com.sc.student2student.moduls.City;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Saturncube-5 on 24-Jan-18.
 */

public class GetCityApi {

    private String url = WebServices.GET_CITY_API;
    private String TAG = getClass().getSimpleName();
    private Context context;
    private OnResultReceived onResultReceived;
    private boolean flagprogress;
    private LoadingDialog loadingDialog;
    private RequestQueue queue;

    public GetCityApi(Context context, OnResultReceived onResultReceived, boolean flagprogress) {
        this.context = context;
        this.onResultReceived = onResultReceived;
        this.flagprogress = flagprogress;
        queue = Volley.newRequestQueue(context);
        getResponse();
    }

    public interface OnResultReceived {
        public void OnResult(String result);
    }

    public void show() {
        if (flagprogress) {
            loadingDialog = new LoadingDialog(context);
        }
    }

    public void hide() {
        if (flagprogress) {
            loadingDialog.hide();
        }
    }

    private void getResponse() {
        show();
        Log.e(TAG, "URL : " + url);
        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e(TAG, "Response : " + response.toString());
                try {
                    if (response.has("success")) {
                        if (response.getString("success").equals("0")) {
                            hide();
                            String msg = response.getString("msg");
                            DialogManager.errorDialog(context, "Error!", msg);
                        } else if (response.getString("success").equals("1")) {
//                            String msg = response.getString("msg");
//                            DialogManager.errorDialogfinish(context, "", msg);
                            AppConstants.cityArrayList.clear();
                            City header = new City();
                            header.setCity_name("Please select City");
                            AppConstants.cityArrayList.add(header);
                            JSONArray array = response.getJSONArray("result");
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject object = array.getJSONObject(i);
                                City city = new City();
                                city.setCity_id(object.optString("city_id"));
                                city.setCity_name(object.optString("city_name"));
                                city.setLatitude(object.optString("latitude"));
                                city.setLongitude(object.optString("longitude"));
                                AppConstants.cityArrayList.add(city);
                            }
                            if (onResultReceived != null) {
                                onResultReceived.OnResult(response.toString());
                            }
                            hide();
                        } else {
                            hide();
                            Toast.makeText(context, context.getString(R.string.something_was_wrong_with_api), Toast.LENGTH_LONG).show();
                        }
                    } else {
                        hide();
                        Toast.makeText(context, context.getString(R.string.something_was_wrong_with_api), Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    hide();
                    Log.e(TAG, "Line " + Thread.currentThread().getStackTrace()[2].getLineNumber() + " Error:... " + e.getMessage());
                }
            }
        }, new Response.ErrorListener()

        {
            @Override
            public void onErrorResponse(VolleyError error) {
                hide();
                Log.e(TAG, "error : " + error.getMessage());
                VolleyLog.e("Error.Response", "");
            }
        })

        {
            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Cache-Control", "no-cache");
                return params;
            }
        };
        postRequest.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(postRequest);
    }
}
