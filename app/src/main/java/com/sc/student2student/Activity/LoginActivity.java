package com.sc.student2student.Activity;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.Spanned;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.iid.FirebaseInstanceId;
import com.sc.student2student.R;
import com.sc.student2student.api.LoginApi;
import com.sc.student2student.common.AppConstants;
import com.sc.student2student.common.DialogManager;
import com.sc.student2student.common.MyTagHandler;
import com.sc.student2student.common.Preferences;
import com.sc.student2student.databinding.ActivityLoginBinding;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;

public class LoginActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {
    private static final int RC_SIGN_IN = 9001;
    ActivityLoginBinding binding;
    CallbackManager callbackManager;
    private String TAG = getClass().getSimpleName();
    private GoogleApiClient mGoogleApiClient;


    @SuppressWarnings("deprecation")
    public static Spanned fromHtml(String html) {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(html, null, new MyTagHandler());
        } else {
            result = Html.fromHtml(html, null, new MyTagHandler());
        }
        return result;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(LoginActivity.this);
        resetPreferences();

        callbackManager = CallbackManager.Factory.create();
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);

        printKeyHash();
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult result) {
                        AccessToken token = result.getAccessToken();
                        final Profile profile = Profile.getCurrentProfile();
                        Preferences.setValue(LoginActivity.this, Preferences.USER_NAME, "");
                        Preferences.setValue(LoginActivity.this, Preferences.USER_PIC, "");
                        if (profile != null) {
                            Log.e(TAG, "User pic: " + profile.getProfilePictureUri(900, 900));
                            Log.e(TAG, "User Link: " + profile.getLinkUri());
                            Preferences.setValue(LoginActivity.this, Preferences.USER_PIC, "" + profile.getProfilePictureUri(900, 900));
                        } else {
                            Preferences.setValue(LoginActivity.this, Preferences.USER_PIC, "");
                        }
                        GraphRequest request = GraphRequest.newMeRequest(
                                result.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(
                                            JSONObject object,
                                            GraphResponse response) {
                                        Log.e("User", object + "");
                                        Log.e(TAG, "Graph Response: " + response);
                                        Log.e(TAG, "User Name: " + object.optString("name"));
                                        Log.e(TAG, "Email: " + object.optString("email"));
                                        Log.e(TAG, "Gender: " + object.optString("gender"));
                                        Log.e(TAG, "App Id: " + object.optString("id"));

                                        if (object.has("email")) {
                                            if (!object.optString("email").equals("")) {
                                                Preferences.setValue(LoginActivity.this, Preferences.USER_NAME, object.optString("name"));

                                                try {
                                                    facebookLogin(object.optString("email"));
                                                } catch (UnsupportedEncodingException e) {
                                                    e.printStackTrace();
                                                }


                                            } else {
                                                Toast.makeText(LoginActivity.this, "Not getting email", Toast.LENGTH_LONG).show();
                                            }
                                        } else {
                                            Toast.makeText(LoginActivity.this, "Not getting email", Toast.LENGTH_LONG).show();
                                        }
                                    }
                                });

                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,name,email,gender, birthday");
                        request.setParameters(parameters);
                        request.executeAsync();
                    }

                    @Override
                    public void onError(FacebookException error) {
                        Log.e("Error", error.toString());
                    }

                    @Override
                    public void onCancel() {
                        Log.e("Cancel", "Cancel");
                    }
                });

        binding.edtPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    if (isValidate()) {
                        Preferences.setValue(LoginActivity.this, Preferences.USER_PIC, "");
                        Preferences.setValue(LoginActivity.this, Preferences.USER_NAME, "");
                        AppConstants.connectTo = AppConstants.WEB;
                        login();
                    }
                }
                return false;
            }
        });

        binding.btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (isValidate()) {
                            Preferences.setValue(LoginActivity.this, Preferences.USER_PIC, "");
                            Preferences.setValue(LoginActivity.this, Preferences.USER_NAME, "");
                            AppConstants.connectTo = AppConstants.WEB;
                            login();
                        }
                    }
                }, 300);
            }
        });


/*
        binding.tvTermsncondition.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {
                Integer pageNumber = 0;
                final AlertDialog.Builder dialogbuilder = new AlertDialog.Builder(LoginActivity.this);
                final LayoutInflater layoutInflater = getLayoutInflater();
                final View dialogview = layoutInflater.inflate(R.layout.itempdfviewer, null);
                dialogbuilder.setView(dialogview);
                final AlertDialog d = dialogbuilder.create();
                d.show();

                final TextView textView = (TextView) dialogview.findViewById(R.id.tv_pdf);

                textView.setText(fromHtml(getString(R.string.pdf_text)));

                */
/*final PDFView pdfView= (PDFView) dialogview.findViewById(R.id.pdfviewer);
                pdfView.fromAsset("Student.pdf")
                        .defaultPage(pageNumber)
                        .enableSwipe(true)
                        .swipeHorizontal(false)
                        .enableAnnotationRendering(true)
                        .scrollHandle(new DefaultScrollHandle(RegisterActivity.this))
                        .load();*//*


                //startActivity(new Intent(RegisterActivity.this, TermsNConditionActivity.class));
            }
        });
*/


        binding.imgFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                LoginManager.getInstance().logOut();
                LoginManager.getInstance().logInWithReadPermissions(
                        LoginActivity.this,
                        Arrays.asList("public_profile",
                                "user_friends", "email"));


            }
        });

        binding.imgGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (AppConstants.isNetworkAvailable("", "", LoginActivity.this)) {
                    signIn();
                }

            }
        });

        binding.relForgotPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(LoginActivity.this, ForgotPasswordActivity.class);
                        startActivity(intent);
                    }
                }, 300);
            }
        });

        binding.btnSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                        startActivity(intent);
                    }
                }, 300);
            }
        });


    }

    private void resetPreferences() {
        //Preferences.setValue(LoginActivity.this, Preferences.LATITUDE, "");
        //Preferences.setValue(LoginActivity.this, Preferences.LONGITUDE, "");
        Preferences.setValue(LoginActivity.this, Preferences.isLocationUpdated, false);
        Preferences.setValue(LoginActivity.this, Preferences.isIntiGroupArray, false);
    }


    private void login() {
        if (AppConstants.isNetworkAvailable(getString(R.string.network_title), getString(R.string.network_msg), LoginActivity.this)) {
            String envelope = "&email=" + binding.edtEmail.getText().toString().trim();
            envelope = envelope + "&password=" + binding.edtPassword.getText().toString().trim();
            envelope = envelope + "&gcm_id=" + FirebaseInstanceId.getInstance().getToken();
            envelope = envelope + "&device_id=" + Settings.Secure.getString(LoginActivity.this.getContentResolver(), Settings.Secure.ANDROID_ID);
            envelope = envelope + "&type=1";
            envelope = envelope + "&connectTo=" + AppConstants.WEB;
            envelope = envelope + "&profile_pic=";
            envelope = envelope + "&name=";
            envelope = envelope + "&date=" + new SimpleDateFormat("ddMMyyHHmmss").format(Calendar.getInstance().getTime());
            new LoginApi(envelope, LoginActivity.this, new LoginApi.OnResultReceived() {
                @Override
                public void onResult(String result) throws UnsupportedEncodingException {
                    navigateToHome();
                }
            }, getSupportFragmentManager(), true);
        }
    }

    private void facebookLogin(String email) throws UnsupportedEncodingException {
        if (AppConstants.isNetworkAvailable(getString(R.string.network_title), getString(R.string.network_msg), LoginActivity.this)) {
            String envelope = "&email=" + email;
            envelope = envelope + "&password=";
            envelope = envelope + "&gcm_id=" + FirebaseInstanceId.getInstance().getToken();
            envelope = envelope + "&device_id=" + Settings.Secure.getString(LoginActivity.this.getContentResolver(), Settings.Secure.ANDROID_ID);
            envelope = envelope + "&type=1";
            envelope = envelope + "&connectTo=" + AppConstants.FACEBOOK;
            envelope = envelope + "&profile_pic=" + Preferences.getValue_String(LoginActivity.this, Preferences.USER_PIC);
            envelope = envelope + "&name=" + URLEncoder.encode(Preferences.getValue_String(LoginActivity.this, Preferences.USER_NAME), AppConstants.encodeType);
            envelope = envelope + "&date=" + new SimpleDateFormat("ddMMyyHHmmss").format(Calendar.getInstance().getTime());
            new LoginApi(envelope, LoginActivity.this, new LoginApi.OnResultReceived() {
                @Override
                public void onResult(String result) throws UnsupportedEncodingException {
                    navigateToHome();
                }
            }, getSupportFragmentManager(), true);
        }

    }

    private void googleLogin(String email) throws UnsupportedEncodingException {
        if (AppConstants.isNetworkAvailable(getString(R.string.network_title), getString(R.string.network_msg), LoginActivity.this)) {
            String envelope = "&email=" + email;
            envelope = envelope + "&password=";
            envelope = envelope + "&gcm_id=" + FirebaseInstanceId.getInstance().getToken();
            envelope = envelope + "&device_id=" + Settings.Secure.getString(LoginActivity.this.getContentResolver(), Settings.Secure.ANDROID_ID);
            envelope = envelope + "&type=1";
            envelope = envelope + "&connectTo=" + AppConstants.GOOGLE;
            envelope = envelope + "&profile_pic=" + Preferences.getValue_String(LoginActivity.this, Preferences.USER_PIC);
            envelope = envelope + "&name=" + URLEncoder.encode(Preferences.getValue_String(LoginActivity.this, Preferences.USER_NAME), AppConstants.encodeType);
            envelope = envelope + "&date=" + new SimpleDateFormat("ddMMyyHHmmss").format(Calendar.getInstance().getTime());
            new LoginApi(envelope, LoginActivity.this, new LoginApi.OnResultReceived() {
                @Override
                public void onResult(String result) throws UnsupportedEncodingException {
                    navigateToHome();
                }
            }, getSupportFragmentManager(), true);
        }

    }

    private void navigateToHome() {
        String currentVersion = "";
        try {
            PackageManager manager = getPackageManager();
            PackageInfo info = null;
            info = manager.getPackageInfo(getPackageName(), 0);
            currentVersion = info.versionName;

            Log.e(TAG, "Version Name : " + currentVersion);

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String versionName = Preferences.getValue_String(LoginActivity.this, Preferences.VERSION_NAME);
        if (!versionName.equals("")) {
            if (!versionName.equals(currentVersion)) {
                Intent intent = new Intent(LoginActivity.this, TermsNConditionActivity.class);
                startActivity(intent);
                finish();
            } else {
                Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                startActivity(intent);
                finish();
            }
        } else {
            Intent intent = new Intent(LoginActivity.this, TermsNConditionActivity.class);
            startActivity(intent);
            finish();
        }
    }

    private boolean isValidate() {
        boolean flagValidate = true;
        String validateMsg = "";

        if (binding.edtEmail.getText().toString().trim().equals("")) {
            validateMsg = getString(R.string.text_validate_email);
            flagValidate = false;
        } else if (!AppConstants.validateEmail(binding.edtEmail.getText().toString().trim())) {
            validateMsg = getString(R.string.text_validate_valid_email);
            flagValidate = false;
        } else if (binding.edtPassword.getText().toString().trim().equals("")) {
            validateMsg = getString(R.string.text_validate_password);
            flagValidate = false;
        }

        if (flagValidate) {
            return true;
        } else {
            DialogManager.errorDialog(LoginActivity.this, getString(R.string.validation_title), validateMsg);
            return false;
        }
    }

    public void printKeyHash() {
        // Add code to print out the key hash
        try {
            PackageInfo info = LoginActivity.this.getPackageManager().getPackageInfo(
                    "com.sc.student2student",
                    PackageManager.GET_SIGNATURES);
            for (android.content.pm.Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e(" Your KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private void signIn() {
        Log.e(TAG, "Login with Google");
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.e(TAG, "handleSignInResult:" + result.isSuccess());

        Preferences.setValue(LoginActivity.this, Preferences.USER_NAME, "");
        Preferences.setValue(LoginActivity.this, Preferences.USER_PIC, "");
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.

            GoogleSignInAccount acct = result.getSignInAccount();
            Log.e(TAG, "Sign in Status: " + acct.getDisplayName());
            //new GoogleLogin(acct.getEmail(), acct.getDisplayName(), AppConstants.GOOGLE, acct.getId(), "", "" + acct.getPhotoUrl()).execute();
            //Call Google sign in here.............
            //googleLogin(acct.getEmail(), acct.getDisplayName(), acct.getId(), acct.getPhotoUrl().toString());
            if (acct.getPhotoUrl() != null) {
                if (!acct.getPhotoUrl().toString().equals("")) {
                    Preferences.setValue(LoginActivity.this, Preferences.USER_NAME, acct.getDisplayName());
                    Preferences.setValue(LoginActivity.this, Preferences.USER_PIC, acct.getPhotoUrl().toString());
                    try {
                        googleLogin(acct.getEmail());
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
            } else {
                Preferences.setValue(LoginActivity.this, Preferences.USER_PIC, "");
                try {
                    googleLogin(acct.getEmail());
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                // googleLogin(acct.getEmail(), acct.getDisplayName(), acct.getId(), "");
            }
            Log.e(TAG, "Update UI true");
        } else {
            // Signed out, show unauthenticated UI.
            Log.e(TAG, "Update UI false....");
        }
    }

    public View returnview() {
        return binding.edtPassword;
    }
}
