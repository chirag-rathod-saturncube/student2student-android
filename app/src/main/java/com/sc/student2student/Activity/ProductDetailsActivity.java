package com.sc.student2student.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.sc.student2student.R;
import com.sc.student2student.adapter.PhotoAdapter;
import com.sc.student2student.adapter.SlideShowAdapter;
import com.sc.student2student.api.CreateProductChatApi;
import com.sc.student2student.api.DeleteProductApi;
import com.sc.student2student.api.MakeanOfferApi;
import com.sc.student2student.api.ProvodeServiceApi;
import com.sc.student2student.api.ReportProductApi;
import com.sc.student2student.common.AppConstants;
import com.sc.student2student.common.DialogManager;
import com.sc.student2student.common.Preferences;
import com.sc.student2student.common.WebServices;
import com.sc.student2student.databinding.ActivityProductDetailsBinding;
import com.sc.student2student.databinding.DialogAcceptRejectBinding;
import com.sc.student2student.databinding.MakeOfferDialogBinding;
import com.sc.student2student.databinding.ProductReportDialogBinding;
import com.squareup.picasso.Picasso;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import static com.sc.student2student.common.AppConstants.product;


public class ProductDetailsActivity extends AppCompatActivity {

    public static ProductDetailsActivity activity;
    private final String TAG = getClass().getSimpleName();
    ActivityProductDetailsBinding binding;
    LinearLayoutManager layoutManager;
    PhotoAdapter adapter;
    ImageView img_back, img_option1, img_option2, img_watch, img_profile;
    TextView tv_title, tv_count, tv_username, lbl_about, tv_aboutproduct, lbl_status, tv_status, tv_product_age, tv_price;
    RatingBar ratingBar;
    ViewPager img_product;
    SlideShowAdapter slideShowAdapter;

    public static ProductDetailsActivity getInstance() {
        return activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_product_details);
        initview();
        AppConstants.action = AppConstants.PRODUCT_DETAILS;
        AppConstants.chatproduct = false;
        Log.e("####################", String.valueOf(AppConstants.chatproduct));
        // requestBuilder = new QBRequestGetBuilder();
        activity = this;
        setdata();
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        binding.relWebsite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = product.getWebsite();

                if (!url.startsWith("http://") && !url.startsWith("https://"))
                    url = "http://" + url;

                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(browserIntent);
            }
        });

        binding.btnMakeoffer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //if (product.getStatus().equals("0")) {
                if (AppConstants.TYPE.equals(AppConstants.PRODUCT)) {
                    makeOfferDialof(ProductDetailsActivity.this);
                } else {
                    Log.e("TAG", "Service");
                    String param = "&user_id=" + Preferences.getValue_String(ProductDetailsActivity.this, Preferences.USER_ID);
                    param = param + "&pro_id=" + product.getProduct_id();
                    param = param + "&price=";
                    param = param + "&type=2";
                    param = param + "&date=" + new SimpleDateFormat("ddMMyyHHmmss").format(Calendar.getInstance().getTime());
                    new ProvodeServiceApi(param, ProductDetailsActivity.this, new ProvodeServiceApi.OnResultReceived() {
                        @Override
                        public void onResult(String result) throws UnsupportedEncodingException {
                            product.setStatus("1");
                            if (AppConstants.serivceList.size() > 0) {
                                AppConstants.serivceList.get(AppConstants.pos).setStatus("1");
                            }

                            String msg = "Hello, I am interested in your service";
                            try {
                                if (AppConstants.isNetworkAvailable(getString(R.string.network_title),
                                        getString(R.string.network_msg), ProductDetailsActivity.this)) {
                                    String envelope = "&prod_id=" + AppConstants.product.getProduct_id();
                                    envelope = envelope + "&user_id=" +
                                            Preferences.getValue_String(ProductDetailsActivity.this,
                                                    Preferences.USER_ID);
                                    envelope = envelope + "&msg=" + URLEncoder
                                            .encode(msg, AppConstants.encodeType);
                                    envelope = envelope + "&date=" + new SimpleDateFormat("ddMMyyHHmmss").format(Calendar.getInstance().getTime());
                                    new CreateProductChatApi(envelope, ProductDetailsActivity.this,
                                            new CreateProductChatApi.OnResultReceived() {
                                                @Override
                                                public void onResult(String result)
                                                        throws UnsupportedEncodingException {
                                                    startActivity(new Intent(ProductDetailsActivity.this,
                                                            ApiChatActivity.class));
                                                }
                                            }, true);
                                }
                            } catch (UnsupportedEncodingException e) {
                                Log.e(TAG, "UnsupportedEncodingException : " + e.getMessage());
                            }
                        }
                    }, true);
                }
                //} else {
                //    Toast.makeText(ProductDetailsActivity.this, "You already placed a bid", Toast.LENGTH_SHORT).show();
                //}
            }
        });


        binding.btnChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String msg = "";
                    if (AppConstants.TYPE.equals(AppConstants.PRODUCT)) {
                        msg = "Hello, I am interested in your product";
                    } else {
                        msg = "Hello, I am interested in your service";
                    }

                    if (AppConstants.isNetworkAvailable(getString(R.string.network_title),
                            getString(R.string.network_msg), ProductDetailsActivity.this)) {
                        String envelope = "&prod_id=" + AppConstants.product.getProduct_id();
                        envelope = envelope + "&user_id=" +
                                Preferences.getValue_String(ProductDetailsActivity.this,
                                        Preferences.USER_ID);


                        envelope = envelope + "&msg=" + URLEncoder
                                .encode(msg, AppConstants.encodeType);
                        envelope = envelope + "&date=" + new SimpleDateFormat("ddMMyyHHmmss").format(Calendar.getInstance().getTime());
                        new CreateProductChatApi(envelope, ProductDetailsActivity.this,
                                new CreateProductChatApi.OnResultReceived() {
                                    @Override
                                    public void onResult(String result)
                                            throws UnsupportedEncodingException {
                                        startActivity(new Intent(ProductDetailsActivity.this,
                                                ApiChatActivity.class));
                                    }
                                }, true);
                    }
                } catch (UnsupportedEncodingException e) {
                    Log.e(TAG, "UnsupportedEncodingException : " + e.getMessage());
                }
            }
        });


        binding.btnDelete.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View v) {
                Delete_Dialog(ProductDetailsActivity.this, "Are you sure want to delete?", product.getProduct_id());
            }
        });

        binding.btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.TYPE = product.getType();
                AppConstants.ACTION_PRODUCT = AppConstants.EDIT;
                startActivity(new Intent(ProductDetailsActivity.this, SellProductActivity.class));
            }
        });


        binding.relProfile.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View v) {
                AppConstants.USER_ID = AppConstants.product.getUser_id();
                AppConstants.view_profile = true;
                startActivity(new Intent(ProductDetailsActivity.this, MyProfileActivity.class));
            }
        });


        binding.relToolbar.imgOption2.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View v) {
                ProductReportDialog(ProductDetailsActivity.this);
            }
        });

        binding.relToolbar.imgOption1.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View v) {
                String shareBody = "Hello, Please check this product : " + WebServices.DOMAIN + "product.php?id=" + product.getProduct_id();
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(Intent.EXTRA_SUBJECT, product.getProduct_name());
                sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, "Share via..."));
            }
        });
    }

    private void initview() {

        img_back = (ImageView) findViewById(R.id.img_back);
        img_option1 = (ImageView) findViewById(R.id.img_option1);
        img_option2 = (ImageView) findViewById(R.id.img_option2);
        img_watch = (ImageView) findViewById(R.id.img_watch);
        img_profile = (ImageView) findViewById(R.id.img_profile);

        tv_title = (TextView) findViewById(R.id.tv_title);
        tv_count = (TextView) findViewById(R.id.tv_count);
        tv_username = (TextView) findViewById(R.id.tv_username);
        lbl_about = (TextView) findViewById(R.id.lbl_about);
        tv_aboutproduct = (TextView) findViewById(R.id.tv_aboutproduct);
        lbl_status = (TextView) findViewById(R.id.lbl_status);
        tv_status = (TextView) findViewById(R.id.tv_status);
        tv_product_age = (TextView) findViewById(R.id.tv_product_age);
        tv_price = (TextView) findViewById(R.id.tv_price);

        ratingBar = (RatingBar) findViewById(R.id.ratingBar);

        img_product = (ViewPager) findViewById(R.id.img_product);

        binding.relToolbar.imgOption1.setImageResource(R.drawable.share);
        binding.relToolbar.imgOption1.setVisibility(View.VISIBLE);
        binding.relToolbar.imgOption2.setImageResource(R.drawable.about);
        binding.relToolbar.imgOption2.setVisibility(View.VISIBLE);
        binding.relToolbar.tvTitle.setText(product.getProduct_name());
        layoutManager = new LinearLayoutManager(ProductDetailsActivity.this, LinearLayoutManager.HORIZONTAL, false);
        binding.photoReclerview.setLayoutManager(layoutManager);
    }

    @Override
    public void onBackPressed() {
        if (Preferences.getValue_Boolean(ProductDetailsActivity.this, Preferences.DEEP_LINK, false)) {
            Preferences.setValue_Boolean(ProductDetailsActivity.this, Preferences.DEEP_LINK, false);
            Intent intent = new Intent(ProductDetailsActivity.this, HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        } else {
            finish();
        }
    }

    public void makeOfferDialof(final Context activity) {
        final MakeOfferDialogBinding dialogBinding;
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        LayoutInflater inflater = ((Activity) activity).getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.make_offer_dialog, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog b = dialogBuilder.create();
        b.show();
        dialogBinding = MakeOfferDialogBinding.bind(dialogView);

        dialogBinding.tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialogBinding.edtPrice.getText().toString().equals("")) {
                    DialogManager.errorDialog(activity, "Error!", "Please enter price");
                    return;
                }
                b.dismiss();
                String param = "&user_id=" + Preferences.getValue_String(ProductDetailsActivity.this, Preferences.USER_ID);
                param = param + "&pro_id=" + product.getProduct_id();
                try {
                    param = param + "&price=" + URLEncoder.encode(dialogBinding.edtPrice.getText().toString(), AppConstants.encodeType);
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                param = param + "&type=1";
                param = param + "&date=" + new SimpleDateFormat("ddMMyyHHmmss").format(Calendar.getInstance().getTime());

                new MakeanOfferApi(param, ProductDetailsActivity.this, new MakeanOfferApi.OnResultReceived() {
                    @Override
                    public void onResult(String result) throws UnsupportedEncodingException {
                        product.setStatus("1");
                        if (AppConstants.productList.size() > 0) {
                            AppConstants.productList.get(AppConstants.pos).setStatus("1");
                        }

                        String msg = "I am offering €"
                                + dialogBinding.edtPrice.getText().toString()
                                + " for "
                                + product.getProduct_name();
                        try {
                            if (AppConstants.isNetworkAvailable(getString(R.string.network_title),
                                    getString(R.string.network_msg), ProductDetailsActivity.this)) {
                                String envelope = "&prod_id=" + AppConstants.product.getProduct_id();
                                envelope = envelope + "&user_id=" +
                                        Preferences.getValue_String(ProductDetailsActivity.this,
                                                Preferences.USER_ID);
                                envelope = envelope + "&msg=" + URLEncoder
                                        .encode(msg, AppConstants.encodeType);
                                envelope = envelope + "&date=" + new SimpleDateFormat("ddMMyyHHmmss").format(Calendar.getInstance().getTime());
                                new CreateProductChatApi(envelope, ProductDetailsActivity.this,
                                        new CreateProductChatApi.OnResultReceived() {
                                            @Override
                                            public void onResult(String result)
                                                    throws UnsupportedEncodingException {
                                                startActivity(new Intent(ProductDetailsActivity.this,
                                                        ApiChatActivity.class));
                                            }
                                        }, true);
                            }
                        } catch (UnsupportedEncodingException e) {
                            Log.e(TAG, "UnsupportedEncodingException : " + e.getMessage());
                        }
                    }
                }, true);
            }
        });

        dialogBinding.tvOk.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    dialogBinding.tvOk.setTextColor(Color.WHITE);
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    dialogBinding.tvOk.setTextColor(Color.WHITE);
                }
                return false;
            }
        });

    }

    public void ProductReportDialog(final Context activity) {
        final ProductReportDialogBinding dialogBinding;
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        LayoutInflater inflater = ((Activity) activity).getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.product_report_dialog, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog b = dialogBuilder.create();
        b.show();
        dialogBinding = ProductReportDialogBinding.bind(dialogView);


        dialogBinding.tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (dialogBinding.edtDesc.getText().toString().equals("")) {
                    DialogManager.errorDialog(activity, "Error!", "Please enter Report");
                    return;
                }

                b.dismiss();
                String param = "&pro_id=" + product.getProduct_id();
                param = param + "&user_id=" + Preferences.getValue_String(ProductDetailsActivity.this, Preferences.USER_ID);
                try {
                    param = param + "&msg=" + URLEncoder.encode(dialogBinding.edtDesc.getText().toString(), AppConstants.encodeType);
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                param = param + "&date=" + new SimpleDateFormat("ddMMyyHHmmss").format(Calendar.getInstance().getTime());
                new ReportProductApi(param, ProductDetailsActivity.this, new ReportProductApi.OnResultReceived() {
                    @Override
                    public void onResult(String result) throws UnsupportedEncodingException {

                    }
                }, true);
            }
        });

        dialogBinding.tvOk.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    dialogBinding.tvOk.setTextColor(Color.WHITE);
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    dialogBinding.tvOk.setTextColor(Color.WHITE);
                }
                return false;
            }
        });

    }

    private void setdata() {
        Log.e(TAG, "setdata(){...}");
        if (AppConstants.TYPE.equals(AppConstants.PRODUCT)) {
            Log.e(TAG, "AppConstants.TYPE.equals(AppConstants.PRODUCT(){...}");
            binding.btnMakeoffer.setText("Make an Offer");
            binding.relWebsite.setVisibility(View.GONE);
            //binding.relPrice.setVisibility(View.VISIBLE);
            binding.relStatus.setVisibility(View.VISIBLE);
            // binding.relPricePer.setVisibility(View.GONE);
            lbl_about.setText("Product description");
        } else {
            binding.btnMakeoffer.setText("Request service");
            binding.relStatus.setVisibility(View.GONE);
            if (!product.getWebsite().equals("")) {
                if (product.getWebsite() != null) {
                    Log.e(TAG, "product.getWebsite() != null  1 ");
                    binding.relWebsite.setVisibility(View.VISIBLE);
                } else {
                    Log.e(TAG, "product.getWebsite() = null  2 ");
                    binding.relWebsite.setVisibility(View.GONE);
                }
            } else {
                Log.e(TAG, "product.getWebsite() = null  3 ");
                binding.relWebsite.setVisibility(View.GONE);
            }
            if (AppConstants.product.getPrice_status().equals("0")) {
                binding.relPrice.setVisibility(View.GONE);
                // binding.relPricePer.setVisibility(View.GONE);
            } else {
                binding.relPrice.setVisibility(View.VISIBLE);
                //binding.relPricePer.setVisibility(View.VISIBLE);
            }
            lbl_about.setText("Service description");
        }
        if (AppConstants.product.getUser_id().equals(Preferences.getValue_String(ProductDetailsActivity.this, Preferences.USER_ID))) {
            binding.relToolbar.imgOption2.setVisibility(View.GONE);
        } else {
            binding.relToolbar.imgOption2.setVisibility(View.VISIBLE);
        }
        if (product.getUser_id().equals(Preferences.getValue_String(ProductDetailsActivity.this, Preferences.USER_ID))) {
            binding.relBottom.setVisibility(View.GONE);
            binding.relBottom1.setVisibility(View.VISIBLE);
        } else {
            binding.relBottom.setVisibility(View.VISIBLE);
            binding.relBottom1.setVisibility(View.GONE);
        }
        slideShowAdapter = new SlideShowAdapter(ProductDetailsActivity.this, R.layout.slide_item, product.getImages(), true);
        binding.imgProduct.setAdapter(slideShowAdapter);
        slideShowAdapter.notifyDataSetChanged();
        binding.tvCount.setText("1/" + product.getImages().size());
        binding.imgProduct.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                binding.tvCount.setText((position + 1) + "/" + product.getImages().size());
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
        if (!product.getProduct_user().getProfile_pic().equals("")) {
            Picasso.with(ProductDetailsActivity.this).load(product.getProduct_user().getProfile_pic()).placeholder(R.drawable.profile).error(R.drawable.profile).transform(AppConstants.getTransformation(binding.imgProfile)).into(binding.imgProfile);
        }
        binding.ratingBar.setRating(Float.parseFloat(product.getProduct_user().getRating()));
        binding.tvUsername.setText(product.getProduct_user().getName());
        binding.tvAboutproduct.setText(product.getProduct_desc());

        if (AppConstants.TYPE.equals(AppConstants.PRODUCT)) {
            if (!product.getPrice().equals("")) {
                if (product.getPrice() != null) {

                    binding.tvPrice.setText(product.getPrice());
                }
            } else {
                binding.tvPrice.setText("FREE");
            }

        } else {
            binding.tvPrice.setText(product.getPrice() + " " + product.getPrice_per());
        }

        Log.e(TAG, "get Data In Price_per" + product.getPrice_per());
        //binding.tvPricePer.setText(product.getPrice_per());
        binding.tvWebsite.setText(product.getWebsite());
        if (product.getCondition_name().equals("Not applicable")) {
            binding.tvStatus.setText("-----");
        } else {
            binding.tvStatus.setText(product.getCondition_name());
        }
        binding.tvProductAge.setText("Since " + product.getCrt_date());

        /*if (product.getIs_watch().equals("0")) {
            binding.imgWatch.setImageResource(R.drawable.heart_outline);
        } else {
            binding.imgWatch.setImageResource(R.drawable.heart);
        }*/

        if (product.getImages().size() > 0) {
            adapter = new PhotoAdapter(ProductDetailsActivity.this, product.getImages(), new PhotoAdapter.OnResultReceived() {
                @Override
                public void onResult(int i) {

                }
            });
            binding.photoReclerview.setAdapter(adapter);
        }
    }

    public void Delete_Dialog(Context activity, String msg, final String pro_id) {
        final DialogAcceptRejectBinding dialogBinding;
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        LayoutInflater inflater = ((Activity) activity).getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_accept_reject, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog b = dialogBuilder.create();
        b.show();
        dialogBinding = DialogAcceptRejectBinding.bind(dialogView);

        dialogBinding.tvDialogMsg.setText(msg);

        dialogBinding.btnConfirm.setText("Delete");

        dialogBinding.btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
                String param = "&pro_id=" + pro_id;
                param = param + "&date=" + new SimpleDateFormat("ddMMyyHHmmss").format(Calendar.getInstance().getTime());
                new DeleteProductApi(param, ProductDetailsActivity.this, new DeleteProductApi.OnResultReceived() {
                    @Override
                    public void onResult(String result) throws UnsupportedEncodingException {
                        if (AppConstants.product_refresh != null) {
                            AppConstants.product_refresh.load();
                        }
                        if (AppConstants.service_refresh != null) {
                            AppConstants.service_refresh.load();
                        }
                        finish();
                    }
                }, true);
            }
        });
        dialogBinding.btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();

            }
        });
    }
}