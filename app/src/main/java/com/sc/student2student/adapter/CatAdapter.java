package com.sc.student2student.adapter;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sc.student2student.Activity.ProductListActivity;
import com.sc.student2student.Activity.SellProductActivity;
import com.sc.student2student.Activity.SubCategoriesActivity;
import com.sc.student2student.R;
import com.sc.student2student.common.AppConstants;
import com.sc.student2student.databinding.ItemCatBinding;
import com.sc.student2student.moduls.Cat;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by pc4 on 5/30/2016.
 */
public class CatAdapter extends RecyclerView.Adapter<CatAdapter.CustomViewHolder> {
    List<Cat> infos = null;
    String type, level;
    private ArrayList<Cat> arrayList;
    private Context mContext;
    private String TAG = getClass().getSimpleName();

    public CatAdapter(Context context, List<Cat> list, String type, String level) {
        this.infos = list;
        this.arrayList = new ArrayList<>();
        this.arrayList.addAll(infos);
        this.mContext = context;
        this.level = level;
        this.type = type;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        CustomViewHolder viewHolder;
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ItemCatBinding binding = DataBindingUtil.inflate(inflater, R.layout.item_cat, viewGroup, false);
        viewHolder = new CustomViewHolder(binding.getRoot(), binding);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final CustomViewHolder customViewHolder, final int pos) {
        final Cat list = infos.get(pos);

        if (!AppConstants.isSell) {
            if (level.equals("1")) {
                customViewHolder.binding.tvCategory.setText(list.getCategory_name());
            } else {
                customViewHolder.binding.tvCategory.setText(list.getCategory_name() + " (" + list.getTotal_product() + ")");
            }
        } else {
            customViewHolder.binding.tvCategory.setText(list.getCategory_name());
        }

        if (!list.getCat_img().equals("")) {

            Log.e(TAG, "Categories pic : " + list.getCat_img());


/*
            Blurry.with(mContext)
                    .radius(10)
                    .sampling(8)
                    .color(Color.argb(66, 255, 255, 0))
                    .async()
                    .animate(500)
                    .capture(findViewById(R.id.category_image))
                    .into((ImageView)findViewById(R.id.category_image));
*/

            Picasso.with(mContext).load(list.getCat_img())
                    .transform(AppConstants.getTransformation(customViewHolder.binding.categoryImage))
                    .into(customViewHolder.binding.categoryImage, new Callback() {
                        @Override
                        public void onSuccess() {
                            customViewHolder.binding.progressProduct.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            customViewHolder.binding.progressProduct.setVisibility(View.GONE);
                        }
                    });
        }

        customViewHolder.binding.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.TYPE = type;
                AppConstants.cat = list;
                if (list.getHave_subcategory().equals("1")) {
                    Intent intent = new Intent(mContext, SubCategoriesActivity.class);
                    mContext.startActivity(intent);
                } else {
                    //go to product page...
                    if (!AppConstants.isSell) {
                        Intent intent = new Intent(mContext, ProductListActivity.class);
                        mContext.startActivity(intent);
                    } else {
                        AppConstants.cat = list;
                        mContext.startActivity(new Intent(mContext, SellProductActivity.class));
                    }
                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return (null != infos ? infos.size() : 0);
    }


    public class CustomViewHolder extends RecyclerView.ViewHolder {
        ItemCatBinding binding;

        public CustomViewHolder(View view, ItemCatBinding binding) {
            super(view);
            this.binding = binding;
        }
    }
}

