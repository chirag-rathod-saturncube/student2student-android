package com.sc.student2student.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.sc.student2student.R;
import com.sc.student2student.adapter.PlaceAutocompleteAdapter;
import com.sc.student2student.common.AppConstants;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class SelectLocationActivity extends AppCompatActivity implements OnMapReadyCallback, LocationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    final static int REQUEST_LOCATION = 199;
    private static final LatLngBounds BOUNDS_GREATER_SYDNEY = new LatLngBounds(
            new LatLng(-34.041458, 150.790100), new LatLng(-33.682247, 151.383362));
    /*Permission on 6.x*/
    final private int REQUEST_CODE_ASK_LOCATION_PERMISSIONS = 124;
    String TAG = getClass().getSimpleName();
    TextView tv_address;
    Button btnNext;
    ImageView img_back, img_done, img_close;
    TextView tv_header_title;
    GoogleMap map;
    MapFragment mapFragment;
    boolean flagZoom = true;
    LocationRequest mLocationRequest;
    LocationManager locationManager;
    Location mLastLocation;
    PendingResult<LocationSettingsResult> result;
    int delay = 2000;
    boolean flagSetLocation = true;
    private LatLng center;
    private GoogleApiClient mGoogleApiClient;


    //Google places integrate:
    private Handler mUiHandler = new Handler();
    private AutoCompleteTextView mAutocompleteView;
    private PlaceAutocompleteAdapter mAdapter;
    //Google places api implement here
    private AdapterView.OnItemClickListener mAutocompleteClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            hideSoftKeyboard();
            mAutocompleteView.clearFocus();
            String address = mAutocompleteView.getText().toString();
            Log.e(TAG, "Address: " + address);
            String[] parts = address.split(",");
            Log.e(TAG, "Country: " + parts[parts.length - 1]);
            getLocationFromAddress(address);

           /* AppConstants.post_location = address;
            AppConstants.post_country = parts[parts.length - 1].toLowerCase().trim();*/
            //finish();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_location);
        initView();
        buildGoogleApiClient();
        getLocationPermission();

        img_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.map_lat = String.valueOf(center.latitude);
                AppConstants.map_lng = String.valueOf(center.longitude);

                AppConstants.user.setLatitude(String.valueOf(center.latitude));
                AppConstants.user.setLongitude(String.valueOf(center.longitude));
                getCompleteAddressString(center, true);

            }
        });


        //Google places integrate here.
        mAdapter = new PlaceAutocompleteAdapter(this, mGoogleApiClient, BOUNDS_GREATER_SYDNEY, null);
        mAutocompleteView.setOnItemClickListener(mAutocompleteClickListener);
        mAutocompleteView.setAdapter(mAdapter);


        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAutocompleteView.setText("");
            }
        });

    }

    public void initView() {
        mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(SelectLocationActivity.this);
        tv_address = (TextView) findViewById(R.id.tv_address);
        img_back = (ImageView) findViewById(R.id.img_back);
        img_done = (ImageView) findViewById(R.id.img_option2);
        img_done.setImageResource(R.drawable.ic_done);
        img_done.setVisibility(View.VISIBLE);
        tv_header_title = (TextView) findViewById(R.id.tv_title);
        tv_header_title.setText("Select Location");
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        //Google places integrate here
        mAutocompleteView = (AutoCompleteTextView) findViewById(R.id.autocomplete_places);
        img_close = (ImageView) findViewById(R.id.img_close);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void getLocationPermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            int hasCameraPermission = checkSelfPermission(android.Manifest.permission.ACCESS_FINE_LOCATION);
            if (hasCameraPermission != PackageManager.PERMISSION_GRANTED) {
                if (!shouldShowRequestPermissionRationale(android.Manifest.permission.ACCESS_FINE_LOCATION)) {
                    showMessageOKCancel("You need to allow access to Location",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (Build.VERSION.SDK_INT >= 23)
                                        requestPermissions(new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                                                REQUEST_CODE_ASK_LOCATION_PERMISSIONS);
                                }
                            });
                    return;
                }
                requestPermissions(new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                        REQUEST_CODE_ASK_LOCATION_PERMISSIONS);
                return;
            }
            enableGPSDialog();
        } else {
            enableGPSDialog();
        }
    }

    protected synchronized void buildGoogleApiClient() {
        Log.e(TAG, "buildGoogleApiClient(){..}");
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        Log.e(TAG, "onMapReady(){..}");
        map = googleMap;
        locationButtonEnable();
        googleMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                center = googleMap.getCameraPosition().target;
                getCompleteAddressString(center, false);

            }
        });

    }

    private void setUpMap() {
        map.getUiSettings().setCompassEnabled(true);
        map.getUiSettings().setTiltGesturesEnabled(true);
        map.getUiSettings().setRotateGesturesEnabled(true);
        map.getUiSettings().setScrollGesturesEnabled(true);
        map.getUiSettings().setZoomControlsEnabled(true);
        map.getUiSettings().setZoomGesturesEnabled(true);

        /*Log.e(TAG, "Selected Map Type: " + Preferences.getValue_String(SelectLocationActivity.this, Preferences.mapType));
        if (Preferences.getValue_String(SelectLocationActivity.this, Preferences.mapType).equals(AppConstants.MAP_NORMAL)) {
            map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        } else if (Preferences.getValue_String(SelectLocationActivity.this, Preferences.mapType).equals(AppConstants.MAP_TERRAIN)) {
            map.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
        } else {
            map.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
        }*/

    }

    public void locationButtonEnable() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                map.setMyLocationEnabled(true);
                setUpMap();
            }
        } else {
            map.setMyLocationEnabled(true);
            setUpMap();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_LOCATION:
                switch (resultCode) {
                    case Activity.RESULT_OK: {
                        Toast.makeText(SelectLocationActivity.this, "Location enabled", Toast.LENGTH_LONG).show();
                        mUiHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                Log.e(TAG, "Ready to work with GPS");
                                //requestLocationUpdate();
                            }
                        }, delay);
                        delay += 2000;
                        break;
                    }

                    case Activity.RESULT_CANCELED: {
                        Toast.makeText(SelectLocationActivity.this, "Location not enabled, user cancelled.", Toast.LENGTH_LONG).show();
                        break;
                    }

                    default: {
                        break;
                    }
                }
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_LOCATION_PERMISSIONS:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission Granted
                    Toast.makeText(this, "Permission granted", Toast.LENGTH_LONG).show();
                    mUiHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            enableGPSDialog();
                        }
                    }, delay);
                    delay += 2000;

                } else {
                    // Permission Denied
                    Toast.makeText(SelectLocationActivity.this, "Location Denied", Toast.LENGTH_SHORT)
                            .show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.e(TAG, "onConnected(){..}");
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(200);
        if (ContextCompat.checkSelfPermission(SelectLocationActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            enableGPSDialog();
        }
    }

    public void enableGPSDialog() {
        Log.e(TAG, "enableGPSDialog(){..}");
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);
        result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        Log.e(TAG, "Ready to work with GPS");
                        locationButtonEnable();
                        requestLocationUpdate();
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            status.startResolutionForResult(SelectLocationActivity.this, REQUEST_LOCATION);
                        } catch (IntentSender.SendIntentException e) {

                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        //...
                        break;
                }
            }
        });

    }

    public void requestLocationUpdate() {
        Log.e(TAG, "requestLocationUpdate(){..}");
        if (ContextCompat.checkSelfPermission(SelectLocationActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, SelectLocationActivity.this);
        } else {
            Log.e(TAG, "Permission not granted: ");
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        //Log.e(TAG, "onLocationChanged(){..}");
        //Log.e(TAG, "Lat: " + location.getLatitude() + " Lng: " + location.getLongitude());
        if (AppConstants.map_lat.equals("") && AppConstants.map_lng.equals("")) {
            mLastLocation = location;
            if (flagSetLocation) {
                if (mLastLocation != null) {
                    if (map != null) {
                        map.clear();
                        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                        map.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                        if (flagZoom) {
                            map.animateCamera(CameraUpdateFactory.zoomTo(12));
                            flagZoom = false;
                        }
                    }
                }
                flagSetLocation = false;
            }
        } else {

            Log.e(TAG, "Select Map Lat: " + AppConstants.map_lat);
            Log.e(TAG, "Select Map Lng: " + AppConstants.map_lng);

            mLastLocation = location;
            if (flagSetLocation) {
                if (mLastLocation != null) {
                    if (map != null) {
                        map.clear();
                        LatLng latLng = new LatLng(Double.parseDouble(AppConstants.map_lat),
                                Double.parseDouble(AppConstants.map_lng));
                        map.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                        if (flagZoom) {
                            map.animateCamera(CameraUpdateFactory.zoomTo(12));
                            flagZoom = false;
                        }
                    }
                }
                flagSetLocation = false;
            }
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new android.app.AlertDialog.Builder(SelectLocationActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    private String getCompleteAddressString(LatLng center, boolean isClose) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(center.latitude, center.longitude, 1);
            Log.e(TAG, "My Current City " + addresses.get(0).getAddressLine(0));
            AppConstants.Current_City = addresses.get(0).getAddressLine(0);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                Log.e(TAG, "My Current City " + returnedAddress.getLocality());

                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress
                            .append(returnedAddress.getAddressLine(i)).append(
                            "\n");
                }
                strAdd = strReturnedAddress.toString();
                Log.e(TAG, "My Current location address: " + strAdd);
                AppConstants.map_address = strAdd;
                AppConstants.user.setLocation(strAdd);


                if (!strAdd.equals("")) {

                    String[] address = strAdd.split(",");
                    Log.e("Address length-->", "" + address.length);

                    int add_length = address.length;


                    if (add_length == 4) {

                        AppConstants.user.setState(address[3]);
                        AppConstants.user.setCity(address[2]);
                        AppConstants.user.setAddress(address[0] + "," + address[1]);

                    } else if (add_length == 3) {
                        AppConstants.user.setState(address[2]);
                        AppConstants.user.setCity(address[1]);
                        AppConstants.user.setAddress(address[0]);

                    } else if (add_length == 2) {
                        AppConstants.user.setState(address[1]);
                        AppConstants.user.setCity(address[0]);
                        AppConstants.user.setAddress("");
                    } else if (add_length == 1) {
                        AppConstants.user.setState(address[0]);
                        AppConstants.user.setCity("");
                        AppConstants.user.setAddress("");
                    } else {
                    }
                }


                mAutocompleteView.setText(strAdd);
                getCountryName(SelectLocationActivity.this, center.latitude, center.longitude, isClose);
            } else {
                Log.e(TAG, "My Current location address No Address returned!");
            }
        } catch (
                Exception e)

        {
            e.printStackTrace();
            Log.e(TAG, "My Current location address No Address returned!");
        }
        return strAdd;
    }

    public void getCountryName(Context context, double latitude, double longitude, boolean isClose) {
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        List<Address> addresses = null;
        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1);
            Address result;
            if (addresses != null && !addresses.isEmpty()) {
                Log.e(TAG, "Country: " + addresses.get(0).getCountryName());
                AppConstants.map_country = addresses.get(0).getCountryName();
                AppConstants.user.setCountry(addresses.get(0).getCountryName());
                if (isClose) {
                    AppConstants.view_profile = false;
                    finish();
                }
            } else {
                Log.e(TAG, "Country not found");
            }
        } catch (IOException ignored) {
            //do something
        }
    }

    public void getLocationFromAddress(String strAddress) {
        Geocoder coder = new Geocoder(this);
        List<Address> address;
        try {
            address = coder.getFromLocationName(strAddress, 5);
            if (address != null) {
                Address location = address.get(0);
                if (map != null) {
                    map.clear();
                    LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                    map.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                    map.animateCamera(CameraUpdateFactory.zoomTo(12));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void hideSoftKeyboard() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }


}
