package com.sc.student2student.moduls;

/**
 * Created by pc4 on 10/08/2017.
 */

public class ProductGroup {
    String group_id, prod_id, prod_user_name, prod_title, prod_img, totalMessages, lastMessage;

    public String getProd_user_name() {
        return prod_user_name;
    }

    public void setProd_user_name(String prod_user_name) {
        this.prod_user_name = prod_user_name;
    }

    public String getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    public String getGroup_id() {
        return group_id;
    }

    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }

    public String getProd_id() {
        return prod_id;
    }

    public void setProd_id(String prod_id) {
        this.prod_id = prod_id;
    }

    public String getProd_title() {
        return prod_title;
    }

    public void setProd_title(String prod_title) {
        this.prod_title = prod_title;
    }

    public String getProd_img() {
        return prod_img;
    }

    public void setProd_img(String prod_img) {
        this.prod_img = prod_img;
    }

    public String getTotalMessages() {
        return totalMessages;
    }

    public void setTotalMessages(String totalMessages) {
        this.totalMessages = totalMessages;
    }
}
