package com.sc.student2student.Activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.sc.student2student.R;
import com.sc.student2student.adapter.FaqAdapter;
import com.sc.student2student.common.ExpandableRecyclerAdapter;
import com.sc.student2student.moduls.Faq;
import com.sc.student2student.moduls.FaqItem;

import java.util.ArrayList;
import java.util.List;

public class FaqActivity extends AppCompatActivity {
    TextView tvTitle;

    RecyclerView mRecyclerView;
    LinearLayoutManager mLayoutManager;
    FaqAdapter adapter;
    List<Faq> faqList;
    ImageView img_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faq);
        initView();
        faqList = new ArrayList<>();
        LoadFaqforNeedypeople();

        adapter = new FaqAdapter(FaqActivity.this, faqList);
        adapter.setMode(ExpandableRecyclerAdapter.MODE_ACCORDION);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(FaqActivity.this));
        mRecyclerView.setAdapter(adapter);

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    private void initView() {

        tvTitle = (TextView) findViewById(R.id.tv_title);

        img_back = (ImageView) findViewById(R.id.img_back);

        mRecyclerView = (RecyclerView) findViewById(R.id.rv_list);
        mLayoutManager = new LinearLayoutManager(FaqActivity.this);
        mLayoutManager = new LinearLayoutManager(FaqActivity.this, LinearLayoutManager.VERTICAL, false);
        mLayoutManager.setSmoothScrollbarEnabled(true);
        mLayoutManager.setRecycleChildrenOnDetach(true);
        mRecyclerView.setLayoutManager(mLayoutManager);
        tvTitle.setText("FAQ");


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


    private void LoadFaqforNeedypeople() {
        faqList.add(new Faq("How can I change my password?", R.drawable.ic_play));
        FaqItem faqItem = new FaqItem();
        faqItem.setAnswer("You can change your password by going to your profile page in the menu and then choose to adjust it.");
        faqList.add(new Faq(faqItem));


        faqList.add(new Faq("If I want to share an experience...", R.drawable.ic_play));
        FaqItem faqItem1 = new FaqItem();
        faqItem1.setAnswer("After you made a deal with another user you will be able to review him/her.");
        faqList.add(new Faq(faqItem1));

        faqList.add(new Faq("How do I delete my account?", R.drawable.ic_play));
        FaqItem faqItem2 = new FaqItem();
        faqItem2.setAnswer("You can delete your account in the menu.");
        faqList.add(new Faq(faqItem2));


        faqList.add(new Faq("How do I report someone?", R.drawable.ic_play));
        FaqItem faqItem3 = new FaqItem();
        faqItem3.setAnswer("If you suspect someone is a scam or you had an unpleasant experience with a fellow user we'd like to hear so. In this case open the chat with the specific person and click on the flag symbol in the upper right corner.");
        faqList.add(new Faq(faqItem3));


        faqList.add(new Faq("Where can I see the privacy policy of Student2Student?", R.drawable.ic_play));
        FaqItem faqItem5 = new FaqItem();
        faqItem5.setAnswer("The privacy policy can be found on our website http://students2students.nl");
        faqList.add(new Faq(faqItem5));


        faqList.add(new Faq("I want to read the user agreement, where do I do that?", R.drawable.ic_play));
        FaqItem faqItem6 = new FaqItem();
        faqItem6.setAnswer("The terms of use can be found on our website http://students2students.nl");
        faqList.add(new Faq(faqItem6));


        faqList.add(new Faq("You have a great idea, a request for an extra category or just  want to contact us, how do you do it?", R.drawable.ic_play));
        FaqItem faqItem7 = new FaqItem();
        faqItem7.setAnswer("Great that you want to get in touch! The easiest way to do so is by sending us an email at info@students2students.nl.");
        faqList.add(new Faq(faqItem7));
/*
        faqList.add(new Faq("How do I invite a volunteer?",R.drawable.ic_play));
        FaqItem faqItem8 = new FaqItem();
        faqItem8.setAnswer("Once you select either ‘Book now’ or ‘Schedule booking’ you will get an option to invite volunteer. Click on ‘Invite your Volunteer’ to look for the online Volunteers. Click on their name to view the detail profile of a volunteer. You can review their testimonial before sending the request. Click on ’Send request’ to invite a volunteer.");
        faqList.add(new Faq(faqItem8));

        faqList.add(new Faq("How can I review the request I had sent?",R.drawable.ic_play));
        FaqItem faqItem9 = new FaqItem();
        faqItem9.setAnswer("CGo to ‘Notification’ tab to review all the requests sent. Click on ‘View Request’ to see the detail of any particular request.");
        faqList.add(new Faq(faqItem9));

        faqList.add(new Faq("If I change my mind, then can I cancel the request?",R.drawable.ic_play));
        FaqItem faqItem10 = new FaqItem();
        faqItem10.setAnswer("Yes, you can do so by visiting ‘Notification’ tab. Click on ‘Cancel request’ to cancel any particular request with apology note.");
        faqList.add(new Faq(faqItem10));

        faqList.add(new Faq("How can I contact the Volunteer?",R.drawable.ic_play));
        FaqItem faqItem11 = new FaqItem();
        faqItem11.setAnswer("Once your request is accepted, the ‘Call’ and ‘Chat’ options will be activated to contact the volunteer. You can access ‘call’ button in the top right corner of the Volunteer’s profile. Alternatively, you can ‘chat’ with the volunteer.");
        faqList.add(new Faq(faqItem11));

        faqList.add(new Faq("How can I search a Volunteer without requesting to book for any service?",R.drawable.ic_play));
        FaqItem faqItem12 = new FaqItem();
        faqItem12.setAnswer("Go to ‘Map’ to search for the online volunteer. You can also search for any specific volunteer by clicking on ‘Search volunteer here’ on the top of the map.");
        faqList.add(new Faq(faqItem12));

        faqList.add(new Faq("If I want to share my experience, then how can I do that?",R.drawable.ic_play));
        FaqItem faqItem13 = new FaqItem();
        faqItem13.setAnswer("You can go to ‘Blog’ to write your story where you can also attach up to four pictures. The blog can be shared in Facebook, Instagram, Twitter and Google+.");
        faqList.add(new Faq(faqItem13));

        faqList.add(new Faq("How can I track all my appointments?",R.drawable.ic_play));
        FaqItem faqItem14 = new FaqItem();
        faqItem14.setAnswer("If you go to ‘My appointment’ you can review all your ‘Upcoming appointments’. To review historical appointments click on the ‘Expired appointment’.");
        faqList.add(new Faq(faqItem14));

        faqList.add(new Faq("How can I change the password?",R.drawable.ic_play));
        FaqItem faqItem15 = new FaqItem();
        faqItem15.setAnswer("You can change the password in the ‘Setting’ option.");
        faqList.add(new Faq(faqItem15));

        faqList.add(new Faq("Can I delete my account?",R.drawable.ic_play));
        FaqItem faqItem16 = new FaqItem();
        faqItem16.setAnswer("Yes, you can delete the account in the ‘Setting’ option.");
        faqList.add(new Faq(faqItem16));
*/
    }


}
