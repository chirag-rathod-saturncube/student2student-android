package com.sc.student2student.moduls;

/**
 * Created by Saturncube-5 on 21-Sep-17.
 */

public class PricePer {

    String price_id,price_name,code,active_flag;

    public String getPrice_id() {
        return price_id;
    }

    @Override
    public String toString() {
        return price_name;
    }

    public void setPrice_id(String price_id) {
        this.price_id = price_id;
    }

    public String getPrice_name() {
        return price_name;
    }

    public void setPrice_name(String price_name) {
        this.price_name = price_name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getActive_flag() {
        return active_flag;
    }

    public void setActive_flag(String active_flag) {
        this.active_flag = active_flag;
    }
}
