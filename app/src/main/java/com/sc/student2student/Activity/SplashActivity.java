package com.sc.student2student.Activity;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.sc.student2student.R;
import com.sc.student2student.common.Preferences;


public class SplashActivity extends AppCompatActivity {
    private static String userEmail;
    private final int TIMEOUT = 2000;
    String TAG = getClass().getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                onLogin();
            }
        }, TIMEOUT);

    }


    private void onLogin() {

        userEmail = Preferences.getValue_String(getBaseContext(), Preferences.USER_EMAIL);
        if (userEmail != null) {
            if (!userEmail.equals("")) {

                String currentVersion = "";
                try {
                    PackageManager manager = getPackageManager();
                    PackageInfo info = null;
                    info = manager.getPackageInfo(getPackageName(), 0);
                    currentVersion = info.versionName;

                    Log.e(TAG, "Version Name : " + currentVersion);

                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }
                String versionName = Preferences.getValue_String(SplashActivity.this, Preferences.VERSION_NAME);
                if (!versionName.equals("")) {
                    if (!versionName.equals(currentVersion)) {
                        Intent intent = new Intent(SplashActivity.this, TermsNConditionActivity.class);
                        startActivity(intent);
                        finish();
                    } else {
                        Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
                        startActivity(intent);
                        finish();
                    }
                } else {
                    Intent intent = new Intent(SplashActivity.this, TermsNConditionActivity.class);
                    startActivity(intent);
                    finish();
                }


            } else {
                Intent Intent_login = new Intent(SplashActivity.this, LoginActivity.class);
                startActivity(Intent_login);
                finish();
            }
        } else {
            Intent Intent_login = new Intent(SplashActivity.this, LoginActivity.class);
            startActivity(Intent_login);
            finish();
        }

    }
}
