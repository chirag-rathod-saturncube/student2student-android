package com.sc.student2student.adapter;

import com.sc.student2student.R;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;


import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;


/**
 * Created by pc4 on 5/30/2016.
 */
public class ProductPhotosAdapter extends RecyclerView.Adapter<ProductPhotosAdapter.CustomViewHolder> {
    ArrayList<File> infos = null;
    OnResultReceived onResultReceived;
    private ArrayList<File> arrayList;
    private Context mContext;
    private String TAG = getClass().getSimpleName();

    public ProductPhotosAdapter(Context context, ArrayList<File> list, OnResultReceived OnResultReceived) {
        this.infos = list;
        this.arrayList = new ArrayList<>();
        this.arrayList.addAll(infos);
        this.mContext = context;
        this.onResultReceived = OnResultReceived;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_photo, null);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final CustomViewHolder customViewHolder, final int pos) {
        final File list = infos.get(pos);


        Picasso.with(mContext).load(list).into(customViewHolder.img_photo1);


        customViewHolder.img_remove1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                infos.remove(pos);
                notifyDataSetChanged();
                onResultReceived.onResult(pos);

            }
        });
    }

    public ArrayList<File> getFiles() {
        return infos;
    }

    @Override
    public int getItemCount() {
        return (null != infos ? infos.size() : 0);
    }

    public interface OnResultReceived {
        public void onResult(int i);
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {

        ImageView img_photo1, img_remove1;


        public CustomViewHolder(View view) {
            super(view);

            this.img_photo1 = (ImageView) view.findViewById(R.id.img_pic);
            this.img_remove1 = (ImageView) view.findViewById(R.id.img_remove);


        }
    }
}

