package com.sc.student2student.Activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.sc.student2student.R;
import com.sc.student2student.adapter.WatchpagerAdapter;
import com.sc.student2student.common.AppConstants;
import com.sc.student2student.common.Preferences;
import com.sc.student2student.databinding.ActivityWatchlistBinding;
import com.sc.student2student.fragement.Product_watchlist_fragement;
import com.sc.student2student.fragement.Service_watchlist_fragement;


public class WatchListActivity extends AppCompatActivity {
    ActivityWatchlistBinding binding;
    String name[] = {"My Products", "My Services"};
    WatchpagerAdapter adapter;
    Fragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_watchlist);
        adapter = new WatchpagerAdapter(getSupportFragmentManager(), name);
        binding.viewpager.setAdapter(adapter);
        binding.tablayout.setupWithViewPager(binding.viewpager);
        binding.viewpager.post(new Runnable() {
            @Override
            public void run() {
                binding.viewpager.setCurrentItem(0);
                fragment = adapter.getRegisteredFragment(0);
            }
        });

        setView();

        binding.imgDrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        binding.viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                fragment = adapter.getRegisteredFragment(position);
                AppConstants.page_display = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        binding.imgListtype.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Preferences.setValue_Boolean(WatchListActivity.this, Preferences.LIST_TYPE, false);
                setView();
                if (AppConstants.product_watch_refresh != null) {
                    AppConstants.product_watch_refresh.refresh_view();
                }
                if (AppConstants.service_watch_refresh != null) {
                    AppConstants.service_watch_refresh.refresh_view();
                }
                if (fragment != null) {
                    if (fragment instanceof Product_watchlist_fragement) {
                        ((Product_watchlist_fragement) fragment).ChangeListType();
                    } else if (fragment instanceof Service_watchlist_fragement) {
                        ((Service_watchlist_fragement) fragment).ChangeListType();
                    }

                } else {
                    Log.e("TAG", "Fragement null");
                }
            }
        });

        binding.imgGridtype.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Preferences.setValue_Boolean(WatchListActivity.this, Preferences.LIST_TYPE, true);
                setView();
                if (AppConstants.product_watch_refresh != null) {
                    AppConstants.product_watch_refresh.refresh_view();
                }
                if (AppConstants.service_watch_refresh != null) {
                    AppConstants.service_watch_refresh.refresh_view();
                }
                if (fragment != null) {
                    if (fragment instanceof Product_watchlist_fragement) {
                        ((Product_watchlist_fragement) fragment).ChangeListType();
                    } else if (fragment instanceof Service_watchlist_fragement) {
                        ((Service_watchlist_fragement) fragment).ChangeListType();
                    }

                } else {
                    Log.e("TAG", "Fragement null");
                }
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        setView();
    }

    private void setView() {

        if (Preferences.getValue_Boolean(WatchListActivity.this, Preferences.LIST_TYPE, true)) {
            binding.imgListtype.setVisibility(View.VISIBLE);
            binding.imgGridtype.setVisibility(View.GONE);
        } else {
            binding.imgListtype.setVisibility(View.GONE);
            binding.imgGridtype.setVisibility(View.VISIBLE);
        }
        if (!Preferences.getValue_String(WatchListActivity.this, Preferences.USER_PIC).equals("")) {
            //Picasso.with(WatchListActivity.this).load(Preferences.getValue_String(WatchListActivity.this, Preferences.USER_PIC)).placeholder(R.drawable.profile).into(binding.draweritem.profilePic);
        }


        if (AppConstants.ACTION.equals(AppConstants.WATCH_LIST)) {
            binding.tvTitle.setText("Watch List");
        } else {
            binding.viewpager.post(new Runnable() {
                @Override
                public void run() {
                    binding.viewpager.setCurrentItem(AppConstants.page_display);
                    fragment = adapter.getRegisteredFragment(AppConstants.page_display);
                }
            });
            binding.tvTitle.setText("My List");
        }


        // binding.draweritem.tvUsername.setText(Preferences.getValue_String(WatchListActivity.this, Preferences.USER_NAME));
        //binding.draweritem.tvEmail.setText(Preferences.getValue_String(WatchListActivity.this, Preferences.USER_EMAIL));
        binding.imgDrawer.setImageResource(R.drawable.back);

    }
}
