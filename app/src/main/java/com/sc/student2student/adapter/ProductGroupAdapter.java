package com.sc.student2student.adapter;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sc.student2student.Activity.ApiChatActivity;
import com.sc.student2student.R;
import com.sc.student2student.api.DeleteChatApi;
import com.sc.student2student.api.UpdateMessageApi;
import com.sc.student2student.common.AppConstants;
import com.sc.student2student.common.Preferences;
import com.sc.student2student.databinding.ItemProductGroupItemBinding;
import com.sc.student2student.moduls.ProductGroup;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by pc4 on 5/30/2016.
 */
public class ProductGroupAdapter extends RecyclerView.Adapter<ProductGroupAdapter.CustomViewHolder> {
    List<ProductGroup> infos = null;
    private ArrayList<ProductGroup> arrayList;
    private Context mContext;
    private String TAG = getClass().getSimpleName();

    public ProductGroupAdapter(Context context, List<ProductGroup> list) {
        this.infos = list;
        this.arrayList = new ArrayList<>();
        this.arrayList.addAll(infos);
        this.mContext = context;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        CustomViewHolder viewHolder;
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ItemProductGroupItemBinding listBinding =
                DataBindingUtil.inflate(inflater, R.layout.item_product_group_item, viewGroup, false);
        viewHolder = new CustomViewHolder(listBinding.getRoot(), listBinding);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final CustomViewHolder holder, final int pos) {
        final ProductGroup list = infos.get(pos);
        try {
            holder.listBinding.textDialogName.setText(URLDecoder.decode(list.getProd_user_name(), AppConstants.encodeType));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        holder.listBinding.textDialogLastMessage.setText(list.getLastMessage());
        Log.e(TAG, "list getTotalMessages" + list.getTotalMessages());

        if (!list.getTotalMessages().equals("")) {
            if (!list.getTotalMessages().equals("0")) {
                holder.listBinding.textDialogUnreadCount.setVisibility(View.VISIBLE);
                holder.listBinding.textDialogUnreadCount.setText("" + list.getTotalMessages());
            } else {
                holder.listBinding.textDialogUnreadCount.setVisibility(View.GONE);
            }
        } else {
            holder.listBinding.textDialogUnreadCount.setVisibility(View.GONE);
        }


        /*if (Preferences.getValue_String(mContext, list.getGroup_id()) != null && !Preferences.getValue_String(mContext, list.getGroup_id()).equals("")) {
            //if (list.getGroup_id() != null && !list.getGroup_id().equals("")) {
            Log.e(TAG, "if" + list.getGroup_id());
            int prefValue = Integer.parseInt(Preferences.getValue_String(mContext, list.getGroup_id()));
            //int prefValue = Integer.parseInt(list.getGroup_id());
            int newValue = Integer.parseInt(list.getTotalMessages());
            int finalValue = newValue - prefValue;
            if (finalValue > 0) {
                Log.e(TAG, "if if" + list.getGroup_id());
                holder.listBinding.textDialogUnreadCount.setVisibility(View.VISIBLE);
                holder.listBinding.textDialogUnreadCount.setText("" + finalValue);
            } else {
                Log.e(TAG, "if else" + list.getGroup_id());
                holder.listBinding.textDialogUnreadCount.setVisibility(View.GONE);
            }
        } else {
            Log.e(TAG, "else" + list.getGroup_id());
            if (!list.getLastMessage().equals("")) {
                if (!list.getLastMessage().equals("0")) {
                    holder.listBinding.textDialogUnreadCount.setVisibility(View.VISIBLE);
                    holder.listBinding.textDialogUnreadCount.setText(list.getTotalMessages());
                } else {
                    holder.listBinding.textDialogUnreadCount.setVisibility(View.GONE);
                }
            } else {
                holder.listBinding.textDialogUnreadCount.setVisibility(View.GONE);
            }
        }*/


        if (!list.getProd_img().equals("")) {
            //holder.listBinding.proImageLoad.setVisibility(View.VISIBLE);
            Picasso.with(mContext)
                    .load(list.getProd_img())
                    .transform(AppConstants.getTransformation(holder.listBinding.imgProduct))
                    .placeholder(R.drawable.blank_profile)
                    .into(holder.listBinding.imgProduct, new Callback() {
                        @Override
                        public void onSuccess() {
                            holder.listBinding.proImageLoad.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            holder.listBinding.proImageLoad.setVisibility(View.GONE);
                        }
                    });
        } else {
            holder.listBinding.proImageLoad.setVisibility(View.GONE);
        }


        holder.listBinding.root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.group_id = list.getGroup_id();
                AppConstants.unread_count = list.getTotalMessages();
                AppConstants.productID = infos.get(pos).getProd_id();
                AppConstants.chatproduct = true;

                String envelope = "&group_id=" + list.getGroup_id();
                envelope = envelope + "&date=" + new SimpleDateFormat("ddMMyyHHmmss").format(Calendar.getInstance().getTime());
                Log.e(TAG, "list getGroup_id" + list.getGroup_id());
                infos.get(pos).setTotalMessages("0");
                notifyDataSetChanged();
                //Calling unread api for group ....
                new UpdateMessageApi(envelope, mContext, new UpdateMessageApi.OnResultReceived() {
                    @Override
                    public void onResult(String result) throws UnsupportedEncodingException {
                        mContext.startActivity(new Intent(mContext, ApiChatActivity.class));
                    }
                }, true);



/*                String param = "&user_id=" + getValue_String(mContext, Preferences.USER_ID);
                param = param + "&pro_id=" + list.getProd_id();
                new GetProductDetails(param, mContext, new GetProductDetails.OnResultReceived() {
                    @Override
                    public void onResult(String result) throws UnsupportedEncodingException {
                        mContext.startActivity(new Intent(mContext,
                                ApiChatActivity.class));
                    }

                    @Override
                    public void onFailed(VolleyError result) throws UnsupportedEncodingException {

                    }
                }, true);*/
            }
        });

        holder.listBinding.relSwipe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String envelope;
                envelope = "&user_id=" + Preferences.getValue_String(mContext, Preferences.USER_ID);
                envelope = envelope + "&group_id=" + list.getGroup_id();
                envelope = envelope + "&date=" + new SimpleDateFormat("ddMMyyHHmmss").format(Calendar.getInstance().getTime());
                infos.remove(pos);
                notifyDataSetChanged();

                new DeleteChatApi(envelope, mContext, new DeleteChatApi.OnResultReceived() {
                    @Override
                    public void onResult(String result) throws UnsupportedEncodingException {

                    }
                }, false);
            }
        });
    }

    @Override
    public int getItemCount() {
        return (null != infos ? infos.size() : 0);
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {
        ItemProductGroupItemBinding listBinding;

        public CustomViewHolder(View view, ItemProductGroupItemBinding listBinding) {
            super(view);
            this.listBinding = listBinding;
        }
    }
}

