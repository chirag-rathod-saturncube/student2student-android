package com.sc.student2student.adapter;

import com.sc.student2student.R;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;


import com.sc.student2student.moduls.Reviews;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/*
Created by pc4 on 5/30/2016.
*/
public class ReviewsAdapter extends RecyclerView.Adapter<ReviewsAdapter.CustomViewHolder> {
    List<Reviews> infos = null;
    private ArrayList<Reviews> arrayList;
    private Context mContext;
    private String TAG = getClass().getSimpleName();

    public ReviewsAdapter(Context context, List<Reviews> list) {
        this.infos = list;
        this.arrayList = new ArrayList<>();
        this.arrayList.addAll(infos);
        this.mContext = context;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_reviews, null);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final CustomViewHolder customViewHolder, final int pos) {
        final Reviews list = infos.get(pos);
        customViewHolder.tv_rname.setText(list.getName());
        if (!list.getTitle().equals("")) {
            customViewHolder.tv_reviews.setVisibility(View.VISIBLE);
            customViewHolder.tv_reviews.setText(list.getTitle());
        } else {
            customViewHolder.tv_reviews.setVisibility(View.GONE);
        }
        customViewHolder.ratingBar.setRating(Float.parseFloat(list.getRating()));
    }

    @Override
    public int getItemCount() {
        return (null != infos ? infos.size() : 0);
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {
        CardView card_view;
        ImageView item_image;
        TextView tv_rname, tv_reviews;
        RatingBar ratingBar;
        CircleImageView profile_pic;

        public CustomViewHolder(View view) {
            super(view);
            this.card_view = (CardView) view.findViewById(R.id.card_view);
            this.item_image = (ImageView) view.findViewById(R.id.item_image);
            this.ratingBar = (RatingBar) view.findViewById(R.id.ratingBar);
            this.tv_rname = (TextView) view.findViewById(R.id.tv_rname);
            this.tv_reviews = (TextView) view.findViewById(R.id.tv_reviews);
        }
    }
}

