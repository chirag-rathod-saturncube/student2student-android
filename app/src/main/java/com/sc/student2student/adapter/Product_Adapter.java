package com.sc.student2student.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.sc.student2student.Activity.ProductDetailsActivity;
import com.sc.student2student.R;
import com.sc.student2student.api.AddWatchListApi;
import com.sc.student2student.api.GetProductDetails;
import com.sc.student2student.api.RemoveWatchListApi;
import com.sc.student2student.common.AppConstants;
import com.sc.student2student.common.Preferences;
import com.sc.student2student.moduls.Product;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


/**
 * Created by pc4 on 5/30/2016.
 */
public class Product_Adapter extends RecyclerView.Adapter<Product_Adapter.CustomViewHolder> {
    List<Product> infos = null;
    int layout;
    private ArrayList<Product> arrayList;
    private Context mContext;
    //Product_watchlist_fragement fragment;
    // Service_watchlist_fragement fragment1;
    private String TAG = getClass().getSimpleName();
    private Target target = new Target() {
        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
            Log.e(TAG, "onBitmapLoaded(){..}");
        }

        @Override
        public void onBitmapFailed(Drawable errorDrawable) {
            Log.e(TAG, "onBitmapFailed(){..}");
        }


        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {
            Log.e(TAG, "onPrepareLoad(){..}");
        }
    };


    public Product_Adapter(Context context, List<Product> list, int layout) {
        this.infos = list;
        // this.fragment = fragment;
        this.arrayList = new ArrayList<>();
        this.arrayList.addAll(infos);
        this.mContext = context;
        this.layout = layout;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(layout, null);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final CustomViewHolder customViewHolder, final int pos) {
        final Product list = infos.get(pos);
        if (AppConstants.TYPE.equals(AppConstants.PRODUCT)) {
            customViewHolder.rel_price.setVisibility(View.VISIBLE);
            if (!list.getPrice().equals("")) {
                if (list.getPrice() != null) {
                    customViewHolder.tv_price.setVisibility(View.VISIBLE);
                } else {
                    customViewHolder.tv_price.setVisibility(View.INVISIBLE);
                }
            } else {
                customViewHolder.tv_price.setVisibility(View.INVISIBLE);
            }
        } else {
            customViewHolder.rel_price.setVisibility(View.VISIBLE);
            if (list.getPrice_status().equals("0")) {
                customViewHolder.tv_price.setVisibility(View.INVISIBLE);
            } else {
                customViewHolder.tv_price.setVisibility(View.VISIBLE);
            }
        }

        if (list.getCrt_date() == null) {
            customViewHolder.rel_date_of_placement.setVisibility(View.INVISIBLE);
        } else {
            if (list.getCrt_date().equals("")) {
                customViewHolder.rel_date_of_placement.setVisibility(View.INVISIBLE);
            } else {
                if (Preferences.getValue_Boolean(mContext, Preferences.LIST_TYPE, true)) {
                    customViewHolder.rel_date_of_placement.setVisibility(View.VISIBLE);
                } else {
                    customViewHolder.rel_date_of_placement.setVisibility(View.GONE);
                }
            }
        }
        Log.e(TAG, "getPrice_per" + list.getPrice_per());
        customViewHolder.tv_productname.setText(list.getProduct_name());
        //customViewHolder.tv_product_desc.setText(list.getProduct_desc());
        if (!list.getPrice().equals("")) {
            if (list.getPrice() != null) {
                customViewHolder.tv_price.setText(list.getPrice() + " " + list.getPrice_per());
            } else {
                customViewHolder.tv_price.setText("");
            }
        } else {
            customViewHolder.tv_price.setText("");
        }
        customViewHolder.tv_date.setText(list.getCrt_date());

        if (!Preferences.getValue_Boolean(mContext, Preferences.isLocationUpdated, true)) {
            customViewHolder.tv_distance.setText(list.getDistance());
            customViewHolder.tv_distance.setVisibility(View.GONE);
        } else {
            customViewHolder.tv_distance.setText(list.getDistance());
            customViewHolder.tv_distance.setVisibility(View.VISIBLE);
        }

        //customViewHolder.tv_distance.setText(list.getDistance());
        // customViewHolder.progress_product.setVisibility(View.VISIBLE);

        Log.e(TAG, "Image Loading");
        Log.e(TAG, "Image Loading ..................................................");
        try {
            Log.e(TAG, list.getProduct_small_image());
            if (!list.getProduct_small_image().equals("")) {
                Log.e(TAG, "getProduct_small_image()" + "if block ......");
                Log.e(TAG, "Small image URL = " + list.getProduct_small_image());
                Log.e(TAG, " getProduct_small_image ..................................................");
                Log.e(TAG, "Medium path : " + list.getProduct_medium_image());

                Picasso.with(mContext)
                        .load(list.getProduct_small_image())
                        .into(customViewHolder.product_image, new Callback() {
                            @Override
                            public void onSuccess() {
                                Log.e(TAG, "onSuccess(){..}");

                                Picasso.with(mContext)
                                        .load(list.getProduct_medium_image())
                                        .into(new Target() {
                                            @Override
                                            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                                                Log.e(TAG, "onBitmapLoaded(){..}");
                                                customViewHolder.product_image.setImageBitmap(bitmap);

                                                Picasso.with(mContext)
                                                        .load(list.getProduct_image())
                                                        .into(new Target() {
                                                            @Override
                                                            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                                                                Log.e(TAG, "onBitmapLoaded(){..}");
                                                                customViewHolder.product_image.setImageBitmap(bitmap);
                                                            }

                                                            @Override
                                                            public void onBitmapFailed(Drawable errorDrawable) {
                                                                Log.e(TAG, "onBitmapFailed(){..}");
                                                            }

                                                            @Override
                                                            public void onPrepareLoad(final Drawable placeHolderDrawable) {
                                                                Log.e(TAG, "onPrepareLoad(){..}");


                                                                customViewHolder.product_image.setImageDrawable(placeHolderDrawable);


                                                                //customViewHolder.product_image.setImageDrawable(placeHolderDrawable);
                                                            }
                                                        });
                                            }

                                            @Override
                                            public void onBitmapFailed(Drawable errorDrawable) {
                                                Log.e(TAG, "onBitmapFailed(){..}");
                                            }

                                            @Override
                                            public void onPrepareLoad(final Drawable placeHolderDrawable) {
                                                Log.e(TAG, "onPrepareLoad(){..}");


                                                customViewHolder.product_image.setImageDrawable(placeHolderDrawable);


                                                //customViewHolder.product_image.setImageDrawable(placeHolderDrawable);
                                            }
                                        });


                            }

                            @Override
                            public void onError() {
                                Log.e(TAG, "onError(){..}");
                            }
                        });
                Picasso.with(mContext)
                        .load(list.getProduct_small_image())
                        // .transform(AppConstants.getTransformation(customViewHolder.product_image))
                        // .transform(AppConstants.getTransformation(customViewHolder.product_image))
                        .into(customViewHolder.product_image, new Callback() {
                            @Override
                            public void onSuccess() {
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        Log.e(TAG, "getProduct_small_image()" + "onSuccess");
                                        //customViewHolder.progress_product.setVisibility(View.GONE);
                                        Log.e(list.getProduct_medium_image(), "list.getProduct_medium_image()");
                                        if (!list.getProduct_medium_image().equals("")) {
                                            Log.e(TAG, "getProduct_medium_image()");
                                            Log.e(TAG, " getProduct_medium_image ..................................................");
                                            Picasso.with(mContext)
                                                    .load(list.getProduct_medium_image())
                                                    //.transform(AppConstants.getTransformation(customViewHolder.product_image))
                                                    .into(customViewHolder.product_image1, new Callback() {
                                                        @Override
                                                        public void onSuccess() {
                                                            new Handler().postDelayed(new Runnable() {
                                                                @Override
                                                                public void run() {

                                                                    Log.e(TAG, "getProduct_medium_image()" + "onSuccess");
                                                                    //customViewHolder.progress_product.setVisibility(View.GONE);

                                                                    if (!list.getProduct_image().equals("")) {
                                                                        Log.e(TAG, "getProduct_image()");
                                                                        Picasso.with(mContext)
                                                                                .load(list.getProduct_image())
                                                                                //.transform(AppConstants.getTransformation(customViewHolder.product_image))
                                                                                .into(customViewHolder.product_image2, new Callback() {
                                                                                    @Override
                                                                                    public void onSuccess() {
                                                                                        Log.e(TAG, "getProduct_image()" + "onSuccess");
                                                                                        //customViewHolder.progress_product.setVisibility(View.GONE);
                                                                                    }

                                                                                    @Override
                                                                                    public void onError() {
                                                                                        Log.e(TAG, "getProduct_image()" + "onError");
                                                                                        // customViewHolder.progress_product.setVisibility(View.GONE);
                                                                                    }
                                                                                });
                                                                    }
                                                                }
                                                            }, 0);

                                                        }

                                                        @Override
                                                        public void onError() {
                                                            Log.e(TAG, "getProduct_medium_image()" + "onError");
                                                            // customViewHolder.progress_product.setVisibility(View.GONE);
                                                        }
                                                    });
                                        }

                                    }
                                }, 0);
                            }

                            @Override
                            public void onError() {
                                Log.e(TAG, "getProduct_small_image()" + "onError");
                                // customViewHolder.progress_product.setVisibility(View.GONE);
                            }
                        });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        customViewHolder.card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String param = "&user_id=" + Preferences.getValue_String(mContext, Preferences.USER_ID);
                param = param + "&pro_id=" + list.getProduct_id();
                param = param + "&date=" + new SimpleDateFormat("ddMMyyHHmmss").format(Calendar.getInstance().getTime());
                new GetProductDetails(param, mContext, new GetProductDetails.OnResultReceived() {
                    @Override
                    public void onResult(String result) throws UnsupportedEncodingException {
                        AppConstants.pos = pos;
                        AppConstants.TYPE = list.getType();
                        mContext.startActivity(new Intent(mContext, ProductDetailsActivity.class));
                    }

                    @Override
                    public void onFailed(VolleyError result) throws UnsupportedEncodingException {

                    }
                }, true);


            }
        });

        if (list.getIs_watch().equals("0")) {
            customViewHolder.img_favorite.setImageResource(R.drawable.heart_outline);
        } else {
            customViewHolder.img_favorite.setImageResource(R.drawable.heart);
        }

        customViewHolder.img_favorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String param = "&user_id=" + Preferences.getValue_String(mContext, Preferences.USER_ID);
                param = param + "&pro_id=" + list.getProduct_id();
                param = param + "&type=" + list.getType();
                param = param + "&date=" + new SimpleDateFormat("ddMMyyHHmmss").format(Calendar.getInstance().getTime());
                if (list.getIs_watch().equals("0")) {


                    new AddWatchListApi(param, mContext, new AddWatchListApi.OnResultReceived() {
                        @Override
                        public void onResult(String result) throws UnsupportedEncodingException {
                            infos.get(pos).setIs_watch("1");
                            customViewHolder.img_favorite.setImageResource(R.drawable.heart);
                            //fragment.LoadData(true);

                        }
                    }, true);
                } else {

                    new RemoveWatchListApi(param, mContext, new RemoveWatchListApi.OnResultReceived() {
                        @Override
                        public void onResult(String result) throws UnsupportedEncodingException {
                            infos.get(pos).setIs_watch("0");
                            customViewHolder.img_favorite.setImageResource(R.drawable.heart_outline);
                            // fragment.LoadData(true);
                        }
                    }, true);


                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return (null != infos ? infos.size() : 0);
    }


    public class CustomViewHolder extends RecyclerView.ViewHolder {

        TextView tv_productname, tv_product_desc, tv_price, tv_distance, tv_date;
        ImageView img_favorite, product_image, product_image1, product_image2;
        // ProgressBar progress_product;
        CardView card_view;
        RelativeLayout rel_date_of_placement, rel_price;

        public CustomViewHolder(View view) {
            super(view);
            this.tv_productname = (TextView) view.findViewById(R.id.tv_productname);
            this.tv_product_desc = (TextView) view.findViewById(R.id.tv_product_desc);
            this.tv_price = (TextView) view.findViewById(R.id.tv_price);
            this.rel_price = (RelativeLayout) view.findViewById(R.id.rel_price);
            this.tv_distance = (TextView) view.findViewById(R.id.tv_distance);
            this.tv_date = (TextView) view.findViewById(R.id.tv_date);
            this.product_image = (ImageView) view.findViewById(R.id.product_image);
            this.product_image1 = (ImageView) view.findViewById(R.id.product_image1);
            this.product_image2 = (ImageView) view.findViewById(R.id.product_image2);
            this.img_favorite = (ImageView) view.findViewById(R.id.img_favorite);
            this.card_view = (CardView) view.findViewById(R.id.card_view);
            // this.progress_product = (ProgressBar) view.findViewById(R.id.progress_product);
            this.rel_date_of_placement = (RelativeLayout) view.findViewById(R.id.rel_date_of_placement);
        }
    }
}

