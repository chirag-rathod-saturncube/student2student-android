package com.sc.student2student.fcm;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.sc.student2student.Activity.ApiChatActivity;
import com.sc.student2student.Activity.HomeActivity;
import com.sc.student2student.Activity.NotificationActivity;
import com.sc.student2student.R;
import com.sc.student2student.common.AppConstants;
import com.sc.student2student.common.Preferences;
import com.sc.student2student.moduls.GroupMsg;

import java.util.List;
import java.util.Map;

/**
 * Created by Saturncube10 on 22-Jul-16.
 */
public class MyFcmListenerService extends FirebaseMessagingService {
    public static final String INTENT_FILTER = "INTENT_FILTER";
    public static int NOTIFICATION_COUNT = 10;
    public static String unreadCount = "0";
    private Context context = null;

    @Override
    public void onMessageReceived(RemoteMessage message) {
        context = getApplicationContext();
        String from = message.getFrom();
        Map data = message.getData();
        Log.e("Notification ", " Data : " + data.toString());
        Log.e("From ", " From Data  : " + from);
        String msg = data.get("msg").toString();
        String nfType = data.get("nf_type").toString();
        String totalunreadMessages = data.get("totalunreadMessages").toString();
        String totalNotification = data.get("totalNotification").toString();
        Log.e("TAG", "Size : " + AppConstants.groupMsgArrayList.size());

        if (AppConstants.groupMsgArrayList.size() >= 5) {
            GroupMsg groupMsg = new GroupMsg();
            groupMsg.setMsg(msg);
            AppConstants.groupMsgArrayList.add(groupMsg);
        } else {
            GroupMsg groupMsg = new GroupMsg();
            groupMsg.setMsg(msg);
            AppConstants.groupMsgArrayList.add(groupMsg);
        }


        String unreadMsg = "";
        if (Integer.parseInt(totalunreadMessages) > 0) {
            unreadMsg += totalunreadMessages + " Messages";
            AppConstants.totalMessages = totalunreadMessages;
        }

        if (Integer.parseInt(totalNotification) > 0) {
            unreadMsg += totalNotification + " Notifications";
            AppConstants.totalNotification = totalNotification;
        }

        if (Integer.parseInt(totalNotification) > 0 && Integer.parseInt(totalNotification) > 0) {
            unreadMsg = "";
            unreadMsg += totalunreadMessages + " Messages ";
            unreadMsg += totalNotification + " Notifications";
            AppConstants.totalMessages = totalunreadMessages;
            AppConstants.totalNotification = totalNotification;
        }


        Intent intent = new Intent(INTENT_FILTER);
        sendBroadcast(intent);


        if (nfType.equals("7")) {
            if (!AppConstants.isChatScreenOpen) {
                if (AppConstants.groupMsgArrayList.size() >= 5) {
                    String prefRequestId = Preferences.getValue_String(getBaseContext(), Preferences.NOT_REQUEST_ID);
                    if (!prefRequestId.equals("")) {
                        Preferences.setValue(context, Preferences.NOT_REQUEST_ID, "" + prefRequestId);
                        Preferences.saveGroupMsgArrayList(context, Preferences.GROUP_MSG_ARRAY_LIST, AppConstants.groupMsgArrayList);
                        Preferences.setValue(context, Preferences.isSaveGroupMsg, true);
                        groupNotification(msg, true, Integer.parseInt(prefRequestId), false, unreadMsg);
                    } else {
                        int requestID = (int) System.currentTimeMillis();
                        Preferences.setValue(context, Preferences.NOT_REQUEST_ID, "" + requestID);
                        Preferences.saveGroupMsgArrayList(context, Preferences.GROUP_MSG_ARRAY_LIST, AppConstants.groupMsgArrayList);
                        Preferences.setValue(context, Preferences.isSaveGroupMsg, true);
                        groupNotification(msg, true, requestID, true, unreadMsg);
                    }
                } else {
                    int requestID = (int) System.currentTimeMillis();
                    Preferences.setValue(context, Preferences.NOT_REQUEST_ID, "" + requestID);
                    Preferences.saveGroupMsgArrayList(context, Preferences.GROUP_MSG_ARRAY_LIST, AppConstants.groupMsgArrayList);
                    Preferences.setValue(context, Preferences.isSaveGroupMsg, true);
                    groupNotification(msg, true, requestID, true, unreadMsg);
                }
            }
        } else {
            if (!AppConstants.isNotificationScreenOpen) {
                if (AppConstants.groupMsgArrayList.size() >= 5) {
                    String prefRequestId = Preferences.getValue_String(getBaseContext(), Preferences.NOT_REQUEST_ID);
                    if (!prefRequestId.equals("")) {
                        Preferences.setValue(context, Preferences.NOT_REQUEST_ID, "" + prefRequestId);
                        Preferences.saveGroupMsgArrayList(context, Preferences.GROUP_MSG_ARRAY_LIST, AppConstants.groupMsgArrayList);
                        Preferences.setValue(context, Preferences.isSaveGroupMsg, true);
                        groupNotification(msg, true, Integer.parseInt(prefRequestId), false, unreadMsg);
                    } else {
                        int requestID = (int) System.currentTimeMillis();
                        Preferences.setValue(context, Preferences.NOT_REQUEST_ID, "" + requestID);
                        Preferences.saveGroupMsgArrayList(context, Preferences.GROUP_MSG_ARRAY_LIST, AppConstants.groupMsgArrayList);
                        Preferences.setValue(context, Preferences.isSaveGroupMsg, true);
                        groupNotification(msg, true, requestID, true, unreadMsg);
                    }
                } else {
                    int requestID = (int) System.currentTimeMillis();
                    Preferences.setValue(context, Preferences.NOT_REQUEST_ID, "" + requestID);
                    Preferences.saveGroupMsgArrayList(context, Preferences.GROUP_MSG_ARRAY_LIST, AppConstants.groupMsgArrayList);
                    Preferences.setValue(context, Preferences.isSaveGroupMsg, true);
                    groupNotification(msg, true, requestID, false, unreadMsg);
                }
            }
        }
                /*===============IF WE REQUIRE GROUP NOTIFICATION==================*/
    }


    private void groupNotification(String msg, boolean flag, int requestID, boolean cancelFlag, String unreadMessage) {
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_fcm_logo)
                        .setContentTitle(getString(R.string.app_name))
                        .setSound(defaultSoundUri)
                        .setAutoCancel(true)
                        .setColor(Color.parseColor("#8CC747"))
                        .setContentText(msg);

        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.InboxStyle inboxStyle =
                new NotificationCompat.InboxStyle();

        /*if (flag) {
            if (cancelFlag) {
                mNotificationManager.cancelAll();
            }
            List<GroupMsg> tail = AppConstants.groupMsgArrayList.subList(Math.max(AppConstants.groupMsgArrayList.size() - 5, 0), AppConstants.groupMsgArrayList.size());
            for (int i = 0; i < tail.size(); i++) {
                inboxStyle.addLine(tail.get(i).getMsg());
            }
            inboxStyle.setSummaryText(unreadMessage);
        } else {
            inboxStyle.addLine(msg);
        }*/

        mNotificationManager.cancelAll();
        List<GroupMsg> tail = AppConstants.groupMsgArrayList.subList(Math.max(AppConstants.groupMsgArrayList.size() - 5, 0), AppConstants.groupMsgArrayList.size());
        for (int i = 0; i < tail.size(); i++) {
            inboxStyle.addLine(tail.get(i).getMsg());
        }
        inboxStyle.setSummaryText(unreadMessage);
        mBuilder.setStyle(inboxStyle);

        Intent resultIntent = new Intent(this, HomeActivity.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(HomeActivity.class);
        resultIntent.putExtra("NotiClick", true);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);
        mNotificationManager.notify(requestID, mBuilder.build());
    }


    private void sendNotification(String messageBody) {
        /*Intent intent = new Intent(this, NotificationActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("NotiClick", true);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_fcm_logo)
                .setContentTitle(getResources().getString(R.string.app_name))
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, notificationBuilder.build());*/

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Intent notificationIntent;

        notificationIntent = new Intent(this, NotificationActivity.class);
        notificationIntent.putExtra("NotiClick", true);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        int requestID = (int) System.currentTimeMillis();
        PendingIntent intent = PendingIntent.getActivity(this, requestID, notificationIntent, 0);

        NotificationCompat.Builder builder = new NotificationCompat
                .Builder(this)
                .setDefaults(Notification.DEFAULT_ALL)
                .setSmallIcon(R.drawable.ic_fcm_logo)
                .setContentTitle(getString(R.string.app_name))
                .setSound(defaultSoundUri)
                .setContentText(messageBody)
                .setAutoCancel(true);

        builder.setContentIntent(intent);
        builder.setColor(Color.parseColor("#8CC747"));
        builder.setAutoCancel(true);
        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        long[] pattern = {0, 100, 1500};
        if (!messageBody.equals("null")) {
            v.vibrate(pattern, -1);
            notificationManager.notify(NOTIFICATION_COUNT, builder.build());
            NOTIFICATION_COUNT = 1 + NOTIFICATION_COUNT;
        }

    }

    private void showChatActivity(String messageBody, String group_id) {
        /*Intent intent = new Intent(this, ApiChatActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("NotiClick", true);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_fcm_logo)
                .setContentTitle(getResources().getString(R.string.app_name))
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, notificationBuilder.build());*/

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Intent notificationIntent;

        notificationIntent = new Intent(this, ApiChatActivity.class);
        notificationIntent.putExtra("NotiChatClick", true);
        notificationIntent.putExtra("group_id", group_id);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        int requestID = (int) System.currentTimeMillis();
        PendingIntent intent = PendingIntent.getActivity(this, requestID, notificationIntent, 0);

        NotificationCompat.Builder builder = new NotificationCompat
                .Builder(this)
                .setDefaults(Notification.DEFAULT_ALL)
                .setSmallIcon(R.drawable.ic_fcm_logo)
                .setContentTitle(getString(R.string.app_name))
                .setSound(defaultSoundUri)
                .setContentText(messageBody)
                .setAutoCancel(true);

        builder.setContentIntent(intent);
        builder.setColor(Color.parseColor("#8CC747"));
        builder.setAutoCancel(true);
        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        long[] pattern = {0, 100, 1500};
        if (!messageBody.equals("null")) {
            v.vibrate(pattern, -1);
            notificationManager.notify(NOTIFICATION_COUNT, builder.build());
            NOTIFICATION_COUNT = 1 + NOTIFICATION_COUNT;
        }
    }
}
