package com.sc.student2student.common;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.sc.student2student.R;


/**
 * Created by pc4 on 11/19/2016.
 */

public class DialogManager {
    public static void errorDialog(Context activity, String title, String msg) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        LayoutInflater inflater = ((Activity) activity).getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_response_error, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog b = dialogBuilder.create();
        b.show();

        final ImageView imgClose = (ImageView) dialogView.findViewById(R.id.img_close);
        final TextView tvTitle = (TextView) dialogView.findViewById(R.id.tv_dialog_title);
        final TextView tvMsg = (TextView) dialogView.findViewById(R.id.tv_dialog_msg);
        final TextView tvOk = (TextView) dialogView.findViewById(R.id.tv_ok);

        tvTitle.setText(title.substring(0, 1).toUpperCase() + title.substring(1));
        tvOk.setText("OK");
        tvMsg.setText(msg);

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
            }
        });

        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
            }
        });


        tvOk.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    tvOk.setTextColor(Color.WHITE);
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    tvOk.setTextColor(Color.WHITE);
                }
                return false;
            }
        });


    }

    public static void errorDialog(final Context activity,String msg) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        LayoutInflater inflater = ((Activity) activity).getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_response_error, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog b = dialogBuilder.create();
        b.show();

        final ImageView imgClose = (ImageView) dialogView.findViewById(R.id.img_close);
        final TextView tvTitle = (TextView) dialogView.findViewById(R.id.tv_dialog_title);
        final TextView tvMsg = (TextView) dialogView.findViewById(R.id.tv_dialog_msg);
        final TextView tvOk = (TextView) dialogView.findViewById(R.id.tv_ok);

        // tvTitle.setText(title.substring(0, 1).toUpperCase() + title.substring(1));
        tvTitle.setText("");
        tvOk.setText("OK");
        tvMsg.setText(msg);

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
                ((Activity) activity).finish();
            }
        });

        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
                ((Activity) activity).finish();
            }
        });


        tvOk.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    tvOk.setTextColor(Color.WHITE);
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    tvOk.setTextColor(Color.WHITE);
                }
                return false;
            }
        });


    }

    public static void errorDialog(Context activity, String title, String msg, final OnactionResult onactionResult) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        LayoutInflater inflater = ((Activity) activity).getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_response_error, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog b = dialogBuilder.create();
        b.setCancelable(false);
        b.show();

        final ImageView imgClose = (ImageView) dialogView.findViewById(R.id.img_close);
        final TextView tvTitle = (TextView) dialogView.findViewById(R.id.tv_dialog_title);
        final TextView tvMsg = (TextView) dialogView.findViewById(R.id.tv_dialog_msg);
        final TextView tvOk = (TextView) dialogView.findViewById(R.id.tv_ok);

        tvTitle.setText(title.substring(0, 1).toUpperCase() + title.substring(1));
        tvOk.setText("OK");
        tvMsg.setText(msg);

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
            }
        });

        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
                onactionResult.Action();
            }
        });


        tvOk.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    tvOk.setTextColor(Color.WHITE);
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    tvOk.setTextColor(Color.WHITE);
                }
                return false;
            }
        });


    }

    public static void responseUnwonted(final Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(context.getString(R.string.network_title));
        builder.setMessage(context.getString(R.string.network_msg));
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

   /* public static void resetPassword(final Context activity, String title, String msg) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        LayoutInflater inflater = ((Activity) activity).getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_response_error, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog b = dialogBuilder.create();
        b.show();

        final ImageView imgClose = (ImageView) dialogView.findViewById(R.id.img_close);
        final TextView tvTitle = (TextView) dialogView.findViewById(R.id.tv_dialog_title);
        final TextView tvMsg = (TextView) dialogView.findViewById(R.id.tv_dialog_msg);
        final TextView tvOk = (TextView) dialogView.findViewById(R.id.tv_ok);

        tvTitle.setText(title.substring(0, 1).toUpperCase() + title.substring(1));
        tvOk.setText("OK");
        tvMsg.setText(msg);

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
            }
        });

        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ForgotPasswordActivity) activity).finish();
                b.dismiss();
            }
        });


        tvOk.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    tvOk.setTextColor(Color.WHITE);
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    tvOk.setTextColor(Color.rgb(6, 190, 188));
                }
                return false;
            }
        });
    }

    public static void filterDialog(final Context activity) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        LayoutInflater inflater = ((Activity) activity).getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dailgo_category, null);
        dialogBuilder.setView(dialogView);

        final AlertDialog b = dialogBuilder.create();
        b.show();

        final ImageView img_close = (ImageView) dialogView.findViewById(R.id.img_close);

        final RecyclerView recycler_view = (RecyclerView) dialogView.findViewById(R.id.recycler_view);
        LinearLayoutManager mLayoutManager;
        mLayoutManager = new LinearLayoutManager(activity);
        mLayoutManager.setSmoothScrollbarEnabled(true);
        recycler_view.setLayoutManager(mLayoutManager);

        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
            }
        });

       *//* if (AppConstants.selectedCategory.size() > 0) {
            for (int i = 0; i < AppConstants.catArrayList.size(); i++) {
                if (AppConstants.catArrayList.get(i).catItem != null) {
                    if (AppConstants.selectedCategory.containsKey(AppConstants.catArrayList.get(i).catItem.getSports_id())) {
                        AppConstants.catArrayList.get(i).catItem.setFlagFilter(true);
                    } else {
                        AppConstants.catArrayList.get(i).catItem.setFlagFilter(false);
                    }
                }
            }
        }
      *//*

       *//* if (AppConstants.catArrayList.size() > 0) {
            CatAdapter adapter = new CatAdapter(activity, AppConstants.catArrayList);
            adapter.setMode(ExpandableRecyclerAdapter.MODE_NORMAL);
            recycler_view.setLayoutManager(new LinearLayoutManager(activity));
            recycler_view.setAdapter(adapter);
        } else {
            tv_no_category.setVisibility(View.VISIBLE);
            recycler_view.setVisibility(View.GONE);
        }*//*


        *//*tv_reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
            }
        });*//*

       *//* tv_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.selectedCategory.clear();
                Preferences.setValue(activity, Preferences.selectedCatMap, "");
                Preferences.setValue(activity, Preferences.isSetCategory, false);

                for (int i = 0; i < AppConstants.catArrayList.size(); i++) {
                    if (AppConstants.catArrayList.get(i).catItem != null) {
                        if (AppConstants.catArrayList.get(i).catItem.isFlagFilter()) {
                            //Log.e("TAG", "isFilter: " + AppConstants.catArrayList.get(i).catItem.isFlagFilter());
                            if (!AppConstants.selectedCategory.containsKey(AppConstants.catArrayList.get(i).catItem.getSports_id())) {
                                AppConstants.selectedCategory.put(AppConstants.catArrayList.get(i).catItem.getSports_id(), AppConstants.catArrayList.get(i).catItem.getSport_name());
                            }
                        }
                    }
                }

                Log.e("TAG", "selected category list: " + AppConstants.selectedCategory.size());
                if (AppConstants.selectedCategory.size() > 0) {
                    Preferences.saveMap(activity, Preferences.selectedCatMap, AppConstants.selectedCategory);
                    Preferences.setValue(activity, Preferences.isSetCategory, true);
                } else {
                    Preferences.setValue(activity, Preferences.selectedCatMap, "");
                    Preferences.setValue(activity, Preferences.isSetCategory, false);
                }


                b.dismiss();
                ((MainActivity) activity).addFilterMarker();
            }
        });*//*

    }*/

    public interface OnactionResult {
        public void Action();
    }


}
