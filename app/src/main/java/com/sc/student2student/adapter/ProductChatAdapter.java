package com.sc.student2student.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sc.student2student.R;
import com.sc.student2student.common.AppConstants;
import com.sc.student2student.common.MyTagHandler;
import com.sc.student2student.databinding.ItemChatMessageLeftLayoutBinding;
import com.sc.student2student.databinding.ItemChatMessageRightLayoutBinding;
import com.sc.student2student.moduls.Messages;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by pc4 on 5/30/2016.
 */
public class ProductChatAdapter extends RecyclerView.Adapter<ProductChatAdapter.CustomViewHolder> {
    List<Messages> infos = null;
    private ArrayList<Messages> arrayList;
    private Context mContext;
    private String TAG = getClass().getSimpleName();

    public ProductChatAdapter(Context context, List<Messages> list) {
        this.infos = list;
        this.arrayList = new ArrayList<>();
        this.arrayList.addAll(infos);
        this.mContext = context;
    }

    public static Spanned fromHtml(String html) {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(html, null, new MyTagHandler());
        } else {
            result = Html.fromHtml(html, null, new MyTagHandler());
        }
        return result;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
       /* CustomViewHolder viewHolder;
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ItemChatMessageLeftLayoutBinding listBinding =
                DataBindingUtil.inflate(inflater, R.layout.item_chat_message_left_layout, viewGroup, false);
        viewHolder = new CustomViewHolder(listBinding.getRoot(), listBinding);
        return viewHolder;*/

        CustomViewHolder viewHolder;
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        switch (i) {
            case 0:
                ItemChatMessageLeftLayoutBinding bindingItemLeft =
                        DataBindingUtil.inflate(inflater, R.layout.item_chat_message_left_layout, viewGroup, false);
                viewHolder = new CustomViewHolder(bindingItemLeft.getRoot(), bindingItemLeft);
                return viewHolder;
            case 1:
                ItemChatMessageRightLayoutBinding bindingItem =
                        DataBindingUtil.inflate(inflater, R.layout.item_chat_message_right_layout, viewGroup, false);
                viewHolder = new CustomViewHolder(bindingItem.getRoot(), bindingItem);
                return viewHolder;

            default:
                ItemChatMessageLeftLayoutBinding bindingItem1 =
                        DataBindingUtil.inflate(inflater, R.layout.item_chat_message_left_layout, viewGroup, false);
                viewHolder = new CustomViewHolder(bindingItem1.getRoot(), bindingItem1);
                return viewHolder;
        }
    }

    @Override
    public void onBindViewHolder(final CustomViewHolder holder, final int pos) {
        final Messages list = infos.get(pos);
        switch (holder.getItemViewType()) {
            case 0:
                holder.bindingItemLeft.tvDate.setText(list.getTimestamp());
                if (list.getType().equals("1")) {
                    holder.bindingItemLeft.tvMessage.setVisibility(View.VISIBLE);
                    holder.bindingItemLeft.tvMessage.setText(list.getMsg());
                    holder.bindingItemLeft.relImage.setVisibility(View.GONE);
                } else {
                    holder.bindingItemLeft.tvMessage.setVisibility(View.GONE);
                    holder.bindingItemLeft.relImage.setVisibility(View.VISIBLE);
                    holder.bindingItemLeft.imgSendImage.setImageResource(R.drawable.bg_default);
                    if (!list.getMsg().equals("")) {
                        Picasso.with(mContext)
                                .load(list.getMsg())
                                .placeholder(R.drawable.bg_default)
                                .transform(AppConstants.getTransformation(holder.bindingItemLeft.imgSendImage))
                                .into(holder.bindingItemLeft.imgSendImage, new Callback() {
                                    @Override
                                    public void onSuccess() {
                                        holder.bindingItemLeft.progressProduct.setVisibility(View.GONE);
                                    }

                                    @Override
                                    public void onError() {
                                        holder.bindingItemLeft.progressProduct.setVisibility(View.GONE);
                                        Picasso.with(mContext)
                                                .load(R.drawable.bg_default)
                                                .into(holder.bindingItemLeft.imgSendImage);
                                    }
                                });
                    } else {
                        holder.bindingItemLeft.progressProduct.setVisibility(View.VISIBLE);
                    }
                }
                break;
            case 1:
                holder.bindingItemRight.tvDate.setText(list.getTimestamp());
                if (list.getType().equals("1")) {
                    holder.bindingItemRight.tvMessage.setVisibility(View.VISIBLE);
                    holder.bindingItemRight.tvMessage.setText(list.getMsg());
                    holder.bindingItemRight.relImage.setVisibility(View.GONE);
                } else {
                    holder.bindingItemRight.tvMessage.setVisibility(View.GONE);
                    holder.bindingItemRight.relImage.setVisibility(View.VISIBLE);
                    holder.bindingItemRight.imgSendImage.setImageResource(R.drawable.bg_default);

                    if (!list.getMsg().equals("")) {
                        Picasso.with(mContext)
                                .load(list.getMsg())
                                .placeholder(R.drawable.bg_default)
                                .transform(AppConstants.getTransformation(holder.bindingItemRight.imgSendImage))
                                .into(holder.bindingItemRight.imgSendImage, new Callback() {
                                    @Override
                                    public void onSuccess() {
                                        holder.bindingItemRight.progressProduct.setVisibility(View.GONE);
                                    }

                                    @Override
                                    public void onError() {
                                        holder.bindingItemRight.progressProduct.setVisibility(View.GONE);
                                        Picasso.with(mContext)
                                                .load(R.drawable.bg_default)
                                                .into(holder.bindingItemRight.imgSendImage);
                                    }
                                });
                    } else {
                        holder.bindingItemRight.progressProduct.setVisibility(View.VISIBLE);
                    }

                }
                break;
        }

    }

    @Override
    public int getItemViewType(int position) {
        return infos.get(position).getLayoutType();
    }

    @Override
    public int getItemCount() {
        return (null != infos ? infos.size() : 0);
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {
        ItemChatMessageLeftLayoutBinding bindingItemLeft;
        ItemChatMessageRightLayoutBinding bindingItemRight;

        public CustomViewHolder(View view, ItemChatMessageLeftLayoutBinding bindingItemLeft) {
            super(view);
            this.bindingItemLeft = bindingItemLeft;
        }

        public CustomViewHolder(View view, ItemChatMessageRightLayoutBinding bindingItemRight) {
            super(view);
            this.bindingItemRight = bindingItemRight;
        }

    }
}

