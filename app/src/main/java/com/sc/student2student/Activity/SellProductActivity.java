package com.sc.student2student.Activity;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.edmodo.cropper.CropImageView;
import com.sc.student2student.R;
import com.sc.student2student.adapter.PhotoAdapter;
import com.sc.student2student.adapter.ProductPhotosAdapter;
import com.sc.student2student.api.EditProductApi;
import com.sc.student2student.api.GetCondtion;
import com.sc.student2student.api.GetPricePer;
import com.sc.student2student.common.AppConstants;
import com.sc.student2student.common.CustomMultipartRequest;
import com.sc.student2student.common.DialogManager;
import com.sc.student2student.common.LoadingDialog;
import com.sc.student2student.common.Preferences;
import com.sc.student2student.common.WebServices;
import com.sc.student2student.databinding.ActivitySellProductBinding;
import com.sc.student2student.moduls.Condition;
import com.sc.student2student.moduls.PricePer;
import com.sc.student2student.volley.VolleySingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SellProductActivity extends AppCompatActivity {
    public static final int REQUEST_CODE_TAKE_PICTURE = 2;
    public static final int REQUEST_CODE_GALLERY = 1;
    final private int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124;
    ActivitySellProductBinding binding;
    ArrayAdapter<Condition> adapter;
    ArrayAdapter<PricePer> pricePerArrayAdapter;
    ProductPhotosAdapter ProductPhotosAdapter;
    String condition_id = "";
    String priceper_name = "";
    String Price = "";
    String website = "";
    LinearLayoutManager layoutManager;
    LinearLayoutManager layoutManager1;
    String selectedImagePath = "";
    Map<String, String> mHeaderPart = new HashMap<>();
    Map<String, String> mStringPart = new HashMap<>();
    LoadingDialog loadingDialog;
    PhotoAdapter photoAdapter;
    private String TAG = getClass().getSimpleName();
    private Bitmap bitmap = null;
    private boolean isPriceStatus = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_sell_product);
        initView();
        VolleySingleton.getInstance().init(getApplicationContext());
        AppConstants.photo_list.clear();
        String state = Environment.getExternalStorageState();


        binding.edtTitle.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                int count = 16 - charSequence.length();
                if (count==1){
                    binding.tvCount.setText("You have " + count + " character left");
                }else if (count==0){
                    binding.tvCount.setText("You have exceeded maximum allowed character limit");
                }else {
                    binding.tvCount.setText("You have " + count + " characters left");
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        if (Environment.MEDIA_MOUNTED.equals(state)) {
            AppConstants.mFileTemp = new File(Environment.getExternalStorageDirectory(), AppConstants.TEMP_PHOTO_FILE_NAME);
        } else {
            AppConstants.mFileTemp = new File(getFilesDir(), AppConstants.TEMP_PHOTO_FILE_NAME);
        }

        binding.relAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= 23) {
                    if (checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                            && checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                            && checkSelfPermission(android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        getAllPermission();
                    } else {
                        startDialog();
                    }
                } else {
                    startDialog();
                }
            }
        });

        if (AppConstants.TYPE.equals(AppConstants.PRODUCT)) {
            new GetCondtion("", SellProductActivity.this, new GetCondtion.OnResultReceived() {
                @Override
                public void onResult(String result) throws UnsupportedEncodingException {
                    if (AppConstants.condtionList.size() > 0) {
                        adapter = new ArrayAdapter<Condition>(SellProductActivity.this, R.layout.spinner_item, AppConstants.condtionList);
                        binding.spnCondtion.setAdapter(adapter);
                    }
                }
            }, true);


            binding.spnCondtion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    Condition category = (Condition) binding.spnCondtion.getSelectedItem();
                    if (category.getCon_id().equals("0")) {
                        condition_id = "";
                    } else {
                        condition_id = category.getCon_id();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }


        if (AppConstants.TYPE.equals(AppConstants.SERVICE)) {
            new GetPricePer("", SellProductActivity.this, new GetPricePer.OnResultReceived() {
                @Override
                public void onResult(String result) throws UnsupportedEncodingException {
                    if (AppConstants.priceperList.size() > 0) {
                        pricePerArrayAdapter = new ArrayAdapter<PricePer>(SellProductActivity.this, R.layout.spinner_item, AppConstants.priceperList);
                        binding.spnPriceper.setAdapter(pricePerArrayAdapter);
                    }
                }
            }, true);


            binding.spnPriceper.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    PricePer pricePer = (PricePer) binding.spnPriceper.getSelectedItem();
                    if (pricePer.getPrice_id().equals("0")) {
                        priceper_name = "";
                    } else {
                        priceper_name = pricePer.getPrice_name();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }
        if (AppConstants.TYPE.equals(AppConstants.PRODUCT)) {
            binding.relWebsiteLink.setVisibility(View.GONE);
            binding.relWebsiteStatus.setVisibility(View.GONE);
            binding.relFreeStatus.setVisibility(View.VISIBLE);
        } else {
            //  binding.relWebsiteLink.setVisibility(View.VISIBLE);
            binding.relWebsiteStatus.setVisibility(View.VISIBLE);
            binding.relFreeStatus.setVisibility(View.GONE);
        }


        if (AppConstants.ACTION_PRODUCT.equals(AppConstants.NEW)) {
            binding.relPhoto1.setVisibility(View.GONE);
        } else {
            binding.relPhoto1.setVisibility(View.VISIBLE);
            binding.edtTitle.setText(AppConstants.product.getProduct_name());
            binding.edtDesc.setText(AppConstants.product.getProduct_desc());
            //binding.edtPrice.setText(AppConstants.product.getPrice());
            if (AppConstants.TYPE.equals(AppConstants.PRODUCT)) {


                binding.relPrice.setVisibility(View.VISIBLE);
                binding.relFreeStatus.setVisibility(View.VISIBLE);

                if (!AppConstants.product.getPrice().equals("")) {
                    if (AppConstants.product.getPrice().startsWith("€")) {
                        binding.edtPrice.setText(AppConstants.product.getPrice().substring(1));
                    }
                }
            } else {
                binding.relFreeStatus.setVisibility(View.GONE);
                if (AppConstants.product.getPrice_status().equals("0")) {
                    binding.switchStatus.setChecked(false);
                    isPriceStatus = false;
                    binding.relPrice.setVisibility(View.GONE);
                    binding.relPriceper.setVisibility(View.GONE);
                } else {
                    binding.switchStatus.setChecked(true);
                    isPriceStatus = true;
                    binding.relPrice.setVisibility(View.VISIBLE);
                    binding.relPriceper.setVisibility(View.VISIBLE);
                    binding.edtPrice.setText(AppConstants.product.getPrice());
                }

            }

            condition_id = AppConstants.product.getConditions();

            if (AppConstants.TYPE.equals(AppConstants.PRODUCT)) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        binding.spnCondtion.setSelection(getIndex(binding.spnCondtion, AppConstants.product.getCondition_name()));
                    }
                }, 2000);

            }
/*
            if (AppConstants.TYPE.equals(AppConstants.SERVICE)) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        binding.spnPriceper.setSelection(getIndex(binding.spnPriceper, ));
                    }
                }, 2000);

            }
*/
            if (AppConstants.product.getImages().size() > 0) {
                photoAdapter = new PhotoAdapter(SellProductActivity.this, AppConstants.product.getImages(), new PhotoAdapter.OnResultReceived() {
                    @Override
                    public void onResult(int i) {

                    }
                });

                binding.photoReclerview1.setAdapter(photoAdapter);
                photoAdapter.notifyDataSetChanged();
            } else {
                binding.relPhoto1.setVisibility(View.GONE);
            }


        }

        binding.relToolbar.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        binding.btnSell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AppConstants.ACTION_PRODUCT.equals(AppConstants.NEW)) {
                    if (isValidate()) {
                        AddProductanService();
                    }
                } else {
                    if (isValidate1()) {
                        UpdateProduct();
                    }
                }
            }
        });


        binding.edtPrice.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    binding.btnSell.callOnClick();
                }
                return false;
            }
        });


        if (!AppConstants.TYPE.equals(AppConstants.PRODUCT)) {
            binding.switchStatus.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        binding.relPrice.setVisibility(View.VISIBLE);
                        binding.relPriceper.setVisibility(View.VISIBLE);
                        isPriceStatus = true;
                    } else {
                        binding.relPrice.setVisibility(View.GONE);
                        binding.relPriceper.setVisibility(View.GONE);
                        binding.edtPrice.setText("");
                        isPriceStatus = false;
                    }
                }
            });

            binding.switchWebsiteStatus.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (b) {
                        binding.relWebsiteLink.setVisibility(View.VISIBLE);
                    } else {
                        binding.relWebsiteLink.setVisibility(View.GONE);
                    }
                }
            });


        } else {
            binding.switchFreeStatus.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (b) {
                        binding.relPrice.setVisibility(View.GONE);

                    } else {
                        binding.relPrice.setVisibility(View.VISIBLE);
                    }
                }
            });
        }

    }

    private void initView() {
        // binding.relToolbar.tvTitle.setText("Sell Product");
        binding.tvCount.setText("You can enter a maximum of 16 characters.");

        layoutManager = new LinearLayoutManager(SellProductActivity.this, LinearLayoutManager.HORIZONTAL, false);
        binding.photoReclerview.setLayoutManager(layoutManager);

        if (AppConstants.ACTION_PRODUCT.equals(AppConstants.NEW)) {
            binding.tvCategory.setText(AppConstants.cat.getCategory_name());
        } else {
            binding.tvCategory.setText(AppConstants.product.getCategory_name());
        }
        layoutManager1 = new LinearLayoutManager(SellProductActivity.this, LinearLayoutManager.HORIZONTAL, false);
        binding.photoReclerview1.setLayoutManager(layoutManager1);


        //move from resume to initView

        if (AppConstants.TYPE.equals(AppConstants.PRODUCT)) {
            binding.relToolbar.tvTitle.setText("Product details");
            if (AppConstants.ACTION_PRODUCT.equals(AppConstants.NEW)) {
                binding.btnSell.setText("Upload product");
            } else {
                binding.btnSell.setText("Save");
            }
            binding.relCondition.setVisibility(View.VISIBLE);
            binding.relPriceper.setVisibility(View.GONE);
            binding.relPriceStatus.setVisibility(View.GONE);
        } else {
            binding.relToolbar.tvTitle.setText("Service details");
            if (AppConstants.ACTION_PRODUCT.equals(AppConstants.NEW)) {
                binding.btnSell.setText("Upload service");
            } else {
                binding.btnSell.setText("Save");
            }
            binding.relCondition.setVisibility(View.GONE);
            binding.relPriceStatus.setVisibility(View.VISIBLE);
            binding.relPrice.setVisibility(View.GONE);
            binding.relPriceper.setVisibility(View.GONE);
        }


    }

    @Override
    public void onBackPressed() {
        finish();
    }

    public void startDialog() {
        android.support.v7.app.AlertDialog.Builder myAlertDialog = new android.support.v7.app.AlertDialog.Builder(SellProductActivity.this);
        myAlertDialog.setTitle("Select Photo");
        myAlertDialog.setMessage("How would you like to upload Photo?");

        myAlertDialog.setPositiveButton("Gallery",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        openGallery();
                    }
                });

        myAlertDialog.setNegativeButton("Camera",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        takePicture();
                    }
                });

        /*myAlertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                sp_document.setSelection(0);
            }
        });*/

        myAlertDialog.show();
    }

    private void startCropImage() {

        final Dialog dialog = new Dialog(SellProductActivity.this,
                R.style.DialogFullScreen);

        dialog.setContentView(R.layout.dialog_crop);
        dialog.setCancelable(false);
        final CropImageView cropImageView = (CropImageView) dialog.findViewById(R.id.CropImageView);
        final ImageButton rotateButtonLeft = (ImageButton) dialog.findViewById(R.id.Button_rotate_left);
        final ImageButton rotateButtonRight = (ImageButton) dialog.findViewById(R.id.Button_rotate_right);
        final Button CROP = (Button) dialog.findViewById(R.id.Crop);
        final Button CANCEL = (Button) dialog.findViewById(R.id.Cancel);
        final ProgressBar progress_product = (ProgressBar) dialog.findViewById(R.id.progress_product);

        Log.e("IMAGE_PATH", AppConstants.mFileTemp.getPath());
        Log.e("IMAGE", BitmapFactory.decodeFile(AppConstants.mFileTemp.getPath()) + "");

        bitmap = AppConstants.getRescaledBitmap(AppConstants.mFileTemp.getPath());
        //bitmap = BitmapFactory.decodeFile(AppConstants.mFileTemp.getPath());

        try {
            Bitmap bitmap = BitmapFactory.decodeFile(AppConstants.mFileTemp.getPath());
            bitmap.getWidth();
            cropImageView.setImageBitmap(bitmap);
           /* cropImageView.setAspectRatio(15, 15);
            cropImageView.setFixedAspectRatio(true);*/
        } catch (Exception e) {
            Toast.makeText(SellProductActivity.this, "Image Not Available",
                    Toast.LENGTH_SHORT).show();
            CROP.setVisibility(View.GONE);
        }
        rotateButtonLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cropImageView.rotateImage(-90);
            }
        });


        rotateButtonRight.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                cropImageView.rotateImage(90);
            }
        });

        CROP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String imagePath;
                bitmap = cropImageView.getCroppedImage();
                new AsyncTask<Void, Void, Void>() {
                    @Override
                    protected void onPreExecute() {
                        super.onPreExecute();
                        loadingDialog = new LoadingDialog(SellProductActivity.this, "", false);
                    }

                    @Override
                    protected Void doInBackground(Void... params) {
                        if (storeImageToSDCard(bitmap) != null) {
                            //AppConstants.photo_list.add(0, storeImageToSDCard(bitmap));
                            AppConstants.photo_list.add(storeImageToSDCard(bitmap));
                        }
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void aVoid) {
                        super.onPostExecute(aVoid);
                        loadingDialog.hide();
                        if (AppConstants.photo_list.size() > 0) {
                            ProductPhotosAdapter = new ProductPhotosAdapter(SellProductActivity.this, AppConstants.photo_list, new ProductPhotosAdapter.OnResultReceived() {
                                @Override
                                public void onResult(int i) {
                          /*  AppConstants.photo_list.remove(i);
                            ProductPhotosAdapter.notifyDataSetChanged();*/
                                }
                            });
                            binding.photoReclerview.setAdapter(ProductPhotosAdapter);
                            ProductPhotosAdapter.notifyDataSetChanged();
                        }

                        dialog.dismiss();
                    }
                }.execute();


            }
        });
        CANCEL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS: {
                Map<String, Integer> perms = new HashMap<String, Integer>();
                // Initial
                perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                // Fill with results
                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);
                // Check for ACCESS_FINE_LOCATION
                if (perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    // All Permissions Granted
                    Toast.makeText(SellProductActivity.this, "All Permissions are Granted", Toast.LENGTH_SHORT)
                            .show();
                    startDialog();
                } else {
                    // Permission Denied
                    Toast.makeText(SellProductActivity.this, "Some Permission is Denied", Toast.LENGTH_SHORT)
                            .show();
                }
            }
            break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private File storeImageToSDCard(Bitmap processedBitmap) {
        File file = null;
        try {
            // TODO Auto-generated method stub
            OutputStream output;
            File filepath = Environment.getExternalStorageDirectory();
            File dir = new File(filepath.getAbsolutePath() + "/" + AppConstants.APP_NAME + "/");
            dir.mkdirs();
            String imge_name = AppConstants.APP_NAME + System.currentTimeMillis() + ".jpg";
            file = new File(dir, imge_name);
            if (file.exists()) {
                file.delete();
                file.createNewFile();
            } else {
                file.createNewFile();
            }

            try {
                output = new FileOutputStream(file);
                processedBitmap.compress(Bitmap.CompressFormat.PNG, 50, output);
                output.flush();
                output.close();
                int file_size = Integer.parseInt(String.valueOf(file.length() / 1024));

                selectedImagePath = file.getAbsolutePath();

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return file;
    }

    private void openGallery() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, REQUEST_CODE_GALLERY);
    }

    private void takePicture() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        try {
            Uri mImageCaptureUri = null;
            // mImageCaptureUri = Uri.fromFile(AppConstants.mFileTemp);
            mImageCaptureUri = FileProvider.getUriForFile(SellProductActivity.this, this.getApplicationContext().getPackageName() + ".provider", AppConstants.mFileTemp);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
            intent.putExtra("return-data", true);
            startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != Activity.RESULT_OK) {
            return;
        }
        switch (requestCode) {
            case REQUEST_CODE_GALLERY:
                try {
                    InputStream inputStream = getContentResolver().openInputStream(data.getData());
                    FileOutputStream fileOutputStream = new FileOutputStream(AppConstants.mFileTemp);
                    AppConstants.copyStream(inputStream, fileOutputStream);
                    fileOutputStream.close();
                    inputStream.close();
                    Log.e("TAG", "Starts For Crop: ");
                    startCropImage();
                } catch (Exception e) {
                    Log.e("TAG", "Exception: " + e.getMessage());
                }
                break;
            case REQUEST_CODE_TAKE_PICTURE:
                Log.e("TAG", "Starts For Crop: ");
                startCropImage();
                break;
        }

    }

    //Permission methods
    public void getAllPermission() {
        List<String> permissionsNeeded = new ArrayList<String>();
        if (Build.VERSION.SDK_INT >= 23) {
            final List<String> permissionsList = new ArrayList<String>();

            if (!addPermission(permissionsList, android.Manifest.permission.CAMERA))
                permissionsNeeded.add("Camera");
            if (!addPermission(permissionsList, android.Manifest.permission.READ_EXTERNAL_STORAGE))
                permissionsNeeded.add("Read External Storage");
            if (!addPermission(permissionsList, Manifest.permission.WRITE_EXTERNAL_STORAGE))
                permissionsNeeded.add("Write External Storage");

            if (permissionsList.size() > 0) {
                if (permissionsNeeded.size() > 0) {
                    // Need Rationale
                    String message = "You need to grant access to " + permissionsNeeded.get(0);
                    for (int i = 1; i < permissionsNeeded.size(); i++)
                        message = message + ", " + permissionsNeeded.get(i);
                    showMessageOKCancel(message,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (Build.VERSION.SDK_INT >= 23) {
                                        requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                                                REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                                    }
                                }
                            });
                    // return;
                }
                requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                        REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                //return;
            }
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new android.app.AlertDialog.Builder(SellProductActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    private boolean addPermission(List<String> permissionsList, String permission) {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                permissionsList.add(permission);
                // Check for Rationale Option
                if (!shouldShowRequestPermissionRationale(permission))
                    return false;
            }
        }
        return true;
    }

    private boolean isValidate() {
        boolean flagValidate = true;
        String validateMsg = "";
        if (AppConstants.TYPE.equals(AppConstants.PRODUCT)) {
            //Log.e(TAG, "Available status in product: " + binding.switchStatus.isSelected());
            if (binding.edtTitle.getText().toString().trim().equals("")) {
                validateMsg = "Please enter product title";
                flagValidate = false;
            } else if (binding.edtTitle.getText().length() > 35) {
                validateMsg = "Please enter less then 35 character";
                flagValidate = false;
            } else if (binding.edtDesc.getText().toString().trim().equals("")) {
                validateMsg = "Please enter product description";
                flagValidate = false;
            } else if (condition_id.equals("")) {
                validateMsg = "Please select condition";
                flagValidate = false;
            } else if (AppConstants.photo_list.size() == 0) {
                validateMsg = "Please add product image";
                flagValidate = false;
            } /*else if (binding.edtPrice.getText().toString().trim().equals("")) {
                validateMsg = "Please enter price";
                flagValidate = false;
            }*/ else {
                flagValidate = true;
            }
        } else {
            Log.e(TAG, "Available status in service: " + isPriceStatus);
            if (binding.edtTitle.getText().toString().trim().equals("")) {
                validateMsg = "Please enter product title";
                flagValidate = false;
            } else if (binding.edtDesc.getText().toString().trim().equals("")) {
                validateMsg = "Please enter product description";
                flagValidate = false;
            } else if (AppConstants.photo_list.size() == 0) {
                validateMsg = "Please add product image";
                flagValidate = false;
            } else if (isPriceStatus) {
                if (binding.edtPrice.getText().toString().trim().equals("")) {
                    validateMsg = "Please enter price";
                    flagValidate = false;
                } else {
                    flagValidate = true;
                }
            } else {
                flagValidate = true;
            }
        }

        if (flagValidate) {
            return true;
        } else {
            DialogManager.errorDialog(SellProductActivity.this,
                    "Error!", validateMsg);
            return false;
        }
    }


    private boolean isValidate1() {
        boolean flagValidate = true;
        String validateMsg = "";
        if (AppConstants.TYPE.equals(AppConstants.PRODUCT)) {
            if (binding.edtTitle.getText().toString().trim().equals("")) {
                validateMsg = "Please enter product title";
                flagValidate = false;
            } else if (binding.edtDesc.getText().toString().trim().equals("")) {
                validateMsg = "Please enter product description";
                flagValidate = false;
            } else if (condition_id.equals("")) {
                validateMsg = "Please select condition";
                flagValidate = false;
            } else if (AppConstants.photo_list.size() == 0) {
                if (photoAdapter != null) {
                    if (photoAdapter.getFiles().size() == 0) {
                        validateMsg = "Please add product image";
                        flagValidate = false;
                    }
                } else {
                    validateMsg = "Please add product image";
                    flagValidate = false;
                }
            } /*else if (binding.edtPrice.getText().toString().trim().equals("")) {
                validateMsg = "Please enter price";
                flagValidate = false;
            }*/ else {
                flagValidate = true;
            }
        } else {
            if (binding.edtTitle.getText().toString().trim().equals("")) {
                validateMsg = "Please enter product title";
                flagValidate = false;
            } else if (binding.edtDesc.getText().toString().trim().equals("")) {
                validateMsg = "Please enter product description";
                flagValidate = false;

            } else if (AppConstants.photo_list.size() == 0) {
                if (photoAdapter != null) {
                    if (photoAdapter.getFiles().size() == 0) {
                        validateMsg = "Please add product image";
                        flagValidate = false;
                    }
                } else {
                    validateMsg = "Please add product image";
                    flagValidate = false;
                }
            } else if (isPriceStatus) {
                if (binding.edtPrice.toString().trim().equals("")) {
                    validateMsg = "Please enter price";
                    flagValidate = false;
                } else {
                    flagValidate = true;
                }
            } else {
                flagValidate = true;
            }
        }

        if (flagValidate) {
            return true;
        } else {
            DialogManager.errorDialog(SellProductActivity.this,
                    "Error!", validateMsg);
            return false;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    private void AddProductanService() {
        String param = "&user_id=" + Preferences.getValue_String(SellProductActivity.this, Preferences.USER_ID);
        param = param + "&date=" + new SimpleDateFormat("ddMMyyHHmmss").format(Calendar.getInstance().getTime());
        Price = binding.edtPrice.getText().toString();

        if (!binding.edtWebsiteLink.getText().toString().contains("https://")) {
            website = "https://" + binding.edtWebsiteLink.getText().toString();
        }
        Log.e(TAG, "Link : " + website);


        try {
            param = param + "&title=" + URLEncoder.encode(binding.edtTitle.getText().toString(), AppConstants.encodeType);
            param = param + "&desc=" + URLEncoder.encode(binding.edtDesc.getText().toString(), AppConstants.encodeType);
            param = param + "&cat_id=" + URLEncoder.encode(AppConstants.cat.getCat_id(), AppConstants.encodeType);
            param = param + "&con_id=" + URLEncoder.encode(condition_id, AppConstants.encodeType);
            if (AppConstants.TYPE.equals(AppConstants.PRODUCT)) {
                param = param + "&price=" + URLEncoder.encode(Price, AppConstants.encodeType);
            } else {
                param = param + "&website=" + URLEncoder.encode(website, AppConstants.encodeType);
                if (isPriceStatus) {
                    param = param + "&price=" + URLEncoder.encode(Price, AppConstants.encodeType);
                    param = param + "&price_status=1";
                    param = param + "&price_per=" + URLEncoder.encode(priceper_name, AppConstants.encodeType);
                } else {
                    param = param + "&price=0";
                    param = param + "&price_status=0";
                }
            }
            param = param + "&type=" + AppConstants.TYPE;

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        if (ProductPhotosAdapter.getFiles().size() > 0) {
            String url = WebServices.ADD_SELL_PRODUCT + param;

            Log.e("Url", url);

            loadingDialog = new LoadingDialog(SellProductActivity.this, "", false);

            CustomMultipartRequest mCustomRequest = new CustomMultipartRequest(Request.Method.POST, SellProductActivity.this, url, new Response.Listener<NetworkResponse>() {
                @Override
                public void onResponse(NetworkResponse networkResponse) {

                    Log.e("mudit", new String(networkResponse.data));

                    try {
                        final JSONObject response = new JSONObject(new String(networkResponse.data));

                        if (response.has("error")) {
                            if (loadingDialog != null) {
                                loadingDialog.hide();
                            }
                            DialogManager.errorDialog(SellProductActivity.this, "Validation Error", response.optString("error"));
                        } else if (response.has("success")) {
                            loadingDialog.hide();
                            AppConstants.isChangeListing = true;
                            try {
                                DialogManager.errorDialog(SellProductActivity.this, "success", response.optString("success"), new DialogManager.OnactionResult() {
                                    @Override
                                    public void Action() {
                                        try {
                                            SellPorSerActivity.getInstance().finish();
                                            SubCategoriesActivity.getInstance().finish();
                                            finish();
                                        } catch (NullPointerException e) {
                                            Log.e(TAG, "NullPointerException : " + e.getMessage());
                                        }
                                    }
                                });
                            } catch (NullPointerException e) {
                                Log.e(TAG, "NullPointerException : " + e.getMessage());
                            }
                            /*QBUser user = new QBUser(response.optString("result") + "@gmail.com", "1234567890");
                            user.setEmail(response.optString("result") + "@gmail.com");
                            user.setFullName(binding.edtTitle.getText().toString());
                            user.setPhone("");
                            StringifyArrayList<String> tags = new StringifyArrayList<String>();
                            tags.add("Students");
                            user.setTags(tags);
                            user.setWebsite("");

                            QBUsers.signUp(user).performAsync(new QBEntityCallback<QBUser>() {
                                @Override
                                public void onSuccess(QBUser user, Bundle args) {
                                    //QBUser currentUser = getUserFromSession();
                                    DialogManager.errorDialog(SellProductActivity.this, "success", response.optString("success"), new DialogManager.OnactionResult() {
                                        @Override
                                        public void Action() {
                                            try {
                                                SellPorSerActivity.getInstance().finish();
                                                SubCategoriesActivity.getInstance().finish();
                                            } catch (NullPointerException e) {
                                                Log.e(TAG, "NullPointerException : " + e.getMessage());
                                            }
                                            finish();
                                        }
                                    });
                                    if (loadingDialog != null) {
                                        loadingDialog.hide();
                                    }


                                }

                                @Override
                                public void onError(QBResponseException error) {
                                    // error

                                    Log.e("QBResponseException", error.getMessage());
                                }
                            });*/
                        } else {
                            Toast.makeText(SellProductActivity.this, getResources().getString(R.string.something_was_wrong_with_api), Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    Log.e("onErrorResponse", volleyError.getMessage());
                }
            }, ProductPhotosAdapter.getFiles(), mStringPart, mHeaderPart, "image[]");

            mCustomRequest.setRetryPolicy(new DefaultRetryPolicy(
                    30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleySingleton.getInstance().addToRequestQueue(mCustomRequest);

        } else {
            DialogManager.errorDialog(SellProductActivity.this,
                    "Error!", "Please add product image");
        }

    }

    private int getIndex(Spinner spinner, String myString) {
        int index = 0;
        for (int i = 0; i < spinner.getCount(); i++) {
            if (spinner.getItemAtPosition(i).toString().equals(myString)) {
                index = i;
            }
        }
        return index;
    }


    private void UpdateProduct() {
        String param = "&user_id=" + Preferences.getValue_String(SellProductActivity.this, Preferences.USER_ID);
        param = param + "&date=" + new SimpleDateFormat("ddMMyyHHmmss").format(Calendar.getInstance().getTime());
        Price = binding.edtPrice.getText().toString();
        website = binding.edtWebsiteLink.getText().toString();
        try {
            param = param + "&pro_title=" + URLEncoder.encode(binding.edtTitle.getText().toString(), AppConstants.encodeType);
            param = param + "&pro_desc=" + URLEncoder.encode(binding.edtDesc.getText().toString(), AppConstants.encodeType);
            param = param + "&category_id=" + URLEncoder.encode(AppConstants.product.getCategory(), AppConstants.encodeType);
            param = param + "&con_id=" + URLEncoder.encode(condition_id, AppConstants.encodeType);
            if (AppConstants.TYPE.equals(AppConstants.PRODUCT)) {
                param = param + "&price=" + URLEncoder.encode(Price, AppConstants.encodeType);
            } else {
                param = param + "&website=" + URLEncoder.encode(website, AppConstants.encodeType);
                if (isPriceStatus) {
                    param = param + "&price=" + URLEncoder.encode(Price, AppConstants.encodeType);
                    param = param + "&price_status=1";
                    param = param + "&con_id=" + URLEncoder.encode(priceper_name, AppConstants.encodeType);
                } else {
                    param = param + "&price=0";
                    param = param + "&price_status=0";
                }
            }
            param = param + "&type=" + AppConstants.TYPE;
            param = param + "&pro_id=" + AppConstants.product.getProduct_id();

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        String url = WebServices.UPDATE_PRODUCT + param;

        Log.e("Url", url);


        if (ProductPhotosAdapter != null && ProductPhotosAdapter.getFiles().size() > 0) {

            loadingDialog = new LoadingDialog(SellProductActivity.this, "", false);

            CustomMultipartRequest mCustomRequest = new CustomMultipartRequest(Request.Method.POST, SellProductActivity.this, url, new Response.Listener<NetworkResponse>() {
                @Override
                public void onResponse(NetworkResponse networkResponse) {

                    Log.e("mudit", new String(networkResponse.data));

                    try {
                        final JSONObject response = new JSONObject(new String(networkResponse.data));

                        if (response.has("error")) {
                            if (loadingDialog != null) {
                                loadingDialog.hide();
                            }
                            DialogManager.errorDialog(SellProductActivity.this, "Validation Error", response.optString("error"));
                        } else if (response.has("success")) {
                            AppConstants.isChangeListing = true;
                            DialogManager.errorDialog(SellProductActivity.this, "success", response.optString("success"), new DialogManager.OnactionResult() {
                                @Override
                                public void Action() {
                                    ProductDetailsActivity.getInstance().finish();
                                    finish();
                                }
                            });
                            if (loadingDialog != null) {
                                loadingDialog.hide();
                            }
                        } else {
                            Toast.makeText(SellProductActivity.this, getResources().getString(R.string.something_was_wrong_with_api), Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    Log.e("onErrorResponse", volleyError.getMessage());
                }
            }, ProductPhotosAdapter.getFiles(), mStringPart, mHeaderPart, "image[]");

            mCustomRequest.setRetryPolicy(new DefaultRetryPolicy(
                    30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleySingleton.getInstance().addToRequestQueue(mCustomRequest);

        } else {

            new EditProductApi(param, SellProductActivity.this, new EditProductApi.OnResultReceived() {
                @Override
                public void onResult(String result) throws UnsupportedEncodingException {
                    AppConstants.isChangeListing = true;
                    if (AppConstants.TYPE.equals(AppConstants.PRODUCT)) {
                        if (AppConstants.product_refresh != null) {
                            AppConstants.product_refresh.load();
                        }
                    } else {
                        if (AppConstants.service_refresh != null) {
                            AppConstants.service_refresh.load();
                        }
                    }
                }
            }, true);


        }

    }


}
