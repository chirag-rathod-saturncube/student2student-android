package com.sc.student2student.moduls;

import java.util.ArrayList;

/**
 * Created by Saturncube10 on 26-May-17.
 */

public class Product {
    String product_id, product_name, product_desc, product_image,product_small_image,product_medium_image, user_id, category, category_name, conditions, price, is_watch, type, status, condition_name, crt_date, distance;

    String price_status;

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    String website;



    String price_per;
    User product_user;
    ArrayList<Images> images;

    public String getPrice_status() {
        return price_status;
    }

    public void setPrice_status(String price_status) {
        this.price_status = price_status;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getCrt_date() {
        return crt_date;
    }

    public void setCrt_date(String crt_date) {
        this.crt_date = crt_date;
    }

    public String getCondition_name() {
        return condition_name;
    }

    public void setCondition_name(String condition_name) {
        this.condition_name = condition_name;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public User getProduct_user() {
        return product_user;
    }

    public void setProduct_user(User product_user) {
        this.product_user = product_user;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public ArrayList<Images>    getImages() {
        return images;
    }

    public void setImages(ArrayList<Images> images) {
        this.images = images;
    }

    public String getIs_watch() {
        return is_watch;
    }

    public void setIs_watch(String is_watch) {
        this.is_watch = is_watch;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getProduct_desc() {
        return product_desc;
    }

    public void setProduct_desc(String product_desc) {
        this.product_desc = product_desc;
    }

    public String getProduct_image() {
        return product_image;
    }

    public void setProduct_image(String product_image) {
        this.product_image = product_image;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getConditions() {
        return conditions;
    }

    public void setConditions(String conditions) {
        this.conditions = conditions;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPrice_per() {
        return price_per;
    }

    public void setPrice_per(String price_per) {

        this.price_per = price_per;
    }
    public String getProduct_small_image() {
        return product_small_image;
    }

    public void setProduct_small_image(String product_small_image) {
        this.product_small_image = product_small_image;
    }

    public String getProduct_medium_image() {
        return product_medium_image;
    }

    public void setProduct_medium_image(String product_medium_image) {
        this.product_medium_image = product_medium_image;
    }
}
