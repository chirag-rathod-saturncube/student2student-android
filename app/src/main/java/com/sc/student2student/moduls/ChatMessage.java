package com.sc.student2student.moduls;

import java.util.ArrayList;

/**
 * Created by pc4 on 11/08/2017.
 */

public class ChatMessage {
    String group_id, prod_id, prod_title, prod_img, pro_desc, prod_user_id, prod_user_name;
    ArrayList<Messages> messagesArrayList;

    public String getProd_user_name() {
        return prod_user_name;
    }

    public void setProd_user_name(String prod_user_name) {
        this.prod_user_name = prod_user_name;
    }

    public String getProd_user_id() {
        return prod_user_id;
    }

    public void setProd_user_id(String prod_user_id) {
        this.prod_user_id = prod_user_id;
    }

    public ArrayList<Messages> getMessagesArrayList() {
        return messagesArrayList;
    }

    public void setMessagesArrayList(ArrayList<Messages> messagesArrayList) {
        this.messagesArrayList = messagesArrayList;
    }

    public String getGroup_id() {
        return group_id;
    }

    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }

    public String getPro_desc() {
        return pro_desc;
    }

    public void setPro_desc(String pro_desc) {
        this.pro_desc = pro_desc;
    }

    public String getProd_id() {
        return prod_id;
    }

    public void setProd_id(String prod_id) {
        this.prod_id = prod_id;
    }

    public String getProd_title() {
        return prod_title;
    }

    public void setProd_title(String prod_title) {
        this.prod_title = prod_title;
    }

    public String getProd_img() {
        return prod_img;
    }

    public void setProd_img(String prod_img) {
        this.prod_img = prod_img;
    }
}
