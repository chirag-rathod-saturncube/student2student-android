package com.sc.student2student.Activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.sc.student2student.R;
import com.sc.student2student.adapter.ProductGroupAdapter;
import com.sc.student2student.api.ProductGroupListApi;
import com.sc.student2student.common.AppConstants;
import com.sc.student2student.common.Preferences;
import com.sc.student2student.databinding.ActivityChatListBinding;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class InboxProductActivity extends AppCompatActivity {
    ActivityChatListBinding binding;
    ProductGroupAdapter adapter;
    LinearLayoutManager mLayoutManager;
    private String TAG = getClass().getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_chat_list);
        initView();
        AppConstants.action = AppConstants.INBOX;
        AppConstants.chatproduct = true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadRecord();
    }

    private void loadRecord() {
        if (AppConstants.isNetworkAvailable(getString(R.string.network_title),
                getString(R.string.network_msg), InboxProductActivity.this)) {
            String envelope = "&user_id=" + Preferences.getValue_String(InboxProductActivity.this, Preferences.USER_ID);
            envelope = envelope + "&date=" + new SimpleDateFormat("ddMMyyHHmmss").format(Calendar.getInstance().getTime());
            new ProductGroupListApi(envelope, InboxProductActivity.this, new ProductGroupListApi.OnResultReceived() {
                @Override
                public void onResult(String result) throws UnsupportedEncodingException {
                    if (AppConstants.productGroupArrayList.size() > 0) {
                        binding.relNoRecord.setVisibility(View.GONE);
                        adapter = new ProductGroupAdapter(InboxProductActivity.this, AppConstants.productGroupArrayList);
                        binding.recyclerView.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                    } else {
                        binding.relNoRecord.setVisibility(View.VISIBLE);
                    }
                }
            }, true);
        }
    }

    private void initView() {
        mLayoutManager = new LinearLayoutManager(InboxProductActivity.this);
        mLayoutManager = new LinearLayoutManager(InboxProductActivity.this, LinearLayoutManager.VERTICAL, false);
        mLayoutManager.setSmoothScrollbarEnabled(true);
        mLayoutManager.setRecycleChildrenOnDetach(true);
        binding.recyclerView.setLayoutManager(mLayoutManager);
        binding.toolbar.tvTitle.setText("Chat");
        binding.toolbar.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
