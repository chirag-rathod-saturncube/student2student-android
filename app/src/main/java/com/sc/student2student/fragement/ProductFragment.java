package com.sc.student2student.fragement;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.VolleyError;
import com.sc.student2student.R;
import com.sc.student2student.adapter.CatAdapter;
import com.sc.student2student.api.GetProCatApi;
import com.sc.student2student.common.AppConstants;
import com.sc.student2student.common.GridSpacingItemDecoration;
import com.sc.student2student.common.UINotifierProductCategory;
import com.sc.student2student.common.Utils;
import com.sc.student2student.databinding.FragmentProductCategoryBinding;

import java.io.UnsupportedEncodingException;

import static android.text.style.TtsSpan.ARG_TEXT;

/**
 * Created by pc4 on 20/07/2017.
 */

public class ProductFragment extends Fragment implements UINotifierProductCategory {
    FragmentProductCategoryBinding binding;

    String TAG = getClass().getSimpleName();
    View rootView;

    boolean flagLoading = true;
    int pastVisibleItems, visibleItemCount, totalItemCount;
    int pageCount = 0;
    int limit = 20;
    CatAdapter adapter;
    GridLayoutManager gridLayoutManager;

    public static Fragment newInstance(String text) {
        Fragment frag = new ProductFragment();
        Bundle args = new Bundle();
        args.putString(ARG_TEXT, text);
        frag.setArguments(args);
        return frag;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_product_category, container, false);
        Utils.notifierProductCategory = this;
        binding = FragmentProductCategoryBinding.bind(rootView);
        initView();
        binding.swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadFragData(false);
            }
        });
        return rootView;
    }

    private void initView() {

        gridLayoutManager = new GridLayoutManager(getActivity(), 2, LinearLayoutManager.VERTICAL, false);
        binding.recyclerView.setLayoutManager(gridLayoutManager);
        gridLayoutManager.setSmoothScrollbarEnabled(false);
        int spacing = 10;
        boolean includeEdge = true;
        binding.recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, spacing, includeEdge));
    }

    @Override
    public void loadFragData(boolean flag) {
        Log.e(TAG, "Product fragment....");
        String envelope = "&type=" + AppConstants.PRODUCT;
        envelope = envelope + "&parent_id=0";
        new GetProCatApi(envelope, getContext(), new GetProCatApi.OnResultReceived() {
            @Override
            public void onResult(String result) throws UnsupportedEncodingException {
                binding.swipeRefresh.setRefreshing(false);
                if (AppConstants.catProArrayList.size() > 0) {
                    adapter = new CatAdapter(getActivity(), AppConstants.catProArrayList, AppConstants.PRODUCT, "1");
                    binding.recyclerView.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                } else {
                    //binding.tvNoRecord.setVisibility(View.GONE);
                    //binding.relSearch.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailed(VolleyError result) throws UnsupportedEncodingException {
                binding.swipeRefresh.setRefreshing(false);
            }
        }, flag);


        /*if (AppConstants.isNetworkAvailable(getActivity())) {
            pageCount = 0;
            AppConstants.quotesPendingArrayList.clear();
            if (Preferences.getValue_String(getActivity(), Preferences.ROLE).equals(AppConstants.CLIENT)) {
                String envelope = "&user_id=" + Preferences.getValue_String(getActivity(), Preferences.USER_ID);
                envelope = envelope + "&start=" + pageCount;
                new ClientPendingQuotesApi(envelope, getActivity(), new ClientPendingQuotesApi.OnResultReceived() {
                    @Override
                    public void onResult(String result) throws UnsupportedEncodingException {
                        flagLoading = AppConstants.resetDataFlag(result, limit);
                        binding.swipeRefresh.setRefreshing(false);
                        binding.relNoRecord.setVisibility(View.GONE);
                        adapter = new PendingQuotesAdapter(getActivity(), AppConstants.quotesPendingArrayList);
                        binding.recyclerView.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                    }
                }, flag);
            } else {
                String envelope = "&user_id=" + Preferences.getValue_String(getActivity(), Preferences.USER_ID);
                envelope = envelope + "&start=" + pageCount;
                new ManagerNewQuotesApi(envelope, getActivity(), new ManagerNewQuotesApi.OnResultReceived() {
                    @Override
                    public void onResult(String result) throws UnsupportedEncodingException {
                        flagLoading = AppConstants.resetDataFlag(result, limit);
                        binding.swipeRefresh.setRefreshing(false);
                        binding.relNoRecord.setVisibility(View.GONE);
                        adapter = new PendingQuotesAdapter(getActivity(), AppConstants.quotesPendingArrayList);
                        binding.recyclerView.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                    }
                }, false);
            }
        }*/
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);


    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
