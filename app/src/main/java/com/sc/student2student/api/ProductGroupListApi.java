package com.sc.student2student.api;

import com.sc.student2student.R;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import com.sc.student2student.common.AppConstants;
import com.sc.student2student.common.DialogManager;
import com.sc.student2student.common.LoadingDialog;
import com.sc.student2student.common.WebServices;
import com.sc.student2student.moduls.ProductGroup;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by pc4 on 7/6/2016.
 */
public class ProductGroupListApi {

    String TAG = getClass().getSimpleName();
    Context mContext;
    String envelope = "";
    LoadingDialog loadingDialog;
    RequestQueue queue;
    boolean flagProgress;
    private String url = WebServices.PRODUCT_GROUP_LIST_API;
    private OnResultReceived onResultReceived;

    public ProductGroupListApi(String envelope, Context mContext, OnResultReceived onResultReceived, boolean flagProgress) {
        this.envelope = envelope;
        this.mContext = mContext;
        this.onResultReceived = onResultReceived;
        this.flagProgress = flagProgress;
        queue = Volley.newRequestQueue(mContext);
        getResponse();
    }

    public void show() {
        if (flagProgress) {
            loadingDialog = new LoadingDialog(mContext, "", false);
        }
    }

    public void hide() {
        if (flagProgress) {
            loadingDialog.hide();
        }
    }


    public void getResponse() {
        show();
        Log.e(TAG, "Url: " + url + envelope);
        JsonObjectRequest postRequest = new JsonObjectRequest(com.android.volley.Request.Method.GET, url + envelope, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.e(TAG, "Response: " + response);
                            if (response.has("success")) {
                                if (response.optString("success").equals("1")) {
                                    AppConstants.totalNotification = response.optString("totalNotification");
                                    AppConstants.totalMessages = response.optString("totalunreadMessages");

                                    // Log.e(TAG,"response.optString(\"totalNotification\") : "+response.optString("totalNotification"));
                                    // Log.e(TAG,"AppConstants.totalNotification : "+AppConstants.totalNotification);

                                    JSONArray result = response.getJSONArray("groups");
                                    AppConstants.productGroupArrayList.clear();
                                    for (int i = 0; i < result.length(); i++) {
                                        JSONObject object = result.getJSONObject(i);
                                        ProductGroup productGroup = new ProductGroup();
                                        productGroup.setGroup_id(object.optString("group_id"));
                                        Log.e(TAG, "group_id" + object.optString("group_id"));
                                        productGroup.setProd_id(object.optString("prod_id"));
                                        AppConstants.productGroup.setProd_id(object.optString("prod_id"));
                                        productGroup.setProd_user_name(object.optString("prod_user_name"));
                                        productGroup.setProd_title(object.optString("prod_title"));
                                        productGroup.setProd_img(object.optString("prod_img"));
                                        productGroup.setTotalMessages(object.optString("totalMessages"));

                                        if (object.has("messages")) {
                                            JSONArray messageArray = object.getJSONArray("messages");
                                            if (messageArray.length() > 0) {
                                                for (int j = 0; j < messageArray.length(); j++) {
                                                    JSONObject messageObj = messageArray.getJSONObject(j);
                                                    if (messageObj.optString("type").equals("1"))
                                                        productGroup.setLastMessage(messageObj.optString("msg"));
                                                    else
                                                        productGroup.setLastMessage("Image");
                                                }
                                            } else {
                                                productGroup.setLastMessage("");
                                            }
                                        }
                                        AppConstants.productGroupArrayList.add(productGroup);
                                    }
                                    Log.e(TAG, "Size: " + AppConstants.productGroupArrayList.size());

                                    hide();
                                    if (onResultReceived != null) try {
                                        onResultReceived.onResult(response.toString());
                                    } catch (UnsupportedEncodingException e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                    hide();
                                    DialogManager.errorDialog(mContext, "Error!", response.optString("msg"));
                                }
                            } else {
                                hide();
                                Toast.makeText(mContext, mContext.getString(R.string.something_was_wrong_with_api), Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception e) {
                            hide();
                            Log.e(TAG, "Line " + Thread.currentThread().getStackTrace()[2].getLineNumber() + " Error:... " + e.getMessage());
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hide();
                        VolleyLog.d("Error.Response", "");
                    }
                }
        ) {
            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Cache-Control", "no-cache");
                return params;
            }
        };

        postRequest.setRetryPolicy(new DefaultRetryPolicy(
                30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(postRequest);
    }

    public interface OnResultReceived {
        public void onResult(String result) throws UnsupportedEncodingException;
    }
}
