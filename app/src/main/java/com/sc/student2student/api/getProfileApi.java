package com.sc.student2student.api;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.sc.student2student.R;
import com.sc.student2student.common.AppConstants;
import com.sc.student2student.common.DialogManager;
import com.sc.student2student.common.LoadingDialog;
import com.sc.student2student.common.Preferences;
import com.sc.student2student.common.WebServices;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by pc4 on 7/6/2016.
 */
public class getProfileApi {

    String TAG = getClass().getSimpleName();
    Context mContext;
    String envelope = "";
    LoadingDialog loadingDialog;
    RequestQueue queue;
    boolean flagProgress;
    private String url = WebServices.GET_PROFILE;
    private OnResultReceived onResultReceived;

    public getProfileApi(String envelope, Context mContext, OnResultReceived onResultReceived, boolean flagProgress) {
        this.envelope = envelope;
        this.mContext = mContext;
        this.onResultReceived = onResultReceived;
        this.flagProgress = flagProgress;
        queue = Volley.newRequestQueue(mContext);
        getResponse();
    }

    public void show() {
        if (flagProgress) {
            loadingDialog = new LoadingDialog(mContext, "", false);
        }
    }

    public void hide() {
        if (flagProgress) {
            loadingDialog.hide();
        }
    }

    public void getResponse() {
        show();
        Log.e(TAG, "Url: " + url + envelope);
        JsonObjectRequest postRequest = new JsonObjectRequest(com.android.volley.Request.Method.GET, url + envelope, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.e(TAG, "Response: " + response);
                            if (response.has("error")) {
                                hide();
                                DialogManager.errorDialog(mContext, "Error!", response.optString("error"));
                            } else if (response.has("success")) {
                                JSONObject object = response.getJSONObject("result");
                                AppConstants.user.setUser_id(object.optString("user_id"));
                                AppConstants.user.setName(object.optString("name"));
                                AppConstants.user.setEmail(object.optString("email"));
                                AppConstants.user.setProfile_pic(object.optString("profile_pic"));
                                AppConstants.chat_username = object.optString("name");
                                AppConstants.chat_userprofile = object.optString("profile_pic");
                                AppConstants.user.setLocation(object.optString("location"));
                                AppConstants.user.setLatitude(object.optString("latitude"));
                                AppConstants.user.setLongitude(object.optString("longitude"));
                                AppConstants.user.setAddress(object.optString("address"));
                                AppConstants.user.setCity(object.optString("city"));

                                Preferences.setValue(mContext, Preferences.CITY_PROFILE, object.optString("city"));

                                AppConstants.user.setRating_counter(object.optString("rating_counter"));
                                AppConstants.user.setState(object.optString("state"));
                                AppConstants.user.setCountry(object.optString("country"));
                                AppConstants.user.setAbout(object.optString("about"));
                                AppConstants.user.setChat_pass(object.optString("chat_pass"));
                                AppConstants.user.setRating(object.optString("rating"));
                                AppConstants.user.setCrt_date(object.optString("crt_date"));

                                if (AppConstants.USER_ID.equals(Preferences.getValue_String(mContext, Preferences.USER_ID))) {
                                    Preferences.setValue(mContext, Preferences.USER_NAME, object.optString("name"));
                                    Preferences.setValue(mContext, Preferences.USER_PIC, object.optString("profile_pic"));
                                }

                                hide();
                                if (onResultReceived != null) try {
                                    onResultReceived.onResult(response.toString());
                                } catch (UnsupportedEncodingException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                hide();
                                Toast.makeText(mContext, mContext.getString(R.string.something_was_wrong_with_api), Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception e) {
                            hide();
                            Log.e(TAG, "Line " + Thread.currentThread().getStackTrace()[2].getLineNumber() + " Error:... " + e.getMessage());
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hide();
                        VolleyLog.d("Error.Response", "");
                        AppConstants.VolleyException(mContext, error);
                    }
                }
        ) {
            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Cache-Control", "no-cache");
                return params;
            }
        };
        AppConstants.SetTimeOut(postRequest);
        queue.add(postRequest);
    }


    public interface OnResultReceived {
        public void onResult(String result) throws UnsupportedEncodingException;
    }
}
