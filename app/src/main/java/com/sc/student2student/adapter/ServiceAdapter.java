package com.sc.student2student.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.sc.student2student.Activity.ProductDetailsActivity;
import com.sc.student2student.R;
import com.sc.student2student.api.AddWatchListApi;
import com.sc.student2student.api.GetProductDetails;
import com.sc.student2student.api.RemoveWatchListApi;
import com.sc.student2student.common.AppConstants;
import com.sc.student2student.common.Preferences;
import com.sc.student2student.fragement.Service_watchlist_fragement;
import com.sc.student2student.moduls.Product;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


/**
 * Created by pc4 on 5/30/2016.
 */
public class ServiceAdapter extends RecyclerView.Adapter<ServiceAdapter.CustomViewHolder> {
    List<Product> infos = null;
    //Product_watchlist_fragement fragment;
    Service_watchlist_fragement fragment;
    int layout;
    private ArrayList<Product> arrayList;
    private Context mContext;
    private String TAG = getClass().getSimpleName();

    public ServiceAdapter(Context context, List<Product> list, int layout, Service_watchlist_fragement fragment) {
        this.infos = list;
        this.fragment = fragment;
        this.arrayList = new ArrayList<>();
        this.arrayList.addAll(infos);
        this.mContext = context;
        this.layout = layout;
    }


    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(layout, null);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final CustomViewHolder customViewHolder, final int pos) {
        Log.e(TAG, "ServiceAdapter");
        final Product list = infos.get(pos);
        if (list.getPrice().equals("")) {
            customViewHolder.tv_price.setVisibility(View.GONE);
        } else {
            customViewHolder.tv_price.setVisibility(View.VISIBLE);
        }
        if (list.getDistance().equals("")) {
            customViewHolder.tv_distance.setVisibility(View.GONE);
        } else {
            customViewHolder.tv_distance.setVisibility(View.VISIBLE);
        }

      /*  if (list.getCrt_date() == null) {
            customViewHolder.rel_date_of_placement.setVisibility(View.INVISIBLE);
        } else {
            if (list.getCrt_date().equals("")) {
                customViewHolder.rel_date_of_placement.setVisibility(View.INVISIBLE);
            } else {
                if (Preferences.getValue_Boolean(mContext, Preferences.LIST_TYPE, true)) {
                    customViewHolder.rel_date_of_placement.setVisibility(View.VISIBLE);
                } else {
                    customViewHolder.rel_date_of_placement.setVisibility(View.GONE);
                }
            }
        }*/

        /*customViewHolder.tv_productname.setText(list.getProduct_name());
//      customViewHolder.tv_product_desc.setText(list.getProduct_desc());
        customViewHolder.tv_price.setText(list.getPrice());
        customViewHolder.tv_date.setText(list.getCrt_date());
        customViewHolder.tv_distance.setText(list.getDistance());
        customViewHolder.progress_product.setVisibility(View.VISIBLE);
        if (!list.getProduct_image().equals("")) {
            Picasso.with(mContext)
                    .load(list.getProduct_image())
                    .transform(AppConstants.getTransformation(customViewHolder.product_image))
                    .into(customViewHolder.product_image, new Callback() {
                        @Override
                        public void onSuccess() {
                            customViewHolder.progress_product.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            customViewHolder.progress_product.setVisibility(View.GONE);
                        }
                    });
        }

        customViewHolder.card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String param = "&user_id=" + Preferences.getValue_String(mContext, Preferences.USER_ID);
                param = param + "&pro_id=" + list.getProduct_id();
                new GetProductDetails(param, mContext, new GetProductDetails.OnResultReceived() {
                    @Override
                    public void onResult(String result) throws UnsupportedEncodingException {
                        AppConstants.pos = pos;
                        AppConstants.TYPE = list.getType();
                        mContext.startActivity(new Intent(mContext, ProductDetailsActivity.class));
                    }

                    @Override
                    public void onFailed(VolleyError result) throws UnsupportedEncodingException {

                    }
                }, true);


            }
        });

        if (list.getIs_watch().equals("0")) {
            customViewHolder.img_favorite.setImageResource(R.drawable.heart_outline);
        } else {
            customViewHolder.img_favorite.setImageResource(R.drawable.heart);
        }

        customViewHolder.img_favorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String param = "&user_id=" + Preferences.getValue_String(mContext, Preferences.USER_ID);
                param = param + "&pro_id=" + list.getProduct_id();
                param = param + "&type=" + list.getType();
                if (list.getIs_watch().equals("0")) {


                    new AddWatchListApi(param, mContext, new AddWatchListApi.OnResultReceived() {
                        @Override
                        public void onResult(String result) throws UnsupportedEncodingException {
                            infos.get(pos).setIs_watch("1");
                            customViewHolder.img_favorite.setImageResource(R.drawable.heart);
                           // fragment.LoadData(true);

                        }
                    }, true);
                } else {

                    new RemoveWatchListApi(param, mContext, new RemoveWatchListApi.OnResultReceived() {
                        @Override
                        public void onResult(String result) throws UnsupportedEncodingException {
                            infos.get(pos).setIs_watch("0");
                            customViewHolder.img_favorite.setImageResource(R.drawable.heart_outline);
                            fragment.LoadData(true);

                        }
                    }, true);


                }
            }
        });*/


        customViewHolder.tv_productname.setText(list.getProduct_name());
//      customViewHolder.tv_product_desc.setText(list.getProduct_desc());
        customViewHolder.tv_price.setText(list.getPrice());
        customViewHolder.tv_date.setText(list.getCrt_date());
        customViewHolder.tv_distance.setText(list.getDistance());
        customViewHolder.progress_product.setVisibility(View.VISIBLE);
        if (!list.getProduct_image().equals("")) {
            Picasso.with(mContext)
                    .load(list.getProduct_image())
                    .transform(AppConstants.getTransformation(customViewHolder.product_image))
                    .into(customViewHolder.product_image, new Callback() {
                        @Override
                        public void onSuccess() {
                            customViewHolder.progress_product.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            customViewHolder.progress_product.setVisibility(View.GONE);
                        }
                    });
        }

        customViewHolder.card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String param = "&user_id=" + Preferences.getValue_String(mContext, Preferences.USER_ID);
                param = param + "&pro_id=" + list.getProduct_id();
                param = param + "&date=" + new SimpleDateFormat("ddMMyyHHmmss").format(Calendar.getInstance().getTime());
                new GetProductDetails(param, mContext, new GetProductDetails.OnResultReceived() {
                    @Override
                    public void onResult(String result) throws UnsupportedEncodingException {
                        AppConstants.pos = pos;
                        AppConstants.TYPE = list.getType();
                        mContext.startActivity(new Intent(mContext, ProductDetailsActivity.class));
                    }

                    @Override
                    public void onFailed(VolleyError result) throws UnsupportedEncodingException {

                    }
                }, true);


            }
        });

        if (list.getIs_watch().equals("0")) {
            customViewHolder.img_favorite.setImageResource(R.drawable.heart_outline);
        } else {
            customViewHolder.img_favorite.setImageResource(R.drawable.heart);
        }

        customViewHolder.img_favorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String param = "&user_id=" + Preferences.getValue_String(mContext, Preferences.USER_ID);
                param = param + "&pro_id=" + list.getProduct_id();
                param = param + "&type=" + list.getType();
                param = param + "&date=" + new SimpleDateFormat("ddMMyyHHmmss").format(Calendar.getInstance().getTime());
                if (list.getIs_watch().equals("0")) {
                    new AddWatchListApi(param, mContext, new AddWatchListApi.OnResultReceived() {
                        @Override
                        public void onResult(String result) throws UnsupportedEncodingException {
                            infos.get(pos).setIs_watch("1");
                            customViewHolder.img_favorite.setImageResource(R.drawable.heart);
                            //fragment.LoadData(true);

                        }
                    }, true);
                } else {

                    new RemoveWatchListApi(param, mContext, new RemoveWatchListApi.OnResultReceived() {
                        @Override
                        public void onResult(String result) throws UnsupportedEncodingException {
                            infos.get(pos).setIs_watch("0");
                            customViewHolder.img_favorite.setImageResource(R.drawable.heart_outline);
                            fragment.LoadData(true);

                        }
                    }, true);


                }
            }
        });


    }


    @Override
    public int getItemCount() {
        return (null != infos ? infos.size() : 0);
    }


    public class CustomViewHolder extends RecyclerView.ViewHolder {

        TextView tv_productname, tv_product_desc, tv_price, tv_distance, tv_date;
        ImageView img_favorite, product_image;
        ProgressBar progress_product;
        CardView card_view;
        RelativeLayout rel_date_of_placement;

        public CustomViewHolder(View view) {
            super(view);
            this.tv_productname = (TextView) view.findViewById(R.id.tv_productname);
            this.tv_product_desc = (TextView) view.findViewById(R.id.tv_product_desc);
            this.tv_price = (TextView) view.findViewById(R.id.tv_price);
            this.tv_distance = (TextView) view.findViewById(R.id.tv_distance);
            this.tv_date = (TextView) view.findViewById(R.id.tv_date);
            this.product_image = (ImageView) view.findViewById(R.id.product_image);
            this.img_favorite = (ImageView) view.findViewById(R.id.img_favorite);
            this.card_view = (CardView) view.findViewById(R.id.card_view);
            this.progress_product = (ProgressBar) view.findViewById(R.id.progress_product);
            this.rel_date_of_placement = (RelativeLayout) view.findViewById(R.id.rel_date_of_placement);
        }
    }
}

