package com.sc.student2student.common;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.sc.student2student.moduls.GroupMsg;

import java.util.ArrayList;

/**
 * Created by pc4 on 11/19/2016.
 */

public class Preferences {
    public static final String APP_PREFERENCES = AppConstants.APP_NAME;
    public static final String USER_EMAIL = "USER_EMAIL";
    public static final String CONNECT_TO = "CONNECT_TO";
    public static final String USER_NAME = "USER_NAME";
    public static final String USER_PIC = "USER_PIC";
    public static final String USER_ID = "USER_ID";
    public static final String LIST_TYPE = "LIST_TYPE";
    public static final String LOCATION = "LOCATION";
    public static final String MIN_PRICE = "MIN_PRICE";
    public static final String MAX_PRICE = "MAX_PRICE";
    public static final String LATITUDE = "LATITUDE";
    public static final String LONGITUDE = "LONGITUDE";
    public static final String SORT_BY = "SORT_BY";
    public static final String MILES = "MILES";

    public static final String ADDRESS = "ADDRESS";
    public static final String CITY = "CITY";
    public static final String STATE = "STATE";
    public static final String COUNTRY = "COUNTRY";

    public static final String firstOpen = "firstOpen";
    public static final String DEEP_LINK = "DEEP_LINK";
    public static final String UnreadCount = "UnreadCount";

    public static final String selectCityPosition = "SelectCityPosition";
    public static final String CITY_PROFILE = "CITY_PROFILE";
    public static final String LATITUDE_PROFILE = "LATITUDE_PROFILE";
    public static final String LONGITUDE_PROFILE = "LONGITUDE_PROFILE";


    public static final String isLocationUpdated = "isLocationUpdated";

    public static final String GROUP_MSG_ARRAY_LIST = "GROUP_MSG_ARRAY_LIST";
    public static final String NOT_REQUEST_ID = "NOT_REQUEST_ID";
    public static final String isSaveGroupMsg = "isSaveGroupMsg";
    public static final String isIntiGroupArray = "isIntiGroupArray";
    public static final String VERSION_NAME = "VERSION_NAME";


    public static void setValue(Context context, String Key, String Value) {
        SharedPreferences settings = context.getSharedPreferences(
                APP_PREFERENCES, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(Key, Value);
        editor.commit();
    }

    public static void setValue(Context context, String Key, boolean Value) {
        SharedPreferences settings = context.getSharedPreferences(
                APP_PREFERENCES, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(Key, Value);
        editor.commit();
    }

    public static void setValue_Boolean(Context context, String Key, boolean Value) {
        SharedPreferences settings = context.getSharedPreferences(
                APP_PREFERENCES, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(Key, Value);
        editor.commit();
    }

    public static boolean getValue_Boolean(Context context, String Key, boolean Default) {
        if (context != null) {
            SharedPreferences settings = context.getSharedPreferences(APP_PREFERENCES, 0);
            return settings.getBoolean(Key, Default);
        } else {
            return false;
        }
    }

    public static String getValue_String(Context context, String Key) {
        SharedPreferences settings = context.getSharedPreferences(
                APP_PREFERENCES, 0);
        return settings.getString(Key, "");
    }

    public static void saveGroupMsgArrayList(Context context, String key, ArrayList<GroupMsg> arrayList) {
        SharedPreferences settings = context.getSharedPreferences(APP_PREFERENCES, 0);
        SharedPreferences.Editor editor = settings.edit();
        Gson gson = new Gson();
        String json = gson.toJson(arrayList);
        editor.putString(key, json);
        editor.commit();
    }

    //Get Map
    public static String getMap(Context context, String key) {
        SharedPreferences settings = context.getSharedPreferences(APP_PREFERENCES, 0);
        return settings.getString(key, "");
    }


}
