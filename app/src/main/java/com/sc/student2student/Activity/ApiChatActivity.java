package com.sc.student2student.Activity;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.edmodo.cropper.CropImageView;
import com.sc.student2student.R;
import com.sc.student2student.adapter.ProductChatAdapter;
import com.sc.student2student.api.FileUploaderRequestApi;
import com.sc.student2student.api.GetProductDetails;
import com.sc.student2student.api.ProductChatHistoryApi;
import com.sc.student2student.api.ProductChatReloadApi;
import com.sc.student2student.api.SendMessageApi;
import com.sc.student2student.api.getProfileApi;
import com.sc.student2student.common.AppConstants;
import com.sc.student2student.common.LoadingDialog;
import com.sc.student2student.common.Preferences;
import com.sc.student2student.common.WebServices;
import com.sc.student2student.databinding.ActivityApiChatBinding;
import com.sc.student2student.moduls.ChatMessage;
import com.sc.student2student.moduls.Messages;
import com.sc.student2student.moduls.Product;
import com.sc.student2student.volley.VolleySingleton;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.sc.student2student.common.AppConstants.messagesArrayList;


public class ApiChatActivity extends AppCompatActivity {
    /*Image Upload*/
    public static final int REQUEST_CODE_TAKE_PICTURE = 2;
    public static final int REQUEST_CODE_GALLERY = 1;
    /*Permission 6.x*/
    final private int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124;
    ActivityApiChatBinding binding;
    ProductChatAdapter adapter;
    LinearLayoutManager mLayoutManager;
    boolean isScrolling = true;
    int pastVisibleItems, visibleItemCount, totalItemCount;
    boolean flagReopen = false;
    Handler handler;
    Product list;
    String selectedImagePath = "";
    private String TAG = getClass().getSimpleName();
    private int mScrollY;
    private Bitmap bitmap = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_api_chat);
        initView();

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                //Cry about not being clicked on
                loadChatHistory();
                AppConstants.send_image_path = "";

                Log.e(TAG, "Group Id : " + AppConstants.group_id);

                flagReopen = false;
            } else if (extras.getBoolean("NotiChatClick")) {
                //Do your stuff here mate :)
                AppConstants.group_id = extras.getString("group_id");
                loadChatHistory();
                AppConstants.send_image_path = "";
                flagReopen = true;
            }
        }

        //image upload code
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            AppConstants.mFileTemp = new File(Environment.getExternalStorageDirectory(), AppConstants.TEMP_PHOTO_FILE_NAME);
        } else {
            AppConstants.mFileTemp = new File(getFilesDir(), AppConstants.TEMP_PHOTO_FILE_NAME);
        }

        //Preferences.setValue(ApiChatActivity.this, AppConstants.group_id, AppConstants.unread_count);

        AppConstants.isNeedRefreshCounter = true;

        binding.relMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String param = "&user_id=" + Preferences.getValue_String(ApiChatActivity.this, Preferences.USER_ID);
                if (AppConstants.chatproduct) {
                    Log.e("####################", "IF");
                    Log.e("####################", String.valueOf(AppConstants.chatproduct));
                    // param = param + "&pro_id=" + AppConstants.chatMessage.getProd_id();
                    param = param + "&pro_id=" + AppConstants.productID;
                    Log.e("#############", "###########" + AppConstants.productID);
                } else if (AppConstants.action.equals(AppConstants.PRODUCT_DETAILS)) {
                    Log.e("####################", "ELSE");
                    Log.e("####################", String.valueOf(AppConstants.chatproduct));
                    param = param + "&pro_id=" + AppConstants.PRODUCT_ID;
                } else {
                }
                param = param + "&date=" + new SimpleDateFormat("ddMMyyHHmmss").format(Calendar.getInstance().getTime());
                new GetProductDetails(param, ApiChatActivity.this, new GetProductDetails.OnResultReceived() {
                    @Override
                    public void onResult(String result) {
                        Log.e(TAG, "onResult");
                        Intent intent = new Intent(ApiChatActivity.this, ProductDetailsActivity.class);
                        startActivity(intent);
                    }

                    @Override
                    public void onFailed(VolleyError result) throws UnsupportedEncodingException {
                        Log.e(TAG, "onResult");
                    }
                }, true);
            }
        });

        binding.btnChatSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String msg = binding.edtChatMessage.getText().toString();
                    if (!msg.equals("")) {
                        sendMessage(msg, "1");
                    }
                } catch (Exception e) {
                    Log.e(TAG, e.getMessage());
                }
            }
        });

        binding.recyclerView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v,
                                       int left, int top, int right, int bottom,
                                       int oldLeft, int oldTop, int oldRight, int oldBottom) {
                if (bottom < oldBottom) {
                    scrollToLastPosition();
                }
            }
        });


        binding.recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                mScrollY += dy;
                visibleItemCount = mLayoutManager.getChildCount();
                totalItemCount = mLayoutManager.getItemCount();
                pastVisibleItems = mLayoutManager.findFirstVisibleItemPosition();
                if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                    isScrolling = true;
                } else {
                    isScrolling = false;
                }

                /*if (flagLoading) {
                    if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                        super.onScrolled(recyclerView, dx, dy);
                        pageCount = pageCount + limit;
                        flagLoading = false;
                        loadMoreData();
                    } else {
                        Log.e(TAG, "No Record found");
                    }
                }*/

            }
        });


        binding.toolbar.imgOption2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String param = "&user_id=" + AppConstants.chatMessage.getProd_user_id();
                param = param + "&date=" + new SimpleDateFormat("ddMMyyHHmmss").format(Calendar.getInstance().getTime());
                new getProfileApi(param, ApiChatActivity.this, new getProfileApi.OnResultReceived() {
                    @Override
                    public void onResult(String result) throws UnsupportedEncodingException {
                        startActivity(new Intent(ApiChatActivity.this, ReportActivity.class));
                    }
                }, true);
            }
        });


        binding.buttonChatAttachment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= 23) {
                    if (checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                            && checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                            && checkSelfPermission(android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        getAllPermission();
                    } else {
                        startDialog(ApiChatActivity.this);
                    }
                } else {
                    startDialog(ApiChatActivity.this);
                }
            }
        });

        handler = new Handler();
        //start handler
        doRefresh();
    }

    private void sendMessage(String msg, String type) throws UnsupportedEncodingException {

        isScrolling = true;
        Messages messages = new Messages();
        messages.setUser_id(Preferences.getValue_String(ApiChatActivity.this, Preferences.USER_ID));
        messages.setName(Preferences.getValue_String(ApiChatActivity.this, Preferences.USER_NAME));
        messages.setMsg(msg);
        messages.setType(type);
        messages.setTimestamp("Just Now");
        messages.setLayoutType(1);
        AppConstants.chatMessage.getMessagesArrayList().add(messages);
        adapter.notifyDataSetChanged();
        binding.relNoRecord.setVisibility(View.GONE);
        adapter.notifyDataSetChanged();
        if (isScrolling) {
            binding.recyclerView.smoothScrollToPosition(
                    binding.recyclerView.getAdapter().getItemCount() - 1);
        }
        binding.edtChatMessage.setText("");

        String envelope = "&group_id=" + AppConstants.group_id;
        envelope = envelope + "&user_id="
                + Preferences.getValue_String(ApiChatActivity.this
                , Preferences.USER_ID);
        envelope = envelope
                + "&msg=" + URLEncoder.encode(msg, AppConstants.encodeType);
        envelope = envelope
                + "&type=" + type;
        envelope = envelope + "&date=" + new SimpleDateFormat("ddMMyyHHmmss").format(Calendar.getInstance().getTime());
        if (type.equals("1")) {
            new SendMessageApi(envelope, ApiChatActivity.this
                    , new SendMessageApi.OnResultReceived() {
                @Override
                public void onResult(String result) throws UnsupportedEncodingException {

                }
            }, false);
        } else {
            if (!AppConstants.send_image_path.equals("")) {
                AppConstants.send_position = AppConstants.chatMessage.getMessagesArrayList().size() - 1;
                VolleySingleton.getInstance().init(getApplicationContext());
                Map<String, String> mHeaderPart = new HashMap<>();
                Map<String, String> mStringPart = new HashMap<>();

                /*mHeaderPart.put("Content-Type", "application/json");
                mHeaderPart.put("Cache-Control", "no-cache");
                mHeaderPart.put("Content-type", "multipart/form-data;");
                mHeaderPart.put("deviceID", Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID));*/

                //final LoadingDialog loadingDialog = new LoadingDialog(ApiChatActivity.this, "", false);
                String url = WebServices.SEND_MESSAGE_API + envelope;
                Log.e(TAG, "Url: " + url);
                FileUploaderRequestApi fileUploaderRequestApi =
                        new FileUploaderRequestApi(Request.Method.POST,
                                ApiChatActivity.this, url, new Response.Listener<NetworkResponse>() {
                            @Override
                            public void onResponse(NetworkResponse response) {
                                Log.e(TAG, "Response: " + new String(response.data));
                                try {
                                    JSONObject object = new JSONObject(new String(response.data));
                                    if (object.has("msg")) {
                                        AppConstants.chatMessage.getMessagesArrayList()
                                                .get(AppConstants.send_position)
                                                .setMsg(object.optString("msg"));
                                        adapter.notifyDataSetChanged();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                              /*
                                loadingDialog.hide();
                                finish();*/
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.e(TAG, "Error: " + error.toString());
                              /*
                                loadingDialog.hide();
                                finish();*/
                            }
                        }, new File(AppConstants.send_image_path), mStringPart, mHeaderPart);
                fileUploaderRequestApi.setRetryPolicy(new DefaultRetryPolicy(
                        30000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                VolleySingleton.getInstance().addToRequestQueue(fileUploaderRequestApi);
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        AppConstants.isChatScreenOpen = false;
        handler.removeCallbacksAndMessages(null);
    }


    @Override
    protected void onStart() {
        super.onStart();
        AppConstants.isChatScreenOpen = true;
    }


    private void doRefresh() {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (AppConstants.isChatScreenOpen) {
                    reloadData();
                    handler.postDelayed(this, 3000);
                }
            }
        }, 3000);
    }


    private void scrollToLastPosition() {
        if (messagesArrayList.size() > 0) {
            binding.recyclerView.postDelayed(new Runnable() {
                @Override
                public void run() {
                    try {
                        binding.recyclerView
                                .smoothScrollToPosition(binding.recyclerView.getAdapter()
                                        .getItemCount() - 1);
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                }
            }, 50);
        }
    }

    private void reloadData() {
        if (AppConstants.isNetworkAvailable(getString(R.string.network_title)
                , getString(R.string.network_msg)
                , ApiChatActivity.this)) {
            String envelope = "&group_id=" + AppConstants.group_id;
            envelope = envelope + "&user_id=" + Preferences.getValue_String(ApiChatActivity.this, Preferences.USER_ID);
            envelope = envelope + "&date=" + new SimpleDateFormat("ddMMyyHHmmss").format(Calendar.getInstance().getTime());
            new ProductChatReloadApi(envelope, ApiChatActivity.this, new ProductChatReloadApi.OnResultReceived() {
                @Override
                public void onResult(String result) {

                    if (AppConstants.chatMessageReload.getMessagesArrayList().size()
                            > AppConstants.chatMessage.getMessagesArrayList().size()) {
                        int newRecord = AppConstants.chatMessageReload.getMessagesArrayList().size()
                                - AppConstants.chatMessage.getMessagesArrayList().size();
                        isScrolling = true;
                        AppConstants.chatMessage.getMessagesArrayList().clear();
                        AppConstants.chatMessage.getMessagesArrayList().addAll(AppConstants.chatMessageReload.getMessagesArrayList());
                        if (adapter != null) {
                            adapter.notifyDataSetChanged();
                            if (isScrolling) {
                                binding.recyclerView.smoothScrollToPosition(
                                        binding.recyclerView.getAdapter().getItemCount() - 1);
                            }
                        }
                    }

                }
            }, false);
        }
    }

    private void loadChatHistory() {
        //AppConstants.messagesArrayList.clear();
        if (AppConstants.isNetworkAvailable(getString(R.string.network_title)
                , getString(R.string.network_msg)
                , ApiChatActivity.this)) {
            String envelope = "&group_id=" + AppConstants.group_id;
            envelope = envelope + "&user_id=" + Preferences.getValue_String(ApiChatActivity.this, Preferences.USER_ID);
            envelope = envelope + "&date=" + new SimpleDateFormat("ddMMyyHHmmss").format(Calendar.getInstance().getTime());
            new ProductChatHistoryApi(envelope, ApiChatActivity.this, new ProductChatHistoryApi.OnResultReceived() {
                @Override
                public void onResult(String result) {
                    setScreenData();
                    if (Preferences.getValue_String(ApiChatActivity.this
                            , Preferences.USER_ID)
                            .equals(AppConstants.chatMessage.getProd_user_id())) {
                        binding.toolbar.imgOption2.setVisibility(View.GONE);
                    } else {
                        binding.toolbar.imgOption2.setVisibility(View.VISIBLE);
                    }

                    adapter = new ProductChatAdapter(ApiChatActivity.this, AppConstants.chatMessage.getMessagesArrayList());
                    binding.recyclerView.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                    if (AppConstants.chatMessage.getMessagesArrayList().size() > 0) {
                        binding.relNoRecord.setVisibility(View.GONE);
                        //flagLoading = AppConstants.resetDataFlag(result, limit);
                        binding.recyclerView.scrollToPosition(binding.recyclerView.getAdapter().getItemCount() - 1);
                    } else {
                        binding.relNoRecord.setVisibility(View.VISIBLE);
                    }

                }
            }, true);
        }
    }

    private void setScreenData() {
        ChatMessage product = AppConstants.chatMessage;
        binding.tvProductName.setText(product.getProd_title());
        binding.tvProductDesc.setText(product.getPro_desc());
        if (!product.getProd_img().equals("")) {
            binding.proImageLoad.setVisibility(View.VISIBLE);
            Picasso.with(ApiChatActivity.this)
                    .load(product.getProd_img())
                    .into(binding.imgProduct, new Callback() {
                        @Override
                        public void onSuccess() {
                            binding.proImageLoad.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            binding.proImageLoad.setVisibility(View.GONE);
                        }
                    });
        } else {
            binding.proImageLoad.setVisibility(View.GONE);
        }
        binding.toolbar.tvTitle.setText(product.getProd_user_name());
    }

    private void initView() {
        binding.proImageLoad.setVisibility(View.GONE);
        binding.toolbar.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mLayoutManager = new LinearLayoutManager(ApiChatActivity.this, LinearLayoutManager.VERTICAL, false);
        mLayoutManager.setSmoothScrollbarEnabled(true);
        binding.recyclerView.setLayoutManager(mLayoutManager);


        binding.toolbar.imgOption2.setVisibility(View.GONE);
        binding.toolbar.imgOption2.setImageResource(R.drawable.chat_info);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (flagReopen) {
            Intent intent1 = new Intent(ApiChatActivity.this, HomeActivity.class);
            intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent1);
            finish();
        } else {
            finish();
        }
    }

    //Image upload processing
    public void startDialog(Context context) {


        android.support.v7.app.AlertDialog.Builder myAlertDialog = new android.support.v7.app.AlertDialog.Builder(ApiChatActivity.this);
        myAlertDialog.setTitle("Select Photo");
        myAlertDialog.setMessage("How would you like to upload Photo?");

        myAlertDialog.setPositiveButton("Gallery",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        openGallery();
                    }
                });

        myAlertDialog.setNegativeButton("Camera",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        takePicture();
                    }
                });

        /*myAlertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                sp_document.setSelection(0);
            }
        });*/

        myAlertDialog.show();

    }

    private void startCropImage(boolean isRotate) {

        final Dialog dialog = new Dialog(ApiChatActivity.this,
                R.style.DialogFullScreen);

        dialog.setContentView(R.layout.dialog_crop);
        dialog.setCancelable(false);
        final CropImageView cropImageView = (CropImageView) dialog.findViewById(R.id.CropImageView);
        final ImageButton rotateButtonLeft = (ImageButton) dialog.findViewById(R.id.Button_rotate_left);
        final ImageButton rotateButtonRight = (ImageButton) dialog.findViewById(R.id.Button_rotate_right);
        final Button CROP = (Button) dialog.findViewById(R.id.Crop);
        final Button CANCEL = (Button) dialog.findViewById(R.id.Cancel);

        CROP.setText("CROP");
        CANCEL.setText("CANCEL");
        Log.e("IMAGE_PATH", AppConstants.mFileTemp.getPath());
        Log.e("IMAGE", BitmapFactory.decodeFile(AppConstants.mFileTemp.getPath()) + "");
        bitmap = AppConstants.getRescaledBitmap(AppConstants.mFileTemp.getPath());
        try {
            cropImageView.setImageBitmap(bitmap);
            if (android.os.Build.VERSION.RELEASE.startsWith("4.1")
                    || android.os.Build.VERSION.RELEASE.startsWith("4.4")
                    || android.os.Build.VERSION.RELEASE.startsWith("5.0")) {
                if (isRotate) {
                    cropImageView.post(new Runnable() {
                        @Override
                        public void run() {
                            cropImageView.rotateImage(90);
                        }
                    });
                }
            }
        } catch (Exception e) {
            Toast.makeText(ApiChatActivity.this, "Image Not Available",
                    Toast.LENGTH_SHORT).show();
            CROP.setVisibility(View.GONE);
        }
        rotateButtonLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cropImageView.rotateImage(-90);
            }
        });


        rotateButtonRight.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                cropImageView.rotateImage(90);
            }
        });

        CROP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bitmap = cropImageView.getCroppedImage();
                final LoadingDialog loadingDialog;
                loadingDialog = new LoadingDialog(ApiChatActivity.this, "", false);
                new AsyncTask<Void, Void, Void>() {

                    @Override
                    protected void onPreExecute() {
                        dialog.dismiss();
                        super.onPreExecute();
                    }

                    @Override
                    protected Void doInBackground(Void... params) {
                        AppConstants.send_image_path = storeImageToSDCard(bitmap);
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void aVoid) {
                        super.onPostExecute(aVoid);
                        loadingDialog.hide();
                        dialog.dismiss();
                        AppConstants.send_image_path = selectedImagePath;
                       /* binding.imgQuotes.setVisibility(View.VISIBLE);
                        binding.tvUploadImage.setVisibility(View.GONE);
                        binding.imgQuotes.setImageBitmap(bitmap);*/
                        //set image here....
                        Log.e(TAG, "Image upload here");
                        try {
                            sendMessage("", "2");
                        } catch (Exception e) {
                            Log.e(TAG, e.getMessage());
                        }

                    }
                }.execute();
            }
        });
        CANCEL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS: {
                Map<String, Integer> perms = new HashMap<String, Integer>();
                // Initial
                perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                // Fill with results
                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);
                // Check for ACCESS_FINE_LOCATION
                if (perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    // All Permissions Granted
                    Toast.makeText(ApiChatActivity.this, "All Permissions are Granted", Toast.LENGTH_SHORT)
                            .show();
                    startDialog(ApiChatActivity.this);
                } else {
                    // Permission Denied
                    Toast.makeText(ApiChatActivity.this, "Some Permission is Denied", Toast.LENGTH_SHORT)
                            .show();
                }
            }
            break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private String storeImageToSDCard(Bitmap processedBitmap) {
        try {
            // TODO Auto-generated method stub
            OutputStream output;
            File filepath = Environment.getExternalStorageDirectory();
            File dir = new File(filepath.getAbsolutePath() + "/Maid_Easy/");
            dir.mkdirs();
            String imge_name = "Maid_Easy_" + System.currentTimeMillis() + ".jpg";
            File file = new File(dir, imge_name);
            if (file.exists()) {
                file.delete();
                file.createNewFile();
            } else {
                file.createNewFile();
            }

            try {
                output = new FileOutputStream(file);
                processedBitmap.compress(Bitmap.CompressFormat.PNG, 100, output);
                output.flush();
                output.close();
                int file_size = Integer.parseInt(String.valueOf(file.length() / 1024));
                selectedImagePath = file.getAbsolutePath();
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return selectedImagePath;
    }

    private void openGallery() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, REQUEST_CODE_GALLERY);
    }

    private void takePicture() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        try {
            Uri mImageCaptureUri = null;
            mImageCaptureUri = Uri.fromFile(AppConstants.mFileTemp);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
            intent.putExtra("return-data", true);
            startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != Activity.RESULT_OK) {
            return;
        }
        switch (requestCode) {
            case REQUEST_CODE_GALLERY:
                try {
                    InputStream inputStream = getContentResolver().openInputStream(data.getData());
                    FileOutputStream fileOutputStream = new FileOutputStream(AppConstants.mFileTemp);
                    AppConstants.copyStream(inputStream, fileOutputStream);
                    fileOutputStream.close();
                    inputStream.close();
                    Log.e(TAG, "Starts For Crop: ");
                    startCropImage(false);
                } catch (Exception e) {
                    Log.e(TAG, "Exception: " + e.getMessage());
                }
                break;
            case REQUEST_CODE_TAKE_PICTURE:
                Log.e(TAG, "Starts For Crop: ");
                startCropImage(true);
                break;
        }

    }

    //Permission methods
    public void getAllPermission() {
        List<String> permissionsNeeded = new ArrayList<String>();
        if (Build.VERSION.SDK_INT >= 23) {
            final List<String> permissionsList = new ArrayList<String>();

            if (!addPermission(permissionsList, android.Manifest.permission.CAMERA))
                permissionsNeeded.add("Camera");
            if (!addPermission(permissionsList, android.Manifest.permission.READ_EXTERNAL_STORAGE))
                permissionsNeeded.add("Read External Storage");
            if (!addPermission(permissionsList, Manifest.permission.WRITE_EXTERNAL_STORAGE))
                permissionsNeeded.add("Write External Storage");

            if (permissionsList.size() > 0) {
                if (permissionsNeeded.size() > 0) {
                    // Need Rationale
                    String message = "You need to grant access to " + permissionsNeeded.get(0);
                    for (int i = 1; i < permissionsNeeded.size(); i++)
                        message = message + ", " + permissionsNeeded.get(i);

                    showMessageOKCancel(message,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (Build.VERSION.SDK_INT >= 23) {
                                        requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                                                REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                                    }
                                }
                            });
                    return;
                }
                requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                        REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                return;
            }
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new android.app.AlertDialog.Builder(ApiChatActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("CANCEL", null)
                .create()
                .show();
    }

    private boolean addPermission(List<String> permissionsList, String permission) {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                permissionsList.add(permission);
                // Check for Rationale Option
                if (!shouldShowRequestPermissionRationale(permission))
                    return false;
            }
        }
        return true;
    }

}

