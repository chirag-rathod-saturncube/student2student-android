package com.sc.student2student.moduls;

/**
 * Created by Saturncube10 on 29-May-17.
 */

public class Notification {
    String noti_id, req_id, notification_msg, user_id, not_by_user_id, pro_id, nf_type, type, active_flag, crt_date, name, profile_pic, product_name, product_image, rat_provide;

    public String getRat_provide() {
        return rat_provide;
    }

    public void setRat_provide(String rat_provide) {
        this.rat_provide = rat_provide;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getProduct_image() {
        return product_image;
    }

    public void setProduct_image(String product_image) {
        this.product_image = product_image;
    }

    public String getNoti_id() {
        return noti_id;
    }

    public void setNoti_id(String noti_id) {
        this.noti_id = noti_id;
    }

    public String getReq_id() {
        return req_id;
    }

    public void setReq_id(String req_id) {
        this.req_id = req_id;
    }

    public String getNotification_msg() {
        return notification_msg;
    }

    public void setNotification_msg(String notification_msg) {
        this.notification_msg = notification_msg;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getNot_by_user_id() {
        return not_by_user_id;
    }

    public void setNot_by_user_id(String not_by_user_id) {
        this.not_by_user_id = not_by_user_id;
    }

    public String getPro_id() {
        return pro_id;
    }

    public void setPro_id(String pro_id) {
        this.pro_id = pro_id;
    }

    public String getNf_type() {
        return nf_type;
    }

    public void setNf_type(String nf_type) {
        this.nf_type = nf_type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getActive_flag() {
        return active_flag;
    }

    public void setActive_flag(String active_flag) {
        this.active_flag = active_flag;
    }

    public String getCrt_date() {
        return crt_date;
    }

    public void setCrt_date(String crt_date) {
        this.crt_date = crt_date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }
}
