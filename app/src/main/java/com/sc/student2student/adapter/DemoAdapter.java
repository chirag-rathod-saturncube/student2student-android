package com.sc.student2student.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sc.student2student.R;
import com.sc.student2student.common.AppConstants;
import com.sc.student2student.databinding.ItemDemoLayoutBinding;
import com.sc.student2student.moduls.Demo;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by pc4 on 5/30/2016.
 */
public class DemoAdapter extends RecyclerView.Adapter<DemoAdapter.CustomViewHolder> {
    List<Demo> infos = null;
    private ArrayList<Demo> arrayList;
    private Context mContext;
    private String TAG = getClass().getSimpleName();


    public DemoAdapter(Context context, List<Demo> list) {
        this.infos = list;
        this.arrayList = new ArrayList<>();
        this.arrayList.addAll(infos);
        this.mContext = context;
    }


    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
      /*  View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_event_layout, null);
        CustomViewHolder viewHolder = new CustomViewHolder(view);*/
        CustomViewHolder viewHolder;
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ItemDemoLayoutBinding listBinding =
                DataBindingUtil.inflate(inflater, R.layout.item_demo_layout, viewGroup, false);
        viewHolder = new CustomViewHolder(listBinding.getRoot(), listBinding);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final CustomViewHolder customViewHolder, final int pos) {
        final Demo list = infos.get(pos);
        if (!list.getImageUrl().equals("")) {
            Picasso.with(mContext)
                    .load(list.getImageUrl())
                    .transform(AppConstants.getTransformation(customViewHolder.listBinding.itemImage))
                    .into(customViewHolder.listBinding.itemImage);
        }

        customViewHolder.listBinding.tvName.setText(list.getName());
        customViewHolder.listBinding.tvDesc.setText(list.getName());

    }

    @Override
    public int getItemCount() {
        return (null != infos ? infos.size() : 0);
    }


    public class CustomViewHolder extends RecyclerView.ViewHolder {
        ItemDemoLayoutBinding listBinding;

        public CustomViewHolder(View view, ItemDemoLayoutBinding listBinding) {
            super(view);
            this.listBinding = listBinding;
        }
    }
}

