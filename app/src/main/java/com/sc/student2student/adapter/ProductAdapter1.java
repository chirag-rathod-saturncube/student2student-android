package com.sc.student2student.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sc.student2student.Activity.ProductDetailsActivity;
import com.sc.student2student.R;
import com.sc.student2student.api.AddWatchListApi;
import com.sc.student2student.api.RemoveWatchListApi;
import com.sc.student2student.common.AppConstants;
import com.sc.student2student.common.Preferences;
import com.sc.student2student.moduls.Product;
import com.squareup.picasso.Picasso;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


/**
 * Created by pc4 on 5/30/2016.
 */
public class ProductAdapter1 extends RecyclerView.Adapter<ProductAdapter1.CustomViewHolder> {
    List<Product> infos = null;
    int layout;
    private ArrayList<Product> arrayList;
    private Context mContext;
    private String TAG = getClass().getSimpleName();

    public ProductAdapter1(Context context, List<Product> list, int layout) {
        this.infos = list;
        this.arrayList = new ArrayList<>();
        this.arrayList.addAll(infos);
        this.mContext = context;
        this.layout = layout;
    }


    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(layout, null);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final CustomViewHolder customViewHolder, final int pos) {
        Log.e(TAG, "Product_Adapter1");
        final Product list = infos.get(pos);
        customViewHolder.tv_productname.setText(list.getProduct_name());
        customViewHolder.tv_product_desc.setText(list.getProduct_desc());


        if (!list.getProduct_image().equals("")) {
            Picasso.with(mContext).load(list.getProduct_image()).transform(AppConstants.getTransformation(customViewHolder.product_image)).into(customViewHolder.product_image);
        }

        customViewHolder.card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.product = list;
                AppConstants.pos = pos;
                AppConstants.TYPE = list.getType();
                mContext.startActivity(new Intent(mContext, ProductDetailsActivity.class));
            }
        });


        if (list.getIs_watch().equals("0")) {
            customViewHolder.img_favorite.setImageResource(R.drawable.heart_outline);
        } else {
            customViewHolder.img_favorite.setImageResource(R.drawable.heart);
        }

        customViewHolder.img_favorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String param = "&user_id=" + Preferences.getValue_String(mContext, Preferences.USER_ID);
                param = param + "&pro_id=" + list.getProduct_id();
                param = param + "&type=" + list.getType();
                param = param + "&date=" + new SimpleDateFormat("ddMMyyHHmmss").format(Calendar.getInstance().getTime());
                if (list.getIs_watch().equals("0")) {
                    new AddWatchListApi(param, mContext, new AddWatchListApi.OnResultReceived() {
                        @Override
                        public void onResult(String result) throws UnsupportedEncodingException {
                            infos.get(pos).setIs_watch("1");
                            customViewHolder.img_favorite.setImageResource(R.drawable.heart);

                        }
                    }, true);
                } else {

                    new RemoveWatchListApi(param, mContext, new RemoveWatchListApi.OnResultReceived() {
                        @Override
                        public void onResult(String result) throws UnsupportedEncodingException {
                            infos.get(pos).setIs_watch("0");
                            customViewHolder.img_favorite.setImageResource(R.drawable.heart_outline);
                            infos.remove(pos);
                            notifyDataSetChanged();

                        }
                    }, true);


                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return (null != infos ? infos.size() : 0);
    }


    public class CustomViewHolder extends RecyclerView.ViewHolder {

        TextView tv_productname, tv_product_desc;
        ImageView img_favorite, product_image;
        CardView card_view;

        public CustomViewHolder(View view) {
            super(view);
            this.tv_productname = (TextView) view.findViewById(R.id.tv_productname);
            this.tv_product_desc = (TextView) view.findViewById(R.id.tv_product_desc);
            this.product_image = (ImageView) view.findViewById(R.id.product_image);
            this.img_favorite = (ImageView) view.findViewById(R.id.img_favorite);
            this.card_view = (CardView) view.findViewById(R.id.card_view);
        }
    }
}

