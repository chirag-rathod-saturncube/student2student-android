package com.sc.student2student.api;

import com.sc.student2student.R;


import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.webkit.MimeTypeMap;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;

import org.apache.http.HttpEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Map;

/*
Ver : 1.0
Last Updated : 27-Nov-2015
This class handles the multipart api calls
 */

public class FileUploaderRequestApi extends Request<NetworkResponse> {
    //private final Map<String, File> mFilePartData;
    private final File mFilePartData;
    private final Map<String, String> mStringPart;
    private final Map<String, String> mHeaderPart;
    private final Response.Listener<NetworkResponse> mListener;
    private final Response.ErrorListener mErrorListener;
    private MultipartEntityBuilder mEntityBuilder = MultipartEntityBuilder.create();
    private HttpEntity mHttpEntity;
    private Context mContext;

    public FileUploaderRequestApi(int method, Context mContext, String url, Response.Listener<NetworkResponse> listener, Response.ErrorListener errorListener, File mFilePartData, Map<String, String> mStringPart, Map<String, String> mHeaderPart) {
        super(method, url, errorListener);
        mListener = listener;
        this.mErrorListener = errorListener;
        this.mFilePartData = mFilePartData;
        this.mStringPart = mStringPart;
        this.mHeaderPart = mHeaderPart;
        this.mContext = mContext;
        mEntityBuilder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
        buildMultipartFileEntity();
        buildMultipartTextEntity();
        mHttpEntity = mEntityBuilder.build();
    }

    public static String getMimeType(Context context, String url) {
        Uri uri = Uri.fromFile(new File(url));
        String mimeType = null;
        if (uri.getScheme().equals(ContentResolver.SCHEME_CONTENT)) {
            ContentResolver cr = context.getContentResolver();
            mimeType = cr.getType(uri);
        } else {
            String fileExtension = MimeTypeMap.getFileExtensionFromUrl(uri
                    .toString());
            mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                    fileExtension.toLowerCase());
        }
        return mimeType;
    }


    private void buildMultipartFileEntity() {
        Log.e("File name", mFilePartData.getAbsolutePath());
        String mimeType = getMimeType(mContext, mFilePartData.toString());
        mEntityBuilder.addBinaryBody("image", mFilePartData, ContentType.create(mimeType), mFilePartData.getName());
    }

    private void buildMultipartTextEntity() {
        for (Map.Entry<String, String> entry : mStringPart.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            if (key != null && value != null)
                mEntityBuilder.addTextBody(key, value);
        }
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        return mHeaderPart;
    }


    @Override
    public String getBodyContentType() {
        return mHttpEntity.getContentType().getValue();
    }

    @Override
    public byte[] getBody() throws AuthFailureError {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            mHttpEntity.writeTo(bos);
        } catch (IOException e) {
            Log.e("TAG", "IOException : " + e.getMessage());
            //VolleyLog.e("IOException writing to ByteArrayOutputStream");
        }
        return bos.toByteArray();
    }

    @Override
    protected Response<NetworkResponse> parseNetworkResponse(NetworkResponse response) {
        try {
            return Response.success(response, HttpHeaderParser.parseCacheHeaders(response));
        } catch (Exception e) {
            return Response.error(new ParseError(e));
        }
    }


    @Override
    protected void deliverResponse(NetworkResponse response) {
        mListener.onResponse(response);
    }

    @Override
    public void deliverError(VolleyError error) {
        mErrorListener.onErrorResponse(error);
    }

}
